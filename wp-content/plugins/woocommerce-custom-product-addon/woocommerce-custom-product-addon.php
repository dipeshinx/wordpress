<?php
/*
  Plugin Name: Custom Woocommerce Product Addon
  Plugin URI: http://www.orangecreative.net
  Description: CSV Import for product addons
  Author: Dharmesh Patel
  Version: 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class ImportCsvAddon {

    private $my_plugin_screen_name;
    private static $instance;

    /* ...... */

    static function GetInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function manage_csv_menu() {

        $this->my_plugin_screen_name = add_menu_page(
                'Import Product Addon CSV', 'Import Product Addon CSV', 'manage_options', __FILE__, array($this, 'RenderPage'), plugins_url('/img/icon.png', __DIR__)
        );
    }

    public function uploadRemoteImageAndAttach($image_url, $parent_id){

    $image = $image_url;

    $get = wp_remote_get( $image );

    $type = wp_remote_retrieve_header( $get, 'content-type' );

    if (!$type)
        return false;

    $mirror = wp_upload_bits( basename( $image ), '', wp_remote_retrieve_body( $get ) );

    $attachment = array(
        'post_title'=> basename( $image ),
        'post_mime_type' => $type
    );

    $attach_id = wp_insert_attachment( $attachment, $mirror['file'], $parent_id );

    require_once(ABSPATH . 'wp-admin/includes/image.php');

    $attach_data = wp_generate_attachment_metadata( $attach_id, $mirror['file'] );

    wp_update_attachment_metadata( $attach_id, $attach_data );

    return $attach_id;

    }
    public function RenderPage() {
        global $wpdb;
        $get_post_meta = get_post_meta(114932, '_product_addons', $single);



        if ($_FILES) {

            wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_nonce');
            /* --- security verification --- */
            if (!wp_verify_nonce($_POST['wp_custom_attachment_nonce'], plugin_basename(__FILE__))) {
// return $id;
            } // end if

            if ($_FILES['wp_product_csv']['tmp_name'] != '') {
                $csvFile = file($_FILES['wp_product_csv']['tmp_name']);
                foreach ($csvFile as $line) {
                    $fetch_data_csv[] = str_getcsv($line);
                }
            }

            $keys = $fetch_data_csv[0];


            unset($fetch_data_csv[0]);

            $Group_Name = array();
            $Option_Label = array();
            $Group_Description = $Group_Description_Key = array();

            foreach ($keys as $k => $v) {
                $cn = 0;
                $cn1 = 0;
                if (@substr_count($v, 'Group_Name_') > 0 && @substr_count($v, 'Option_Label') == 0) {
                    $cn++;
                    $Group_Name[$v] = $k;
                }

                if (@substr_count($v, 'Group_Description_') > 0) {
                    $cn1++;
                    $Group_Description_Key[$v] = $k;
                }
            }



            $a = 0;
            foreach ($Group_Name as $a => $b) { 

                $group_key = substr("$a", (strrpos($a,'_') + 1));
                foreach ($keys as $k => $v) {
                    if (@substr_count($v, $a . '_Option_Label_') > 0) {
                        $Group_Options[$b][$k] = $v;
                    }
                    if (@substr_count($v, 'Group_Description_'.$group_key) > 0) {
                        $Group_Description[$b] = $Group_Description_Key[$v];
                    }
                }
            }

            $cnt = 0;
            foreach ($fetch_data_csv as $a => $b) {
                $cnt++;
                $pro_ = '';
                $pro_decription = array();
                foreach ($Group_Options as $x => $y) {
                    $pro[$a][] = $b[$x];
                    foreach ($y as $p => $q) {
                        $pro_[$b[$x]][$b[$p]] = $b[$p + 1];
                    }
                    $pro_decription[$b[$x]] = $b[$Group_Description[$x]];
                }
                $product_name[$cnt]['name'] = $b[2];
                $product_name[$cnt]['sku'] = $b[1];
                $product_name[$cnt]['price'] = $b[6];
                $product_name[$cnt]['description'] = $b[3];
                $product_name[$cnt]['brand'] = $b[0];
                $product_name[$cnt]['group_options'] = $pro_;
                $product_name[$cnt]['group_description'] = $pro_decription;
                $product_name[$cnt]['category'] = $b[5];
                $product_name[$cnt]['image_url'] = $b[7];
                //$product_name[$cnt]['various_options'] = $pro_;
            }
            $cnt = 0;
            $options = "";
            foreach ($product_name as $k => $y) {
                $c = 0;
                $product_name_ = '';
                foreach ($y['group_options'] as $a => $b) {

                    $product_name_[$c]['name'] = $a;
                    $product_name_[$c]['description'] = $y['group_description'][$a];
                    $product_name_[$c]['type'] = 'checkbox';
                    $product_name_[$c]['position'] = '0';
                    foreach ($b as $e => $f) {
                        $product_name_[$c]['options'][] = array(
                            'label' => $e,
                            'price' => $f,
                            'min' => $min,
                            'max' => $max
                        );
                    }
                    $c++;
                }
                $product_name[$k]['various_options'] = $product_name_;
                $cnt++;
            }
            
            foreach ($product_name as $key => $data) {

                $results = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_type='product' AND post_status='publish' AND post_title='".$data['name']."'");

                if($results)
                {
                    $post_id = $results[0]->ID;

                    wp_update_post(array(
                        'ID'=>$post_id,
                        'post_title' => $data['name'],
                        'post_content' => $data['description'],
                        'post_status' => 'publish',
                        'post_type' => "product",
                    ));
                }
                else
                {
                    $post_id = wp_insert_post(array(
                        'post_title' => $data['name'],
                        'post_content' => $data['description'],
                        'post_status' => 'publish',
                        'post_type' => "product",
                    ));
                }
                wp_set_object_terms($post_id, 'simple', 'product_type');
                update_post_meta($post_id, '_regular_price', $data['price']);
                update_post_meta($post_id, '_sale_price', $data['price']);
                update_post_meta($post_id, '_sku', $data['sku']);
                update_post_meta($post_id, '_brand', $data['brand']);
                update_post_meta($post_id, '_product_addons', $data['various_options']);
                
                $attach_id = $this->uploadRemoteImageAndAttach($data['image_url'],$post_id);
                update_post_meta($post_id, '_product_image_gallery', $attach_id);

                if($data['category']!='')
                {
                    $category_data = explode(">",$data['category']);
                    $category = $category_data[0];
                    $sub_category = $category_data[1];

                    $term_category = term_exists($category,'product_cat');
                    $term_category_taxonomy_id = $term_category['term_taxonomy_id'];

                    $term_subcategory = term_exists($sub_category,'product_cat');
                    $term_subcategory_taxonomy_id = $term_subcategory['term_taxonomy_id'];

                    $category_term_data = get_term( $term_category['term_id']);
                    $subcategory_term_data = get_term( $term_subcategory['term_id']);

                    
                    $count_category = (int)$category_term_data->count + 1;
                    $count_subcategory = (int)$subcategory_term_data->count + 1;

                    $wpdb->query("UPDATE  $wpdb->term_taxonomy SET count = ".$count_category." WHERE term_taxonomy_id = ".$term_category_taxonomy_id);
                    $wpdb->query("UPDATE  $wpdb->term_taxonomy SET count = ".$count_subcategory." WHERE term_taxonomy_id = ".$term_subcategory_taxonomy_id);


                    $wpdb->query( "INSERT INTO $wpdb->term_relationships (object_id, term_taxonomy_id, term_order) VALUES (".$post_id.",".$term_category_taxonomy_id.",1)");
                    $wpdb->query( "INSERT INTO $wpdb->term_relationships (object_id, term_taxonomy_id, term_order) VALUES (".$post_id.",".$term_subcategory_taxonomy_id.",1)");
                }


            }



            //update_post_meta(114942, '_product_addons', $data_);
        }
        ?>
        <div class="wrap">
        <h2>Import Product Addon CSV</h2>
        <table class="form-table">
        <form name="checkout" method="post" class="checkout woocommerce-checkout"  enctype="multipart/form-data">

            <tr>
                <th> Upload your CSV here.</th>
                <td><input type="file" id="wp_product_csv" name="wp_product_csv"  /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <button type="submit" class="button-primary"><?php esc_attr_e('Import CSV', 'woocommerce'); ?></button>
                </td>

            </tr>

        </form>
        </table>
        </div>
        <?php
    }

    public function InitPlugin() {
        add_action('admin_menu', array($this, 'manage_csv_menu'));
    }

}

$addCSV = ImportCsvAddon::GetInstance();
$addCSV->InitPlugin();
?>