jQuery(document).ready(function(){
    var wapikey = jQuery('#webinar_api_key input').val();
    
    if(wapikey !== '') {
        var url = 'https://api.citrixonline.com/oauth/authorize?client_id='+wapikey;
        var button = '<div style="margin-top:10px;"><a href="'+url+'" class="button button-primary button-small">Connect to Webinar</a></div>';
        jQuery('#webinar_api_key').append(button);
    }
    
    var mapikey = jQuery('#meeting_api_key input').val();
    
    if(mapikey !== '') {
        var url = 'https://api.citrixonline.com/oauth/authorize?client_id='+mapikey;
        var button = '<div style="margin-top:10px;"><a href="'+url+'" class="button button-primary button-small">Connect to Meeting</a></div>';
        jQuery('#meeting_api_key').append(button);
    }
    
});