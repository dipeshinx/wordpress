<?php

function webinar_enqueue($hook) {
    if ($hook == 'theme-settings_page_acf-options-go-to-settings') {
        wp_enqueue_script('webinar', plugin_dir_url(__FILE__) . 'js/webinar.js');
    }
}

add_action('admin_enqueue_scripts', 'webinar_enqueue');

function webinar_authorize() {
    if (isset($_GET) && !empty($_GET) && isset($_GET['page']) && $_GET['page'] == 'acf-options-go-to-settings' && isset($_GET['type'])) {
        if ($_GET['type'] == 'webinar') {
            $citrix = new Citrix('webinars');
        }
        if ($_GET['type'] == 'meeting') {
            $citrix = new Citrix('meetings');
        }
    }
}

add_action('admin_init', 'webinar_authorize');

function create_webinar($data) {
    

    $key = get_field('webinars_organizer_key', 'option');
    $token = get_field('webinars_access_token', 'option');

    //set POST variables
    $url = 'https://api.citrixonline.com:443/G2W/rest/organizers/' . $key . '/webinars';
    
    $sdate = DateTime::createFromFormat('m/d/Y', $data['date']);
    $edate = DateTime::createFromFormat('m/d/Y', $data['date']);
    $stime = $data['starttime'];
    $etime = $data['endtime'];
    $times = array();
    $arr = array();
    $arr['startTime'] = $sdate->format('Y-m-d') . "T" . $stime . ":00Z";
    $arr['endTime'] = $edate->format('Y-m-d') . "T" . $etime . ":00Z";
    array_push($times, $arr);

    $fields = array('subject' => $data['title'],
        'description' => $data['desc'],
        'timeZone' => '',
        'times' => $times
    );

    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: OAuth oauth_token=' . $token;

    $encoded_fields = json_encode($fields);

    //open connection
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_USERAGENT, 'OSD PHP Client/3.0');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, null);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded_fields);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    
    $response = new OSDResponse();
    $raw_response = curl_exec($ch);
    $raw_headers_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $response->body = substr($raw_response, $raw_headers_size);
    $response->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $response->headers = parse_headers(substr($raw_response, 0, $raw_headers_size));
    
    //close connection
    curl_close($ch);
    
    switch ($response->status) {
      case 200 :
      case 201 :
      case 204 :
        return $response->body;
        break;
      case 400 :
        return '400 error';
        break;
      case 401 :
        return '401 error';
        break;
      default :
        return $response->status.' error';
        break;
    }
}

function encode_attributes($attributes) {
    $return = array();
    foreach ($attributes as $key => $value) {
        $return[] = urlencode($key) . '=' . urlencode($value);
    }
    return join('&', $return);
}

class OSDResponse {

    public $body;
    public $status;
    public $headers;

    public function json_body() {
        return json_decode($this->body, TRUE);
    }

}

function parse_headers($headers) {
    $list = array();
    $headers = str_replace("\r", "", $headers);
    $headers = explode("\n", $headers);
    foreach ($headers as $header) {
        if (strstr($header, ':')) {
            $name = strtolower(substr($header, 0, strpos($header, ':')));
            $list[$name] = trim(substr($header, strpos($header, ':') + 1));
        }
    }
    return $list;
}


function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}