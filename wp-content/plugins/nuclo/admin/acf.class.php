<?php
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'      => false
	));
        
        acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme User Fields Settings',
		'menu_title'	=> 'User Fields',
		'parent_slug' 	=> 'theme-general-settings',
	));
        
        acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Custom Post Settings',
		'menu_title'	=> 'Custom Post',
		'parent_slug' 	=> 'theme-general-settings',
	));
        
        acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Go To Settings',
		'menu_title'	=> 'Go To Settings',
		'parent_slug' 	=> 'theme-general-settings',
	));
	
}