<?php

if (!function_exists('curl_init')) {
    throw new Exception('Citrix needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
    throw new Exception('Citrix needs the JSON PHP extension.');
}

class Citrix {
    
    public $type;
    
    public function __construct($type) {
        $this->type = $type;
        $this->get_organizer_key();
    }

    /**
     * Gets current user id
     */
    public function get_organizer_key() {

        if (array_key_exists('code', $_GET)) {
            if (get_field($this->type.'_organizer_key', 'option') == '') {
                $code = $_GET['code'];
                $access_token = $this->get_access_token($code);
                if ($access_token) {                    
                    return get_field($this->type.'_organizer_key', 'option');
                }
            }
        }

        if (!get_field($this->type.'_organizer_key', 'option') or !get_field($this->type.'_access_token', 'option'))
            return 0;
        
        $reponse = json_decode($this->make_request("https://api.citrixonline.com/G2W/rest/organizers/".get_field($this->type.'_organizer_key', 'option')."/$this->type?oauth_token=".get_field($this->type.'_access_token', 'option')),true);
        
        
        if(isset($reponse['int_err_code']) and $reponse['int_err_code'] != '') {       
            throw new Exception($reponse['int_err_code']);
        } else {
            return get_field($this->type.'_organizer_key', 'option');
        }

    }

    /**
     * Gets the access token for current user. If $_get['code']/?code={responseKey} is available and access_token not set then 
     * it will make request with code to get access token. 
     *
     * @return string access token
     */
    public function get_access_token($code = null) {

        if (get_field($this->type.'_access_token', 'option') != '')
            return get_field($this->type.'_access_token', 'option');

        if ($code) {

            $reponse = json_decode($this->make_request("https://api.citrixonline.com/oauth/access_token?grant_type=authorization_code&code={$code}&client_id=" . get_field($this->type.'_api_key', 'option')),true);
            
            
            if (isset($reponse['int_err_code']) and $reponse['int_err_code'] != '') {
                throw new Exception($reponse['int_err_code']);
            } else {
                $this->set_data($reponse);
                return get_field($this->type.'_access_token', 'option');
            }
        }
    }


    /**
     * Makes an HTTP request. This method can be overridden by subclasses if
     * developers want to do fancier things or use something other than curl to
     * make the request.
     *
     * @param string $url The URL to make the request to
     * @param array $params The parameters to use for the POST body
     *
     * @return string The response text
     */
    public static function make_request($url) {
        //  Initiate curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $utf8_result = utf8_encode($result);
        return $utf8_result;
    }

    /**
     * Store an array's values with array's key as key with prefix 'citrix_'.
     * 
     */
    protected function set_data($array) {
        update_option('options_'.$this->type.'_organizer_key', $array['organizer_key']);
        update_option('options_'.$this->type.'_access_token', $array['access_token']);
        $current_time = time();
        $expires_in = $array['expires_in'];
        $expires_time = $current_time + $expires_in;
        $expires_date = gmdate("d, M Y \T H:i:s \Z", $expires_time);        
        update_option('options_'.$this->type.'_expires_in', $expires_date);
        update_option('options_'.$this->type.'_response', json_encode($array));
    }

}