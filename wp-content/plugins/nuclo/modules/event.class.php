<?php

/**
 * @package  Event
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Event {

    public $per_page;
    /**
     * Adds the generic hooks that are required throughout
     * the theme.156
     * 
     * @since 1.0
     */
    public function __construct() {
        add_action('init', array($this, 'register_post_type'));
        add_action('init', array($this, 'mark_attendance'));
        add_action('init', array($this, 'create_event'));
        add_action('init', array($this, 'select_event'));
        add_action('init', array($this, 'update_event'));
        add_action('init', array($this, 'rewrites_init'), 1, 0);
        add_action('update_group',array($this, 'reasign_sessions'),1,2);
        add_action('nuclo_scheduled_email', array($this, 'event_reminder'));
        add_action('wp_ajax_event_qtip_content', array($this, 'event_qtip_content'));
        add_action('wp_ajax_WP_FullCalendar', array($this, 'WP_FullCalendar'));
        add_action('wp_ajax_delete_event', array($this, 'delete_event'));
        add_action('pre_get_posts', array($this, 'filter_query'), 1, 1);
        add_action('wp_ajax_session_list_reschedule', array($this, 'session_list_reschedule'));
        add_action('wp_ajax_reschedule_session', array($this, 'reschedule_session'));

        $this->per_page = isset($_REQUEST['record_per_page'])?$_REQUEST['record_per_page']:20;
    }

    /**
     * @since 1.0
     */
    public function register_post_type() {
        if (function_exists('get_field')) :
            $labels = array(
                'name' => _x(get_field('event_general_name', 'option'), 'post type general name', 'nuclo'),
                'singular_name' => _x(get_field('event_singular_name', 'option'), 'post type singular name', 'nuclo'),
                'menu_name' => _x(get_field('event_general_name', 'option'), 'admin menu', 'nuclo'),
                'name_admin_bar' => _x(get_field('event_singular_name', 'option'), 'add new on admin bar', 'nuclo'),
                'add_new' => _x('Add New', strtolower(get_field('event_singular_name', 'option')), 'nuclo'),
                'add_new_item' => __('Add New ' . get_field('event_singular_name', 'option'), 'nuclo'),
                'new_item' => __('New ' . get_field('event_singular_name', 'option'), 'nuclo'),
                'edit_item' => __('Edit ' . get_field('event_singular_name', 'option'), 'nuclo'),
                'view_item' => __('View ' . get_field('event_singular_name', 'option'), 'nuclo'),
                'all_items' => __('All ' . get_field('event_general_name', 'option'), 'nuclo'),
                'search_items' => __('Search ' . get_field('event_general_name', 'option'), 'nuclo'),
                'parent_item_colon' => __('Parent ' . get_field('event_general_name', 'option') . ':', 'nuclo'),
                'not_found' => __('No ' . strtolower(get_field('event_general_name', 'option')) . ' found.', 'nuclo'),
                'not_found_in_trash' => __('No ' . strtolower(get_field('event_general_name', 'option')) . ' found in Trash.', 'nuclo')
            );

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => 'event'),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array('title', 'editor', 'author')
            );

            register_post_type('event', $args);
        endif;
    }

    /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('event/page/([0-9]{1,})/?', 'index.php?post_type=event&paged=$matches[1]', 'top');
        add_rewrite_rule('event/(.+?)/([^/]*)/?$', 'index.php?event=$matches[1]&action=$matches[2]', 'top');
    }

    /**
     * @since 1.0
     */
    public function create_event() {

        if (isset($_POST) && !empty($_POST['nuclo_create_event']) && wp_verify_nonce($_POST['nuclo_create_event'], 'nuclo_create_event')) {

            $data = $_POST;
            $data['nc_course'] = get_field('bpm', 'option');
            $data['nc_edate'] = date('m/d/Y', (strtotime(str_replace('-', '/', $data['nc_sdate'])) + (5 * 365 * 24 * 60 * 60)));
            $data['recurrence_days'] = 0;

            $validate = $this->validate($data);

            if ($validate == true && !is_array($validate)) {

                $my_event = array(
                    'post_title' => $data['nc_title'],
                    'post_type' => 'event',
                    'post_status' => 'publish',
                    'post_parent' => $data['nc_course'],
                    'post_content' => $data['nc_note']
                );

                $event_id = wp_insert_post($my_event);

                $this->save_meta($event_id, $data, false);

                if ($data['event_type'] == 'Repeat') {
                    $days = $this->get_recurrence_days($data);
                    $this->recurring($days, $event_id, $data);
                }

                if (!empty($data['nc_group'])) {
                    foreach ($data['nc_group'] as $group) {
                        $facilitators = get_post_meta($group, 'facilitators');
                        $all_facilitators = array_merge($facilitators, $data['grp_facilitators']);
                        $unique_facilitators = array_unique($all_facilitators);
                        update_nuclo_meta($group, 'post', 'facilitators', $unique_facilitators);
                    }
                }

                if (!empty($data['grp_facilitators'])) {
                    update_nuclo_meta($event_id, 'post', 'facilitator', $data['grp_facilitators']);
                }

                if (!empty($data['nc_group'])) {
                    update_nuclo_meta($event_id, 'post', 'groups', $data['nc_group']);
                }

                if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
                    //Workflow::save_session($event_id);
                    do_action('save_session_workflow', $event_id);
                    $_SESSION['WORKFLOW']['SUCCESS'] = '';
                    $_SESSION['WORKFLOW']['SUCCESS'] = 'Session successfully created.';
                    wp_safe_redirect('/compose-email/');
                    exit;
                }

                $url = strtok($data['_wp_http_referer'], '?');

                wp_safe_redirect($url . '/?msg=success');

                exit;
            }
        }
    }
    
    /**
     * @since  1.0
     */
    private function get_recurrence_days($data) {

        $start_date = strtotime($data['nc_sdate']);
        $end_date = strtotime($data['nc_edate']);

        $data['start'] = strtotime($this->nc_sdate . " " . $this->nc_stime, current_time('timestamp'));
        $data['end'] = strtotime($this->nc_edate . " " . $this->nc_etime, current_time('timestamp'));

        $weekdays = explode(",", $data['recurrence_byday']); //what days of the week (or if monthly, one value at index 0)

        $matching_days = array();
        $aDay = 86400;  // a day in seconds
        $aWeek = $aDay * 7;

        //TODO can this be optimized?
        switch ($data['recurrence_freq']) {
            case 'daily':
                //If daily, it's simple. Get start date, add interval timestamps to that and create matching day for each interval until end date.
                $current_date = $start_date;
                while ($current_date <= $end_date) {
                    $matching_days[] = $current_date;
                    $current_date = $current_date + ($aDay * 1);
                }
                break;
            case 'weekly':
                //sort out week one, get starting days and then days that match time span of event (i.e. remove past events in week 1)
                $start_of_week = get_option('start_of_week'); //Start of week depends on WordPress
                //first, get the start of this week as timestamp
                $event_start_day = date('w', $start_date);
                //then get the timestamps of weekdays during this first week, regardless if within event range
                $start_weekday_dates = array(); //Days in week 1 where there would events, regardless of event date range
                for ($i = 0; $i < 7; $i++) {
                    $weekday_date = $start_date + ($aDay * $i); //the date of the weekday we're currently checking
                    $weekday_day = date('w', $weekday_date); //the day of the week we're checking, taking into account wp start of week setting

                    if (in_array($weekday_day, $weekdays)) {
                        $start_weekday_dates[] = $weekday_date; //it's in our starting week day, so add it
                    }
                }
                //for each day of eventful days in week 1, add 7 days * weekly intervals
                foreach ($start_weekday_dates as $weekday_date) {
                    //Loop weeks by interval until we reach or surpass end date
                    while ($weekday_date <= $end_date) {
                        if ($weekday_date >= $start_date && $weekday_date <= $end_date) {
                            $matching_days[] = $weekday_date;
                        }
                        $weekday_date = $weekday_date + ($aWeek * 1);
                    }
                }//done!
                break;
            case 'monthly':
                //loop months starting this month by intervals
                $current_arr = getdate($start_date);
                $end_arr = getdate($end_date);
                $end_month_date = strtotime(date('Y-m-t', $end_date)); //End date on last day of month
                $current_date = strtotime(date('Y-m-1', $start_date)); //Start date on first day of month
                while ($current_date <= $end_month_date) {
                    $last_day_of_month = date('t', $current_date);
                    //Now find which day we're talking about
                    $current_week_day = date('w', $current_date);
                    $matching_month_days = array();
                    //Loop through days of this years month and save matching days to temp array
                    for ($day = 1; $day <= $last_day_of_month; $day++) {
                        if ((int) $current_week_day == $data['recurrence_byday']) {
                            $matching_month_days[] = $day;
                        }
                        $current_week_day = ($current_week_day < 6) ? $current_week_day + 1 : 0;
                    }
                    //Now grab from the array the x day of the month
                    $matching_day = ($data['recurrence_byweekno'] > 0) ? $matching_month_days[$data['recurrence_byweekno'] - 1] : array_pop($matching_month_days);
                    $matching_date = strtotime(date('Y-m', $current_date) . '-' . $matching_day);
                    if ($matching_date >= $start_date && $matching_date <= $end_date) {
                        $matching_days[] = $matching_date;
                    }
                    //add the number of days in this month to make start of next month
                    $current_arr['mon'] += 1;
                    if ($current_arr['mon'] > 12) {
                        //FIXME this won't work if interval is more than 12
                        $current_arr['mon'] = $current_arr['mon'] - 12;
                        $current_arr['year']++;
                    }
                    $current_date = strtotime("{$current_arr['year']}-{$current_arr['mon']}-1");
                }
                break;
            case 'yearly':
                //If yearly, it's simple. Get start date, add interval timestamps to that and create matching day for each interval until end date.
                $month = date('m', $data['start']);
                $day = date('d', $data['start']);
                $year = date('Y', $data['start']);
                $end_year = date('Y', $data['end']);
                if (@mktime(0, 0, 0, $day, $month, $end_year) < $data['end'])
                    $end_year--;
                while ($year <= $end_year) {
                    $matching_days[] = mktime(0, 0, 0, $month, $day, $year);
                    $year++;
                }
                break;
        }
        sort($matching_days);
        return $matching_days;
    }
    
    /**
     * @since  1.0
     */
    private function recurring($days, $parent_id, $data) {
        if (count($days) > 0) {
            $event['event_type'] = 'None';
            $event['all_day'] = $data['all_day'];
            $event['acf'] = $data['acf'];
            $event['nc_title'] = $data['nc_title'];
            $event['nc_course'] = $data['nc_course'];
            $event['grp_franchisee'] = $data['grp_franchisee'];
            $event['grp_facilitators'] = $data['grp_facilitators'];
            $event['nc_group'] = $data['nc_group'];

            foreach ($days as $day) {
                $event['nc_sdate'] = date("m/d/Y", $day);
                $event['nc_stime'] = $data['nc_stime'];
                $meta_fields['_start_ts'] = strtotime($event['nc_sdate'] . ' ' . $event['nc_stime']);
                if ($data['recurrence_days'] > 0) {
                    $event['nc_edate'] = date("m/d/Y", $meta_fields['_start_ts'] + ($data['recurrence_days'] * 60 * 60 * 24));
                } else {
                    $event['nc_edate'] = $event['nc_sdate'];
                }
                $event['nc_etime'] = $data['nc_etime'];
                $event['nc_start_time_prefix'] = $data['nc_start_time_prefix'];
                $event['nc_end_time_prefix'] = $data['nc_end_time_prefix'];
                $event['grp_facilitators'] = $data['grp_facilitators'];
                $event['address'] = $data['address'];
                $event['nc_month'] = date('m', strtotime($event['nc_sdate']));


                $my_event = array(
                    'post_title' => $event['nc_title'],
                    'post_type' => 'event',
                    'post_status' => 'publish',
                    'post_parent' => $event['nc_course']
                );
                $event_id = wp_insert_post($my_event);

                update_post_meta($event_id, 'parent_event', $parent_id);
                update_post_meta($event_id, 'event_type', '');
                update_post_meta($event_id, 'attendance_status', 'not-marked');                
                $value = date("n", $day);
                update_field('field_56ea7569abf6a', $value, $event_id);

                $this->save_meta($event_id, $event);
                
                update_nuclo_meta($event_id, 'post', 'facilitators', $event['grp_facilitators']);

                $this->save_group($event_id, $event['nc_group'], $event['nc_sdate'], $event['grp_facilitators']);
            }
        }
    }

    /**
     * @since  1.0
     */
    private function save_group($ID, $groups, $date, $facilitators) {
        $new_groups = array();
        $date = date('Ymd', strtotime($date));
        if (!empty($groups)) {
            $args = array('post_type' => 'group', 'post__in' => $groups, 'meta_query' => array('relation' => 'AND', array('key' => 'group_start_date', 'value' => $date, 'compare' => '<=', 'type' => 'DATE'), array('key' => 'group_end_date', 'value' => $date, 'compare' => '>=', 'type' => 'DATE')), 'posts_per_page' => -1);
            $group_query = new WP_Query($args);
            if ($group_query->have_posts()) {
                while ($group_query->have_posts()) {
                    $group_query->the_post();
                    $new_groups[] = get_the_ID();
                }
            }
            wp_reset_postdata();
            update_nuclo_meta($ID, 'post', 'groups', $new_groups);
            if (!empty($new_groups)) {
                update_nuclo_meta($ID, 'post', 'facilitator', $facilitators);
            }
        }
    }
    
    public function validate_select_event($data) {

        global $group_messsage;

        if (empty($data['event_id'])) {
            $error['event_id'] = 'Please select session.';
        }

        if (!empty($error)) {
            $group_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }
	
    /**
     * @since 1.0
     */
    public function select_event() {
        $data = $_POST;
		
         if (isset($data['select_workflow']) && $data['select_workflow'] == '1') {
			 $validate = $this->validate_select_event($data);
			 if ($validate == true && !is_array($validate)) {
				$event_id = $data['event_id'];
				if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
					$group_id = isset($_SESSION['WORKFLOW']['GROUP_ID']) ? $_SESSION['WORKFLOW']['GROUP_ID'] : 0;
					
					
					if ($group_id != 0) {                    
						$this->assign_group($group_id,$event_id);
					}

					//Workflow::save_session($event_id);
					do_action('save_session_workflow', $event_id);
					$_SESSION['WORKFLOW']['SUCCESS'] = '';
					$_SESSION['WORKFLOW']['SUCCESS'] = 'Session successfully assigned.';
					wp_safe_redirect(home_url('compose-email'));
					exit;
				}
			}
        }
    }

    /**
     * @since  1.0
     */
    private function assign_group($group_id, $event_id) {
        $group_start_date = get_post_meta($group_id, 'group_start_date', true);
        $group_end_date = get_post_meta($group_id, 'group_end_date', true);
        $facilitators = get_post_meta($group_id, 'facilitators');
        $groups = get_post_meta($event_id, 'groups');
        if(!in_array($group_id,$groups)) {
            add_post_meta($event_id,'groups',$group_id);
        }
        $meta = array();
        $meta[] = array('key' => 'parent_event', 'value' => $event_id, 'compare' => '==');
        $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!=');
        $meta[] = array('key' => 'start_date', 'value' => $group_start_date, 'compare' => '>=', 'type' => 'DATE');
        $meta[] = array('key' => 'start_date', 'value' => $group_end_date, 'compare' => '<=', 'type' => 'DATE');
        $meta['relation'] = 'AND';
        $child_args = array('post_type' => 'event', 'meta_query' => $meta, 'repeat' => true, 'posts_per_page' => -1);
        $child_query = new WP_Query($child_args);
        if ($child_query->have_posts()) {
            while ($child_query->have_posts()) {
                $child_query->the_post();
                $groups = get_post_meta(get_the_ID(), 'groups');
                $groups[] = $group_id;
                $new_groups = array_unique($groups);
                update_nuclo_meta(get_the_ID(), 'post', 'groups', $new_groups);
                if (!empty($new_groups)) {
                    update_nuclo_meta(get_the_ID(), 'post', 'facilitator', $facilitators);
                }
            }
        }
        wp_reset_postdata();
    }

    /**
     * @since 1.0
     */
    public function update_event() {

        if (isset($_POST) && !empty($_POST['nuclo_update_event']) && wp_verify_nonce($_POST['nuclo_update_event'], 'nuclo_update_event')) {

            $data = $_POST;

            $validate = $this->validate($data);

            if ($validate == true && !is_array($validate)) {

                $my_event = array(
                    'ID' => $data['nc_id'],
                    'post_title' => $data['nc_title'],
                    'post_type' => 'event',
                    'post_status' => 'publish',
                    'post_content' => $data['nc_note']
                );

                $event_id = wp_update_post($my_event);

                $this->save_meta($event_id, $data, false);

                if (isset($data['submit-future'])) {
                    $this->update_facilitators($event_id, $data);
                }
                
                update_nuclo_meta($event_id, 'post', 'facilitator', $data['grp_facilitators']);
                
                $parent_id = get_post_meta($event_id,'parent_event',true);
                
                $parent_facilitators = get_post_meta($parent_id,'facilitator');
                
                foreach($data['grp_facilitators'] as $facilitator) {
                    if(!in_array($facilitator,$parent_facilitators)) {
                        add_post_meta($parent_id,'facilitator',$facilitator);
                    }
                }
                
                $groups = get_post_meta($event_id, 'groups');
                if (!empty($groups)) {                    
                    foreach ($groups as $group) {
                        $grp_facilitators = get_post_meta($group, 'facilitators');
                        $merge_grp_facilitators = array_merge($grp_facilitators, $data['grp_facilitators']);
                        $unique_grp_facilitators = array_unique($merge_grp_facilitators);
                        update_nuclo_meta($group, 'post', 'facilitators', $unique_grp_facilitators);
                    }
                }

                if (isset($_SESSION['WORKFLOW']['EVENT_ID']) && $_SESSION['WORKFLOW']['EVENT_ID'] == $event_id) {
                    $_SESSION['WORKFLOW']['SUCCESS'] = '';
                    $_SESSION['WORKFLOW']['SUCCESS'] = 'Session successfully updated.';
                    wp_safe_redirect('/compose-email/');
                    exit;
                }

                $url = strtok($data['_wp_http_referer'], '?');

                wp_safe_redirect($url . '/?msg=success');

                exit;
            }
        }
    }

    /**
     * @since  1.0
     */
    private function update_facilitators($event_id, $data) {
        $parent_event = get_post_meta($event_id, 'parent_event', true);
        $start_date   = get_post_meta($event_id, 'start_date', true);
        
        $current_facilitators = get_post_meta($event_id, 'facilitator');
        $post_facilitators    = $data['grp_facilitators'];
        $comman_facilitators  = array();
        $remove_facilitators  = array();
        
        foreach($current_facilitators as $current_facilitator) {
            if(in_array($current_facilitator,$post_facilitators)) {
                $comman_facilitators[] = $current_facilitator;
            } else {
                $remove_facilitators[] = $current_facilitator;
            }
        }
        
        $future_args = array('post_type' => 'event', 'posts_per_page' => -1, 'meta_query' => array(array('key' => 'start_date', 'value' => $start_date, 'compare' => '>'), array('key' => 'parent_event', 'value' => $parent_event, 'compare' => '='), 'relation' => 'AND'));
        $future_events = new WP_Query($future_args);
        if ($future_events->have_posts()) {
            while ($future_events->have_posts()) {
                $future_events->the_post();
                $groups = get_post_meta(get_the_ID(), 'groups');
                if (!empty($groups)) {
                    $my_facilitators    = get_post_meta(get_the_ID(), 'facilitator');
                    foreach($remove_facilitators as $remove_facilitator) {
                        if(($key = array_search($remove_facilitator, $my_facilitators)) !== false ){
                            unset($my_facilitators[$key]);
                        }
                    }   
                    $merge_facilitators = array_merge($my_facilitators,$post_facilitators);
                    $all_facilitators   = array_unique($merge_facilitators);
                    update_nuclo_meta(get_the_ID(), 'post', 'facilitator', $all_facilitators);
                    if (!empty($data['nc_stime'])) {
                        $value = date("H:i", strtotime("{$data['nc_stime']} {$data['nc_start_time_prefix']}"));
                        $field_name = 'field_569613a9959a7';
                        update_field($field_name, $value, get_the_ID());
                    }

                    if (!empty($data['nc_etime'])) {
                        $value = date("H:i", strtotime("{$data['nc_etime']} {$data['nc_end_time_prefix']}"));
                        $field_name = 'field_569613be959a8';
                        update_field($field_name, $value, get_the_ID());
                    }
                    if (!empty($data['address'])) {
                        update_post_meta(get_the_ID(), 'address', $data['address']);
                    }
                    foreach ($groups as $group) {
                        $grp_facilitators = get_post_meta($group, 'facilitators');
                        if(!empty($grp_facilitators)){
                            $merge_grp_facilitators = array_merge($grp_facilitators, $all_facilitators);                        
                            $unique_grp_facilitators = array_unique($merge_grp_facilitators);
                        }
                        update_nuclo_meta($group, 'post', 'facilitators', $unique_grp_facilitators);
                    }                    
                }
            }
        }
        wp_reset_postdata();
    }

    /**
     * @since  1.0
     */
    public function validate($data) {

        global $group_messsage;

        if (empty($data['nc_title'])) {
            $error['nc_title'] = 'Please enter session name';
        } else if (strlen($data['nc_title']) < 3) {
            $error['nc_title'] = 'Too short session name';
        } else if (strlen($data['nc_title']) > 150) {
            $error['nc_title'] = 'Too long session name';
        }

        if (empty($data['nc_sdate'])) {
            $error['nc_sdate'] = 'Start date is required';
        } else if (strtotime($data['nc_sdate']) < strtotime('-1 day', time())) {
            $error['nc_sdate'] = 'Start date must be future date';
        }

        if (isset($data['nuclo_create_event']) && empty($data['nc_course'])) {
            $error['nc_course'] = 'Courses is required';
        }
        if (!is_singular('event') && empty($data['nc_month'])) {
            //$error['nc_month'] = 'Course Month is required';  
        }
        if (empty($data['address'])) {
            $error['venue'] = 'Venue is required';
        }

//        if(empty($data['time_zone'])){
//            $error['time_zone'] = 'Please select timezone for event.';   
//        }

        if (empty($data['all_day'])) {
//            $time_pattern = '/^(0?\d|1[0-2]):[0-5]\d\s(am|pm)$/i';
//            $start_time = preg_match($time_pattern, strtolower($error['nc_stime']));
//            $end_time = preg_match($time_pattern, strtolower($error['nc_etime']));
//            
//            if(!$start_time) {
//                $error['nc_stime'] = 'Invalid start time';
//            }
//            if(!$end_time) {
//                $error['nc_etime'] = 'Invalid end time';
//            }
        }

        if (!empty($data['event_type'])) {
            if (empty($data['recurrence_freq'])) {
                $error['recurrence_freq'] = 'Please select recurrence frequency';
            } else if (!in_array($data['recurrence_freq'], array('daily', 'weekly', 'monthly', 'yearly'))) {
                $error['recurrence_freq'] = 'Invalid recurrence frequency';
            } else {
                switch ($data['recurrence_freq']) {
                    case 'daily':

                        break;
                    case 'weekly':

                        $bydays = array_diff($data['recurrence_bydays'], array(0, 1, 2, 3, 4, 5, 6));
                        if (empty($data['recurrence_bydays'])) {
                            $error['recurrence_bydays'] = 'Please select day for weekly interval';
                        } else if (!empty($bydays)) {
                            $error['recurrence_bydays'] = 'Invalid day of week';
                        }
                        break;
                    case 'monthly':

                        $byweekno = in_array($data['recurrence_byweekno'], array(-1, 1, 2, 3, 4));
                        if (empty($data['recurrence_byweekno'])) {
                            $error['recurrence_byweekno'] = 'Please select week number of month';
                        } else if (!in_array($data['recurrence_byweekno'], array(-1, 1, 2, 3, 4))) {
                            $error['recurrence_byweekno'] = 'Invalid week number of month';
                        }
                        $byday = in_array($data['recurrence_byday'], array(0, 1, 2, 3, 4, 5, 6));
                        if (!in_array($data['recurrence_byday'], array(0, 1, 2, 3, 4, 5, 6))) {
                            $error['recurrence_byday'] = 'Invalid day of month';
                        }
                    case 'yearly':

                        break;
                }
            }
            if (!empty($data['recurrence_days']) && !ctype_digit($data['recurrence_days'])) {
                $error['recurrence_days'] = 'Please enter a valid number.';
            }
        }



        if (empty($data['grp_franchisee']) && (!current_user_can('editor'))) {
            $error['grp_franchisee'] = 'Please select franchisee.';
        }

        if (empty($data['grp_facilitators'])) {
            $error['grp_facilitators'] = 'Please select facilitators.';
        }

//        if(empty($data['nc_group'])){
//            $error['nc_group'] = 'Please select participant group.';
//        }

        if (!empty($error)) {
            $group_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }

    /**
     * @since 1.0
     */
    private function save_meta($post_id, $data, $child = true) {

        if (!empty($data['nc_sdate'])) {
            $sdate = DateTime::createFromFormat('m/d/Y', $data['nc_sdate']);
            $value = $sdate->format('Ymd');
            $field_name = 'field_56961375959a5';
            update_field($field_name, $value, $post_id);
        }

        if (!empty($data['nc_edate'])) {
            $edate = DateTime::createFromFormat('m/d/Y', $data['nc_edate']);
            $value = $edate->format('Ymd');
            $field_name = 'field_56961394959a6';
            update_field($field_name, $value, $post_id);
        }

        if (!empty($data['nc_stime'])) {
            $value = date("H:i", strtotime("{$data['nc_stime']} {$data['nc_start_time_prefix']}"));
            $field_name = 'field_569613a9959a7';
            update_field($field_name, $value, $post_id);
        }

        if (!empty($data['nc_etime'])) {
            $value = date("H:i", strtotime("{$data['nc_etime']} {$data['nc_end_time_prefix']}"));
            $field_name = 'field_569613be959a8';
            update_field($field_name, $value, $post_id);
        }

        if (!empty($data['address'])) {
            update_post_meta($post_id, 'address', $data['address']);
        }

        $field_name = 'field_56ea3847bec4f';
        $value = ($data['all_day'] == 1) ? 1 : 0;
        update_field($field_name, $value, $post_id);

        $field_name = 'field_56d571a1f05c8';
        $value = ($data['event_type'] == 'Repeat') ? 'Repeat' : 'None';
        update_field($field_name, $value, $post_id);

        if (current_user_can('editor')) {
            $data['grp_franchisee'] = get_current_user_franchisee_id();
        }

        if (!empty($data['grp_franchisee'])) {
            $value = (current_user_can('editor')) ? get_current_user_franchisee_id() : $data['grp_franchisee'];
            update_field('franchisee', $value, $post_id);
        }

        if (!empty($data['nc_month'])) {
            $field_name = 'field_56ea7569abf6a';
            $value = $data['nc_month'];
            update_field($field_name, $value, $post_id);
        }

        if (!empty($data['time_zone'])) {
            update_post_meta($post_id, 'time_zone', $data['time_zone']);
        }

        if (!empty($data['time_zone_country'])) {
            update_post_meta($post_id, 'time_zone_country', $data['time_zone_country']);
        }

        if ($data['event_type'] == 'Repeat') {
            if (!empty($data['recurrence_freq'])) {
                $field_name = 'field_56d68b0b374a9';
                $value = $data['recurrence_freq'];
                update_field($field_name, $value, $post_id);
            }
            if (!empty($data['recurrence_interval'])) {
                $field_name = 'field_56d68b69374aa';
                $value = $data['recurrence_interval'];
                update_field($field_name, $value, $post_id);
            }
            if (!empty($data['recurrence_bydays'])) {
                $field_name = 'field_56d68d724f88f';
                $value = $data['recurrence_bydays'];
                update_field($field_name, $value, $post_id);
            }
            if (!empty($data['recurrence_byweekno'])) {
                $field_name = 'field_56d68ecd12507';
                $value = $data['recurrence_byweekno'];
                update_field($field_name, $value, $post_id);
            }
            if (!empty($data['recurrence_byday'])) {
                $field_name = 'field_56d68f5e12509';
                $value = $data['recurrence_byday'];
                update_field($field_name, $value, $post_id);
            }
            if (!empty($data['recurrence_days'])) {
                $field_name = 'field_56d6929293723';
                $value = $data['recurrence_days'];
                update_field($field_name, $value, $post_id);
            }
        }

        if (!empty($data['acf']['field_569612aade112']['address'])) {
            $field_name = 'field_56961289de10f';
            $value = $this->getGeoCounty($data['acf']['field_569612aade112']['address']);
//            $value  = 'India';
            update_field($field_name, $value, $post_id);
        }
    }
    
    /**
     * @since 1.0
     */
    public function reasign_sessions($group_id,$data) {
        
        $new_start_date = date("Ymd", strtotime($data['grp_start_date']));
        $new_end_date   = date("Ymd", strtotime($data['grp_end_date']));
        $old_start_date = get_post_meta($group_id,'group_start_date',true);
        $old_end_date   = get_post_meta($group_id,'group_end_date',true);
        update_post_meta($group_id,'group_start_date',$new_start_date);
        update_post_meta($group_id,'group_end_date',$new_end_date);
        
        if($old_start_date != $new_start_date || $old_end_date != $new_end_date) {
            
            $parent_sessions = array();

            $meta = array();
            $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '==');
            $meta[] = array('key' => 'groups', 'value' => $group_id, 'compare' => '==');
            $meta['relation'] = 'AND';
            $parent_args = array('post_type' => 'event', 'meta_query' => $meta, 'repeat' => true, 'posts_per_page' => -1);
            $parent_query = new WP_Query($parent_args);
            if ($parent_query->have_posts()) {
                while ($parent_query->have_posts()) {
                    $parent_query->the_post();
                    $parent_sessions[] = get_the_ID();
                }
            }
            wp_reset_postdata();

            if(!empty($parent_sessions)) {
                $meta = array();
                $meta[] = array('key' => 'parent_event', 'value' => $parent_sessions, 'compare' => 'IN');
                $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!=');
                $meta[] = array('key' => 'groups', 'value' => $group_id, 'compare' => '==');
                $meta['relation'] = 'AND';
                $child_args = array('post_type' => 'event', 'meta_query' => $meta, 'repeat' => true, 'posts_per_page' => -1);
                $child_query = new WP_Query($child_args);
                if ($child_query->have_posts()) {
                    while ($child_query->have_posts()) {
                        $child_query->the_post();
                        delete_post_meta(get_the_ID(), 'groups', $group_id);
                    }
                }
                wp_reset_postdata();

                foreach($parent_sessions as $parent_session) {
                    $this->assign_group($group_id,$parent_session);
                }

            }
        }
    }
    
    /**
     * @since 1.0
     */
    public function delete_event() {

        $data = $_POST;
        if (!empty($data['id'])) {
            $event_id = (int) $data['id'];
            $main_event_id = get_post_meta($event_id, 'main_session', true);
            if ($main_event_id) {
                delete_post_meta($main_event_id, 'reschedule_session', $event_id);
            }
            wp_delete_post($event_id);
            $return['status'] = 'success';
            $return['data'] = 'Successfully deleted.';
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data'] = 'you can not delete this.';
            echo json_encode($return);
        }
        exit;
    }
    
    /**
     * @since  1.0
     */
    private function getGeoCounty($geoAddress) {
        $url = 'http://maps.google.com/maps/api/geocode/json?address=' . $geoAddress . '&sensor=false';
        $get = file_get_contents($url);
        $geoData = json_decode($get);
        if (json_last_error() !== JSON_ERROR_NONE) {
            //throw new \InvalidArgumentException('Invalid geocoding results');
            return null;
        }

        if (isset($geoData->results[0])) {
            if (isset($geoData->results[0]->country)) {
                foreach ($geoData->results[0]->country as $addressComponent) {
                    if (in_array('administrative_area_level_2', $addressComponent->types)) {
                        return $addressComponent->short_name;
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * @since 1.0
     */
    public function WP_FullCalendar() {

        global $current_user;

        $request = $_REQUEST;
        $user_events = array();

        $start_date = $request['start'];
        $new_start_date = date("Ymd", strtotime($start_date));

        $end_date = $request['end'];
        $new_end_date = date("Ymd", strtotime($end_date));

        $meta_query = array(
            'relation' => 'AND',
            array(
                'key' => 'event_type',
                'value' => 'Repeat',
                'compare' => '!='
            ),
            array(
                'relation' => 'OR',
                array(
                    'key' => 'start_date',
                    'value' => $new_start_date,
                    'compare' => '>=',
                    'type' => 'DATE'
                ),
                array(
                    'key' => 'start_date',
                    'value' => $new_end_date,
                    'compare' => '<',
                    'type' => 'DATE'
                ),
            )
        );

        switch (nuclo_get_user_role($current_user->ID)) {
            case 'editor':
                $meta_query[] = array(
                    'key' => 'franchisee',
                    'value' => get_current_user_franchisee_id()
                );

                break;

            case 'author':
//                $meta_query[] = array(
//                    'key' => 'facilitator',
//                    'value' => "'%" . $current_user->ID . "%'",
//                    'compare' => 'LIKE'
//                );
                $franchisee_id = get_current_user_franchisee_id();
                    $user_events = array();
                    global $wpdb;
                    $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='event'");
                   
                    if ($results) {
                        foreach ($results as $k => $v) {
                            $e_id = $v->ID;
                            $facilitator = array();
                            $facilitator = get_post_meta($e_id, 'facilitator');
                            
                            if (is_array($facilitator) && in_array($current_user->ID, $facilitator)) {
                                $user_events[] = $e_id;
                            }
                        }
                    }
                    
                    if (empty($user_events)) {
                        $user_events = array('0');
                    }

                break;

            case 'contributor':

                $user_events = get_group_event();

                if (empty($user_events)) {
                    $user_events[] = 0;
                }

                break;
        }

        $args = array('post_type' => 'event', 'posts_per_page' => -1, 'meta_query' => $meta_query);

        if (!empty($user_events)) {
            $args['post__in'] = $user_events;
        }
        $cal_events = array();

        $events = new WP_Query($args);

        if ($events->have_posts()) {

            while ($events->have_posts()) {

                $events->the_post();

                global $post;

                $start_date = get_field('start_date');
                $new_start_date = date("Y-m-d", strtotime($start_date));

                $end_date = get_field('end_date');
                if (empty($end_date)) {
                    $end_date = $start_date;
                } else if ($start_date == $end_date) {
                    $end_date = $start_date;
                } else {
                    $end_date = $start_date;
                }
                $new_end_date = date("Y-m-d", strtotime($end_date));

                $start_time = empty(get_field('start_time')) ? '00' : get_field('start_time');
                $end_time = empty(get_field('end_time')) ? '00' : get_field('end_time');

                $start = $new_start_date . 'T' . $start_time . ':00';
                $end = $new_end_date . 'T' . $end_time . ':00';

                $allday = get_field('all_day');

                $event_id = get_the_ID();
                $post_id = wp_get_post_parent_id($event_id);

                $event = array();
                $event['post_id'] = $event_id;
                $event['event_id'] = $post_id ? $post_id : $event_id;
                $event['title'] = $post->post_title;
                $event['url'] = get_permalink();
                $event['start'] = $start;
                $event['end'] = $end;
                $event['allDay'] = $allday;
                $event['borderColor'] = '';
                $event['color'] = '';
                $event['textColor'] = '';

                array_push($cal_events, $event);
            }
        }

        wp_reset_postdata();

        echo json_encode($cal_events);
        exit;
    }

    /**
     * @since 1.0
     */
    public function event_qtip_content() {

        $request = $_REQUEST;
        $post_id = $request['post_id'];

        if (!get_post($post_id)) {
            exit;
        }

        $title = get_the_title($post_id);
        $url = get_permalink($post_id);

        $guide_arry = get_field('guides', $post_id);

        $guides = '';

        $date = get_event_date($post_id);

        foreach ($guide_arry as $guide) {
            $guides .= $guide['user_firstname'] . ' ' . $guide['user_lastname'] . ', ';
        }

        echo '<h4>' . $title . '</h4>';
        echo '<p>' . $date . '</p>';

        if (!empty($guide_arry)) {
            echo '<p><b>Guide : </b>' . rtrim($guides, ', ') . '</p>';
        }

        if (current_user_can('mark_attendance')) {
            echo '<a href="' . $url . '" class="btn btn-primary btn-sm">Mark Attendance</a>';
        }

        if (current_user_can('edit_event')) {
            echo '<a href="#" class="btn btn-success btn-sm">Edit Event</a>';
        }
        exit;
    }

    /**
     * @since 1.0
     */
    public function mark_attendance() {

        global $current_user;

        $request = $_REQUEST;
        
        if (isset($request['nc_action']) && $request['nc_action'] == 'mark_attendance' && current_user_can('mark_attendance') && wp_verify_nonce($request['nc_nuclo'], 'nc_nuclo')) {

            $users = $request['user'];
            $event_id = $request['nc_event_id'];
            $bundle_id = $request['nc_bundle_id'];
            $event = get_post($event_id);

            $not_attended_users = array();

            if ($event) {

                foreach ($users as $user_id => $attendance) {

                    if ($attendance['index'] == 0) {
                        $status = ($attendance['status'] == 'attended') ? 1 : 0;
                        $key = 'module_result_' . $event_id;
                        $update = update_user_meta($user_id, $key, $status);
                        //update_user_meta($user_id, 'module_bundle_' . $bundle_id, $event_id);
                        $row = array('user' => $user_id, 'status' => $attendance['status']);
                        add_row('field_56d408e5d0347', $row, $event_id);
                        update_post_meta($bundle_id, 'event_' . $user_id, $attendance['status']);
                        if ($update) {
                            do_action('session_feedback_action_plan_alert', $user_id);
                        }
                    } else {
                        $status = ($attendance['status'] == 'attended') ? 1 : 0;
                        $update = update_user_meta($user_id, 'module_result_' . $event_id, $status);
                        //update_user_meta($user_id, 'module_bundle_' . $bundle_id, $event_id);
                        $row = array('user' => $user_id, 'status' => $attendance['status']);
                        update_row('field_56d408e5d0347', $attendance['index'], $row, $event_id);
                        update_post_meta($bundle_id, 'event_' . $user_id, $attendance['status']);
                        if ($update && $status) {
                            do_action('session_feedback_action_plan_alert', $user_id);
                        }
                    }
                    update_post_meta($event_id, 'attendance_status', 'marked');
                    if ($attendance['status'] == 'not-attended') {
                        $not_attended_users[] = $user_id;
                    }
                }

                if (($request['save'] == 'reschedule')) {
                    $this->reschedule_session_group($event_id, $not_attended_users);
                    $reschedule_session = get_post_meta($event_id, 'reschedule_session', true);
                    if (!$reschedule_session) {
                        $this->reschedule_session($event_id);
                    }
                }

                update_field("field_56dd554c1d909", $current_user->ID, $event_id);
                update_field("field_56dd555e1d90a", time(), $event_id);

                if (($request['save'] == 'reschedule')) {
                    $url = get_permalink($event_id) . '\?msg=success';
                    wp_redirect($url);
                } else {
                    wp_redirect(get_permalink($event_id));
                }


                exit;
            }
        }
    }

    /**
     * @since 1.0
     */
    public function filter_query($query) {
        if (isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'event' && !is_admin() && !isset($query->query_vars['repeat'])) {

            $query->query_vars['posts_per_page'] = $this->per_page;
            $query->query_vars['paged'] = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;
                
            global $current_user;

            $meta = array();

            $user_events = array();

            $query->set('orderby', 'meta_value_num');
            $query->set('meta_key', 'start_date');
            $query->set('order', 'DESC');

            if ($query->is_main_query() && is_post_type_archive('event')) {

                $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '==');
            }

            switch (nuclo_get_user_role($current_user->ID)) {
                case 'editor':
                    $meta[] = array(
                        'key' => 'franchisee',
                        'value' => get_current_user_franchisee_id()
                    );

                    break;

                case 'author':
//                    $meta[] = array(
//                        'key'       => 'facilitator',
//                        'value'     => "*".$current_user->ID."*",
//                        'compare'   => 'LIKE'
//                    );

                    $franchisee_id = get_current_user_franchisee_id();
                    $user_events = array();
                    global $wpdb;
                    $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='event'");
                   
                    if ($results) {
                        foreach ($results as $k => $v) {
                            $e_id = $v->ID;
                            $facilitator = array();
                            $facilitator = get_post_meta($e_id, 'facilitator');
                            
                            if (is_array($facilitator) && in_array($current_user->ID, $facilitator)) {
                                $user_events[] = $e_id;
                            }
                        }
                    }
                    if (empty($user_events)) {
                        $user_events = array('0');
                    }

                    break;
                case 'contributor':
                    $user_events = get_group_event();
                    if (empty($user_events)) {
                        $user_events[] = 0;
                    }
                    break;
            }

            if ($query->is_main_query()) {
                if (count($meta) > 0) {
                    $meta['relation'] = 'AND';
                }

                $query->query_vars['meta_query'] = $meta;
            }

            if (isset($_GET['search']) && !empty($_GET['search'])) {
                $search = $_GET['search'];
                $search = trim($search);
                $search = stripslashes($search);
                $search = htmlspecialchars($search);
                $query->set('s', $search);
            }

            if (!empty($user_events)) {
                $query->set('post__in', $user_events);
            }
        }
    }

    /**
     * @since 1.0
     */
    public static function event_reminder() {
        // Week before
        $week_start_date = date("Ymd", strtotime("+7 days"));
        $week_start_time = date("H:i", strtotime("+7 days"));
        $week_args = array(
            'post_type' => 'event',
            'post_status' => 'publish',
            'meta_query' => array(
                'relation' => 'AND',
                array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!='),
                array('key' => 'start_date', 'value' => $week_start_date, 'compare' => '='),
                array('key' => 'start_time', 'value' => $week_start_time, 'compare' => '=')
            )
        );
        $week_events = new WP_Query($week_args);
        if ($week_events->have_posts()) {
            while ($week_events->have_posts()) {
                $week_events->the_post();
                $event_id = get_the_ID();
                $month = get_field('month');
                if (!empty($month)) {
                    $dateObj = DateTime::createFromFormat('!m', $month);
                    $monthname = strtolower($dateObj->format('F'));
                    $email_template_id = get_field($monthname . '_training_notification', 'option');
                    if (!empty($email_template_id)) {
                        $email_template_post = get_post($email_template_id);
                        $_message = $email_template_post->post_content;
                        $title = get_field('subject', $email_template_id);
                        $date = get_event_date($event_id);
                        $location = get_field('venue');
                        $groups = get_post_meta('groups');

                        foreach ($groups as $group) {
                            $participants = get_group_users($group);
                            foreach ($participants as $participant) {
                                $user_info = get_userdata($participant);
                                if ($user_info) {

                                    $name = $user_info->first_name . ' ' . $user_info->last_name;
                                    $message = Nuclo::replace_vars($_message, '', array(
                                                '%participant_name%' => $name,
                                                '%date_time%' => $date,
                                                '%month%' => $monthname,
                                                '%location%' => $location['address']
                                    ));

                                    wp_mail($user_info->user_email, $title, $message);
                                }
                            }
                        }
                    }
                }
            }
        }
        wp_reset_postdata();

        // Day before
        $day_start_date = date("Ymd", strtotime("+1 days"));
        $day_start_time = date("H:i", strtotime("+1 days"));
        $day_args = array(
            'post_type' => 'event',
            'post_status' => 'publish',
            'meta_query' => array(
                'relation' => 'AND',
                array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!='),
                array('key' => 'start_date', 'value' => $day_start_date, 'compare' => '='),
                array('key' => 'start_time', 'value' => $day_start_time, 'compare' => '=')
            )
        );
        $day_events = new WP_Query($day_args);
        if ($day_events->have_posts()) {
            while ($day_events->have_posts()) {
                $day_events->the_post();
                $event_id = get_the_ID();
                $month = get_field('month');
                if (!empty($month)) {
                    $dateObj = DateTime::createFromFormat('!m', $month);
                    $monthname = strtolower($dateObj->format('F'));
                    $email_template_id = get_field($monthname . '_training_notification', 'option');
                    if (!empty($email_template_id)) {
                        $email_template_post = get_post($email_template_id);
                        $_message = $email_template_post->post_content;
                        $title = get_field('subject', $email_template_id);
                        $date = get_event_date($event_id);
                        $location = get_field('venue');
                        $groups = get_post_meta('groups');

                        foreach ($groups as $group) {
                            $participants = get_group_users($group);
                            foreach ($participants as $participant) {
                                $user_info = get_userdata($participant);
                                if ($user_info) {

                                    $name = $user_info->first_name . ' ' . $user_info->last_name;
                                    $message = Nuclo::replace_vars($_message, '', array(
                                                '%participant_name%' => $name,
                                                '%date_time%' => $date,
                                                '%month%' => $monthname,
                                                '%location%' => $location['address']
                                    ));

                                    wp_mail($user_info->user_email, $title, $message);
                                }
                            }
                        }
                    }
                }
            }
        }
        wp_reset_postdata();
    }

    /**
     * @since 1.0
     */
    public static function reschedule_session_group($data) {

        global $current_user;
        
        $event_id   = $data['event'];
        $session_id = $data['session'];
        $event      = get_post($event_id);
        $group_id   = get_post_meta($event_id, 'reschedule_session_group', true);
        $month      = get_post_meta($event_id, 'month', true);
        
        if (!$group_id) {
            
            $group_args = array(
                'post_type'     => 'group',
                'post_per_page' => 1,
                'meta_query'    => array(
                    'relation'  => 'AND',
                    array(
                        'key'   => 'reschedule_group_time',
                        'value' => date('mY',strtotime('01-'.$month.'-'.date('Y',time())))
                    )
                )
            );
            
            $group_list = get_post($group_args);
            $group_id   = $group_list->ID;
            
            if(empty($group_list)){
            
                $group_details = array();

                $session_start_date = '01-'.date('m-Y',(strtotime(get_field('start_date', $session_id))));
                $session_end_date   = date('t-m-Y',(strtotime(get_field('start_date', $session_id))));

                $group_details['group_start_date'] = date("Ymd", (strtotime($session_start_date)));
                $group_details['group_end_date'] = date("Ymd", (strtotime($session_end_date)));

                $group_details['group_name'] = 'Absentie Group - '.date('F - Y',strtotime('01-'.$month.'-'.date('Y',time())));
                $group_details['group_franchisee'] = get_field('franchisee', $event_id);
                $group_details['group_facilitators'] = get_field('facilitator', $event_id);
                $group_details['group_users'] = json_decode($data['not_attended_users']);
                $group_details['grp_posts'] = array($event->post_parent);

                $group_post = array(
                    'post_title' => $group_details['group_name'],
                    'post_content' => '',
                    'post_type' => 'group',
                    'post_status' => 'publish',
                    'post_author' => $current_user->ID
                );

                $group_id = wp_insert_post($group_post);

                update_post_meta($group_id, 'group_start_date', $group_details['group_start_date']);
                update_post_meta($group_id, '_group_start_date', 'field_56b8e28634870');
                update_post_meta($group_id, 'group_end_date', $group_details['group_end_date']);
                update_post_meta($group_id, '_group_end_date', 'field_56b8e2b434871');

                update_post_meta($group_id, 'posts', $group_details['grp_posts']);
                update_post_meta($group_id, '_posts', 'field_56b05a535033e');

                update_post_meta($group_id, 'franchisee', $group_details['group_franchisee']);
                update_post_meta($group_id, '_franchisee', 'field_56d5cfe4606d7');

                update_post_meta($group_id, 'users', $group_details['group_users']);
                update_post_meta($group_id, '_users', 'field_56b05a6d5033f');

                update_post_meta($group_id, 'facilitators', $group_details['group_facilitators']);
                update_post_meta($group_id, '_facilitators', 'field_5705cc7b6b1f5');

                update_post_meta($group_id, 'reschedule_group', 1);
                update_post_meta($group_id, 'reschedule_group_session', $event_id);
                update_post_meta($group_id, 'rescheduled_session', $session_id);
                update_post_meta($event_id, 'reschedule_session_group', $group_id);
                update_post_meta($event_id, 'reschedule_group_time', date('mY',strtotime('01-'.$month.'-'.date('Y',time()))));
            }else{
                update_post_meta($group_id, 'users', json_decode($data['not_attended_users']));
                update_post_meta($group_id, '_users', 'field_56b05a6d5033f');
            }
        } else {
            update_post_meta($group_id, 'users', json_decode($data['not_attended_users']));
            update_post_meta($group_id, '_users', 'field_56b05a6d5033f');
        }
        
        return $group_id;
    }

    /**
     * @since 1.0
     */
    public static function reschedule_session() {
        
        if(isset($_POST) ){
            
            $data           = $_POST;            
            $group_id       = self::reschedule_session_group($data);
            $event_id       = $data['event'];
            $reschedule_id  = $data['session'];
            $users          = json_decode($data['not_attended_users']);
            $month          = get_post_meta($event_id, 'month', true);
            
            add_post_meta($reschedule_id,'groups',$group_id);            
            
            foreach($users as $user){
                update_user_meta($user,'reschedule_session_'.date('F',strtotime($month)),TRUE);
            }            
        }
    
    ?>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <h2>Session Reschedule successfully.</h2>
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <?php
        exit();
    }
    
    /*
     *  List of Sessions for reschedule
     * 
     */
    
    public static function session_list_reschedule(){
        
        if(!empty($_REQUEST['nc_event_id']) && !empty($_REQUEST['nc_bundle_id'])){
            
            $users  = array();
            
            foreach($_REQUEST['user'] as $key => $user){
                if(!in_array($key,$users) && ($user['status'] == 'not-attended')){
                    $users[]    = $key;
                }                
            }
            
            $month              = get_post_meta($_REQUEST['nc_event_id'],'month',true);
            $franchisee         = get_post_meta($_REQUEST['nc_event_id'],'franchisee',true);
            $current_year       = date('Y',time());
            $parent_event       = get_post_meta($_REQUEST['nc_event_id'],'parent_event',true);
            
            $session_list_args  = array(
                'post_type'     => 'event',
                'post_parent'   => $_REQUEST['nc_bundle_id'],
                'meta_query'    => array(
                    'relation'  => 'AND',
                    array(
                        'key'       => 'franchisee',
                        'value'     => $franchisee
                    ),
                    array(
                        'key'       => 'month',
                        'value'     => $month
                    ),
                    array(
                        'key'       => 'start_date',
                        'value'     => array(date('Ymd',strtotime('01-'.$month.'-'.$current_year)),date('Ymt',strtotime('01-'.$month.'-'.$current_year))),
                        'compare'   => 'BETWEEN'
                    ),
                    array(
                        'key'       => 'parent_event',
                        'value'     => $parent_event,
                        'compare'   => '!='
                    )
                ),
                'post_per_page' => -1
            ); 
            
            $session_list   = get_posts($session_list_args);
            
            $next_year      = date('Y',strtotime('next year'));
            $next_session_args  = array(
                'post_type'     => 'event',
                'post_parent'   => $_REQUEST['nc_bundle_id'],
                'meta_query'    => array(
                    'relation'  => 'AND',
                    array(
                        'key'       => 'franchisee',
                        'value'     => $franchisee
                    ),
                    array(
                        'key'       => 'month',
                        'value'     => $month
                    ),
                    array(
                        'key'       => 'start_date',
                        'value'     => array(date('Ymd',strtotime('01-'.$month.'-'.$next_year)),date('Ymt',strtotime('01-'.$month.'-'.$next_year))),
                        'compare'   => 'BETWEEN'
                    ),
                    array(
                        'key'       => 'parent_event',
                        'value'     => $parent_event
                    )
                ),
                'post_per_page' => -1
            ); 
            
            $next_year_session_list = get_posts($next_session_args);                 
        
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-12">
            <form action="" id="session_form" method="post" class="white-bg m-b-sm p-sm animated fadeIn" style="padding:0px;">
                <table class="table table-condensed table-bordered tables-others" style="margin-bottom: 0px;">
                    <thead>
                        <tr>
                            <th style="vertical-align: middle;"> Session Name </th>
                            <th style="vertical-align: middle;"> Select </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(!empty($session_list)){
                                foreach($session_list as $session){
                                    $start_date = date('F - Y',strtotime(get_post_meta($session->ID,'start_date',true)));
                        ?>
                        <tr>
                            <td style="vertical-align: middle;font-weight: bold;"><?php echo $session->post_title.' - '.$start_date; ?></td>
                            <td style="vertical-align: middle;font-weight: bold;"><input type="radio" value="<?php echo $session->ID; ?>" name="session" /></td>
                        </tr>
                        <?php } } ?>
                        <?php
                            if(!empty($next_year_session_list)){
                                foreach($next_year_session_list as $session){
                                    $start_date = date('F - Y',strtotime(get_post_meta($session->ID,'start_date',true)));
                        ?>
                        <tr>
                            <td style="vertical-align: middle;font-weight: bold;"><?php echo $session->post_title.' - '.$start_date; ?></td>
                            <td style="vertical-align: middle;font-weight: bold;"><input type="radio" value="<?php echo $session->ID; ?>" name="session" /></td>
                        </tr>
                        <?php } } ?>  
                        <?php if(empty($session_list_args) && empty($next_year_session_list)){ ?>
                        <tr>
                            <td style="vertical-align: middle;font-weight: bold;">No sessions</td>                            
                        </tr>
                        <?php } ?>
                    </tbody>
                    <input type="hidden" name="not_attended_users" value="<?php echo json_encode($users); ?>" />
                    <input type="hidden" name="event" value="<?php echo $_REQUEST['nc_event_id']; ?>" />
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <input type="hidden" name="nc_action" value="reschedule_session" />
                                <button type="button" class="btn btn-primary pull-right" id="reschedule_session" name="submit" value="save">Submit</button>
                                <div class="clearfix"></div>
                            </td>
                        </tr>
                    </tfoot>
                </table>                
            </form>   
        </div>
    </div>
</div>    
<?php
        }   
        exit;
    }

}

new Event();

function get_group_event() {

    global $current_user, $wpdb;

    $meta_query = array();
    $events = array();

    $group_id = get_user_meta($current_user->ID, 'groups', true);

    $query = "SELECT DISTINCT ID FROM $wpdb->posts ";
    $query .= "INNER JOIN $wpdb->postmeta um1 ON um1.post_id = $wpdb->posts.ID JOIN $wpdb->postmeta um2 ON um2.post_id = $wpdb->posts.ID";
    $query .= " WHERE post_type = 'event' ";

    if (is_array($group_id)) {
        $query .= " AND ( ";
        foreach ($group_id as $group) {
            $meta_query[] = " ( um2.meta_key = 'groups' AND um2.meta_value LIKE '%$group%' )";
        }
        $query .= implode('OR', $meta_query);
        $query .= " ) ";
    } else {
        $query .= " AND ( um2.meta_key = 'groups' AND um2.meta_value LIKE 0 )";
    }

    $filter_events = $wpdb->get_results($query);

    foreach ($filter_events as $filter_event) {
        array_push($events, $filter_event->ID);
    }

    return $events;
}

function get_events($search) {
    global $wpdb;

    $events = array();
    if (!empty($search)) {

        $filter_events = $wpdb->get_results(""
                . "SELECT ID FROM $wpdb->posts WHERE (post_title LIKE '%" . $search . "%' AND post_type = 'event') order by post_title ASC");

        foreach ($filter_events as $filter_event) {
            array_push($events, $filter_event->ID);
        }
    }

    return $events;
}

function get_event_count($type = 'upcoming',$group_id=NULL) {

    global $current_user, $post, $child_events;

    $args = array('post_type' => 'event', 'posts_per_page' => -1);

    if (is_singular('event') && $post->event_type == 'Repeat') {
        $meta[] = array('key' => 'parent_event', 'value' => $post->ID, 'compare' => '==');
    } else {
        $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '==');
    }
    
    if(!empty($group_id)) {
        $meta[] = array('key' => 'groups', 'value' => $group_id, 'compare' => '==');
    }

    if ($type == 'upcoming') {
        $meta[] = array('key' => 'start_date', 'value' => date("Ymd"), 'compare' => '>=', 'type' => 'DATE');
    } else if ($type == 'past') {
        $meta[] = array('key' => 'start_date', 'value' => date("Ymd"), 'compare' => '<', 'type' => 'DATE');
    }
    
    if (in_array('author', $current_user->roles)) { 
        $meta[] = array('key' => 'facilitator', 'value' => $current_user->ID, 'compare' => '==');
    }

    $user_events = array();

    switch (nuclo_get_user_role($current_user->ID)) {
        case 'editor':
            $meta[] = array(
                'key' => 'franchisee',
                'value' => get_current_user_franchisee_id()
            );
            break;
        case 'author':
//            $meta[] = array(
//                'key'       => 'facilitator',
//                'value'     => "'%".$current_user->ID."%'",
//                'compare'   => 'LIKE'
//            );
            $franchisee_id = get_current_user_franchisee_id();
            $user_events = array();
            global $wpdb;
            $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='event'");

            if ($results) {
                foreach ($results as $k => $v) {
                    $e_id = $v->ID;
                    $facilitator = array();
                    $facilitator = get_post_meta($e_id, 'facilitator');

                    if (is_array($facilitator) && in_array($current_user->ID, $facilitator)) {
                        $user_events[] = $e_id;
                    }
                }
            }
            if (empty($user_events)) {
                $user_events = array('0');
            }
            break;
        case 'contributor':
            $user_events = array_merge($user_events, get_group_event());
            break;
    }

    if (count($meta) > 1) {
        $meta['relation'] = 'AND';
    }

    if (isset($_GET['search']) && !empty($_GET['search'])) {
        $user_events = array_intersect($user_events, get_events($_GET['search']));
    }

    if (!empty($user_events)) {
        $args['post__in'] = $user_events;
    }

    $args['meta_query'] = $meta;

    $the_query = new WP_Query($args);

    $total = $the_query->found_posts;

    wp_reset_postdata();

    return $total;
}

function get_event_date($post_id = 0) {

    if ($post_id == 0) {
        global $post;
        $post_id = $post->ID;
        $type = $post->event_type;
    }

    $start_date = ($post->start_date) ? ($post->start_date) : (get_field('start_date', $post_id));
    $str_start_date = strtotime($start_date);
    $new_start_date = date("Y-m-d", $str_start_date);

    $end_date = ($post->end_date) ? $post->end_date : get_field('end_date', $post_id);
    $str_end_date = strtotime($end_date);
    $new_end_date = date("Y-m-d", $str_end_date);

    $start_time = ($post->start_time) ? $post->start_time : get_field('start_time', $post_id);
    $end_time = (($post->end_time)) ? $post->end_time : get_field('end_time', $post_id);

    $allday = get_field('all_day', $post_id);

    if ($allday) {
        $date = date('F j, Y', $str_start_date) . '<br/>All Day';
    } else {
        if ($type == 'Repeat') {
            $end_date = date('Y', ($str_start_date) + (5 * 365 * 24 * 60 * 60));
            $date = date('F j, Y', $str_start_date) . ' - ' . $end_date . '<br/>' . date('g:i a', strtotime($start_time)) . ' - ' . date('g:i a', strtotime($end_time));
        } else {
            if ($start_time == $end_time) {
                $date = date('F j, Y', $str_start_date) . '<br/>' . date('g:i a', strtotime($start_time)) . ' - ' . date('g:i a', (strtotime($end_time) + 4 * 60 * 60));
            } else {
                $date = date('F j, Y', $str_start_date) . '<br/>' . date('g:i a', strtotime($start_time)) . ' - ' . date('g:i a', strtotime($end_time));
            }
        }
    }

    return $date;
}

function get_event_list($type = NULL, $list = FALSE, $select_best_idea = false , $repeat = false) {

    global $current_user;
    $user_events = array();
    $event_args = array(
        'post_type'         => 'event',
        'posts_per_page'    => -1,
        'status'            => 'publish',
        'orderby'           => 'meta_value_num',
        'meta_key'          => 'start_date',
        'order'             => 'DESC'    
    );

    if (current_user_can('editor') || current_user_can('author')) {
        $meta[] = array('key' => 'franchisee', 'value' => get_current_user_franchisee_id());
    }
    if(get_current_user_role()=='author')
    {
        $franchisee_id = get_current_user_franchisee_id();
            $user_events = array();
            global $wpdb;
            $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='event'");

            if ($results) {
                foreach ($results as $k => $v) {
                    $e_id = $v->ID;
                    $facilitator = array();
                    $facilitator = get_post_meta($e_id, 'facilitator');

                    if (is_array($facilitator) && in_array($current_user->ID, $facilitator)) {
                        $user_events[] = $e_id;
                    }
                }
            }
            if (empty($user_events)) {
                $user_events = array('0');
            }
    }
    if($repeat){
        $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!=');
    }else{
        $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '==');
    }
    

    if ($type == 'upcoming') {
        $meta[] = array('key' => 'start_date', 'value' => date("Ymd"), 'compare' => '>=', 'type' => 'DATE');
    } else if ($type == 'past') {
        $meta[] = array('key' => 'start_date', 'value' => date("Ymd"), 'compare' => '<', 'type' => 'DATE');
    }

    if (count($meta) > 0) {
        $meta['relation'] = 'AND';
    }
    if (!empty($user_events)) {
        $event_args['post__in'] = $user_events;
    }
    $event_args['meta_query'] = $meta;

    $events     = get_posts($event_args);
    $event_list = array();

    if ($list) {
        foreach ($events as $event) {
            $modules        = get_event_module($event->ID);
            $event_month    = date('F - Y',strtotime($event->start_date));
            $module_list    = (!empty($modules)) ? array_keys($modules) : array();
            if ($select_best_idea && !empty($modules)) {
                foreach ($module_list as $name) {
                    if (!$modules[$name]['disable']) {
                        $event_list[$event->ID] = $event_month .' - '. $event->post_title;
                    }
                }
            } else {
                $event_list[$event->ID] = $event_month .' - '. $event->post_title;
            }
        }
    } else {
        $event_list = $event;
    }

    return $event_list;
}

function get_event_list_session($franchisee_id,$group_id=0) {

    $event_args = array(
        'post_type' => 'event',
        'posts_per_page' => -1,
        'status' => 'publish'
    );

    $meta[] = array('key' => 'franchisee', 'value' => $franchisee_id, 'compare' => '==');

    $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '==');
    
    if($group_id > 0) {        
        $group_end_date = get_post_meta($group_id,'group_end_date',true);
        $meta[] = array('key' => 'end_date', 'value' => $group_end_date, 'compare' => '>', 'type' => 'DATE');
    } else {
        $meta[] = array('key' => 'start_date', 'value' => date("Ymd"), 'compare' => '>=', 'type' => 'DATE');
    }

    if (count($meta) > 0) {
        $meta['relation'] = 'AND';
    }

    $event_args['meta_query'] = $meta;

    $events = get_posts($event_args);

    return $events;
}

function get_event_module($event_id) {

    global $current_user;

    $months     = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
    $bpm_obj    = new BPM($current_user->ID);
    $month      = date('n',strtotime(get_post_meta($event_id, 'start_date', true)));
    
    if (isset($bpm_obj->months[$months[$month]]) && !empty($bpm_obj->months[$months[$month]])) {
        unset($bpm_obj->months[$months[$month]]['feedback']);
        unset($bpm_obj->months[$months[$month]]['event']);
    }
    $modules        = (isset($bpm_obj->months[$months[$month]]) && !empty($bpm_obj->months[$months[$month]])) ? $bpm_obj->months[$months[$month]] : array();
    $module_list    = (!empty($modules)) ? array_keys($modules) : array();

    foreach ($module_list as $key => $name) {
        if (get_post_meta($event_id, 'module_' . ($key + 1), true)) {
            $modules[$name]['disable'] = true;
        }
    }

    return $modules;
}

function get_event_module_name($event_id, $event_module_id) {

    global $current_user;

    $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
    $bpm_obj = new BPM($current_user->ID);
    $month      = date('n',strtotime(get_post_meta($event_id, 'start_date', true)));

    unset($bpm_obj->months[$months[$month]]['feedback']);
    unset($bpm_obj->months[$months[$month]]['event']);

    $modules = $bpm_obj->months[$months[$month]];
    $module_list = (!empty($modules)) ? array_keys($modules) : array();

    return $module_list[$event_module_id - 1];
}

function get_event_name($event_id) {
    $event_list = get_event_list('past', true , false , true);

    return $event_list[$event_id];
}