<?php

/**
 * @package  Webinar
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Webinar {

    public $timezone;
    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
        $this->timezone = array("Pacific/Samoa" => "(GMT-11:00) Midway Island, Samoa", "Pacific/Honolulu" => "(GMT-10:00) Hawaii", "America/Anchorage" => "(GMT-08:00) Alaska", "America/Phoenix" => "(GMT-07:00) Arizona", "America/Los_Angeles" => "(GMT-07:00) Pacific Time (US and Canada);Tijuana", "America/Denver" => "(GMT-06:00) Mountain Time (US and Canada)", "America/Bogota" => "(GMT-05:00) Bogota, Lima, Quito", "America/Chicago" => "(GMT-05:00) Central Time (US and Canada)", "America/Mexico_City" => "(GMT-05:00) Mexico City", "America/Caracas" => "(GMT-04:30) Caracas, La Paz", "America/New_York" => "(GMT-04:00) Eastern Time (US and Canada)", "America/Indianapolis" => "(GMT-04:00) Indiana (East)", "America/Guyana" => "(GMT-04:00) Georgetown", "America/Halifax" => "(GMT-03:00) Atlantic Time (Canada)", "America/Santiago" => "(GMT-03:00) Santiago", "America/Buenos_Aires" => "(GMT-03:00) Buenos Aires", "America/St_Johns" => "(GMT-02:30) Newfoundland", "America/Sao_Paulo" => "(GMT-02:00) Brasilia", "Atlantic/Cape_Verde" => "(GMT-01:00) Cape Verde Is.", "GMT" => "(GMT) Greenwich Mean Time", "Atlantic/Azores" => "(GMT+00:00) Azores", "Africa/Casablanca" => "(GMT) Casablanca, Monrovia", "Europe/London" => "(GMT) Dublin, Edinburgh, Lisbon, London", "Africa/Malabo" => "(GMT+01:00) West Central Africa", "Africa/Cairo" => "(GMT+02:00) Cairo", "Europe/Brussels" => "(GMT+02:00) Brussels, Copenhagen, Madrid, Paris", "Europe/Amsterdam" => "(GMT+02:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", "Europe/Prague" => "(GMT+02:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", "Africa/Harare" => "(GMT+02:00) Harare, Pretoria", "Europe/Warsaw" => "(GMT+02:00) Sarajevo, Skopje, Sofija, Vilnius, Warsaw, Zagreb", "Europe/Bucharest" => "(GMT+03:00) Bucharest", "Asia/Baghdad" => "(GMT+03:00) Baghdad", "Europe/Moscow" => "(GMT+03:00) Moscow, St. Petersburg, Volgograd", "Asia/Jerusalem" => "(GMT+03:00) Jerusalem", "Asia/Kuwait" => "(GMT+03:00) Kuwait, Riyadh", "Europe/Helsinki" => "(GMT+03:00) Helsinki, Riga, Tallinn", "Africa/Nairobi" => "(GMT+03:00) Nairobi", "Europe/Athens" => "(GMT+03:00) Athens, Istanbul", "Europe/Minsk" => "(GMT+03:00) Minsk", "Asia/Tehran" => "(GMT+03:30) Tehran", "Asia/Tbilisi" => "(GMT+04:00) Baku,Tbilisi, Yerevan", "Asia/Muscat" => "(GMT+04:00) Abu Dhabi, Muscat", "Asia/Kabul" => "(GMT+04:30) Kabul", "Asia/Yekaterinburg" => "(GMT+05:00) Yekaterinburg", "Asia/Karachi" => "(GMT+05:00) Islamabad, Karachi, Tashkent", "Asia/Colombo" => "(GMT+05:30) SriJayawardenepura", "Asia/Calcutta" => "(GMT+05:30) Calcutta, Chennai, Mumbai, New Delhi", "Asia/Katmandu" => "(GMT+05:45) Kathmandu", "Asia/Novosibirsk" => "(GMT+06:00) Almaty, Novosibirsk", "Asia/Dhaka" => "(GMT+06:00) Astana, Dhaka", "Asia/Rangoon" => "(GMT+06:30) Rangoon", "Asia/Jakarta" => "(GMT+07:00) Hanoi, Jakarta", "Asia/Krasnoyarsk" => "(GMT+07:00) Krasnoyarsk", "Asia/Bangkok" => "(GMT+07:00) Bangkok", "Asia/Irkutsk" => "(GMT+08:00) Irkutsk, Ulaan Bataar", "Asia/Shanghai" => "(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi", "Australia/Perth" => "(GMT+08:00) Perth", "Asia/Singapore" => "(GMT+08:00) Kuala Lumpur, Singapore", "Asia/Taipei" => "(GMT+08:00) Taipei", "Asia/Seoul" => "(GMT+09:00) Seoul", "Asia/Yakutsk" => "(GMT+09:00) Yakutsk", "Asia/Tokyo" => "(GMT+09:00) Osaka, Sapporo, Tokyo", "Australia/Darwin" => "(GMT+09:30) Darwin", "Asia/Magadan" => "(GMT+10:00) Magadan, Solomon Is., New Caledonia", "Asia/Vladivostok" => "(GMT+10:00) Vladivostok", "Pacific/Guam" => "(GMT+10:00) Guam, Port Moresby", "Australia/Brisbane" => "(GMT+10:00) Brisbane", "Australia/Adelaide" => "(GMT+10:30) Adelaide", "Australia/Hobart" => "(GMT+11:00) Hobart", "Australia/Sydney" => "(GMT+11:00) Canberra, Melbourne, Sydney", "Pacific/Fiji" => "(GMT+12:00) Fiji, Kamchatka, Marshall Is.", "Pacific/Auckland" => "(GMT+13:00) Auckland, Wellington", "Pacific/Tongatapu" => "(GMT+13:00) Nukualofa");
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('webinar_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('webinar_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('webinar_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('webinar_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('webinar_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('webinar_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('webinar_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('webinar_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('webinar_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('webinar_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('webinar_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('webinar_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('webinar_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('webinar_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'webinar' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'author' )
            );

            register_post_type( 'webinar', $args );
        endif;
    }
    
    /**
     * @since 1.0
     */
    public static function save_post($id, $data) {
        
        $response = create_webinar($data);
        
        $decode_response = json_decode($response);
        
        $data['webinar_key'] = $decode_response->webinarKey;
        
        if(isJson($response)) {
            if(!empty($id)) {
                $my_program = array(
                    'ID'           => $id,
                    'post_title'   => $data['title'],
                    'post_content' => $data['desc'],
                    'post_type'    => 'webinar'
                );
                $program_id = wp_update_post( $my_program );

                self::save_meta($program_id, $data);

            } else {

                $my_program = array(
                    'post_title'   => $data['title'],
                    'post_type'    => 'webinar',
                    'post_status'  => 'publish',
                    'post_content' => $data['desc'],
                    'post_parent'  => get_the_ID()
                );

                $program_id = wp_insert_post( $my_program );

                self::save_meta($program_id, $data);

            }

            return $program_id;
        } else {
            return $response;
        }
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {        
        
        update_post_meta($id, '_webinar_key', 'field_55f7f37638955');
        update_post_meta($id, 'webinar_key', $data['webinar_key']);
        
        Modules::save_meta($id, $data);
    }
   
}

new Webinar();

