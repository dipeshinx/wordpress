<?php

/**
 * @package  Webcast
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Webcast {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('webcast_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('webcast_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('webcast_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('webcast_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('webcast_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('webcast_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('webcast_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('webcast_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('webcast_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('webcast_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('webcast_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('webcast_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('webcast_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('webcast_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'webcast' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'author' )
            );

            register_post_type( 'webcast', $args );
        endif;
    }
    
    /**
     * @since 1.0
     */
    public static function save_post($id, $data) {      
        if(!empty($id)) {
            $program_id = self::update_webcast($id,$data);
            Modules::save_meta($program_id, $data);            
        } else {
            $program_id = self::save_webcast($data);
            Modules::save_meta($program_id, $data);
        }

        return $program_id;
    }
    
    /**
     * @since 1.0
     */
    public static function save_webcast($data) {    

        global $wpdb;
        
        $table_db_name = $wpdb->prefix . "webinarignition";

        $clone = 'new';

        $importcode = $data['importcode'];
        
        $wpdb->insert($table_db_name, array(
            'appname' => stripcslashes($data['nc_title']),
            'camtype' => $clone,
            'total_lp' => '0%%0',
            'total_ty' => '0%%0',
            'total_live' => '0%%0',
            'total_replay' => '0%%0',
            'created' => date('F j, Y')
        ));

        $campaignID = $wpdb->insert_id;

        $my_post = array(
            'post_title' => $data['nc_title'],
            'post_type' => 'webcast',
            'post_content' => $data['nc_desc'],
            'post_status' => 'publish',
            'post_parent'  => get_the_ID()
        );

        $getPostID = wp_insert_post($my_post);

        $wpdb->update($table_db_name, array('postID' => $getPostID), array('id' => $campaignID));

        update_post_meta($getPostID, 'webinarignitionx_meta_box_select', esc_attr($campaignID));

        add_option('webinarignition_campaign_' . $campaignID, "");

        $fullDate = $data["date"];

        if (strpos($fullDate, '-')) {
            $fullDate = explode("-", $fullDate);
        } else {
            $fullDate = explode("/", $fullDate);
        }
        
        $maintitle = stripslashes($data['nc_title']);

        $setDate = $fullDate[2] . "/" . $fullDate[0] . "/" . $fullDate[1];

        $getDate = date('l jS \of F Y', strtotime($setDate));
        $getDate = explode(" ", $getDate);

        $getDay = str_replace("th", "", $getDate[1]);
        $getDay = str_replace("rd", "", $getDay);
        $getDay = str_replace("st", "", $getDay);
        $getDay = str_replace("nd", "", $getDay);

        $setTime = $data["time"];
        $getTime = date("h:i:s A", strtotime($setTime));

        $getTime = explode(" ", $getTime);
        $getHour = explode(":", $getTime[0]);
        $getHour2 = $getHour[0];
        // Check for 0 in front of time..
        if ($getHour2[0] == "0") {
            $getHour2 = str_replace("0", "", $getHour2);
        }

        $timezone = "-5";
        if ($data["venue"][0]["timezone"] != "") {
            $timezone = $data["venue"][0]["timezone"];
        }
        
        $data['timezone'] = $data["venue"][0]["timezone"];

        $host = "Your Name";
        if ($data["host_name"] != "") {
            $host = stripslashes($data["host_name"]);
        }

        $desc = "How We Crush It With Webinars";
        if ($data["title"] != "") {
            $desc = $data["title"];
        }
        
        $embed_html = $data['embed_code'];

        $emailSetup = "Quick Webinar Reminder.\n

    %%INTRO%%

    <b>Date:</b>
    Join us on live :: {DATE}

    <b>Webinar Topic:</b>
    {TITLE}\n

    <b>Webinar Hosts:</b>
    {HOST}\n

    <b># Click here to join:</b>
    {LINK}\n

    # You will be connected to video using your computer's microphone and speakers.  A headset is recommended.\n

    System Requirements:\n

    PC-based attendees
    Required: Windows 7, Vista, XP or 2003 Server\n

    Mac-based attendees
    Required: Mac OS X 10.6 or newer\n

    Mobile attendees
    Required: iPhone, iPad, Android phone or Android tablet\n";


        $notification_times = wi_webinar_live_notification_times($fullDate, $setTime);

        // Data For New Webinar
        $dataArray = array(
            "webinar_desc" => $desc,
            "webinar_host" => $host,
            "webinar_date" => $notification_times['live']['date'],
            "webinar_start_time" => $notification_times['live']['time'],
            "webinar_end_time" => $notification_times['live']['time'],
            "webinar_timezone" => $timezone,
            "lp_metashare_title" => $maintitle,
            "lp_metashare_desc" => $desc,
            "lp_main_headline" => '<h4 class="subheader">Introducing This Exclusive Webinar From ' . $host . '</h4><h2 style="margin-top: -10px;">' . $desc . '</h2>',
            "lp_webinar_month" => $getDate[3],
            "lp_webinar_day" => $getDay,
            "lp_webinar_headline" => $getDate[0] . " The " . $getDate[1],
            "lp_webinar_subheadline" => "", // "At " . $getHour2 . ":" . $getHour[1] . " " . $getTime[1] . " - " . webinarignition_utc_to_abrc($timezone),
            "cd_headline" => '<h4 class="subheader">You Are Viewing The Webinar That Is Not Yet Live - <b>We Go Live Soon!</b></h4><h2 style="margin-top: -10px; margin-bottom: 30px;">Webinar Starts Very Soon</h2>',
            "email_signup_sbj" => "[Reminder] Your Webinar :: $desc",
            "email_signup_body" => str_replace("%%INTRO%%", "Here is the webinar information you just signed up for...<br>", $emailSetup),
            "email_notiff_date_1" => $notification_times['daybefore']['date'],
            "email_notiff_time_1" => $notification_times['daybefore']['time'],
            "email_notiff_status_1" => "queued",
            "email_notiff_sbj_1" => "WEBINAR REMINDER :: Goes Live Tomorrow :: $desc",
            "email_notiff_body_1" => str_replace("%%INTRO%%", "This is a reminder that the webinar you signed up for is tomorrow...<br>", $emailSetup),
            "email_notiff_date_2" => $notification_times['hourbefore']['date'],
            "email_notiff_time_2" => $notification_times['hourbefore']['time'],
            "email_notiff_status_2" => "queued",
            "email_notiff_sbj_2" => "WEBINAR REMINDER :: Goes Live In 1 Hour :: $desc",
            "email_notiff_body_2" => str_replace("%%INTRO%%", "The webinar is live in 1 hour!<br>", $emailSetup),
            "email_notiff_date_3" => $notification_times['live']['date'],
            "email_notiff_time_3" => $notification_times['live']['time'],
            "email_notiff_status_3" => "queued",
            "email_notiff_sbj_3" => "We Are Live",
            "email_notiff_body_3" => str_replace("%%INTRO%%", "We are live, on air and ready to go!<br>", $emailSetup),
            "email_notiff_date_4" => $notification_times['hourafter']['date'],
            "email_notiff_time_4" => $notification_times['hourafter']['time'],
            "email_notiff_status_4" => "queued",
            "email_notiff_sbj_4" => "Replay is live!",
            "email_notiff_body_4" => str_replace("%%INTRO%%", "We just posted the replay video for the webinar tonight...<br>", $emailSetup),
            "email_notiff_date_5" => $notification_times['dayafter']['date'],
            "email_notiff_time_5" => $notification_times['dayafter']['time'],
            "email_notiff_status_5" => "queued",
            "email_notiff_sbj_5" => "WEBINAR REMINDER :: Goes Live Tomorrow :: $desc",
            "email_notiff_body_5" => str_replace("%%INTRO%%", "This is a reminder that the webinar you signed up for is tomorrow...<br>", $emailSetup),
            "email_twilio_date" => $notification_times['live']['date'],
            "email_twilio_time" => $notification_times['hourbefore']['time'],
            "email_twilio_status" => "queued",
            "email_twilio" => "off",
            "twilio_msg" => "The webinar is starting soon! Jump On Live: {LINK}",
            "auto_translate_months" => "Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec",
            "auto_translate_days" => "Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday",
            "lp_banner_bg_style" => "hide",
            "webinar_banner_bg_style" => "hide",
            "smtp_name" => $host,
            "smtp_email" => get_bloginfo("admin_email"),
            'ar_fields_order' => array(
                'ar_name',
                'ar_email'
            ),
            'ar_name' => '',
            'ar_email' => '',
            'lp_optin_name' => 'Enter your name: ',
            'lp_optin_email' => 'Enter your email: ',
            'ar_hidden' => '',
            'replay_video' => $embed_html,
            'webinar_live_video' => $embed_html,
            'footer_branding' => 'hide'
        );
        $obj = array_to_object_wi($dataArray);

        // Save Campaign Setup
        if ($clone == "new") {
            // no clone - new
            update_option('webinarignition_campaign_' . $campaignID, $obj);
        } else if ($clone == "import") {
            // importing campaign -- update Name & Permalink
            $importcode = trim($importcode);
            $webinar = unserialize(base64_decode($importcode));
            $webinar->webinarURLName2 = stripcslashes($data['nc_title']);
            update_option('webinarignition_campaign_' . $campaignID, $webinar);
        } else if ($clone == "auto") {
            // Data For New Webinar
            $dataArray = array(
                "webinar_desc" => $desc,
                "webinar_host" => $host,
                "webinar_date" => "AUTO",
                "lp_metashare_title" => $maintitle,
                "lp_metashare_desc" => $desc,
                "lp_main_headline" => '<h4 class="subheader">Introducing This Exclusive Webinar From ' . $host . '</h4><h2 style="margin-top: -10px;">' . $desc . '</h2>',
                "cd_headline" => '<h4 class="subheader">You Are Viewing The Webinar That Is Not Yet Live </h4><h2 style="margin-top: -10px; margin-bottom: 30px;">We Go Live Soon.</h2>',
                "email_signup_sbj" => "[Reminder] Your Webinar Information ",
                "email_signup_body" => str_replace("%%INTRO%%", "Here is the webinar information you just signed up for...<br>", $emailSetup),
                "email_notiff_sbj_1" => "WEBINAR REMINDER :: Goes Live Tomorrow :: $desc",
                "email_notiff_body_1" => str_replace("%%INTRO%%", "This is a reminder that the webinar you signed up for is tomorrow...<br>", $emailSetup),
                "email_notiff_sbj_2" => "WEBINAR REMINDER :: Goes Live In 1 Hour :: $desc",
                "email_notiff_body_2" => str_replace("%%INTRO%%", "The webinar is live in 1 hour!<br>", $emailSetup),
                "email_notiff_sbj_3" => "We Are Live",
                "email_notiff_body_3" => str_replace("%%INTRO%%", "We are live, on air and ready to go!<br>", $emailSetup),
                "email_notiff_sbj_4" => "Replay is live!",
                "email_notiff_body_4" => str_replace("%%INTRO%%", "We just posted the replay video for the webinar tonight...<br>", $emailSetup),
                "email_notiff_sbj_5" => "WEBINAR REPLAY COMING DOWN SOON :: $desc",
                "email_notiff_body_5" => str_replace("%%INTRO%%", "This is a reminder that the webinar you, the replay is available, but coming down very soon...<br>", $emailSetup),
                "twilio_msg" => "The webinar is starting soon! Jump On Live: {LINK}",
                "email_twilio" => "off",
                "lp_banner_bg_style" => "hide",
                "webinar_banner_bg_style" => "hide",
                "auto_saturday" => "yes",
                "auto_sunday"  => "yes",
                "auto_thursday"	 => "yes",
                "auto_monday"  => "yes",
                "auto_friday"  => "yes",
                "auto_tuesday"  => "yes",
                "auto_wednesday" => "yes",
                "auto_time_1" => "16:00",
                "auto_time_2" => "18:00",
                "auto_time_3" => "20:00",
                "auto_video_length" => "60",
                "auto_translate_months" => "Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec",
                "auto_translate_days" => "Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday",
                "auto_translate_local" => "Local Time",
                "smtp_name" => $host,
                "smtp_email" => get_bloginfo("admin_email"),
                'ar_fields_order' => array(
                    'ar_name',
                    'ar_email'
                ),
                'ar_name' => '',
                'ar_email' => '',
                'lp_optin_name' => 'Enter your name: ',
                'lp_optin_email' => 'Enter your email: ',
            );
            $obj = array_to_object_wi($dataArray);
            // save
            update_option('webinarignition_campaign_' . $campaignID, $obj);
        } else {
            // get option from parent campaign
            $cloneParent = get_option('webinarignition_campaign_' . $clone);
            update_option('webinarignition_campaign_' . $campaignID, $cloneParent);
        }
        
        update_post_meta($getPostID,'host_name'       ,$data['host_name']);
        update_post_meta($getPostID,'embed_code'      ,$data['embed_code']);
        update_post_meta($getPostID,'timezone'        ,$data['timezone']);
        update_post_meta($getPostID,'_host_name'      ,'field_560a6666d6836');
        update_post_meta($getPostID,'_embed_code'     ,'field_560a6672d6837');
        update_post_meta($getPostID,'_embed_code'     ,'field_560a667dd6838');
        
        return $getPostID;
        
    }
    
    /**
     * @since 1.0
     */
    public static function update_webcast($id,$data) {
        global $wpdb;
        
        $table_db_name = $wpdb->prefix . "webinarignition";
        
        $campaign = $wpdb->get_row("SELECT * FROM $table_db_name WHERE postID = '$id'", OBJECT);    
        
        $objectData = get_option('webinarignition_campaign_' . $campaign->id);
        
        if ($objectData->appname == $data['nc_title']) {
        
        } else {
            // Update Inside Of DB
            $wpdb->update($table_db_name, array(
                    'appname' => $data['nc_title']
                ), array('postID' => $id)
            );
            // ReName permalink URL
            $my_post = array();
            $my_post['ID'] = $id;
            $my_post['post_title'] = $data['nc_title'];
            $my_post['post_name'] = $data['nc_title'];
            $my_post['post_content'] = $data['nc_desc'];
           
            wp_update_post($my_post);
        }
        
        $timezone = "-5";
        if ($data["webinar_timezone"] != "") {
            $timezone = $data["webinar_timezone"];
        }

        $data['timezone'] = $data["webinar_timezone"];
        
        $host = "Your Name";
        if ($data["host_name"] != "") {
            $host = stripslashes($data["host_name"]);
        }

        $desc = "How We Crush It With Webinars";
        if ($data["nc_title"] != "") {
            $desc = $data["nc_title"];
        }
        
        $embed_html = $data['embed_code'];
        
        $fullDate = $data["date"];

        if (strpos($fullDate, '-')) {
            $fullDate = explode("-", $fullDate);
        } else {
            $fullDate = explode("/", $fullDate);
        }
        
        $maintitle = stripslashes($data['nc_title']);

        $setDate = $fullDate[2] . "/" . $fullDate[0] . "/" . $fullDate[1];

        $getDate = date('l jS \of F Y', strtotime($setDate));
        $getDate = explode(" ", $getDate);

        $getDay = str_replace("th", "", $getDate[1]);
        $getDay = str_replace("rd", "", $getDay);
        $getDay = str_replace("st", "", $getDay);
        $getDay = str_replace("nd", "", $getDay);

        $setTime = $data["time"];
        $getTime = date("h:i:s A", strtotime($setTime));

        $getTime = explode(" ", $getTime);
        $getHour = explode(":", $getTime[0]);
        $getHour2 = $getHour[0];
        // Check for 0 in front of time..
        if ($getHour2[0] == "0") {
            $getHour2 = str_replace("0", "", $getHour2);
        }
        
        $notification_times = wi_webinar_live_notification_times($fullDate, $setTime);
        
        $objectData->webinarURLName2 = $data['nc_title'];
        //$objectData->webinar_permalink = '';
        $objectData->webinar_desc = $desc;
        $objectData->webinar_host = $host;        
        $objectData->webinar_timezone = $timezone;
        $objectData->webinar_live_video = $embed_html;
        $objectData->replay_video = $embed_html;
        
        if(strtotime(wi_build_time($objectData->webinar_date, $objectData->webinar_time)) != strtotime(wi_build_time($fullDate , $setTime))) {
            //Webinar Date has changed

            $object->email_notiff_date_1 = $notification_times[ 'daybefore' ][ 'date' ];
            $object->email_notiff_time_1 = $notification_times[ 'daybefore' ][ 'time' ];

            $object->email_notiff_date_2 = $notification_times[ 'hourbefore' ][ 'date' ];
            $object->email_notiff_time_2 = $notification_times[ 'hourbefore' ][ 'time' ];

            $object->email_notiff_date_3 = $notification_times[ 'live' ][ 'date' ];
            $object->email_notiff_time_3 = $notification_times[ 'live' ][ 'time' ];

            $object->email_notiff_date_4 = $notification_times[ 'hourafter' ][ 'date' ];
            $object->email_notiff_time_4 = $notification_times[ 'hourafter' ][ 'time' ];

            $object->email_notiff_date_5 = $notification_times[ 'dayafter' ][ 'date' ];
            $object->email_notiff_time_5 = $notification_times[ 'dayafter' ][ 'time' ];

            $object->email_twilio_date = $notification_times[ 'live' ][ 'date' ];
            $object->email_twilio_time = $notification_times[ 'hourbefore' ][ 'time' ];
    
        }
        
        $objectData->webinar_date = $notification_times['live']['date'];
        $objectData->webinar_start_time = $notification_times['live']['time'];
        
        $newDate = $fullDate[2] . "-" . $fullDate[0] . "-" . $fullDate[1];
        $newTime = strtotime($objectData->webinar_start_time);
        $beforeDay = date("Y-m-d", strtotime('-1 day', strtotime($newDate)));
        $beforeDayReformat = date("m-d-Y", strtotime($beforeDay));
        $nextDay = date("Y-m-d", strtotime('+1 day', strtotime($newDate)));
        $nextDayReformat = date("m-d-Y", strtotime($nextDay));
        $beforeTime = date("H:i", strtotime('-1 hour', $newTime));
        $nextTime = date("H:i", strtotime('+1 hour', $newTime));
        
        $emailSetup = "Dear {NAME},\n

%%INTRO%%

<b>Date:</b>
Join us on live :: {DATE}

<b>Webinar Topic:</b>
{TITLE}\n

<b>Webinar Hosts:</b>
{HOST}\n

<b># Click here to join:</b>
{LINK}\n

# You will be connected to video using your computer's microphone and speakers.  A headset is recommended.\n

System Requirements:\n

PC-based attendees
Required: Windows® 7, Vista, XP or 2003 Server\n

Mac-based attendees
Required: Mac OS® X 10.6 or newer\n

Mobile attendees
Required: iPhone®, iPad®, Android™ phone or Android tablet\n";
        
        //
        // -- C/U Email #1 - 1 Day Before Webinar
        //
        // Set: Date / Time / SBJ / Body
        //
        $objectData->email_notiff_date_1 = $beforeDayReformat;
        $objectData->email_notiff_time_1 = $objectData->webinar_start_time;
        $objectData->email_notiff_status_1 = "queued";
        $objectData->email_notiff_sbj_1 = "WEBINAR REMINDER :: Goes Live Tomorrow :: " . $objectData->webinar_desc;
        $objectData->email_notiff_body_1 = str_replace("%%INTRO%%", "This is a reminder that the webinar you signed up for is tomorrow...<br>", $emailSetup);
        //
        // -- C/U Email #2 - 1 Hour Before Webinar
        //
        // Set: Date / Time / SBJ / Body
        //
        $objectData->email_notiff_date_2 = $objectData->webinar_date;
        $objectData->email_notiff_time_2 = $beforeTime;
        $objectData->email_notiff_status_2 = "queued";
        $objectData->email_notiff_sbj_2 = "WEBINAR REMINDER :: Goes Live In 1 Hour :: " . $objectData->webinar_desc;
        $objectData->email_notiff_body_2 = str_replace("%%INTRO%%", "The webinar is live in 1 hour!<br>", $emailSetup);
        //
        // -- C/U Email #3 - Webinar Is Live
        //
        // Set: Date / Time / SBJ / Body
        //
        $objectData->email_notiff_date_3 = $objectData->webinar_date;
        $objectData->email_notiff_time_3 = $objectData->webinar_start_time;
        $objectData->email_notiff_status_3 = "queued";
        $objectData->email_notiff_sbj_3 = "We Are Live";
        $objectData->email_notiff_body_3 = str_replace("%%INTRO%%", "We are live, on air and ready to go!<br>", $emailSetup);
        //
        // -- C/U Email #4 - Replay Video Is Live
        //
        // Set: Date / Time / SBJ / Body
        //
        $objectData->email_notiff_date_4 = $objectData->webinar_date;
        $objectData->email_notiff_time_4 = $nextTime;
        $objectData->email_notiff_status_4 = "queued";
        $objectData->email_notiff_sbj_4 = "Replay Is Ready";
        $objectData->email_notiff_body_4 = str_replace("%%INTRO%%", "We just posted the replay video for the webinar tonight...<br>", $emailSetup);
        //
        // -- C/U Email #5 - 1 Day After Live Webinar
        //
        // Set: Date / Time / SBJ / Body
        //
        $objectData->email_notiff_date_5 = $nextDayReformat;
        $objectData->email_notiff_time_5 = $objectData->webinar_start_time;
        $objectData->email_notiff_status_5 = "queued";
        $objectData->email_notiff_sbj_5 = "Did You See The Replay?";
        $objectData->email_notiff_body_5 = str_replace("%%INTRO%%", "Did you get a chance to check out the webinar replay? It's coming down very soon!<br>", $emailSetup);;
        //
        // -- C/U TXT - 1 Hour Before Live Webinar
        //
        // Set: Date / Time / TEXT
        //
        $objectData->email_twilio_date = $objectData->webinar_date;
        $objectData->email_twilio_time = $beforeTime;
        $objectData->email_twilio_status = "queued";
        // $objectData->twilio_msg = "The webinar is starting soon! Jump On Live: {LINK}";
        // Save New Additions


        $objectData->lp_main_headline = '<h4 class="subheader">Introducing This Exclusive Webinar From ' . $host . '</h4><h2 style="margin-top: -10px;">' . $desc . '</h2>';
        $objectData->lp_webinar_month = $getDate[3];
        $objectData->lp_webinar_day = $getDay;
        $objectData->lp_webinar_headline = $getDate[0] . " The " . $getDate[1];
        $objectData->lp_webinar_subheadline = "At " . $getHour2 . ":" . $getHour[1] . " " . $getTime[1];
        $objectData->cd_headline = '<h4 class="subheader">This Webinar Is Not Live - <b>We Go Live Soon!</b></h4><h2 style="margin-top: -10px; margin-bottom: 30px;">Webinar Starts: ' . $getDate22 . '</h2>';
        $objectData->email_signup_sbj = "[Reminder] Your Webinar Starts: " . $getDate22;
        $objectData->email_signup_body = str_replace("%%INTRO%%", "Here is the webinar information you just signed up for...<br>", $emailSetup);

        update_option('webinarignition_campaign_' . $campaign->id, $objectData);
        
        update_post_meta($id,'host_name'       ,$data['host_name']);
        update_post_meta($id,'embed_code'      ,$data['embed_code']);
        update_post_meta($id,'timezone'        ,$data['timezone']);        
        
        return $id;
        
    }

    
}

new Webcast();