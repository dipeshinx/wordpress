<?php

/**
 * @package  Discussion
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Discussion {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('discussion_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('discussion_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('discussion_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('discussion_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('discussion_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('discussion_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('discussion_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('discussion_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('discussion_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('discussion_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('discussion_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('discussion_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('discussion_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('discussion_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'discussion' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'editor', 'author', 'comments' )
            );

            register_post_type( 'discussion', $args );
        endif;
    }
    
     /**
     * @since 1.0
     */
    public static function save_post($id, $data) {
        if(!empty($id)) {
            $my_program = array(
                'ID'           => $id,
                'post_title'   => $data['nc_title'],
                'post_content' => $data['nc_desc'],
                'post_type'    => 'discussion'
            );
            $program_id = wp_update_post( $my_program );
            
            self::save_meta($program_id, $data);

        } else {

            $my_program = array(
                'post_title'   => $data['nc_title'],
                'post_type'    => 'discussion',
                'post_status'  => 'publish',
                'post_content' => $data['nc_desc'],
                'post_parent'  => get_the_ID()
            );

            $program_id = wp_insert_post( $my_program );
            
            self::save_meta($program_id, $data);
            
        }
        
        return $program_id;
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {
              
        Modules::save_meta($id, $data);
    }
   
}

new Discussion();