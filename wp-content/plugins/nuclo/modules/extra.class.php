<?php

/**
 * @package  Content
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Content {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
        add_action( 'wp_ajax_extras_attachment', array( $this, 'extras_attachment'));
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('extras_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('extras_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('extras_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('extras_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('extras_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('extras_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('extras_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('extras_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('extras_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('extras_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('extras_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('extras_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('extras_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('extras_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => get_field('extras_slug', 'option') ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'editor', 'author' )
            );

            register_post_type( 'extra', $args );
        endif;
    }
    
    /**
     * @since 1.0
     */
    public static function save_post($id, $data) {
       
        if(!empty($id)) {
            $my_program = array(
                'ID'           => $id,
                'post_title'   => $data['nc_title'],
                'post_content' => $data['nc_desc'],
                'post_type'    => 'extra'
            );
            
            $post_id = wp_update_post( $my_program );
            
            self::save_meta($post_id, $data);
            
            $extra_attachment   = array();
            
            if(isset($data['attachedids']) && !empty($data['attachedids'])){
                foreach($data['attachedids'] as $attahcment_id => $extension){
                    $extra_attachment[] = array(
                        'id'    => $attahcment_id,
                        'ext'   => $extension
                    );
                }
            }else{
                $extra_attachment   = array();
            }
            
            update_post_meta($post_id,'extras_attachment',$extra_attachment);
            
            do_action('course_extra_updated',get_the_ID());

        } else {

            $my_program = array(
                'post_title'   => $data['nc_title'],
                'post_type'    => 'extra',
                'post_status'  => 'publish',
                'post_content' => $data['nc_desc'],
                'post_parent'  => get_the_ID()
            );
            
            $post_id = wp_insert_post( $my_program );
            
            self::save_meta($post_id, $data); 
            
            $extra_attachment   = array();
            
            if(isset($data['attachedids']) && !empty($data['attachedids'])){
                foreach($data['attachedids'] as $attahcment_id => $extension){
                    $extra_attachment[] = array(
                        'id'    => $attahcment_id,
                        'ext'   => $extension
                    );
                }
            }else{
                $extra_attachment   = array();
            }
            
            update_post_meta($post_id,'extras_attachment',$extra_attachment);
            
            do_action('course_extra_created',get_the_ID());
            
        }
        
        return $post_id;
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {
        Modules::save_meta($id, $data);
    }
    
    public function extras_attachment(){
            $allowed =  array('doc','docx' ,'ppt', 'pptx', 'pdf');

	    if(!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX)){
	        return;
	    }

	    if(!function_exists('media_handle_upload')){
	        require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
	    }

		$upfilename = $_FILES['files']['name'];
		$extallowed = pathinfo($upfilename, PATHINFO_EXTENSION);

		if(in_array($extallowed, $allowed) ) {
		    $uploadid = media_handle_upload( 'files', '0' );
                    $uploadname = get_the_title($uploadid);
                    $attachments = array('id'=>$uploadid, 'name'=>$uploadname , 'ext'   => $extallowed , 'file_name' => $upfilename);
		} else {
                    $attachments = array('id'=> '0', 'name'=>'ERR');
		}

		echo json_encode($attachments);
		exit();
    }
   
}

new Content();