<?php

/**
 * @package  Bundle
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Bundle {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
        add_action( 'wp_ajax_create_bundle', array($this,'create'));
        add_action( 'wp_ajax_get_bundle', array($this,'get'));
        add_action( 'wp_ajax_asign_bundle' , array($this,'asign'));
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('bundle_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('bundle_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('bundle_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('bundle_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('bundle_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('bundle_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('bundle_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('bundle_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('bundle_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('bundle_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('bundle_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('bundle_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('bundle_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('bundle_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'bundle' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'editor', 'author' )
            );

            register_post_type( 'bundle', $args );
        endif;
    }
    
    /**
     * @since 1.0
     */
    public static function get_post_bundle($course_id, $module_id, $parent_id=0) {
        $args = array(
		'order'       => 'ASC',
		'post_parent' => $course_id,
		'post_status' => 'publish',
		'post_type'   => 'bundle',
	);
        $data = '';
	$bundles = get_children( $args );
        if ( $bundles ) {
            foreach ( $bundles as $bundle ) {  
                if($parent_id == 0 || $parent_id != $bundle->ID) {
                    $data .= '<li class="bn"><a href="#" onclick="event.preventDefault(); move_to_bundle(this);" data-bundle="'.$bundle->ID.'"  data-module="'.$module_id.'" ><i class="fa fa-list-alt"></i> '.get_field('tag',$bundle->ID).'</a></li>';
                }
            }
        }
        return $data;
    }
    
    /**
     * @since 1.0
     */
    public function create() {
        
        $data = $_POST;
        
        $return = array();

        if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && current_user_can( 'create_courseware') && wp_verify_nonce( $data['nc_nuclo'], 'nc_nuclo' ))) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong!';                
            echo json_encode($return);
            exit;
        }

        if(!empty($data['title']) && !empty($data['tag']) && !empty($data['course-id']) && !empty($data['module-id'])) {
            
            $course_id = wp_get_post_parent_id( $data['module-id'] );
            
            if($course_id != $data['course-id']) {
                $return['status'] = 'error';
                $return['data']   = 'Invalid course!';                
                echo json_encode($return);
                exit;
            }
            
            $bundle_post = array(
                        'post_title'   => $data['title'],
                        'post_type'    => 'bundle',
                        'post_status'  => 'publish',
                        'post_name'    => $data['tag'],
                        'post_parent'  => $data['course-id']
            );

            $bundle_id = wp_insert_post( $bundle_post );
            
            if(!empty($bundle_id)) {

                # Adding Month of this module
                update_post_meta( $bundle_id, 'month_of_module', $data['month']);
                update_post_meta( $bundle_id, '_month_of_module', 'field_56f64f266d989');

                update_post_meta( $bundle_id, 'tag', $data['tag']);
                update_post_meta( $bundle_id, '_tag', 'field_5647359efa7c6');
                
                $module_post = array(
                            'ID'           => $data['module-id'],
                            'post_parent'  => $bundle_id
                );
                $module_id = wp_update_post( $module_post );
            }

            if(!empty($bundle_id) && !empty($module_id)) {
                $return['status'] = 'success';
                $return['url']   = get_the_permalink($course_id).'modules';                
                echo json_encode($return);
            } else {
                $return['status'] = 'error';
                $return['data']   = 'Something went wrong!';                
                echo json_encode($return);
            }
        } else {
            $return['status'] = 'error';
            $return['data']   = 'Please enter tag & title!';                
            echo json_encode($return);
        }
        exit;
    }
    
    /**
     * @since 1.0
     */
    public function get() {
        
        $data = $_POST;
        
        $return = array();

        if(!empty($data['course-id']) && !empty($data['module-id'])) {
            
            $parent_id = wp_get_post_parent_id($data['module-id']);
            $post_type = get_post_type($parent_id);
            
            if($post_type == 'bundle') {
                $create = '<a class="btn btn-danger btn-sm m-r-sm" onclick="event.preventDefault(); move_to_bundle(this);" href="#" data-bundle="'.$data['course-id'].'"  data-module="'.$data['module-id'].'"><i class="fa fa-list-alt"></i> Remove from '.get_field('tag',$parent_id).'</a>';
            } else {
                $create = '<a class="btn btn-warning btn-sm m-r-sm" href="#" data-module="'.$data['module-id'].'" data-toggle="modal" data-target="#create-bundle"><i class="fa fa-plus"></i> Create new</a>';                
            }
            
            $data = '<ul class="tag-list clearfix" style="padding: 0">'.Bundle::get_post_bundle($data['course-id'], $data['module-id'],$parent_id).'</ul><div class="create-bundle">'.$create.'<button class="btn btn-white btn-sm cancel" tabindex="2" >Close</button></div>';

            $return['status'] = 'success';
            $return['data']   = $data;   
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data']   = 'Please enter tag & title!';                
            echo json_encode($return);
        }
        exit;
    }
    
    /**
     * @since 1.0
     */
    public function asign() {
        
        $data = $_POST;
        
        $return = array();

        if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && current_user_can( 'edit_courseware'))) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong!';                
            echo json_encode($return);
            exit;
        }
        
        if(!empty($data['module-id']) && !empty($data['bundle-id']) && !empty($data['course-id'])) {
            
            $course_id_m = wp_get_post_parent_id( $data['module-id'] );
            $course_id_b = wp_get_post_parent_id( $data['bundle-id'] );
            
            if(!in_array($data['course-id'], array($course_id_m,$course_id_b,$data['bundle-id']))) {
                $return['status'] = 'error';
                $return['data']   = 'Invalid course!';                
                echo json_encode($return);
                exit;
            }
            
            $module_post = array(
                        'ID'           => $data['module-id'],
                        'post_parent'  => $data['bundle-id']
            );
            $module_id = wp_update_post( $module_post );            

            if(!empty($module_id)) {
                $return['status'] = 'success';
                $return['data']   = 'asign';                
                echo json_encode($return);
            } else {
                $return['status'] = 'error';
                $return['data']   = 'Something went wrong!';                
                echo json_encode($return);
            }
        } else {
            $return['status'] = 'error';
            $return['data']   = 'Invalid data!';                
            echo json_encode($return);
        }
        exit;
        
    }
    
    /**
     * @since 1.0
     */
    public static function save_post($id, $data) {
        if(!empty($id)) {
            $a_bundle = array(
                'ID'           => $id,
                'post_title'   => $data['nc_title'],
                'post_name'    => $data['nc_tag']
            );
            
            $bundle_id = wp_update_post( $a_bundle );

            # Adding Month of this module
            update_post_meta( $bundle_id, 'month_of_module', $data['nc_month']);
            update_post_meta( $bundle_id, '_month_of_module', 'field_56f64f266d989');
            
            self::save_meta($bundle_id, $data);

        }
        return $bundle_id;
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {
        Modules::save_meta($id, $data);
    }
}

new Bundle();
