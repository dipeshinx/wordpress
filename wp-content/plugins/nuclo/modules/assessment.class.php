<?php

/**
 * @package  Assessment
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Assessment {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
        add_action( 'add_meta_boxes', array($this,'add_meta_boxes') );
        add_action( 'save_post', array($this,'save_post_hook'),1,1);
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('assessment_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('assessment_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('assessment_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('assessment_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('assessment_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('assessment_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('assessment_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('assessment_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('assessment_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('assessment_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('assessment_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('assessment_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('assessment_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('assessment_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'assessment' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'editor', 'author' )
            );

            register_post_type( 'assessment', $args );
        endif;
    }
    
     /**
     * @since 1.0.1
     */
    public static function save_post($id, $data) {
        if(!empty($id)) {
            $my_program = array(
                'ID'           => $id,
                'post_title'   => $data['nc_title'],
                'post_content' => $data['nc_desc'],
                'post_type'    => 'assessment'
            );
            $program_id = wp_update_post( $my_program );
            
            $id = get_post_meta($program_id, 'survey_id',true);
            
            if(!empty($id) && !empty($data['nc_title'])) {
                WPSQT::update_title($id,$data['nc_title']);
            }
            
            self::save_meta($program_id, $data);

        } else {

            $my_program = array(
                'post_title'   => $data['nc_title'],
                'post_type'    => 'assessment',
                'post_status'  => 'publish',
                'post_content' => $data['nc_desc'],
                'post_parent'  => get_the_ID()
            );

            $program_id = wp_insert_post( $my_program );
            
            self::save_meta($program_id, $data);
            
        }
        
        return $program_id;
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {
              
        Modules::save_meta($id, $data);
    }
    
     /**
     * @since 1.0
     */
    public function add_meta_boxes() {

        add_meta_box('nuclo_assessment_list', 'Assessment List', array($this,'assessment_list'), 'assessment', 'normal', 'default');

    }
    
    /**
     * @since 1.0
     */
    public function assessment_list() {
        
        global $post;
        
        $assessments = $this->get_assessments();
        $assessment_id = get_post_meta($post->ID, 'survey_id', true);

        echo '<select name="assessment" style="width:100%;"><option value="0">-- Select Assessment --</option>';
        foreach ($assessments as $assessment) {
            
            $is_selected = ( $assessment_id == $assessment->id ) ? 'selected="selected" ' : '';
 
            echo '<option value="'.$assessment->id.'" '.$is_selected.'>'.$assessment->name.'</option>';
        }
        echo '</select>';

    }
    
    /**
     * @since 1.0
     */
    public function get_assessments($type='quiz',$results=false) {
       return $this->assessment_query($type,$results);
    }
    
    /**
     * @since 1.0
     */
    private function assessment_query($type,$results) {

        global $wpdb;
        
        if($results == true) {
            $sql = "SELECT wpsq.*,COUNT(wpar.id) as results 
                FROM `" . WPSQT_TABLE_QUIZ_SURVEYS . "` as wpsq
                LEFT JOIN `" . WPSQT_TABLE_RESULTS . "` as wpar ON wpar.item_id = wpsq.id
                WHERE wpsq.type = '$type'
                GROUP BY wpsq.id
                ORDER BY wpsq.id DESC";
        } else {
            $sql = "SELECT * FROM `" . WPSQT_TABLE_QUIZ_SURVEYS . "` WHERE type = '$type' GROUP BY id ORDER BY id DESC";
        }
        
        $data = $wpdb->get_results($sql);
        
        return $data;
    }
    
    /**
     * @since 1.0
     */
    public function save_post_hook($post_id) {
        if(isset($_POST['assessment']) && !empty($_POST['assessment'])) {
            update_post_meta($post_id, 'survey_id', $_POST['assessment']);
        }
    }
   
}

new Assessment();