<?php

/**
 * @package  Measurement
 * @author   Dipak Pusti <dipak.pusti@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Measurement {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init',         array( $this, 'register_post_type') );
        add_action( 'init',         array( $this, 'rewrites_init'), 1, 0);
        add_action( 'init',         array( $this, 'rewrite_tag'), 10, 0);
        add_filter( 'query_vars',   array( $this, 'query_vars'));
        add_action( 'init',         array( $this, 'add_measurement') );
    }

        /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('measure/(.+?)/([^/]*)/([^/]*)/?$', 'index.php?measure=$matches[1]&type=$matches[2]&id=$matches[3]', 'top');
        add_rewrite_rule('measure/(.+?)/([^/]*)/?$', 'index.php?measure=$matches[1]&type=$matches[2]', 'top');
        
    }
    
    /**
     * @since  1.0
     */
    public function query_vars($query_vars) {
        $query_vars[] = 'type';
        $query_vars[] = 'id';
        return $query_vars;
    }

    /**
     * @since  1.0
     */
    public function rewrite_tag() {
        add_rewrite_tag('%type%',       '([^&]+)');
        add_rewrite_tag('%id%',  '([^&]+)');
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('measure_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('measure_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('measure_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('measure_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('measure_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('measure_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('measure_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('measure_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('measure_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('measure_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('measure_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('measure_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('measure_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('measure_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => get_field('measure_slug', 'option') ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'author' )
            );

            register_post_type( 'measure', $args );
        endif;
    }
    
    /**
     * @since 1.0
     */
    public static function add_measurement() {
        
       
        if(isset($_POST) && !empty($_POST['submit']) && isset( $_POST['add_measurement'])) {
            
            $data = $_POST;
            
            $id = $data['measure_id'];
            
            if(!empty($id)) {
                
                $m_args = array(
                    'ID'           => $id,
                    'post_title'   => $data['nc_title'],
                    'post_type'    => 'measure'
                );
                
                $measure_id = wp_update_post( $m_args );
                
                self::save_meta($measure_id, $data);
                
                do_action('course_'.$data['nc_type'].'_created', $data['parent_id']);

            } else {

                $m_args = array(
                    'post_title'   => $data['nc_title'],
                    'post_type'    => 'measure',
                    'post_status'  => 'publish',
                    'post_parent'  => $data['parent_id'],
                );
                
                $measure_id = wp_insert_post( $m_args );
                
                self::save_meta($measure_id, $data);

                # Dulicating the action plan to create the result set
                if( $data['nc_type'] == 'plan') {
                    
                    # Unsetting the required variables
                    unset($data['nc_type']);
                    unset($data['nc_tag']);

                    # Putting up new values to create results
                    $data['nc_type'] = 'result';
                    $data['nc_tag']  = 'Results';

                    # Creating Result Post
                    $r_args = array(
                        'post_title'   => $data['nc_title'],
                        'post_type'    => 'measure',
                        'post_status'  => 'publish',
                        'post_parent'  => $data['parent_id'],
                    );
                    
                    $result_id = wp_insert_post( $r_args );

                    // Action Plan Relation
                    update_post_meta( $result_id, 'related_action_plan', $measure_id );
                    
                    self::save_meta($result_id, $data);

                    do_action('course_'.$data['nc_type'].'_created', get_the_ID());
                }

                # Relation with results
                update_post_meta( $measure_id, 'related_results', $result_id );

                do_action('course_'.$data['nc_type'].'_created',get_the_ID());
                
            }

            // Redirecting to the modules or Self page base on button click
            if($data['submit'] === 'save') {
                wp_safe_redirect( $data['_wp_http_referer'].'/?msg=success' );
                exit();
            } else {
                wp_redirect(get_the_permalink($data['parent_id']).'modules/');
                exit();
            }
        }
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {

        // Adding Action Plan type
        update_post_meta( $id, 'measure_type', $data['nc_type'] );

        $measures = $data['measures'];

        // Removing Existing Meta If Any
        $total_ms = get_post_meta( $id, 'measurement', true );
        if($total_ms) {
            for ($i=0; $i<$total_ms; $i++) { 
                delete_post_meta( $id,  'measurement_' . $i . '_measure');
                delete_post_meta( $id,  '_measurement_' . $i . '_measure');
                delete_post_meta( $id,  'measurement_' . $i . '_measure_type');
                delete_post_meta( $id,  '_measurement_' . $i . '_measure_type');
            }
        }

        // Adding New Measurements
        foreach ($measures as $key => $measure) {
            update_post_meta( $id, 'measurement_' . $key . '_measure', $measure['name'] );
            update_post_meta( $id, '_measurement_' . $key . '_measure', 'field_56cb1f8a8deff' );
            update_post_meta( $id, 'measurement_' . $key . '_measure_type', $measure['type'] );
            update_post_meta( $id, '_measurement_' . $key . '_measure_type', 'field_56cb1f9c8df00' );
        }

        update_post_meta( $id, 'measurement', count($measures));
        update_post_meta( $id, '_measurement', 'field_56cb1f7c8defe');

        // Adding Matrics
        update_post_meta( $id, 'matrics', $data['matrics_values']);
        update_post_meta( $id, 'a_matrics', $data['a_matrics_values']);

        // Adding Business Impact Units
        update_post_meta( $id, 'revenue', $data['revenue']);
        update_post_meta( $id, 'productivity', $data['productivity']);
        update_post_meta( $id, 'cost_saving', $data['cost_saving']);
        update_post_meta( $id, 'customer_s', $data['customer_s']);
        update_post_meta( $id, 'innovation', $data['innovation']);
        update_post_meta( $id, 'risk_m', $data['risk_m']);
        update_post_meta( $id, 'attitudinal', $data['attitudinal']);

        //Saving General Meta
        Modules::save_meta($id, $data);
    }
}

new Measurement();

function get_results_impact($participants,$result,$group_id = NULL) {
    $return     = array('users'=>0,'revenue'=>0,'costsaving'=>0,'productivity'=>0,'customersatisfaction'=>0,'innovation'=>0,'riskmanage'=>0);     
    if($participants) {
        foreach ($participants as $participant) {
            if($participant > 0 && $result > 0) {            
                $submission_id = get_user_meta($participant, 'submission_result_' . $result, true);
                if(get_post($submission_id)) {
                    $return['users']++;
                    $revenue_array  = explode(',',get_post_meta($submission_id, 'i_revenue', true));
                    $revenue = '';
                    foreach($revenue_array as $revenue_a){
                        $revenue .= $revenue_a;
                    }
                    
                    $costsaving_array  = explode(',',get_post_meta( $submission_id, 'i_costsaving', true));
                    $costsaving = '';
                    foreach($costsaving_array as $costsaving_a){
                        $costsaving .= $costsaving_a;
                    }
                    $productivity_array  = explode(',',get_post_meta( $submission_id, 'i_productivity', true));
                    $productivity = '';
                    foreach($productivity_array as $productivity_a){
                        $productivity .= $productivity_a;
                    }
                    
                    $return['revenue']              += (int)$revenue;
                    $return['costsaving']           += (int)$costsaving;
                    $return['productivity']         += (int)$productivity;
                    $return['customersatisfaction'] += get_post_meta( $submission_id, 'i_customersatisfaction', true);
                    $return['innovation']           += get_post_meta( $submission_id, 'i_innovation', true);
                    $return['riskmanage']           += get_post_meta( $submission_id, 'i_riskmanage', true);
                }
            }
        }
    }
    return $return;
}