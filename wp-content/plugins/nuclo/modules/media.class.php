<?php

/**
 * @package  Media
 * @author   Viral Sonawala
 * @version  1.0
 * @access   public
 */
class Media {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
        add_action( 'pre_get_posts', array($this, 'filter_query'), 1, 1);
    }
    
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('media_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('media_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('media_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('media_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('media_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('media_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('media_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('media_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('media_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('media_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('media_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('media_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('media_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('media_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'media' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'editor', 'author' )
            );

            register_post_type( 'media', $args );
        endif;
    }
    
     /**
     * @since 1.0
     */
    public static function save_post($id, $data) {
        
        if(!empty($id)) {
            $my_program = array(
                'ID'           => $id,
                'post_title'   => $data['nc_title'],
                'post_content' => $data['nc_desc'],
                'post_type'    => 'media'
            );
			
            $program_id = wp_update_post( $my_program );
            $data['course_id'] = get_the_ID();
            self::save_meta($program_id, $data);

        } else {

            $my_program = array(
                'post_title'   => $data['nc_title'],
                'post_type'    => 'media',
                'post_status'  => 'publish',
                'post_content' => $data['nc_desc'],
                'post_parent'  => get_the_ID()
            );
            
            $data['course_id'] = get_the_ID();
            
            $program_id = wp_insert_post( $my_program );
            
            self::save_meta($program_id, $data);
            
        }
        
        return $program_id;
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {
		
        update_post_meta($id,'video_id',$data['video_id']); 
        update_post_meta($id,'course_id',$data['course_id']); 
        //update_post_meta($id,'embedded_code',$data['embedded_code']); 
        Modules::save_meta($id, $data);
    }
    
    public function filter_query($query)
    {
        global $wpdb;
        
        if (is_post_type_archive('media') && $query->query_vars['post_type'] == 'media')
        {
            $query->set('posts_per_page', 16);
            $query->set('orderby', "post_title");
            $query->set('order', "ASC");
        }
        
        if (is_post_type_archive('media') && $query->query_vars['post_type'] == 'media' && !current_user_can('hq_admin') ) {

            
            if(current_user_can('franchisee')) {
                $programs = get_post_meta(get_current_user_franchisee_id() ,'courses',true);
            } else if ( current_user_can('participant')) {
                $programs = Group::get_programs( 'user', get_current_user_id());
            }
            
            if (get_current_user_role()=='author') {
                
                $franchisee_id = get_current_user_franchisee_id();
                $programs = array();
                global $wpdb;
                $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");

                global $current_user;
                if ($results) {
                    foreach ($results as $k => $v) {
                        $g_id = $v->ID;
                        $facilitator = get_post_meta($g_id,'facilitators');
                        if(in_array($current_user->ID, $facilitator))
                        {
                            $posts = get_post_meta($g_id, 'posts', true);
                            foreach ($posts as $post) {
                                if (!in_array($post, $programs)) {
                                    array_push($programs, $post);
                                }
                            }
                        }
                    }
                }
            }
            
            $medias = array();
            if(!empty($programs)) {
                
                $programs_id = implode(',', $programs);
                
                //$results = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_parent IN ($programs_id) AND post_type='media'" );
                
                $results = $wpdb->get_results( "SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'course_id' AND wpm.meta_value IN ($programs_id) AND wp.post_type='media'" );
                
                if($results)
                {
                    foreach($results as $k=>$v)
                    {
                        $medias[] = $v->ID;
                    }
                }
            }
            
            if(empty($medias)) {
                $medias = array('0');
            }
            $query->set( 'post__in', $medias );

        } 
    }
    
    public function get_videothumbnail($post_id)
    {
        $postmeta = get_post_meta($post_id);
        $url = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$postmeta['video_id'][0].'&video_fields=name,thumbnailURL,length&token=Cr2xIgIXRLaT5o42cj5WvAzJJ7R5ga2Hgq3v3cbyeNPgQOKuumuiXA..&_ga=1.97226207.1546096884.1461317617';

        $apidata = file_get_contents($url);
        $jsonData = json_decode($apidata); 
        
        return $jsonData->thumbnailURL;
    }
    
    public function get_video_id($post_id)
    {
        $postmeta = get_post_meta($post_id);
        
        $video_id = $postmeta['video_id'][0];
        
        return $video_id;
    }
	
	public function get_media_by_same_bundle($media_id)
	{
		$bundle_id = wp_get_post_parent_id( $post_ID );
		$id_array = array();
		global $wpdb;
		$results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp WHERE wp.post_parent = $bundle_id AND wp.post_type='media'");
		//echo "<pre>";print_r($results);//exit;
		if ($results) {
			foreach ($results as $k => $v) {
				$media_id = $v->ID;
				$video_id = get_post_meta($media_id,'video_id',true);
				$id_array[] = $video_id;
			}
		}
		//echo "<pre>";print_r($id_array);exit;
		return $id_array;
	}
          
}

new Media();