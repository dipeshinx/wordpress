<?php

/**
 * @package  Submission
 * @author   Dipak Pusti <dipak.pusti@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Submission {

	/**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array( $this, 'register_post_type') );
        add_action( 'init', array( $this, 'save_submission') );
    }

    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('submission_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('submission_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('submission_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('submission_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('submission_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('submission_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('submission_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('submission_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('submission_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('submission_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('submission_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('submission_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('submission_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('submission_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => get_field('submission_slug', 'option') ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'author' )
            );

            register_post_type( 'submission', $args );
        endif;
    }

     /**
     * @since 1.0
     */
    public static function save_submission() {

        if(isset($_POST) && !empty($_POST['submit'])) {

            $user_id = get_current_user_id();
            $user = get_userdata( $user_id );

            if( isset( $_POST['nuclo_create_submission']) ) {
            
                $data = $_POST;
                $type_text = ($data['measure_type'] == 'result')?'Results':'';
                $data['title'] = 'Action Plan '.$type_text.' by '.$user->user_login;
              
                
                $s_args = array(
                    'post_title'   	=> $data['title'],
                    'post_type'    	=> 'submission',
                    'post_status'  	=> 'publish',
                    'post_author'	=> $user_id,
                    'post_parent'       => $data['measure_id']
                );
                
                $submission_id = wp_insert_post( $s_args );

                if(!empty($submission_id)) {
                
    	            self::save_meta($submission_id, $data);

    	            // Updatiing Meta regarding participant's completion status
    	            update_user_meta( $user_id, 'module_result_'.$data['measure_id'], 1 );

                    // Updating meta for storing Action plan and submitted action plan/result
                    if( $data['measure_type'] == 'plan') {
                        update_user_meta( $user_id, 'submission_plan_' . $data['measure_id'], $submission_id );                        
                        $data['html']       = self::action_plan_html($data); 
                        $data['file_name']  = 'Action plan.pdf';                         
                        $data['user_id']    = $user_id;
                        $data['type']       = 'Action Plan';                        
                        do_action('result_supervisor_email',$data); 
                    } else if( $data['measure_type'] == 'result') {
                        update_user_meta( $user_id, 'submission_result_' . $data['measure_id'], $submission_id );                        
                        if(!empty(get_user_meta($user_id,'supervisor_mail',true))){                            
                            $data['html']       = self::action_plan_result_html($data);  
                            $data['file_name']  = 'Action plan Result.pdf';                         
                            $data['user_id']    = $user_id;
                            $data['type']       = 'Action Plan Result';                              
                            do_action('result_supervisor_email',$data);                           
                        }                        
                    }
                    
                    $url    = strtok($data['_wp_http_referer'], '?');                    
                    // Redirecting to the modules or Self page base on button click
    	            wp_safe_redirect( $url.'/?msg=success' );
                   
    	            exit();
    	        }

            } else if( isset( $_POST['nuclo_update_submission']) ) {

                $data = $_POST;
                $submission_id = $data['submit_id'];
                
                if(!empty($submission_id)) {
                
                    self::save_meta($submission_id, $data);
                    
                    $url    = strtok($data['_wp_http_referer'], '?');
                    // Redirecting to the modules or Self page base on button click
                    wp_safe_redirect( $url.'/?msg=success' );
                   
                    exit();
                }

            }
        }
    }
    
    public static function action_plan_html($data){      
        
        $q1 = get_post_meta( $data['measure_id'], 'measurement_0_measure', true );
        $q2 = get_post_meta( $data['measure_id'], 'measurement_1_measure', true );
        $q3 = get_post_meta( $data['measure_id'], 'measurement_2_measure', true );
        $q4 = get_post_meta( $data['measure_id'], 'measurement_3_measure', true );
        $q5 = get_post_meta( $data['measure_id'], 'measurement_4_measure', true );
        $q6 = get_post_meta( $data['measure_id'], 'measurement_5_measure', true );

        $rev = get_post_meta( $data['measure_id'], 'revenue', true);
        $pro = get_post_meta( $data['measure_id'], 'productivity', true);
        $cos = get_post_meta( $data['measure_id'], 'cost_saving', true);
        $cus = get_post_meta( $data['measure_id'], 'customer_s', true);
        $ino = get_post_meta( $data['measure_id'], 'innovation', true);
        $ris = get_post_meta( $data['measure_id'], 'risk_m', true);

        $crestcom_logo_img  = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/crestcom_logo_white_email.png"));
        $vid_email_nb       = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/vid_email_nb.png"));
        $icon01             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon01.png"));
        $icon02             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon02.png"));
        $icon03             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon03.png"));
        $icon04             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon04.png"));
        
        $html  = '<html>';             
        $html .= '<body>';        
        $html .= '<table border="1" cellspacing="0" cellpadding="7">';       
        $html .= '<tr>';
        $html .= '<th style="font-size:12px;width:55%;text-align: center;">Action Plan</th>';
        $html .= '<th style="font-size:12px;width:45%;text-align: center;">Planned</th>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important; text-align: left;width:55%;">'.$q1.'</th>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:45%;">'.$data['nc_bestidea'].'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important; text-align: left;width:55%;">'.$q2.'</th>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:45%;">';
        $html .= '1. '.$data['nc_plan_1'].'<br/>';
        $html .= '2. '.$data['nc_plan_2'].'<br/>';
        $html .= '3. '.$data['nc_plan_3'].'<br/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important; text-align: left;width:55%;">'.$q3.'</th>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:45%; ">';
        $html .= 'Metric : '.$data['att_matrics'].'<br/>';
        $html .= 'Text: '.$data['nc_a_text'].'<br/>';
        $html .= 'Percentage: '.$data['nc_am'].'%<br/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th colspan="2"  style="text-align: left;width:100%;">'.$q4.'</th>';
        $html .= '</tr>';                
        $html .= '<tr>';
        $html .= '<td colspan="2" style="padding:0px;width:100%;">';
        $html .= '<table  border="0" cellspacing="0" cellpadding="7" style="width:100%">';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:55%;">1. Revenue ('. $rev .')</th>';
        $html .= '<td style="text-align: left;width:45%;">'.number_format($data['nc_ri'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:55%;">2. Productivity ('. $pro .')</th>';
        $html .= '<td style="text-align: left;width:45%;">'.number_format($data['nc_pi'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:55%;">3. Cost Saving ('. $cos .')</th>';
        $html .= '<td style="text-align: left;width:45%;">'.number_format($data['nc_csi'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:55%;">4. Customer Satisfaction ('. $cus .')</th>';
        $html .= '<td style="text-align: left;width:45%;">'.number_format($data['nc_cs'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:55%;">5. Innovation ('. $ino .')</th>';
        $html .= '<td style="text-align: left;width:45%;">'.number_format($data['nc_ino'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:55%;">6. Risk Mitigation ('. $ris .')</th>';
        $html .= '<td style="text-align: left;width:45%;">'.number_format($data['nc_rm'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important;text-align: left;width:55%;">'.$q5.'</th>';
        $html .= '<td style="vertical-align: top !important;text-align: left;width:45%;">';
        $html .= 'Measure : '.$data['nc_parent_matrics'].'<br/>';
        $html .= 'Metric : '.$data['nc_matrics'].'<br/>';
        $html .= 'Text : '.$data['nc_m_text'];
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important;text-align: left;width:55%;">'.$q6.'</th>';
        $html .= '<td style="vertical-align: top !important;text-align: left;width:45%;">'.$data['nc_date'].'</td>';
        $html .= '</tr>';
        $html .= '</table>';  
        $html .= '<table style="width:100%;height:60px;background-color:#212240;">';
        $html .= '<tr>';
        $html .= '<td style="width:33.33%;height:100%;padding:40px;vertical-align: middle !important;">';
        $html .= '<img src="data:image/png;base64,'.$crestcom_logo_img.'" style="width:300px;height:47px;margin-top:35px;">';
        $html .= '</td>';   
        $html .= '<td style="width:33.33%;height:100%;padding:20px;text-align:center;vertical-align: middle !important;">';
        $html .= '<img src="data:image/png;base64,'.$vid_email_nb.'" style="width:134px;height:60px;">';
        $html .= '</td>';    
        $html .= '<td style="width:33.33%;height:100%;padding:20px;text-align:right;vertical-align: middle !important;">';
        $html .= '<a href="https://www.facebook.com/pages/Crestcom-Leadership/1461912364125980" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon01.'" style="width:20px;height:20px;">';
        $html .= '</a>';    
        $html .= '<a href="https://www.linkedin.com/company/crestcom-leadership" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon02.'" style="width:20px;height:20px;;">';
        $html .= '</a>';    
        $html .= '<a href="https://twitter.com/crestcomleaders" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon03.'" style="width:20px;height:20px;">';
        $html .= '</a>';    
        $html .= '<a href="https://plus.google.com/117083374140039226855/posts" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon04.'" style="width:20px;height:20px;">';
        $html .= '</a>';    
        $html .= '</td>';    
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</body>';
        $html .= '</html>';
        
        return $html;
    }
    
    public static function action_plan_result_html($data){     
        
        $related_action_plan = get_post_meta( $data['measure_id'], 'related_action_plan', true );
        
        $q1 = get_post_meta( $related_action_plan, 'measurement_0_measure', true );
        $q2 = get_post_meta( $related_action_plan, 'measurement_1_measure', true );
        $q3 = get_post_meta( $related_action_plan, 'measurement_2_measure', true );
        $q4 = get_post_meta( $related_action_plan, 'measurement_3_measure', true );
        $q5 = get_post_meta( $related_action_plan, 'measurement_4_measure', true );
        $q6 = get_post_meta( $related_action_plan, 'measurement_5_measure', true );

        $rev = get_post_meta( $related_action_plan, 'revenue', true);
        $pro = get_post_meta( $related_action_plan, 'productivity', true);
        $cos = get_post_meta( $related_action_plan, 'cost_saving', true);
        $cus = get_post_meta( $related_action_plan, 'customer_s', true);
        $ino = get_post_meta( $related_action_plan, 'innovation', true);
        $ris = get_post_meta( $related_action_plan, 'risk_m', true);
        
        $user_action_plan = get_user_meta(get_current_user_id(), 'submission_plan_' . $related_action_plan, true);
        
        $idea = get_post_meta( $user_action_plan, 'p_action_plan_idea', true );

        $plan_1 = get_post_meta( $user_action_plan, 'p_action_plan_1', true );
        $plan_2 = get_post_meta( $user_action_plan, 'p_action_plan_2', true );
        $plan_3 = get_post_meta( $user_action_plan, 'p_action_plan_3', true );

        $att_mtrcs = get_post_meta( $user_action_plan, 'p_action_plan_att_matrics', true );
        $att_mtrcs_txt = get_post_meta( $user_action_plan, 'p_action_plan_att_text', true );

        $parent_mtrcs = get_post_meta( $user_action_plan, 'p_action_plan_parent_matrics', true );
        $mtrcs = get_post_meta( $user_action_plan, 'p_action_plan_matrics', true );
        $mtrcs_txt = get_post_meta( $user_action_plan, 'p_action_plan_matrics_text', true );

        $p_date = get_post_meta( $user_action_plan, 'p_action_plan_date', true );

        $revenue_array  = explode(',',get_post_meta( $user_action_plan, 'i_revenue', true));
        $revenue = '';
        foreach($revenue_array as $revenue_a){
            $revenue .= $revenue_a;
        }    
        $i_revenue = (int)$revenue;    
        $productivity_array  = explode(',',get_post_meta( $user_action_plan, 'i_productivity', true));
        $productivity = '';
        foreach($productivity_array as $productivity_a){
            $productivity .= $productivity_a;
        }
        $i_productivity = (int)$productivity;   
        $costsaving_array  = explode(',',get_post_meta( $user_action_plan, 'i_costsaving', true));
        $costsaving = '';
        foreach($costsaving_array as $costsaving_a){
            $costsaving .= $costsaving_a;
        }
        $i_costsaving = (int)$costsaving;   
        $i_innovation = get_post_meta( $user_action_plan, 'i_innovation', true);
        $i_customersatisfaction = get_post_meta( $user_action_plan, 'i_customersatisfaction', true);
        $i_riskmanage = get_post_meta( $user_action_plan, 'i_riskmanage', true);
        $i_attitudinal = get_post_meta( $user_action_plan, 'i_attitudinal', true);
        
        $revenue_array  = explode(',',$data['nc_ri']);
        $revenue = '';
        foreach($revenue_array as $revenue_a){
            $revenue .= $revenue_a;
        }    
        $data['nc_ri'] = (int)$revenue; 
        
        $productivity_array  = explode(',',$data['nc_pi']);
        $productivity = '';
        foreach($productivity_array as $productivity_a){
            $productivity .= $productivity_a;
        }
        $data['nc_pi'] = (int)$productivity;   
        
        $costsaving_array  = explode(',',$data['nc_csi']);
        $costsaving = '';
        foreach($costsaving_array as $costsaving_a){
            $costsaving .= $costsaving_a;
        }
        $data['nc_csi'] = (int)$costsaving;   
        
        $crestcom_logo_img  = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/crestcom_logo_white_email.png"));
        $vid_email_nb       = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/vid_email_nb.png"));
        $icon01             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon01.png"));
        $icon02             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon02.png"));
        $icon03             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon03.png"));
        $icon04             = base64_encode(file_get_contents("http://crestcomsupport.com/crestcom15/Signature_landing/images/general_sig_files/icon04.png"));
        
        $html  = '<html>';             
        $html .= '<body>';        
        $html .= '<table border="1" cellspacing="0" cellpadding="7">';       
        $html .= '<tr>';
        $html .= '<th style="font-size:12px;width:33.33%;text-align: center;">Action Plan</th>';
        $html .= '<th style="font-size:12px;width:33.33%;text-align: center;">Planned</th>';
        $html .= '<th style="font-size:12px;width:33.33%;text-align: center;">Result</th>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important; text-align: left;width:33.33%;">'.$q1.'</th>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:33.33%;">'.$idea.'</td>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:33.33%;">'.$data['nc_bestidea'].'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important; text-align: left;width:33.33%;">'.$q2.'</th>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:33.33%;">';
        $html .= '1. '.$plan_1.'<br/>';
        $html .= '2. '.$plan_2.'<br/>';
        $html .= '3. '.$plan_3.'<br/>';
        $html .= '</td>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:33.33%;">';
        $html .= '1. '.$data['nc_plan_1'].'<br/>';
        $html .= '2. '.$data['nc_plan_2'].'<br/>';
        $html .= '3. '.$data['nc_plan_3'].'<br/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important; text-align: left;width:33.33%;">'.$q3.'</th>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:33.33%;">';
        $html .= 'Metric : '.$att_mtrcs.'<br/>';
        $html .= 'Text: '.$att_mtrcs_txt.'<br/>';
        $html .= 'Percentage: '.$i_attitudinal.'%<br/>';
        $html .= '</td>';
        $html .= '<td style="vertical-align: top !important; text-align: left;width:33.33%; ">';
        $html .= 'Metric : '.$data['att_matrics'].'<br/>';
        $html .= 'Text: '.$data['nc_a_text'].'<br/>';
        $html .= 'Percentage: '.$data['nc_am'].'%<br/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th colspan="3"  style="text-align: left;width:100%;">'.$q4.'</th>';
        $html .= '</tr>';                
        $html .= '<tr>';
        $html .= '<td colspan="3" style="padding:0px;width:100%;">';
        $html .= '<table  border="0" cellspacing="0" cellpadding="7" style="width:100%">';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:33.33%;">1. Revenue ('. $rev .')</th>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($i_revenue,0,'',', ').'</td>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($data['nc_ri'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:33.33%;">2. Productivity ('. $pro .')</th>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($i_productivity,0,'',', ').'</td>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($data['nc_pi'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:33.33%;">3. Cost Saving ('. $cos .')</th>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($i_costsaving,0,'',', ').'</td>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($data['nc_csi'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:33.33%;">4. Customer Satisfaction ('. $cus .')</th>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($i_customersatisfaction,0,'',', ').'</td>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($data['nc_cs'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:33.33%;">5. Innovation ('. $ino .')</th>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($i_innovation,0,'',', ').'</td>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($data['nc_ino'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="text-align: left;width:33.33%;">6. Risk Mitigation ('. $ris .')</th>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($i_riskmanage,0,'',', ').'</td>';
        $html .= '<td style="text-align: left;width:33.33%;">'.number_format($data['nc_rm'],0,'',', ').'</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important;text-align: left;width:33.33%;">'.$q5.'</th>';
        $html .= '<td style="vertical-align: top !important;text-align: left;width:33.33%;">';
        $html .= 'Measure : '.$parent_mtrcs.'<br/>';
        $html .= 'Metric : '.$mtrcs.'<br/>';
        $html .= 'Text : '.$mtrcs_txt;
        $html .= '</td>';
        $html .= '<td style="vertical-align: top !important;text-align: left;width:33.33%;">';
        $html .= 'Measure : '.$data['nc_parent_matrics'].'<br/>';
        $html .= 'Metric : '.$data['nc_matrics'].'<br/>';
        $html .= 'Text : '.$data['nc_m_text'];
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<th style="vertical-align: top !important;text-align: left;width:33.33%;">'.$q6.'</th>';
        $html .= '<td style="vertical-align: top !important;text-align: left;width:33.33%;">'.date('m/d/Y',$p_date).'</td>';
        $html .= '<td style="vertical-align: top !important;text-align: left;width:33.33%;">'.$data['nc_date'].'</td>';
        $html .= '</tr>';
        $html .= '</table>';  
        $html .= '<table style="width:100%;height:60px;background-color:#212240;">';
        $html .= '<tr>';
        $html .= '<td style="width:33.33%;height:100%;padding:40px;vertical-align: middle !important;">';
        $html .= '<img src="data:image/png;base64,'.$crestcom_logo_img.'" style="width:300px;height:47px;margin-top:35px;">';
        $html .= '</td>';   
        $html .= '<td style="width:33.33%;height:100%;padding:20px;text-align:center;vertical-align: middle !important;">';
        $html .= '<img src="data:image/png;base64,'.$vid_email_nb.'" style="width:134px;height:60px;">';
        $html .= '</td>';    
        $html .= '<td style="width:33.33%;height:100%;padding:20px;text-align:right;vertical-align: middle !important;">';
        $html .= '<a href="https://www.facebook.com/pages/Crestcom-Leadership/1461912364125980" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon01.'" style="width:20px;height:20px;">';
        $html .= '</a>';    
        $html .= '<a href="https://www.linkedin.com/company/crestcom-leadership" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon02.'" style="width:20px;height:20px;;">';
        $html .= '</a>';    
        $html .= '<a href="https://twitter.com/crestcomleaders" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon03.'" style="width:20px;height:20px;">';
        $html .= '</a>';    
        $html .= '<a href="https://plus.google.com/117083374140039226855/posts" target="_blank">';
        $html .= '<img src="data:image/png;base64,'.$icon04.'" style="width:20px;height:20px;">';
        $html .= '</a>';    
        $html .= '</td>';    
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</body>';
        $html .= '</html>';
        
        return $html;
    }

    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {
      
        // Adding Action Plan type
        update_post_meta($id, 'measure_type', $data['measure_type']);

        // Adding submission details
        update_post_meta($id, 'p_action_plan_idea', $data['nc_bestidea']);
        update_post_meta($id, 'p_action_plan_1', $data['nc_plan_1']);
        update_post_meta($id, 'p_action_plan_2', $data['nc_plan_2']);
        update_post_meta($id, 'p_action_plan_3', $data['nc_plan_3']);
        
        update_post_meta($id, 'p_action_plan_att_matrics', $data['att_matrics']);
        update_post_meta($id, 'p_action_plan_att_text', $data['nc_a_text']);

        update_post_meta($id, 'p_action_plan_parent_matrics', $data['nc_parent_matrics']);
        update_post_meta($id, 'p_action_plan_matrics', $data['nc_matrics']);
        update_post_meta($id, 'p_action_plan_matrics_text', $data['nc_m_text']);

        update_post_meta($id, 'p_action_plan_date', strtotime($data['nc_date']));

        // Adding Business Impact Values
        update_post_meta($id, 'i_revenue', $data['nc_ri']);
        update_post_meta($id, 'i_productivity', $data['nc_pi']);
        update_post_meta($id, 'i_costsaving', $data['nc_csi']);
        update_post_meta($id, 'i_innovation', $data['nc_ino']);
        update_post_meta($id, 'i_customersatisfaction', $data['nc_cs']);
        update_post_meta($id, 'i_riskmanage', $data['nc_rm']);
        update_post_meta($id, 'i_attitudinal', $data['nc_am']);

        $user_ID = get_current_user_id();
        global $current_user, $post;
        
        $get_module_name_id = wp_get_post_parent_id($id);
        $module_name = get_the_title($get_module_name_id);
        
        if ($data['measure_type'] == 'result') {
            $post_title = 'Updated result by ' . $current_user->data->user_email.' fot this '.$module_name;
            $notification_type = 2;
        } else {
            $post_title = 'Action Plan by ' . $current_user->data->user_email.' fot this '.$module_name;
            $notification_type = 1;
        }
        Submission::add_participant_notification($notification_type, $user_ID, $post_title, $module_name);
    }
    
    public static function add_participant_notification($type, $user_id, $post_title, $module_name) {
        
        $check_post_exists = post_exists( trim($post_title) );
        $post_type = '';
        if($check_post_exists != 0)
        {
            $post_data = get_post($check_post_exists);
            
            $post_type = $post_data->post_type;
            
        }
        if($post_type != 'notification')
        {
            $client = array(
                'post_title' => $post_title,
                'post_type' => 'notification',
                'post_status' => 'publish',
                'post_author' => get_current_user_id()
            );

            $client_id = wp_insert_post($client);

            if (!empty($client_id)) {
                $franchiseId = get_post_meta(get_post_meta(get_user_meta($user_id, 'client','single'),'franchisee','single'),'franchise_owner','single');

                # Adding relative notification and post meta
                update_post_meta($client_id, 'notification_type', $type);
                update_post_meta($client_id, 'user_role', 'participant');
                update_post_meta($client_id, 'module', $module_name);
                update_post_meta($client_id, 'user_id', $user_id);
                update_post_meta($client_id, 'franchisee_id', $franchiseId);
                update_post_meta($client_id, 'read', 0);
            }
        }
    }

}

new Submission();

function get_submission_by_type($type,$measure_id = '',$user_id = ''){
    $s_args = array(
        'post_type'         => 'submission',
        'posts_per_page'    => -1,
        'meta_key'          => 'measure_type',
        'meta_value'        => $type,
        'post_status'       => 'publish',       
    );
    
    if(!empty($measure_id)){
        $s_args['post_parent']  = $measure_id;
    }
    
    if(!empty($user_id)){
        $s_args['author']  = $user_id;
    }
    
    $submissions = get_posts( $s_args , ARRAY_A);
    
    return $submissions;
}