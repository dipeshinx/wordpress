<?php

/**
 * @package  Survey
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Survey {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
        add_action( 'add_meta_boxes', array($this,'add_meta_boxes') );
        add_action( 'save_post', array($this,'save_post_hook'),1,1);
        add_action( 'pre_get_posts', array($this, 'filter_query'), 1, 1);
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('survey_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('survey_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('survey_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('survey_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('survey_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('survey_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('survey_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('survey_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('survey_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('survey_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('survey_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('survey_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('survey_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('survey_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'survey' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'editor', 'author' )
            );

            register_post_type( 'survey', $args );
        endif;
    }
    
    public function filter_query($query) {
        
        if (isset($query->query_vars['post_type'])) {
            if ($query->query_vars['post_type'] == 'survey' && !is_admin() && is_post_type_archive('survey')) {
                
                $survey_id_array = array();
                global $wpdb;
                
                $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp WHERE wp.post_type='survey'");
                
                foreach($results as $k=>$v){ 
                    $survey_id = $v->ID;
                    $course_id = wp_get_post_parent_id($survey_id);
                    if (get_current_user_role()=='editor') {
                        $course_id_array = Programs::get_all_courses_for_survey('franchisee');
                        if(in_array($course_id, $course_id_array)) {
                            $survey_id_array[] =  $survey_id;
                        }
                    }
                    if(get_current_user_role()=='author')
                    {
                        $course_id_array = Programs::get_all_courses_for_survey('author');
                        if(in_array($course_id, $course_id_array)) {
                            $survey_id_array[] =  $survey_id;
                        }
                    }
                }
                
                if (empty($survey_id_array)) {
                        $survey_id_array = array('0');
                    }
                $query->set('post__in', $survey_id_array);

            }
        }
    }
    
     /**
     * @since 1.0.1
     */
    public static function save_post($id, $data) {
        if(!empty($id)) {
            $my_program = array(
                'ID'           => $id,
                'post_title'   => $data['nc_title'],
                'post_content' => $data['nc_desc'],
                'post_type'    => 'survey'
            );
            $program_id = wp_update_post( $my_program );
            
            $id = get_post_meta($program_id, 'survey_id',true);
            
            if(!empty($id) && !empty($data['nc_title'])) {
                WPSQT::update_title($id,$data['nc_title']);
            }
            
            self::save_meta($program_id, $data);

        } else {

            $my_program = array(
                'post_title'   => $data['nc_title'],
                'post_type'    => 'survey',
                'post_status'  => 'publish',
                'post_content' => $data['nc_desc'],
                'post_parent'  => get_the_ID()
            );

            $program_id = wp_insert_post( $my_program );
            
            self::save_meta($program_id, $data);
            
        }
        
        return $program_id;
    }
    
    /**
     * @since 1.0
     */
    public static function save_meta($id, $data) {
              
        Modules::save_meta($id, $data);
    }
    
    /**
     * @since 1.0
     */
    public function add_meta_boxes() {

        add_meta_box('nuclo_surveys_list', 'Surveys List', array($this,'surveys_list'), 'survey', 'normal', 'default');

    }
    
    /**
     * @since 1.0
     */
    public function surveys_list() {
        
        global $post;
        
        $surveys = $this->get_surveys();
        
        $survey_id = get_post_meta($post->ID, 'survey_id', true);

        echo '<select name="survey" style="width:100%;"><option value="0">-- Select Survey --</option>';
        foreach ($surveys as $survey) {
            
            $is_selected = ( $survey_id == $survey->id ) ? 'selected="selected"' : '';
 
            echo '<option value="'.$survey->id.'" '.$is_selected.'>'.$survey->name.'</option>';
        }
        echo '</select>';

    }
    
    /**
     * @since 1.0
     */
    public function get_surveys($type='survey',$results=false) {
       return $this->survey_query($type,$results);
    }
    
    /**
     * @since 1.0
     */
    private function survey_query($type,$results) {

        global $wpdb;
        
        if($results == true) {
            $sql = "SELECT wpsq.*,COUNT(wpar.id) as results 
                FROM `" . WPSQT_TABLE_QUIZ_SURVEYS . "` as wpsq
                LEFT JOIN `" . WPSQT_TABLE_RESULTS . "` as wpar ON wpar.item_id = wpsq.id
                WHERE wpsq.type = '$type'
                GROUP BY wpsq.id
                ORDER BY wpsq.id DESC";
        } else {
            $sql = "SELECT * FROM `" . WPSQT_TABLE_QUIZ_SURVEYS . "` WHERE type = '$type' GROUP BY id ORDER BY id DESC";
        }
        
        $data = $wpdb->get_results($sql);
        
        return $data;
    }
    
    /**
     * @since 1.0
     */
    public function save_post_hook($post_id) {
        if(isset($_POST['survey']) && !empty($_POST['survey'])) {
            update_post_meta($post_id, 'survey_id', $_POST['survey']);
        }
    }
    
    
   
}

new Survey();

function get_survey_users_count($users=array(),$survey_id=0) {
    global $wpdb;
    $user_emails = array();
    $user_count = 0;
    $item_id = get_post_meta($survey_id,'survey_id',true);
    
    if($users) {
        foreach($users as $user) {
            $user_info     = get_userdata($user);
            if($user_info) {
                $user_emails[] = "'".$user_info->user_email."'";
            }
        }
        $user_count = $wpdb->get_var( "SELECT COUNT(id) FROM ".WPSQT_TABLE_RESULTS." WHERE person_name IN (".implode(',',$user_emails).") AND item_id = ".$item_id );        
    }
    return $user_count;
}

function get_survey_feedback($users=array(),$survey_id=0,$part=0) {
    global $wpdb;
    $option = array('poor'=>1,'fair'=>2,'good'=>3,'excellent'=>4);
    $item_id = get_post_meta($survey_id,'survey_id',true);
    $user_total = 0;
    if($users) {
        $user_count = 0;
        $user_point = 0;
        foreach($users as $user) {
            $user_info = get_userdata($user);
            if($user_info) {               
                $resultid = $wpdb->get_var( "SELECT id FROM ".WPSQT_TABLE_RESULTS." WHERE person_name = '".$user_info->user_email."' AND item_id = ".$item_id );
                if($resultid) {
                    $survey = new SurveyReport('',$resultid);
                    $result = $survey->single_result();
                    $section = $result['sections'][$part];
                    if (isset($section['questions'])) {
                        $question_point = 0;
                        $question_count = 0;
                        foreach ($section['questions'] as $questionArray) {
                            if($questionArray['type'] == 'Likert') {
                                $questionId = $questionArray['id'];
                                $given = strtolower( $section['answers'][$questionId]['given'] );
                                $point = isset($option[$given])?$option[$given]:0;
                                $question_point += $point; 
                                $question_count++;
                            }
                        }
                        if($question_count > 0)
                            $question_total = $question_point/$question_count;
                        else
                            $question_total = 0;
                    }
                    $user_point += $question_total;
                    $user_count++;
                }
            }
        } 
        if($user_count > 0)
            $user_total = $user_point/$user_count;
        else
            $user_total = 0;
    }
    
    return number_format((float)$user_total, 2, '.', '');
}