<?php

/**
 * @package  Program Action
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class ProgramAction {

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action('init', array($this, 'rewrites_init'), 1, 0);
        add_action('init', array($this, 'rewrite_tag'), 1, 0);
        add_action('program', array($this, 'action'), 10, 0);
        add_action('program', array($this, 'create_online'), 1, 0);
        add_action('wp_ajax_load_weeks', array($this, 'load_weeks'));
        add_action('wp_ajax_save_semester', array($this, 'save_semester'));
        add_action('wp_ajax_save_week', array($this, 'save_week'));
        add_action('wp_ajax_upload_file', array($this, 'upload_file'));
        add_action('wp_ajax_upload_agreement', array($this, 'upload_agreement'));
        add_action('wp_ajax_load_file', array($this, 'load_file'));
        add_action('wp_ajax_file_delete', array($this, 'file_delete'));
        add_action('wp_ajax_agreement_delete', array($this, 'agreement_delete'));        
        add_filter('query_vars', array($this, 'query_vars'));
    }

    /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('^courseware/(.+?)/([^/]*)/([^/]*)/([^/]*)/?$', 'index.php?programs=$matches[1]&action=$matches[2]&module=$matches[3]&modid=$matches[4]', 'top');
        add_rewrite_rule('^courseware/(.+?)/([^/]*)/([^/]*)/?$', 'index.php?programs=$matches[1]&action=$matches[2]&module=$matches[3]', 'top');
        add_rewrite_rule('courseware/(.+?)/page/([0-9]{1,})/?', 'index.php?post_type=programs&action=$matches[1]&paged=$matches[3]', 'top');
    }

    /**
     * @since  1.0
     */
    public function query_vars($query_vars) {
        $query_vars[] = 'module';
        $query_vars[] = 'modid';
        return $query_vars;
    }

    /**
     * @since  1.0
     */
    public function rewrite_tag() {
        add_rewrite_tag('%module%', '([^&]+)');
        add_rewrite_tag('%modid%', '([^&]+)');
    }


    /**
     * @since  1.0
     */
    public function action() {
        $action = get_query_var('action');
        switch ($action) {
            case "content":
                $this->add_content();
                break;
            case "curriculum":
                if (has_term('Online','course_type') || has_term('DHH','course_type')) {
                    $url = get_permalink(get_the_ID()) . 'modules';
                    wp_redirect($url);
                    exit;
                }
                break;
            case "modules":
                $this->add_module();
                break;
            case "settings":
                $this->add_settings();
                break;
            default:
        }
    }

    /**
     * @since  1.0
     */
    private function add_content() {
        $data = $_POST;
        //$error = new Errors;
        if (isset($data) && !empty($data['submit']) && wp_verify_nonce($data['nc_nuclo'], 'nc_nuclo') && current_user_can('create_courseware') && is_integer(get_the_ID())) {
            $my_program = array('ID' => get_the_ID(), 'post_content' => $data['nc_desc']);
            wp_update_post($my_program);
            if ($data['submit'] == 'next') {
                //$error->reset();               
                if (has_term('Online','course_type') || has_term('DHH','course_type')) {
                    $url = get_permalink(get_the_ID()) . 'modules';
                } else {
                    $url = get_permalink(get_the_ID()) . 'curriculum';
                }
                wp_redirect($url);
                exit;
            } else {
                //$error->get_success('content',  __( "Content Successfully save", "nuclo" ));
            }
        } else {
            //$error->get_error('type', __( "Somthing went wrong", "nuclo" ));
        }
    }

    /**
     * @since  1.0
     */
    private function add_settings() {
        $data = $_POST;
        //$error = new Errors;
        if (isset($data) && !empty($data['nc_submit']) && wp_verify_nonce($data['nc_nuclo'], 'nc_nuclo') && current_user_can('create_courseware') && is_integer(get_the_ID())) {
            $my_program = array('ID' => get_the_ID(), 'post_title' => $data['nc_title']);
            $post_id = wp_update_post($my_program);

            if ($post_id) {
                update_field("field_56235b0d2f499", $data['nc_announcement'], $post_id);
                update_field("field_56123a2874144", $data['nc_certificate'], $post_id);
                update_field("field_55c9dae554fee", $data['nc_disabled'], $post_id);
                update_field("field_56c3fc862b354", $data['products'], $post_id);
                update_field("field_572235dd56c19", $data['nc_video'], $post_id);


                $files = $_FILES;

                foreach ($files as $key => $value) {

                    $file_id = $this->upload_agreement($key);

                    if ($file_id) {

                        $data['agreements'][$key]['agreement'] = $file_id;
                    } else {
                        if (!empty($data['agreements'][$key]['agreement'])) {
                            unset($data['agreements'][$key]);
                        }
                    }
                }

                update_field("field_569bec9f32f0c", $data['agreements'], $post_id);
            }
        } else {
            //$error->get_error('type', __( "Somthing went wrong", "nuclo" ));
        }
    }

    /**
     *  @since 1.0
     */
    public function load_weeks() {
        $data = $_POST;
        $semesters = get_field('semesters', $data['program_id']);
        if (isset($semesters[($data['sem_id'] - 1)])) {
            $weeks = $semesters[($data['sem_id'] - 1)]['weeks'];
        }

        if (isset($data['type']) && $data['type'] == 'option') {
            $return .= '<option value="">All Week</option>';
            if (!empty($weeks)) {
                foreach ($weeks as $key => $week) {
                    if ($data['current'] == ($key + 1)) {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }
                    $return .= '<option value="' . ($key + 1) . '" ' . $selected . '>' . $week['week'] . '</option>';
                }
            }
            echo $return;
        } else {
            $return = '<ul class="todo-list small-list">';
            if (!empty($weeks)) {
                foreach ($weeks as $key => $week) {
                    $return .= '<li data-id="' . $key . '">' . $week['week'] . '<input type="hidden" value="' . $key . '" name="weeks[]" /></li>';
                }
            }
            $return .= '</ul>';
            $return .= '<input type="hidden" value="' . $data['sem_id'] . '" name="semester" />';
            wp_nonce_field('nuclo', 'nuclo');
            echo $return;
        }
        exit;
    }

    /**
     *  @since 1.0
     */
    public function save_semester() {
        $data = $_POST;
        if (!empty($data['program_id']) && wp_verify_nonce($data['nuclo'], 'nuclo') && current_user_can('create_courseware')) {
            $this->delete_semester($data['program_id']);
            $this->add_semester($data['program_id'], count($data['semesters']));
        }
        exit;
    }

    /**
     *  @since 1.0
     */
    private function delete_semester($id) {
        $semesters = get_post_meta($id, 'semesters', true);
        for ($i = 0; $i < $semesters; $i++) {
            delete_post_meta($id, 'semesters_' . $i . '_semester');
            delete_post_meta($id, '_semesters_' . $i . '_semester');
        }
    }

    /**
     *  @since 1.0
     */
    private function add_semester($id, $count) {
        for ($i = 0; $i < $count; $i++) {
            update_post_meta($id, 'semesters_' . $i . '_semester', 'Semester ' . ($i + 1));
            update_post_meta($id, '_semesters_' . $i . '_semester', 'field_55cc73165b554');
        }
        update_post_meta($id, 'semesters', $count);
        update_post_meta($id, '_semesters', 'field_55cc73065b553');
    }

    /**
     *  @since 1.0
     */
    public function save_week() {
        $data = $_POST;
        if (!empty($data['program_id']) && wp_verify_nonce($data['nuclo'], 'nuclo') && current_user_can('create_courseware') && !empty($data['semester'])) {
            $this->delete_week($data['program_id'], count($data['weeks']), ($data['semester'] - 1));
            $this->add_week($data['program_id'], count($data['weeks']), ($data['semester'] - 1));
        }
        exit;
    }

    /**
     *  @since 1.0
     */
    private function delete_week($id, $count, $semester) {
        $weeks = get_post_meta($id, 'semesters_' . $semester . '_weeks', true);
        if ($count < $weeks) {
            for ($i = 0; $i < $weeks; $i++) {
                delete_post_meta($id, 'semesters_' . $semester . '_weeks_' . $i . '_week');
                delete_post_meta($id, '_semesters_' . $semester . '_weeks_' . $i . '_week');
            }
        }
    }

    /**
     *  @since 1.0
     */
    private function add_week($id, $count, $semester) {
        for ($i = 0; $i < $count; $i++) {
            update_post_meta($id, 'semesters_' . $semester . '_weeks_' . $i . '_week', 'Week ' . ($i + 1));
            update_post_meta($id, '_semesters_' . $semester . '_weeks_' . $i . '_week', 'field_55cc732e5b556');
        }
        update_post_meta($id, 'semesters_' . $semester . '_weeks', $count);
        update_post_meta($id, '_semesters_' . $semester . '_weeks', 'field_55cc731e5b555');
    }

    /**
     *  @since 1.0
     */
    public function add_module() {
        $data = $_POST;
        $module = get_query_var('module');
        switch ($module) {
            case "content":
                if (isset($data) && !empty($data['submit']) && wp_verify_nonce($data['nuclo'], 'nuclo') && current_user_can('create_courseware') && is_integer(get_the_ID())) {

                    $program_id = get_query_var('modid');

                    if (!empty($program_id)) {
                        $my_program = array(
                            'ID' => $program_id,
                            'post_content' => $data['desc']
                        );

                        $program_id = wp_update_post($my_program);
                        update_post_meta($program_id, 'my_semester', 'Semester ' . $data['semester']);
                        update_post_meta($program_id, '_my_semester', 'field_55ccf6120a3fd');
                        update_post_meta($program_id, 'my_week', 'Week ' . $data['week']);
                        update_post_meta($program_id, '_my_week', 'field_55ccf6200a3fe');
                        $originalDate = $data['date'];
                        $newDate = date("Ymd", strtotime($originalDate));
                        update_post_meta($program_id, 'date', $newDate);
                        update_post_meta($program_id, '_date', 'field_55ccf6270a3ff');
                    } else {

                        $my_program = array(
                            'post_title' => 'Content',
                            'post_type' => 'programs',
                            'post_status' => 'publish',
                            'post_content' => $data['desc'],
                            'post_parent' => get_the_ID()
                        );

                        $program_id = wp_insert_post($my_program);
                        update_post_meta($program_id, 'my_semester', 'Semester ' . $data['semester']);
                        update_post_meta($program_id, '_my_semester', 'field_55ccf6120a3fd');
                        update_post_meta($program_id, 'my_week', 'Week ' . $data['week']);
                        update_post_meta($program_id, '_my_week', 'field_55ccf6200a3fe');
                        $originalDate = $data['date'];
                        $newDate = date("Ymd", strtotime($originalDate));
                        update_post_meta($program_id, 'date', $newDate);
                        update_post_meta($program_id, '_date', 'field_55ccf6270a3ff');
                    }

                    if (!empty($program_id)) {
                        $url = get_permalink(get_the_ID()) . 'modules';
                        wp_redirect($url);
                        exit;
                    } else {
                        //$error->get_success('content',  __( "Content Successfully save", "nuclo" ));
                    }
                }
                break;
            default:
        }
    }

    /**
     * @since 1.0
     */
    public function upload_file() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX && current_user_can('create_courseware')))
            return;

        $allowed = array('pdf', 'doc', 'docx');

        $upfilename = $_FILES['files']['name'];

        $post_id = $_POST['post_id'];

        $extallowed = pathinfo($upfilename, PATHINFO_EXTENSION);

        if (in_array($extallowed, $allowed) && $_FILES['files']['size'] <= '5000000') {

            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            $file_id = media_handle_upload('files', 0);

            $data = array();

            if ($file_id) :

                $total = get_post_meta($post_id, 'important_documents', true);

                $total = empty($total) ? 0 : $total;

                update_post_meta($post_id, 'important_documents_' . $total . '_document', $file_id);
                update_post_meta($post_id, '_important_documents_' . $total . '_document', 'field_56235b382f49b');
                update_post_meta($post_id, 'important_documents', ($total + 1));
                update_post_meta($post_id, '_important_documents', 'field_56235b202f49a');

                $filename_only = basename(get_attached_file($file_id));

                $data['file'] = '<a href="#" data-download="' . home_url() . '?media_dl=' . $file_id . '" data-toggle="modal" data-target="#load-file" data-url="' . wp_get_attachment_url($file_id) . '" data-file="' . $file_id . '" data-fname="' . $filename_only . '">' . $filename_only . '</a>';

                echo json_encode($data);

            endif;
        } else {

            $data['error'] = 'Invalid file!';

            echo json_encode($data);
        }
        exit;
    }

    /**
     * @since 1.0
     */
    public function upload_agreement($file) {

        $allowed = array('pdf', 'doc', 'docx');

        $upfilename = $_FILES[$file]['name'];

        $extallowed = pathinfo($upfilename, PATHINFO_EXTENSION);

        if (in_array($extallowed, $allowed) && $_FILES[$file]['size'] <= '5000000') {

            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            $file_id = media_handle_upload($file, 0);

            if (!is_wp_error($file_id)) :
                return $file_id;
            else:
                return 0;
            endif;
        } else {

            return 0;
        }
    }

    /**
     * @since 1.0
     */
    public function load_file() {
        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX ))
            return;

        $url = $_REQUEST['file'];

        $ext = pathinfo($url, PATHINFO_EXTENSION);

        if ($ext == 'mp4') {
            echo do_shortcode('[video src="' . $url . '"]');
        } else {
            echo do_shortcode('[gview file="' . $url . '"]');
        }
        exit;
    }

    /**
     * @since 1.0
     */
    public function agreement_delete() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX && current_user_can('create_courseware')))
            return;

        $file_id = $_POST['file_id'];
        $field_key = "field_569bec9f32f0c";   // ACF field name
        $post_id = $_POST['post_id'];       // Post ID of draw to update

        $status = update_field($field_key, '', $post_id);

        if ($status) {
            $args = array('post_status' => 'inherit', 'posts_per_page' => 1, 'post_type' => 'attachment', 'post__in' => array($file_id));

            $the_query = new WP_Query($args);

            if ($the_query->found_posts > 0) {
                wp_delete_attachment(intval($file_id));
                echo 1;
            } else {
                echo 0;
            }

            wp_reset_postdata();
        }

        exit;
    }

    /**
     * @since 1.0
     */
    public function file_delete() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX && current_user_can('create_courseware')))
            return;

        $file_id = $_POST['file_id'];
        $field_key = "field_56235b202f49a";   // ACF field name
        $post_id = $_POST['post_id'];       // Post ID of draw to update
        // Get member_entries field
        $value = get_field($field_key, $post_id);

        $new_value = array();
        foreach ($value as $id => $entry) {
            if ($entry['document']['ID'] == $file_id)
                continue;
            $new_value[$id] = $entry;
        }

        $status = update_field($field_key, $new_value, $post_id);

        if ($status) {
            $args = array('post_status' => 'inherit', 'posts_per_page' => 1, 'post_type' => 'attachment', 'post__in' => array($file_id));

            $the_query = new WP_Query($args);

            if ($the_query->found_posts > 0) {
                wp_delete_attachment(intval($file_id));
                echo 1;
            } else {
                echo 0;
            }

            wp_reset_postdata();
        }

        exit;
    }

    /**
     * @since 1.0
     */
    public function create_online() {

        $data = $_POST;

        if (isset($data) && !empty($data['submit']) && wp_verify_nonce($data['nc_create_online'], 'nc_create_online')) {

            $validate = $this->validate($data);

            if ($validate == true && !is_array($validate)) {
                $online_course = array(
                    'post_title'   => wp_strip_all_tags($data['nc_title']),
                    'post_content' => wp_strip_all_tags($data['nc_desc']),
                    'post_status'  => 'publish',
                    'post_type'    => 'programs'
                );

                $course_id = wp_insert_post($online_course);
                if ($course_id) {
                    wp_set_object_terms($course_id, 125, 'course_type');
                    $url = get_permalink($course_id).'content';
                    wp_redirect($url);
                    exit;
                }
            }
        }
    }

    /**
     * @since 1.0
     */
    public function validate($data) {

        global $onlinecourse_messsage;

        if (empty($data['nc_title'])) {
            $error['nc_title'] = 'Please enter course name.';
        }
        if (!empty($error)) {
            $onlinecourse_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }

}

new ProgramAction();