<?php

/**
 * @package  WPSQT
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class WPSQT {

    public $wpsqt_id;
    public $post_id;

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct($id = '', $post_id = '') {
        $this->wpsqt_id = $id;
        $this->post_id = $post_id;
        $this->wpsqt_id = get_post_meta($this->post_id, 'survey_id', true);
        add_action('wp_ajax_sqo', array($this, 'questions_reorder'));
        add_action('wp_ajax_lq', array($this, 'load_question'));
        add_action('wp_ajax_lo', array($this, 'load_option'));
        add_action('wp_ajax_sq', array($this, 'save_question'));
        add_action('wp_ajax_dq', array($this, 'delete_question'));
    }

    /**
     * @since 1.0
     */
    public function create_survey($type) {

        global $wpdb;        

        $name      = sanitize_text_field($_POST['wpsqt_name']);
        $progress  = sanitize_text_field($_POST['wpsqt_show_progress_bar']);
        $messages  = sanitize_text_field($_POST['wpsqt_finish_message']);
        $pass_mark = sanitize_text_field($_POST['wpsqt_pass_mark']);

        if (empty($this->wpsqt_id)) {

            $wpdb->insert(WPSQT_TABLE_QUIZ_SURVEYS, array(
                'name' => $name,
                'type' => $type,
                'timestamp' => date("Y-m-d H:i:s"),
                'user_id' => get_current_user_id(),
                'settings' => serialize(array(
                    'status' => 'enabled',
                    'finish_display' => 'Custom finish message',
                    'limit_one' => 'no',
                    'limit_one_wp' => 'yes',
                    'limit_one_cookie' => 'no',
                    'save_resume' => 'yes',
                    'question_numbers' => 'yes',
                    'timer' => 0,
                    'pdf_template' => '',
                    'send_user' => 'no',
                    'show_progress_bar' => $progress,
                    'show_results_limited' => 'no',
                    'contact' => 'no',
                    'use_pdf' => 'no',
                    'store_results' => 'yes',
                    'notificaton_type' => 'none',
                    'pass_finish'=>'no',
                    'use_wp' => 'yes',
                    'email_template' => '',
                    'fail_review'=>'no',
                    'pass_finish_message'=>'',
                    'notification_email' => '',
                    'pass_mark' => $pass_mark,
                    'finish_message' => $messages
                ))
            ));
            $this->wpsqt_id = $wpdb->insert_id;
        } else {
            $wpdb->update(WPSQT_TABLE_QUIZ_SURVEYS, array(
                'name' => $name,
                'settings' => serialize(array(
                    'status' => 'enabled',
                    'finish_display' => 'Custom finish message',
                    'limit_one' => 'no',
                    'limit_one_wp' => 'yes',
                    'limit_one_cookie' => 'no',
                    'save_resume' => 'yes',
                    'timer' => 0,
                    'send_user' => 'no',
                    'pdf_template' => '',
                    'store_results' => 'yes',
                    'question_numbers' => 'yes',
                    'show_progress_bar' => $progress,
                    'show_results_limited' => 'no',
                    'contact' => 'no',
                    'notificaton_type' => 'none',
                    'use_wp' => 'yes',
                    'pass_finish'=>'no',
                    'email_template' => '',
                    'pass_finish_message'=>'',
                    'notification_email' => '',
                    'fail_review'=>'no',
                    'pass_mark' => $pass_mark,
                    'finish_message' => $messages
                ))
            ), array(
                'id' => $this->wpsqt_id
            ));
        }
        
        update_post_meta($this->post_id, 'survey_id', $this->wpsqt_id);
    }

    /**
     * @since 1.0
     */
    public function save_sections($data) {

        global $wpdb;

        $sections = $data['sections'];

        foreach ($sections as $section) {
            if (!empty($section['id']) && !isset($section['delete'])) {
                $wpdb->update(
                WPSQT_TABLE_SECTIONS, array(
                    'item_id' => $this->wpsqt_id,
                    'name' => $section['name'],
                    'limit' => '0',
                    'desc' => trim($section['desc']),
                    'difficulty' => ''
                ), array(
                    'id' => $section['id']
                )
                );
            } else if (!empty($section['id']) && isset($section['delete'])) {
                $wpdb->delete(WPSQT_TABLE_SECTIONS, array('id' => trim($section['id'])));
            } else if (!empty($section['name'])) {
                $wpdb->insert(
                WPSQT_TABLE_SECTIONS, array(
                    'item_id' => $this->wpsqt_id,
                    'name' => trim($section['name']),
                    'desc' => trim($section['desc']),
                    'limit' => '0',
                    'difficulty' => '',
                )
                );
            }
        }
    }

    /**
     * @since 1.0
     */
    public function get_survey() {
        global $wpdb;
        $return = array('name' => '', 'progress_bar' => '', 'finish_message' => '');
        if (!empty($this->wpsqt_id)) {
            $surveys = $wpdb->get_row("SELECT * FROM " . WPSQT_TABLE_QUIZ_SURVEYS . " WHERE id=" . $this->wpsqt_id);
            $settings = unserialize($surveys->settings);
            $return['name'] = $surveys->name;
            $return['progress_bar'] = isset($settings['show_progress_bar']) ? $settings['show_progress_bar'] : '';
            $return['finish_message'] = isset($settings['finish_message']) ? $settings['finish_message'] : '';
            
            $return = $settings;
        }
        return $return;
    }

    /**
     * @since 1.0
     */
    public function get_post_survey($post_id) {
        $this->wpsqt_id = get_post_meta($post_id, 'survey_id', true);
        return $this->get_survey();
    }

    /**
     * @since 1.0
     */
    public function get_sections() {
        global $wpdb;
        $sections = $wpdb->get_results("SELECT * FROM " . WPSQT_TABLE_SECTIONS . " WHERE item_id =" . $this->wpsqt_id);
        return $sections;
    }

    /**
     * @since 1.0
     */
    public function get_questions() {

        global $wpdb;

        $questions = $wpdb->get_results("SELECT q.id,q.name,q.order,s.name AS section
                                         FROM " . WPSQT_TABLE_QUESTIONS . " AS q 
                                         LEFT JOIN " . WPSQT_TABLE_SECTIONS . " AS s ON s.id = q.section_id
                                         WHERE q.item_id=" . $this->wpsqt_id . "
                                         ORDER BY q.order ASC LIMIT 100");

        return $questions;
    }

    /**
     * @since 1.0
     */
    function questions_reorder() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX) && !(current_user_can('administrator') || current_user_can('organization_admin') || current_user_can('facilitator')))
            return;

        global $wpdb;

        $data = $_POST;

        foreach ($data['item'] as $order => $id) {

            $wpdb->update(WPSQT_TABLE_QUESTIONS, array('order' => $order + 1), array('id' => $id));
        }

        echo json_encode(array('data' => 'yes'));

        exit;
    }

    /**
     * @since 1.0
     */
    public function load_question() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX) && !(current_user_can('administrator') || current_user_can('organization_admin') || current_user_can('facilitator')))
            return;

        global $wpdb;
        
        $data = $_POST;
        
        $delete = '';
        
        $question = (object) array('type' => '', 'name' => '', 'section_id' => '');
        
        // Show delete button if question exists
        if (!empty($data['qid']) && $data['qid'] != 0 && $data['qid'] != 'undefined') {
            $question = $wpdb->get_row("SELECT * FROM " . WPSQT_TABLE_QUESTIONS . " WHERE id = " . $data['qid']);
            $delete = '<input type="button" name="delete" onclick="delete_question(this)" class="btn btn-danger m-r-md" value="Delete">';
        }
        
        // Get section array
        if (!empty($data['sid']) || $data['sid'] != 0) :
            $sections_array = $wpdb->get_results("SELECT id,name FROM " . WPSQT_TABLE_SECTIONS . " WHERE item_id = " . $data['sid']);
        endif;
        
        // Get Survey type
        if(!empty($data['sid']) || $data['sid'] != 0) :
            $wpsqt_type = $wpdb->get_var("SELECT type FROM " . WPSQT_TABLE_QUIZ_SURVEYS . " WHERE id = " . $data['sid']);
        endif;

        $options = isset($question->meta) ? unserialize($question->meta) : '';
        
        $sections = array();

        foreach ($sections_array as $section) {
            $sections[$section->id] = $section->name;
        }

        $points = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        $required = array('yes' => 'Yes', 'no' => 'No');
        
        if (empty($question->type)) {
            $question->type = 'Multiple Choice';
        }

        if ($question->type == 'Likert' ) {
            $scale = isset($options['likertscale']) ? $options['likertscale'] : '';
        } else if ($question->type == 'Improvement' || $question->type == 'Likert Matrix' ) {
            $scale = isset($options['likertmatrixscale']) ? $options['likertmatrixscale'] : '';
        }

        $answers = isset($options['answers']) ? $options['answers'] : '';

        if ($question->type == 'Free Text') {
            $answers = $options['hint'];
        }

        if ($question->type == 'Slider') {
            $answers = $options['answers'];
        } 
        if ($wpsqt_type == 'survey') {
            
            $type = array(
                'Multiple Choice'   => 'Multiple Choice',
                'Single'            => 'Single', 
                'Dropdown'          => 'Dropdown', 
                'Likert'            => 'Likert', 
                'Likert Matrix'     => 'Likert Matrix',
                'Improvement'       => 'Improvement',
                'Free Text'         => 'Free Text',
                'Slider'            => 'Slider',
            );
            
            $meta = '<div class="form-group"><label>Question Type</label>' . $this->selectbox('wpsqt_type', $type, $question->type, 'onchange="load_option(this);" data-type="survey" data-id="'.$data['qid'].'"') . '</div>';
            
        } else if ($wpsqt_type == 'quiz') {
            
            $type = array('Multiple' => 'Multiple', 'Single' => 'Single', 'Free Text' => 'Free Text');
            $meta = '<div class="form-group"><label>Question Type</label>' . $this->selectbox('wpsqt_type', $type, $question->type, 'onchange="load_option(this);" data-type="quiz" data-id="'.$data['qid'].'"') . '</div>';
            
            $meta .= '<div class="form-group"><label>Points</label>' . $this->selectbox('wpsqt_point', $points, (isset($options['points']) ? $options['points'] : '')) . '</div>';
            
        }

        $return = '<form action="" method="post" class="save_question">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Question Text</label>
                                <textarea name="wpsqt_name" rows="6" cols="40" class="form-control">' . $question->name . '</textarea>
                             </div>
                            <div class="options">
                            ' . $this->options($question->type, $answers, $scale, $wpsqt_type) . '
                            </div>
                        </div>
                        <div class="col-md-4">
                            ' . $meta . '
                            <div class="form-group"><label>Section</label>' . $this->selectbox('wpsqt_section', $sections, $question->section_id) . '</div>
                            <div class="form-group"><label>Required</label><br/>' . $this->radiobox('wpsqt_required', $required, (isset($options['required']) ? $options['required'] : '')) . '</div>
                            <div class="form-group"><label>Additional Text</label><textarea name="wpsqt_explanation" class="form-control">' . (isset($options['add_text']) ? $options['add_text'] : '') . '</textarea></div>
                            <input type="hidden" name="wpsqt_qid" value="' . $data['qid'] . '" />
                            <input type="hidden" name="wpsqt_sid" value="' . $data['sid'] . '" /> 
                            ' . wp_nonce_field('nonce_survey', 'nonce_survey') . '
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="button" name="save" onclick="save_question(this)" class="btn btn-success m-r-md" value="Save">' . $delete . '<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </form>';

        echo $return;

        exit;
    }

    /**
     * @since 1.0
     */
    public function selectbox($name, $data, $default = '', $param = '') {
        $out = '<select class="form-control" name="'.$name.'"' . (!empty($param) ? ' '.$param : '').">\n";

        foreach ($data as $key => $val) {
            $out.='<option value="' . $key . '"' . ($default == $key ? ' selected="selected"' : '') . '>';
            $out.=$val;
            $out.="</option>\n";
        }
        $out.="</select>\n";

        return $out;
    }

    /**
     * @since 1.0
     */
    public function radiobox($name, $data, $default = '', $param = '') {

        $out = '';

        foreach ($data as $key => $val) {
            $out .= '<div class="radio radio-warning radio-inline"><input type="radio" id="' . $key . '" name="' . $name . '" value="' . $key . '" ' . ($default == $key ? ' checked="checked"' : '') . ' ' . (!empty($param) ? ' ' . $param : '') . ' ><label for="' . $key . '">' . $val . '</label></div>';
        }

        return $out;
    }

    /**
     * @since 1.0
     */
    public function options($type, $answer = '', $scale = false, $module = '') {
        $out = '';
        switch ($type) {
            case 'Free Text':
                $out = '<div class="form-group"><label>Hint</label><textarea name="wpsqt_hint" class="form-control" cols="60" rows="3">' . $answer . '</textarea></div>';
                break;

            case 'Likert':
                $likertscale = array('10' => 10, '5' => 5, '3' => 3, 'Agree/Disagree' => 'Poor/Excellent',"Strength/Opportunity" => "Strength/Opportunity");
                $out = '<div class="form-group"><label>Likert Scale</label>' . $this->selectbox('wpsqt_likertscale', $likertscale, $scale) . '</div>';
                break;

            case 'Dropdown':
            case 'Multiple Choice':
            case 'Multiple':
            case 'Single': 
                if($module=='quiz') :
                    $out = '<div class="form-group"><label>' . $type . ' Answers</label>
                        <div id="sub_form_multiple">
                            <table class="table table-striped table-hover">
                                <thead><tr><th>Name</th><th class="text-center">Correct</th><th class="text-center">Delete</th></tr></thead>
                                <tbody>' . $this->formoptions($answer,$module) . '</tbody>
                                <tfoot><tr><td colspan="3"><a href="#" class="btn btn-primary btn-block text-center" title="Add New Answer" onclick="return wsqt_multi_add(\'quiz\')">Add New Answer</a></td></tr></tfoot>
                            </table>
                        </div></div>';
                else :
                    $out = '<div class="form-group"><label>' . $type . ' Answers</label>
                        <div id="sub_form_multiple">
                            <table class="table table-striped table-hover">
                                <thead><tr><th>Name</th><th>Delete</th></tr></thead>
                                <tbody>' . $this->formoptions($answer) . '</tbody>
                                <tfoot><tr><td colspan="2"><a href="#" class="btn btn-primary btn-block text-center" title="Add New Answer" onclick="return wsqt_multi_add(\'survey\')">Add New Answer</a></td></tr></tfoot>
                            </table>
                        </div></div>';
                endif;
                break;

            case 'Likert Matrix':
                $likertscale = array('1-5' => '1-5', 'Disagree/Agree' => 'Disagree/Agree',"Strength/Opportunity" => "Strength/Opportunity");
                $out = '<div class="form-group"><label>Likert Matrix Scale</label>' . $this->selectbox('wpsqt_likertmatrixscale', $likertscale, $scale);
                $out .= '<label>Matrix Options</label>
                        <div id="sub_form_multiple">
                            <table class="table table-striped table-hover">
                                <thead><tr><th>Name</th><th>Delete</th></tr></thead>
                                <tbody>' . $this->formoptions($answer) . '</tbody>
                                <tfoot><tr><td colspan="2"><a href="#" class="btn btn-primary btn-block text-center" title="Add New Answer" onclick="return wsqt_multi_add()">Add New Answer</a></td></tr></tfoot>
                            </table>
                    </div></div>';
                break;

            case 'Improvement':
                $likertscale = array('1-5' => '1-5', 'Disagree/Agree' => 'Disagree/Agree');
                $out = '<div class="form-group"><label>Improvement Scale</label>' . $this->selectbox('wpsqt_likertmatrixscale', $likertscale, $scale);
                $out .= '<label>Matrix Options</label>
                        <div id="sub_form_multiple">
                            <table class="table table-striped table-hover">
                                <thead><tr><th>Name</th><th>Delete</th></tr></thead>
                                <tbody>' . $this->formoptions($answer) . '</tbody>
                                <tfoot><tr><td colspan="2"><a href="#" class="btn btn-primary btn-block text-center" title="Add New Answer" onclick="return wsqt_multi_add()">Add New Answer</a></td></tr></tfoot>
                            </table>
                    </div></div>';
                break;
            case 'Slider':
                $out = '<div class="form-group"><label>' . $type . ' Answers</label>
                        <div id="sub_form_slider">
                            <table class="table table-striped table-hover">
                                <thead><tr><th>Name</th><th class="text-center">Min</th><th class="text-center">Max</th><th class="text-center">Unit</th><th class="text-center">Delete</th></tr></thead>
                                <tbody>' . $this->sliderformoptions($answer,$module) . '</tbody>
                                <tfoot><tr><td colspan="3"><a href="#" class="btn btn-primary btn-block text-center" title="Add New Answer" onclick="return wsqt_multi_slider_add()">Add New Answer</a></td></tr></tfoot>
                            </table>
                        </div></div>';
        }
        return $out;
    }

    /**
     * @since 1.0
     */
    public function formoptions($answers,$module='') {
        $out = '';
        if (empty($answers))
            $answers = array(0 => array('text' => ''));
        foreach ($answers as $key => $answer) {
            $checked = $answer['correct']=='yes'?'checked':'';
            if($module =='quiz') :
                $out .= '<tr><td><input class="form-control" type="text" name="multiple_name[' . $key . ']" value="' . $answer['text'] . '"></td><td class="text-center"><div class="checkbox checkbox-success"><input type="checkbox" name="multiple_correct[' . $key . ']" value="yes" '.$checked.'  id="checkbox' . $key . '"><label for="checkbox' . $key . '"></label></div></td><td class="text-center"><div class="checkbox checkbox-danger"><input type="checkbox" name="multiple_delete[' . $key . ']" value="yes"  id="checkbox' . $key . '"><label for="checkbox' . $key . '"></label></div></td></tr>';
            else :
                $out .= '<tr><td><input class="form-control" type="text" name="multiple_name[' . $key . ']" value="' . $answer['text'] . '"></td><td class="text-center"><div class="checkbox checkbox-danger"><input type="checkbox" name="multiple_delete[' . $key . ']" value="yes"  id="checkbox' . $key . '"><label for="checkbox' . $key . '"></label></div></td></tr>';
            endif;
            
        }
        return $out;
    }
    
    /** 
     * Slider Form options
     */

    public function sliderformoptions($answers,$module='') {
        $out = '';
        if (empty($answers))
            $answers = array(0 => array('text' => ''));
        foreach ($answers as $key => $answer) {
           
                $out .= '<tr>
                            <td>
                            <input class="form-control" type="text" name="slider_name[' . $key . ']" value="' . $answer['slider_name'] . '" size=60>
                            </td>
                            <td class="text-center">
                            <input class="form-control" type="text" name="slider_min_scale[' . $key . ']" value="' . $answer['slider_min_scale'] . '">
                            </td>
                            <td class="text-center">
                            <input class="form-control" type="text" name="slider_max_scale[' . $key . ']" value="' . $answer['slider_max_scale'] . '"></td>
                            <td class="text-center">
                            <input class="form-control" type="text" name="slider_unit[' . $key . ']" value="' . $answer['slider_unit'] . '"></td>
                            <td class="text-center"><div class="checkbox checkbox-danger">
                            <input type="checkbox" name="slider_delete[' . $key . ']" value="yes"  id="checkbox' . $key . '">
                                <label for="checkbox' . $key . '"></label></div></td></tr>';
                        
        }
        return $out;
    }
    /**
     * @since 1.0
     */
    public function load_option() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX))
            return;
        
        global $wpdb;
        
        $data = $_POST;
        
        // Show delete button if question exists
        if (!empty($data['qid']) && $data['qid'] != 0 && $data['qid'] != 'undefined') {
            $question = $wpdb->get_row("SELECT * FROM " . WPSQT_TABLE_QUESTIONS . " WHERE id = " . $data['qid']);
            $options = isset($question->meta) ? unserialize($question->meta) : '';
            $answers = isset($options['answers']) ? $options['answers'] : '';

            if ($data['type'] == 'Free Text') {
                $answers = $options['hint'];
            }
        }
        
        echo $this->options($data['type'],$answers,'',$data['module']);

        exit;
    }

    /**
     * @since 1.0
     */
    public function save_question() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX))
            return;

        if (!isset($_POST['nonce_survey']) || !wp_verify_nonce($_POST['nonce_survey'], 'nonce_survey')) {
            $result['type'] = 'error';
            $result['data'] = array('Invalid access');
            echo json_encode($result);
            exit;
        }

        global $wpdb;
        
        $questionarr = $_POST;

        $update = true;
        $result = array();

        if (!is_numeric($questionarr['wpsqt_qid']) || $questionarr['wpsqt_qid'] == 0) {
            $questionarr['wpsqt_qid'] = 0;
            $update = false;
        } else {
            $where = array('id' => $questionarr['wpsqt_qid']);
            $update = true;
        }

        $validation = $this->validate_data($questionarr);

        if (true !== $validation) {
            $result['type'] = 'error';
            $result['data'] = $validation;
            echo json_encode($result);
            exit;
        }

        $data = $this->clean_data($questionarr);

        if ($update) {
            if (false === $wpdb->update(WPSQT_TABLE_QUESTIONS, $data, $where)) {
                $result['type'] = 'update-error';
                echo json_encode($result);
            } else {
                $result['type'] = 'update';
                echo json_encode($result);
            }
        } else {
            if (false === $wpdb->insert(WPSQT_TABLE_QUESTIONS, $data)) {
                $result['type'] = 'create-error';
                echo json_encode($result);
            } else {
                $result['type'] = 'create';
                echo json_encode($result);
            }
        }

        exit;
    }

    /**
     * @since 1.0
     */
    public function clean_data($data) {

        $return = array();
        $meta = array();

        $return['name'] = $data['wpsqt_name'];
        $return['type'] = $data['wpsqt_type'];
        $return['item_id'] = $data['wpsqt_sid'];
        $return['section_id'] = $data['wpsqt_section'];

        $meta['points'] = empty($data['wpsqt_point']) ? 0 : $data['wpsqt_point'];
        $meta['required'] = empty($data['wpsqt_required']) ? 'no' : $data['wpsqt_required'];
        $meta['add_text'] = $data['wpsqt_explanation'];
        $meta['likertscale'] = empty($data['wpsqt_likertscale']) ? 10 : $data['wpsqt_likertscale'];
        $meta['likertmatrixscale'] = empty($data['wpsqt_likertmatrixscale']) ? '1-5' : $data['wpsqt_likertmatrixscale'];
        $meta['hint'] = $data['wpsqt_hint'];

        if (!empty($data['multiple_name'])) {
            foreach ($data['multiple_name'] as $key => $value) {
                if ($data['multiple_delete'][$key] != 'yes') {
                    $meta['answers'][$key]['text'] = $value;
                    $meta['answers'][$key]['default'] = 'no';
                    $meta['answers'][$key]['correct'] = $data['multiple_correct'][$key];
                }
            }
        }

        
        if (!empty($data['slider_name'])) {
            foreach ($data['slider_name'] as $key => $value) {
                if ($data['slider_delete'][$key] != 'yes') {
                    $meta['answers'][$key]['slider_name'] = $value;
                    $meta['answers'][$key]['slider_min_scale'] = $data['slider_min_scale'][$key];
                    $meta['answers'][$key]['slider_max_scale'] = $data['slider_max_scale'][$key];
                    $meta['answers'][$key]['slider_unit'] = $data['slider_unit'][$key];
                    $meta['answers'][$key]['default'] = 'no';
                   // $meta['answers'][$key]['correct'] = $data['multiple_correct'][$key];
                }
            }
        }
        $return['meta'] = serialize($meta);
        
        return $return;
    }

    /**
     * @since 1.0
     */
    public function validate_data($data) {

        global $wpdb;
        
        // set varibales
        $error = false;
        $msg = array();
        $typearr = array('Multiple Choice' => 'Multiple Choice','Single' => 'Single','Multiple' => 'Multiple', 'Dropdown' => 'Dropdown', 'Likert' => 'Likert', 'Likert Matrix' => 'Likert Matrix', 'Improvement' => 'Improvement', 'Free Text' => 'Free Text', 'Slider' => 'Slider');
        $survey_id = $data['wpsqt_sid'];
        if (empty($survey_id)) {
            $msg['survey'] = 'Invalid access';
            return $msg;
        }
        $sections_array = $wpdb->get_results("SELECT id FROM " . WPSQT_TABLE_SECTIONS . " WHERE item_id = " . $survey_id);
        if (empty($sections_array)) {
            $msg['survey'] = 'Invalid access';
            return $msg;
        }
        $sections = array();
        foreach ($sections_array as $section) {
            array_push($sections, $section->id);
        }
        $type = trim($data['wpsqt_type']);
        $multiples = isset($data['multiple_name']) ? array_filter($data['multiple_name']) : array();

        // validate title
        if (empty($data['wpsqt_name'])) {
            $error = true;
            $msg['title'] = 'Please enter question';
        }
        // validate type
        if (empty($type)) {
            $error = true;
            $msg['type'] = 'Please select question type';
        } else if (!in_array($type, $typearr)) {
            $error = true;
            $msg['type'] = 'Invalid question type';
        }
        // validate section
        if (empty($data['wpsqt_section'])) {
            $error = true;
            $msg['section'] = 'Please select section';
        } else if (!in_array($data['wpsqt_section'], $sections)) {
            $error = true;
            $msg['section'] = 'Invalid section';
        }
        // validate option
        if (($type == 'Multiple Choice' || $type == 'Dropdown') && empty($multiples)) {
            $error = true;
            $msg['option'] = 'Please enter options for question';
        } else if ($type == 'Likert' && empty($data['wpsqt_likertscale'])) {
            $error = true;
            $msg['option'] = 'Please select scale for question';
        } else if ($type == 'Likert Matrix' && empty($data['wpsqt_likertmatrixscale']) && empty($multiples)) {
            $error = true;
            $msg['option'] = 'Please enter options for question';
        }

        if ($error) {
            return $msg;
        } else {
            return true;
        }
    }

    /**
     * @since 1.0
     */
    public function delete_question() {

        if (!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX))
            return;

        if (!isset($_POST['nonce_survey']) || !wp_verify_nonce($_POST['nonce_survey'], 'nonce_survey')) {
            $result['type'] = 'error';
            $result['data'] = array('Invalid access');
            echo json_encode($result);
            exit;
        }

        global $wpdb;

        $questionarr = $_POST;

        if (!is_numeric($questionarr['wpsqt_qid']) || $questionarr['wpsqt_qid'] == 0) {
            $questionarr['wpsqt_qid'] = 0;
            $delete = false;
        } else {
            $where = array('id' => $questionarr['wpsqt_qid']);
            $delete = true;
        }

        if (false === $wpdb->delete(WPSQT_TABLE_QUESTIONS, $where) && !$delete) {
            $result['type'] = 'delete-error';
            echo json_encode($result);
        } else {
            $result['type'] = 'delete';
            echo json_encode($result);
        }

        exit;
    }
    
    /**
     * @since 1.0
     */
    public static function update_title($id,$title) {
        
        global $wpdb;
        
        $wpdb->update(WPSQT_TABLE_QUIZ_SURVEYS, array( 'name' => $title ), array( 'id' => $id ));
        
    }

}

new WPSQT();