<?php

/**
 * @package  Notification
 * @author   Dipak Pusti <dipak.pusti@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Notification {

	/**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array( $this, 'update_notification') );
    }

    public function update_notification() {

    	if(isset($_POST) && !empty($_POST['submit']) && isset( $_POST['nuclo_notifications']) && wp_verify_nonce( $_POST['nuclo_notifications'], 'nuclo_notifications' ) ) {
            
    		# Current Post Data
    		$data = $_POST;

            # Getting Current User ID
            $user_id = get_current_user_id();

            # Adding reminder settings to usermeta
            update_user_meta( $user_id, 'reminder_training', 	$data['training_reminder'] );
            update_user_meta( $user_id, 'reminder_session', 	$data['session_reminder'] );
            update_user_meta( $user_id, 'reminder_actionplan', 	$data['action_reminder'] );
            update_user_meta( $user_id, 'reminder_blogpost', 	$data['blog_reminder'] );

            # Redirect user to settings page
            wp_safe_redirect( $data['_wp_http_referer'].'/?msg=success' );
			exit();
        }
    }

    public static function nuclo_new_user_notification( $user_id ) {
    
        global $wpdb, $wp_hasher;
        $user = get_userdata( $user_id );

        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        $message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
        $message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";

        @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

        // Created user section template
        // Generate something random for a password reset key.
        $key = wp_generate_password( 20, false );

        /** This action is documented in wp-login.php */
        do_action( 'retrieve_password_key', $user->user_login, $key );

        // Now insert the key, hashed, into the DB.
        if ( empty( $wp_hasher ) ) {
            require_once ABSPATH . WPINC . '/class-phpass.php';
            $wp_hasher = new PasswordHash( 8, true );
        }
        $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
        $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

        // Get user notification email
        $post =  get_page_by_title('Welcome Email', OBJECT, 'email');
        $subject = get_post_meta( $post->ID, 'subject', true );
        $template = $post->post_content;

        $to_replace = array(
            'username'         => $user->user_login,
            'activation_url'   => network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login'),
            'login_url'        => wp_login_url(),
        );

        foreach ( $to_replace as $tag => $var ) {
            $template = str_replace( '%' . $tag . '%', $var, $template );
        }

        wp_mail( $user->user_email, $subject, $template);
    }
}

new Notification();