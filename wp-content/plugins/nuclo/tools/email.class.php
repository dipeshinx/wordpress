<?php

/**
 * @package  Email
 * @author   Dipak Pusti <dipak.pusti@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Email {

	/**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        
        add_action( 'init'                          , array( $this, 'register_post_type') );
        add_action( 'init'                          , array( $this, 'rewrites_init') );
        add_action( 'init'                          , array( $this, 'update_template') );
        add_action( 'compose_mail_init'             , array( $this, 'compose_email') );
        add_action( 'nuclo_scheduled_email'         , array( $this, 'scheduled_email_event'));
        add_action( 'wp_ajax_create_template'       , array( $this, 'create_template'));
        add_action( 'wp_ajax_delete_email'          , array( $this, 'delete_email'));
        add_action( 'wp_ajax_delete_template'       , array( $this, 'delete_template'));
        add_action( 'wp_ajax_load_template'         , array( $this, 'load_template'));
        add_action( 'wp_ajax_email_attachment'      , array( $this, 'email_attachment'));
        add_action( 'wp_ajax_delete_attachment'     , array( $this, 'delete_attachment'));
        add_filter( 'pre_get_posts'                 , array( $this, 'filter_query'), 1, 1);
        add_action('compose_mail_edit'              , array( $this,'compose_mail_edit'));
        add_action('compose_mail_edit'              , array( $this,'compose_mail_edit'));
        add_action('nuclo_scheduled_email'          , array( $this,'email_reminder_for_participants'));
        add_action('nuclo_scheduled_email_ceo'      , array( $this,'email_for_ceos'));
    }

    /**
     * @since  1.0
     */
    public function rewrites_init() {         
       add_rewrite_rule('email/(.+?)/([^/]*)/?$', 'index.php?email=$matches[1]&action=$matches[2]', 'top');
    }

    /**
     * @since 1.0
     */
    public function register_post_type() {

    	if( function_exists('get_field') ) :

            $labels = array(
                    'name'               => _x( get_field('email_general_name', 'option'), 'post type general name', 'nuclo' ),
                    'singular_name'      => _x( get_field('email_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('email_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('email_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('email_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('email_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('email_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('email_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('email_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('email_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('email_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('email_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('email_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('email_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => get_field('email_slug', 'option') ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => false,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'editor', 'author' )
            );

            register_post_type( 'email', $args );

        endif;
    }

    /**
     * @since  1.0
     */
    public static function get_type( $post_id ) {
    	
    	$status = get_post_meta( $post_id, 'template_type', true );
        
        if($status == 'system') {
            echo '<span class="text-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="A pre-defined Template that is sent automatically by the system at pre-defined intervals">System Template</span>';
        } else if($status == 'roi') {
            echo '<span class="text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="A Template that is created by modifying an existing template and sent to specific groups at specific times">Custom Template</span>';
        } else {
            echo '&nbsp;';
        }
    }

    
    public static function post_status($post_id){
        
        $status = get_post_field('post_status', $post_id, 'display');
        
        if($status == 'draft'){
            echo '<span class="text-default">Scheduled</span>';
        } else if($status == 'publish'){
            echo '<span class="text-default">Sent</span>';
        } else {
            echo '&nbsp;';
        }
        
    }
    
    public static  function email_date($post_id){
        if(get_post_meta($post_id,'scheduled_time',true)){
            $date   = date('F d, Y H:ia',get_post_meta($post_id,'scheduled_time',true));
            echo '<span class="text-default">'.$date.'</span>';
        } else {
            $date   = date('F d, Y H:ia',strtotime(get_post_field('post_date',$post_id,'display')));
            echo '<span class="text-default">'.$date.'</span>';
        } 
    }

    /**
     * @since  1.0
     */
    public static function action_links( $post_id ) {
    	
        $template = get_post_meta( $post_id, 'template_type', true );
        
        $action = ''; 

//        $action .= '<a href="'.  home_url().'/?post_type=email&p='.$post_id.'&preview=true" class="btn btn-success btn-xs m-r-sm" data-placement="top" data-original-title="Edit" title="" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>';

        if( $template == 'roi' ) {
    	   $action .= '<a href="" class="btn btn-danger btn-xs m-r-sm" data-id="'.$post_id.'" data-placement="top" data-original-title="Delete" title="" data-toggle="tooltip" onclick="deleteTmp(this);"><i class="fa fa-trash-o"></i></a>';
        } else {
            $action .= '<a href="javascript:" data-id="'.$post_id.'" class="btn btn-danger btn-xs m-r-sm" title="Delete" onclick="deleteEmail(this);"><i class="fa fa-trash-o"></i></a>';
        }

        if(!empty($action)) {
            echo  $action;
        }
    }

    /**
     * @since 1.0
     */
    public function create_template() {
        
        $data = $_POST;
        
        $return = array();

        if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && wp_verify_nonce( $data['nc_nonce'], 'nc_nonce' ))) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong';                
            echo json_encode($return);
            exit;
        }
        
        if(!empty($data['nc_title'])) {
            
            $tmpl_args = array(
                        'post_title'   => $data['nc_title'],
                        'post_type'    => 'email',
                        'post_status'  => 'publish'
            );

            $tmpl_id = wp_insert_post( $tmpl_args );

            // Adding post meta for template type
            add_post_meta( $tmpl_id, 'email_type', 'template' );
            add_post_meta( $tmpl_id, '_email_type', 'field_56c2c30dc207d' );

            if(!empty($tmpl_id)) {
                $return['status'] = 'success';
                $return['data']   = get_the_permalink($tmpl_id).'edit';                
                echo json_encode($return);
            } else {
                $return['status'] = 'error';
                $return['data']   = 'Something went wrong';                
                echo json_encode($return);
            }
        } else {
            $return['status'] = 'error';
            $return['data']   = 'Please enter title';                
            echo json_encode($return);
        }
        exit;
    }

    /**
     * @since  1.0
     */
    public function update_template() {

    	if(isset($_POST) && !empty($_POST['submit']) && isset( $_POST['update_template'])) {
            
            $data = $_POST;

            $total_attach = $data['total_attach'];
            $attached_ids = $data['attachedids'];

            // Updating post content
			$args = array(
				'ID' 			=> $data['post_id'],
				'post_title' 	=> $data['tmpl_name'],
				'post_content' 	=> $data['mail_content']
			);

			wp_update_post( $args );

			// Updating Meta
			update_post_meta( $data['post_id'], 'subject', $data['mail_subject'] );
			update_post_meta( $data['post_id'], '_subject', 'field_56c2f8b8d4c82' );

                        update_post_meta( $data['post_id'], 'template_type', $data['tmpl_type'] );
                        update_post_meta( $data['post_id'], '_template_type', 'field_56dd1aa8d6dd5' );

			// Deleting the old attachments if any
			for($i=0; $i<$total_attach;$i++) {
				delete_post_meta( $data['post_id'], 'attachment_'.$i.'_files' );
			}

			// Updating New Attachments
			if (count($attached_ids) > 0) {

				$total_attached = count($attached_ids);
				$current = 0;
				foreach( $attached_ids as $att ) {
					update_post_meta($data['post_id'], 'attachment_'.$current.'_files', $att);
					update_post_meta($data['post_id'], '_attachment_'.$current.'_files', 'field_56c2f76332e25');
					$current++;
				}
				update_post_meta($data['post_id'], 'attachment', $total_attached);
				update_post_meta($data['post_id'], '_attachment', 'field_56c2f74632e24');
			} else {
				update_post_meta($data['post_id'], 'attachment', '0');
				update_post_meta($data['post_id'], '_attachment', 'field_56c2f74632e24');
			}

			wp_safe_redirect( $data['_wp_http_referer'].'/?msg=success' );
			exit();

        }
    }

    /**
     * @since  1.0
     */
    public function delete_template() {

    	if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX )) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong';                
            echo json_encode($return);
            exit;
        }

        $data = $_POST;

        if(!empty($data['id'])) {
            $tmpl_id = (int) $data['id'];
            wp_delete_post($tmpl_id);
            $return['status'] = 'success';
            $return['data']   = 'Successfully deleted.';                
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data']   = 'you can not delete this.';                
            echo json_encode($return);
        }
        exit();
    }

    /**
     * @since  1.0
     */
    public function load_template() {

        if(is_array($_REQUEST) && defined('DOING_AJAX') && DOING_AJAX) { 

            // Requested Template
            $template_id = $_REQUEST['template'];

            // Get Template details
            $template = get_post($template_id);

            // Set up post data
            setup_postdata( $template );
            $container  = 'group_fields';
            if(get_field('session_template',$template_id)){
                $container  = 'session_fields';
            }
            // arranging post variable
            $template_title = $template->post_title;
            $template_content = wpautop($template->post_content);
            $template_subject = get_post_meta( $template_id, 'subject', true );

            // getting post meta
            $meta = get_post_meta($template_id);
            $totalatt = $meta['attachment'][0];
            $attachs = array();
            if($totalatt > 0) {
                for ( $i=0; $i<$totalatt; $i++ ) {
                    $meta_value = get_post_meta( $template_id, 'attachment_'.$i.'_files' );
                    $meta_title = get_the_title( $meta_value[0] );
                    array_push($attachs, array('metaid'=>$meta_value[0], 'metaname'=>$meta_title));
                }
            }

            wp_reset_postdata();
            
            // Creating new array for return value
            $template_data = array('title'  =>  $template_title, 'subject' => $template_subject, 'content'=>$template_content, 'atts'=>$attachs , 'container' => $container , 'template_id' => $template_id);

            echo json_encode($template_data);
            exit();
        } else {
            echo "FAIL";
        }
    }

    /**
     * @since  1.0
     */
    public function email_attachment() {

    	$allowed =  array('doc','docx' ,'ppt', 'pptx', 'pdf');

    	check_ajax_referer('ajax_file_nonce', 'security');

	    if(!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX)){
	        return;
	    }

	    if(!function_exists('media_handle_upload')){
	        require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
	    }

		$upfilename = $_FILES['files']['name'];
		$extallowed = pathinfo($upfilename, PATHINFO_EXTENSION);

		if(in_array($extallowed, $allowed) ) {
		    $uploadid = media_handle_upload( 'files', '0' );
                    $uploadname = get_the_title($uploadid);
                    $attachments = array('id'=>$uploadid, 'name'=>$uploadname , 'file_name' => $upfilename);
		} else {
                    $attachments = array('id'=> '0', 'name'=>'ERR');
		}

		echo json_encode($attachments);
		exit();
    }

    /**
     * @since  1.0
     */
    public function delete_attachment() {

    	if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX)) { 
            echo 0;
            exit;
        }
        
        $data = $_POST;
        
        if(!empty($data['file_id'])) {
            $att_id = (int) $data['file_id'];
            wp_delete_attachment($att_id);
            echo 1;
        } else {
            echo 0;
        }
        exit; 
    }

    /**
     * @since  1.0
     */
    public static function choose_template() {

        $args = array(
            'post_type' => 'email', 
            'posts_per_page' => -1, 
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'     => 'template_type',
                    'value'   => 'roi',
                    'compare' => '=',
                ),
            ),
        );

        $templates = new WP_Query( $args );

        $html = '';
        $html.= '<select class="form-control tpl_container m-b-sm" id="select_template" onchange="loadTemplate();">';
        $html.= '<option value="">-- Select Template --</option>';

        if ( $templates->have_posts() ) {

            while ( $templates->have_posts() ) {
                $templates->the_post();
                $html.= '<option value="'.get_the_id().'">'.get_the_title().'</option>';
            }
        }

        $html.= '</select>';
        wp_reset_postdata();

        return $html;
    }
    
    public static function choose_template_workflow() {

        global $wpdb;
        
        $results = $wpdb->get_results("SELECT wp.ID,wp.post_title FROM $wpdb->posts AS wp WHERE  wp.post_type='email' AND wp.post_name='welcome-email'");

        $html = '';
        $html.= '<select class="form-control tpl_container m-b-sm" onchange="loadTemplate();" id="select_template">';
        $html.= '<option value="">-- Select Template --</option>';

        $html.= '<option value="'.$results[0]->ID.'">'.$results[0]->post_title.'</option>';
        
        $html.= '</select>';

        return $html;
        

    }

    public static function get_franchisee_detail( $user_id ) {
        
        $client_id          = get_user_meta( $user_id, 'client', true );
        $franchisee_id      = get_post_meta( $client_id, 'franchisee', true );
        $franchisee_name    = get_the_title( $franchisee_id );
        $franchisee_owner   = get_post_meta( $franchisee_id, 'franchise_owner', true );
        $owner_info         = get_userdata($franchisee_owner);
        $owner_name         = $owner_info->first_name . ' '. $owner_info->last_name;

        $return['owner']    = $owner_name;
        $return['franc']    = $franchisee_name;
        $return['address']  = get_post_meta( $franchisee_id, 'address', true ).', '.get_post_meta( $franchisee_id, 'city', true ).', '. nc_state_name(get_post_meta( $franchisee_id, 'state', true )) .', '.nc_country_name(get_post_meta( $franchisee_id, 'country', true ));
        $return['contact']  = get_post_meta( $franchisee_id, 'franchisee_contact', true );

        return $return;
    }

    /**
     * @since 1.0
     */
    public function compose_email() {

        if(isset($_POST) && (!empty($_POST['submit']) || !empty($_POST['continue'])) && isset( $_POST['nuclo_compose_email']) && wp_verify_nonce( $_POST['nuclo_compose_email'], 'nuclo_compose_email' )) {
            
            global $current_user;
            $bpm_obj        = new BPM($current_user->ID);
            $preassessment  = $bpm_obj->others[0];
           
// Getting post status as per schedule or send now
            $schedule_tm = '';
            $post_status = 'publish';
            
            if($_POST['submit'] == 'schedule') {
                $schedule_tm = strtotime($_POST['schedule_date']);
                $schedule_tm = $schedule_tm - 19800;
                $post_status = 'draft';
            }
            // Gathering all users and group users
            
            if(get_field('session_template',$_POST['nc_email_id'])){
                $current_year       = date('Y',time());
                
                $current_month_session_args = array(
                    'post_type'     => 'event',
                    'post_status'   => 'publish',                
                    'meta_query'    => array(
                        'relation'  => 'AND',
                        array(
                            'key'       => 'parent_event',
                            'value'     => $_POST['nc_session_id'],
                            'compare'   => '=='
                        ),
                        array(
                            'key'       => 'event_type',
                            'value'     => 'Repeat',
                            'compare'   => '!='
                        ),
                        array(
                            'key'       => 'start_date',
                            'value'     => array(date('Ymd',strtotime('01-'.$_POST['nc_month'].'-'.$current_year)),date('Ymd',strtotime('31-'.$_POST['nc_month'].'-'.$current_year))),
                            'compare'   => 'BETWEEN'
                        )
                    )
                );
                
                $current_month_session      = get_posts($current_month_session_args);               
                $_POST['nc_groups']         = get_post_meta($current_month_session[0]->ID,'groups');
            }
            $users = !empty($_POST['grp_users'])?$_POST['grp_users']:array();
            
            if(isset($_POST['nc_groups']) && !empty($_POST['nc_groups'])){
                foreach ($_POST['nc_groups'] as $key => $group) {
                    // Get group users
                    $group_users = get_post_meta($group, 'users', true );

                    $group_users = (!empty($group_users))?$group_users:array();

                    foreach ($group_users as $key => $user) {
                        if(!in_array( $user, $users ) ) {
                            array_push( $users, $user);
                        }
                    }
                }
            }
            // Prepairing Attachments
            $attachments = array();
            if(isset($_POST['attachedids'])){
                foreach ($_POST['attachedids'] as $value) {
                    array_push($attachments, get_attached_file( $value ));
                }
            }
            // preparing to send email through wordpress
            $subject  = $_REQUEST['email_subject'];
            $content  = stripslashes($_REQUEST['email_content']);
            
            // Preparing to replace the variables
            $to_replace = array(
                'date'      => date('d/m/Y', strtotime($_POST['roi_date']) ),
                'starttime' => date('H:i', strtotime($_POST['roi_stime']) ),
                'endtime'   => date('H:i', strtotime($_POST['roi_etime']) ),
                'location'  => $_POST['roi_location'],
            );

            foreach ( $to_replace as $tag => $var ) {
                $content_replace = str_replace( '%' . $tag . '%', $var, $content );
            }
           
            // Sending the email
            if($post_status == 'publish') {
                
                foreach ($users as $key => $user) {
                    $user_info = array();
                    $user_info = get_userdata($user);
                    //exit;
                    if(is_email($user_info->user_email)){
                        $content = '';
                        // User specific replacements
                        $content = str_replace( '%participant_name%', $user_info->first_name.' '.$user_info->last_name, $content_replace );
                       
                        if((strcmp($subject,'Bullet Proof Manager Training - Orientation Meeting') > 0) || (strcmp($subject,'Bullet Proof Manager Training - Orientation Meeting') == 0)) {
                            $franchisee = $this->get_franchisee_detail($user);

                            $password = wp_generate_password();
                            wp_set_password( $password, $user );
                           
                            
                            $meta[] = array('key' => 'parent_event', 'value' => $_SESSION['WORKFLOW']['EVENT_ID'], 'compare' => '==');
                            $meta[] = array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!=');
                            if (count($meta) > 1) {
                                $meta['relation'] = 'AND';
                            }
                            $args           = array(
                                'post_type'         => 'event',
                                'meta_query'        => $meta,
                                'repeat'            => true,
                                'posts_per_page'    => 1,
                                'orderby'           => 'meta_value_num',
                                'meta_key'          => 'start_date',
                                'order'             => 'ASC'
                            );
                            
                            $event_query    = new WP_Query($args);
                           
                            $start_date     = date('d F, Y',strtotime(get_post_meta($event_query->posts[0]->ID,'start_date',true)));
                            $time           = date('g:i a',strtotime(get_post_meta($event_query->posts[0]->ID,'start_time',true))).' - '.date('g:i a',strtotime(get_post_meta($event_query->posts[0]->ID,'end_time',true)));
                            
                            $address        = get_post_meta($event_query->posts[0]->ID,'address',true);
                           
                            $user_data['user_id']               = $user;   
                            $user_data['user_email']            = $user_info->user_email;   
                            $user_data['username']              = $user_info->user_login;
                            $user_data['password']              = $password;       
                            $user_data['name']                  = $user_info->first_name.' '.$user_info->last_name;
                            $user_data['franchisee_name']       = $franchisee['owner'];
                            $user_data['franchisee']            = $franchisee['franc'];
                            $user_data['franchisee_address']    = $franchisee['address'];
                            $user_data['franchisee_no']         = $franchisee['contact'];
                            $user_data['venue']                 = $address;
                            $user_data['time']                  = $start_date.' '.$time;
                            
                            do_action('register_new_participant',$user_data);
                            
                            $preassement_link   = '<a href="'.get_permalink($preassessment).'" target="_blank">here</a>';
                            
                            $content = str_replace( '%franchisee_name%', $franchisee['owner'], $content );
                            $content = str_replace( '%franchisee%', $franchisee['franc'], $content );
                            $content = str_replace( '%preassessment_link%',$preassement_link, $content );
                            $content = str_replace( '%franchisee_address%', $franchisee['address'], $content );
                            $content = str_replace( '%franchisee_no%', $franchisee['contact'], $content );
                        }
                        
                        $body = '';
                        $body = $content;
                        //exit;
                        wp_mail( $user_info->user_email, $subject, $body, '', $attachments );
                    }
                }
            }
           
            // Generatiing post array
            $args = array(
                'post_type'     => 'email',
                'post_title'    => $subject,
                'post_content'  => $content,
                'post_status'   => $post_status
            );
            
            // Creating new post
            $post_id = wp_insert_post($args);
            
            

            // Generating email type post_meta
            update_post_meta( $post_id, 'email_type', 'compose' );
            update_post_meta( $post_id, '_email_type', 'field_56c2c30dc207d' );

            // Update other Email meta
            update_post_meta( $post_id, 'location', $_POST['roi_location'] );
            update_post_meta( $post_id, '_location', 'field_56dd264db6583' );

            update_post_meta( $post_id, 'date', strtotime($_POST['roi_date']) );
            update_post_meta( $post_id, '_date', 'field_56dd265ab6584' );

            update_post_meta( $post_id, 'start_time', strtotime($_POST['roi_stime']) );
            update_post_meta( $post_id, '_start_time', 'field_56dd2666b6585' );

            update_post_meta( $post_id, 'end_time', strtotime($_POST['roi_etime']) );
            update_post_meta( $post_id, '_end_time', 'field_56dd266fb6586' );

            // Adding attachments to post meta
            $attids = $_REQUEST['attachedids'];

            // Adding the post meta
            if (count($attids) > 0) {
                
              // Total number of attachments
              $total_attached = count($attids);

              // Checking attachments and inserting one by one
              $current = 0;
              foreach( $attids as $att ) {
                update_post_meta($post_id, 'attachment_'.$current.'_files', $att);
                update_post_meta($post_id, '_attachment_'.$current.'_files', 'field_56c2f76332e25');
                $current++;
              }

              // Adding total attacment count in post meta
              update_post_meta($post_id, 'attachment', $total_attached);
              update_post_meta($post_id, '_attachment', 'field_56c2f74632e24');
            } else {
              // Adding total attacment count in post meta
              update_post_meta($post_id, 'attachment', '0');
              update_post_meta($post_id, '_attachment', 'field_56c2f74632e24');
            }

            // Adding users and groups as post meta
            add_post_meta($post_id, 'sent_to_users', $_POST['grp_users']);
            add_post_meta($post_id, 'sent_to_groups', $_POST['nc_groups']);

            // Updating scheduled time if draft
            $redirect_url = 0;
            if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
                //Workflow::save_meeting($post_id);
                do_action('save_meeting_workflow', $post_id);
                $remaining_data = Workflow::check_workflow_status();
                $redirect_url = 1;
                
                $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];
                $workflow_postmeta = get_post_meta($workflow_id);
                $total_participant = $workflow_postmeta['number_of_participant']['0'];    
                $current_added_participant = $workflow_postmeta['current_added_participant']['0']; 
                unset($_SESSION['WORKFLOW']['EVENT_ID']);
                unset($_SESSION['WORKFLOW']['GROUP_ID']);
                if($current_added_participant != $total_participant)
                {
                    if($post_status == 'draft')
                    {
                        $_SESSION['WORKFLOW']['SUCCESS'] = '';
                        $_SESSION['WORKFLOW']['SUCCESS'] = 'Invitations successfully scheduled.';
                    }
                    else
                    {
                        $_SESSION['WORKFLOW']['SUCCESS'] = '';
                        $_SESSION['WORKFLOW']['SUCCESS'] = 'Invitations sent successfully.';
                    }
                    $url= home_url('create-user');
                }
                else
                {
                    if($remaining_data == 0)
                    {
                        if($post_status == 'draft')
                        {
                            $_SESSION['WORKFLOW']['SUCCESS'] = '';
                            $_SESSION['WORKFLOW']['SUCCESS'] = 'Invitations successfully scheduled and you have completed the client workflow.';
                        }
                        else
                        {
                            $_SESSION['WORKFLOW']['SUCCESS'] = '';
                            $_SESSION['WORKFLOW']['SUCCESS'] = 'Invitations sent successfully and you have completed the client workflow.';

                        }
                        $url = home_url('client');
                    }
                    else
                    {
                        if($post_status == 'draft')
                        {
                            $_SESSION['WORKFLOW']['SUCCESS'] = '';
                            $_SESSION['WORKFLOW']['SUCCESS'] = 'Invitations successfully scheduled.';
                        }
                        else
                        {
                            $_SESSION['WORKFLOW']['SUCCESS'] = '';
                            $_SESSION['WORKFLOW']['SUCCESS'] = 'Invitations sent successfully.';
                        }
                        $url= home_url('create-group');
                    }
                }
            }
            
            if($post_status == 'draft') {
                
                add_post_meta($post_id, 'scheduled_time', $schedule_tm);
                
                //do_action('compose_email',$post_id);
                
                
                if($redirect_url == 0)
                    wp_safe_redirect( $_POST['_wp_http_referer'].'/?success=scheduled' );
                else
                    wp_safe_redirect($url);
                exit;
                
            } else {
                //do_action('compose_email',$post_id);
                if($redirect_url == 0)
                    wp_safe_redirect( $_POST['_wp_http_referer'].'/?success=sent' );
                else
                    wp_safe_redirect( $url );
                exit;
            }
        }
    }
    
    public function compose_mail_edit(){
        if(isset($_POST) && (!empty($_POST['submit']) || !empty($_POST['continue'])) && isset( $_POST['nuclo_compose_email']) && wp_verify_nonce( $_POST['nuclo_compose_email'], 'nuclo_compose_email' )) {
            
            $post_args  = array(
                'ID'            => $_POST['ID'],
                'post_title'    => $_POST['email_subject'],
                'post_content'  => $_POST['email_content'],
            );
            
            $schedule_tm = strtotime($_POST['schedule_date']);
            $schedule_tm = $schedule_tm - 19800;
            wp_update_post($post_args);
            
            update_post_meta($_POST['ID'], 'scheduled_time', $schedule_tm);
            
            $attachments = array();
            if(isset($_POST['attachedids'])){
                foreach ($_POST['attachedids'] as $value) {
                    array_push($attachments, get_attached_file( $value ));
                }
            }
            
            if (count($attids) > 0) {
                
              // Total number of attachments
              $total_attached = count($attids);

              // Checking attachments and inserting one by one
              $current = 0;
              foreach( $attids as $att ) {
                update_post_meta($_POST['ID'], 'attachment_'.$current.'_files', $att);
                update_post_meta($_POST['ID'], '_attachment_'.$current.'_files', 'field_56c2f76332e25');
                $current++;
              }

              // Adding total attacment count in post meta
              update_post_meta($_POST['ID'], 'attachment', $total_attached);
              update_post_meta($_POST['ID'], '_attachment', 'field_56c2f74632e24');
            } else {
              // Adding total attacment count in post meta
              update_post_meta($_POST['ID'], 'attachment', '0');
              update_post_meta($_POST['ID'], '_attachment', 'field_56c2f74632e24');
            }
            
            update_post_meta($_POST['ID'], 'sent_to_users', $_POST['grp_users']);
            update_post_meta($_POST['ID'], 'sent_to_groups', $_POST['nc_groups']);
            
            wp_safe_redirect( $_POST['_wp_http_referer'].'&success=scheduled' );
            
        }
    }

    /**
     * @since 1.0
     */
    public static function scheduled_email_event() {
        
        $args = array(  'post_type'      => 'email',
                        'post_status'    => 'draft',
                        'posts_per_page' => -1,
                        'meta_key'       => 'scheduled_time',
                        'meta_value'     => time(),
                        'meta_compare'   => '<=',
                );

        $the_query = new WP_Query( $args );

        while ( $the_query->have_posts() ) {
            
            $the_query->the_post();

            $processedemails = array(); 

            $post_id = get_the_ID();
            $subject = get_the_title();
            $content = get_the_content();

            // Getting post meta for emails
            $users = get_post_meta($post_id, 'sent_to_users', true);
            $groups = get_post_meta($post_id, 'sent_to_groups', true);

            // Gathering all users email id
            foreach ($groups as $key => $group) {
                // Get group users
                $group_users = get_post_meta($group, 'users', true );
                foreach ($group_users as $key => $user) {
                    if(!in_array( $user, $users ) ) {
                        array_push( $users, $user);
                    }
                }
            }

            // Preparing attachments
            $totalatt = get_post_meta($post_id, 'attachment', true);
            if($totalatt > 0) {
                $attachs = array();
                for ( $i=0; $i<$totalatt; $i++ ) {
                    $meta_value = get_post_meta( $post_id, 'attachment_'.$i.'_files', true );
                    array_push($attachs, $meta_value);
                }
            }

            $attachments = array();
            if(!empty($attachs)){
                foreach ($attachs as $value) {
                    array_push($attachments, get_attached_file( $value ));
                }
            }

            // Sending the email
            foreach ($users as $key => $user) {
                $user_info = get_userdata($user);
                if(is_email($user_info->user_email)) {
                    wp_mail( $user_info->user_email, $subject, $content, '', $attachments );
                }
            }

            wp_update_post(array('ID' => get_the_ID(), 'post_status'=>'publish') );
        }

        wp_reset_postdata();
    }

    /**
     * @since 1.0
     */
    public function filter_query($query) {

        if ($query->get('post_type') == 'email' && !is_admin() && is_post_type_archive('email')) {
            
            global $current_user;
            
            $query->set('author',$current_user->ID);
            
            if (isset($_GET['search']) && !empty($_GET['search'])) {
                $search_text = sanitize_text_field($_GET['search']);
                $query->set( 's', $search_text);
            }
            
            $query->set('post_status',array('draft'));
            
            $query->set( 'meta_query', array(                
                array(
                    'key'     => 'email_type',
                    'value'   => 'compose',
                    'compare' => '='
                )
            ));
            
//            
//            if(isset($_GET['nc_template'])) {
//                $template = ( ($_GET['nc_template']=='custom')?'roi':'system');
//                $query->set( 'meta_key', 'template_type' );
//                $query->set( 'meta_value', $template );
//                $query->set( 'meta_compare', '=' );
//            }

//            if (isset($_GET['nc_type']) && $_GET['nc_type'] != 'all') {
//                $query->set( 'meta_query', array(
//                    'relation' => 'AND',
//                    array(
//                        'key'     => 'email_type',
//                        'value'   => 'template',
//                        'compare' => '='
//                    ),
//                    array(
//                        'key'     => 'template_type',
//                        'value'   => $_GET['type'],
//                        'compare' => '='
//                    )
//                ));
//            } else {
//                $query->set( 'meta_key', 'email_type' );
//                $query->set( 'meta_value', 'template' );
//                $query->set( 'meta_compare', '=' );
//            }
        }

        return $query;
    }
    
    public function delete_email(){
        if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX )) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong';                
            echo json_encode($return);
            exit;
        }
        
        $data = $_POST;

        if(!empty($data['id'])) {
            $email_id = (int) $data['id'];
            wp_delete_post($email_id);
            $return['status'] = 'success';
            $return['data']   = 'Successfully deleted.';                
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data']   = 'you can not delete this.';                
            echo json_encode($return);
        }
        exit();
    }
    
    public static function email_reminder_for_participants(){
        
        $number         = 999999999;
        $current_year           = date('Y',time());
        $current_month_session  = date('m',time());
        if($current_month_session == 12){
            $next_year          = $current_year+1;
        }else{
            $next_year          = $current_year;
        }
        $next_month_session     = date('m',strtotime('next month'));
        $current_month          = date('F',time());        
        $next_month             = date('F',strtotime('next month'));
        
        // Code for session reminders for participants
        
        $session_args = array(
            'post_type'         => 'event',
            'post_status'       => 'publish', 
            'posts_per_page'    => -1,      
            'orderby'           => 'meta_value_num',
            'meta_key'          => 'start_date',
            'order'             => 'DESC',
            'meta_query'        => array(
                'relation'      => 'AND', 
                array(
                    'key'       => 'event_type',
                    'value'     => 'Repeat',
                    'compare'   => '!='
                ),
                array(
                    'key'       => 'start_date',
                    'value'     => date('Ymd',strtotime('01-'.$current_month_session.'-'.$current_year)),
                    'compare'   => '>='
                ),
                array(
                    'key'       => 'start_date',
                    'value'     => date('Ymd',strtotime('31-'.$next_month_session.'-'.$next_year)),
                    'compare'   => '<='
                )
            )
        );
        
        $sessions   = new WP_Query($session_args);           
        $months     = array('01' => 'january', '02' => 'february', '03' => 'march', '04' => 'april', 
                      '05' => 'may', '06' => 'june', '07' => 'july', '08' => 'august', '09' => 'september',
                      '10' => 'october', '11' => 'november', '12' => 'december',1 => 'january', 
                        2 => 'february', 3 => 'march', 4 => 'april', 5 => 'may', 6 => 'june', 
                        7 => 'july', 8 => 'august', 9 => 'september', 10 => 'october', 
                        11 => 'november', 12 => 'december');    

        foreach($sessions->posts as $session){
            $seesion_month  = $months[get_post_meta($session->ID,'month',true)];
            if(!empty($seesion_month)){                
                $groups = get_post_meta($session->ID,'groups');                
                if(!empty($groups)){
                    $participants   = array();
                    foreach ($groups as $group) {
                        $participants = array_merge($participants, get_group_users($group));
                    }                    
                    if(!empty($participants)){                        
                        foreach($participants as $participant){
                            $session_reminder_status    = get_user_meta($participant,'session_reminder_mail',true);
                            
                            $template_id            = get_field("{$seesion_month}_training_notification", 'option');                     
                            $days                   = (int)get_field('days',$template_id);
                            $session_reminder_date  = date('d/m/Y',strtotime(get_post_meta($session->ID,'start_date',true))-(24*60*60*$days));
                            $start_date             = date('d F, Y',strtotime(get_post_meta($session->ID,'start_date',true)));
                            $time                   = date('g:i a',strtotime(get_post_meta($session->ID,'start_time',true))).' - '.date('g:i a',strtotime(get_post_meta($session->ID,'end_time',true)));
                            $address                = get_post_meta($session->ID,'address',true);

                            $user_data['template_id']   = $template_id;
                            $user_data['venue']         = $address;
                            $user_data['time']          = $start_date.' '.$time;
                            $user_data['user_id']       = $participant;
                            if(($session_reminder_date == date('d/m/Y',time())) && (!$session_reminder_status)){                                

                                do_action('session_reminder',$user_data);
                            }
                        }
                    }
                }
            }
        }
       
        $participant_user_args = array(
            'role' => 'contributor',
            'number' => $number
        );
        
        $participant_users  = new WP_User_Query($participant_user_args);
       
        // Code for action plan, result , mid assessment and pre assessment reminders for participants.
        
        foreach($participant_users->results as $participant){            
            $bpm_obj        = new BPM($participant->ID);                         
            foreach($bpm_obj->months[$current_month] as $key => $module){                               
                if(isset($bpm_obj->months[$current_month]['event']) && !empty($bpm_obj->months[$current_month]['event'])){
                    if(check_module_status_completed($bpm_obj->months[$current_month]['event'],$participant->ID)){
                        $current_session_date           = date('Y-m-d',strtotime(get_post_meta($bpm_obj->months[$current_month]['event'],'start_date',true))+(7*24*60*60));                        
                        $next_session_date              = date('Y-m-d',strtotime(get_post_meta($bpm_obj->months[$next_month]['event'],'start_date',true))-(7*24*60*60));
                        $action_plan_mail_sent_status   = get_user_meta($participant->ID,'action_plan_reminder_mail',true);
                        $result_mail_sent_status        = get_user_meta($participant->ID,'result_reminder_mail',true);

                        if(($current_session_date  == date('Y-m-d',time())) && (!$action_plan_mail_sent_status)){
                            if(!in_array($key,array('event','feedback'))){
                                foreach($module as $id){
                                    $tag    = get_field('tag',$id);
                                    if($tag == 'Action Plan'){
                                        if(!check_module_status_completed($id,$participant->ID)){                                               
                                            do_action('action_plan_reminder',$participant->ID);
                                        }                                    
                                    }
                                }
                            }
                        }    

                        if(($next_session_date == date('Y-m-d',time())) && (!$result_mail_sent_status)){
                            if(!in_array($key,array('event','feedback'))){   
                                foreach($module as $id){
                                    $tag    = get_field('tag',$id);
                                    if($tag == 'Results'){
                                        if(!check_module_status_completed($id,$participant->ID)){                                             
                                            do_action('result_reminder',$participant->ID);
                                        }                                    
                                    }
                                }
                            }
                        }
                    }
                }                    
            }

            $others                             = $bpm_obj->others;           
            $mid_assessment_date                = date('Y-m-d',(strtotime('+6 month',strtotime(str_replace('/','-', $bpm_obj->group['start'])))));
            $final_assessment_date              = date('Y-m-d',(strtotime('+12 month',strtotime(str_replace('/','-', $bpm_obj->group['start'])))));
            $mid_assessment_mail_sent_status    = get_user_meta($participant->ID,'mid_assessment_reminder_mail',true);
            $final_assessment_mail_sent_status  = get_user_meta($participant->ID,'final_assessment_reminder_mail',true);

            if((!check_module_status_completed($others[1],$participant->ID)) && ($mid_assessment_date == date('Y-m-d',time())) && (!$mid_assessment_mail_sent_status)){
                $data['user_id']    = $participant->ID;
                $data['module_id']  = $others[1];                    
                do_action('mid_assessment_reminder',$data);                
            }

            if((!check_module_status_completed($others[2],$participant->ID)) && ($final_assessment_date == date('Y-m-d',time())) && (!$final_assessment_mail_sent_status)){
                $data['user_id']    = $participant->ID;
                $data['module_id']  = $others[2];                   
                do_action('final_assessment_reminder',$data);
            }       
        }
        
    } 
    
    public static function email_for_ceos(){
      
        $client_args = array(
            'post_type' => 'client',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'number' => 10
        );
        
        $clients    = get_posts($client_args);
        
        foreach($clients as $client){
            $ceo_report_status  = get_post_meta($client->ID,'ceo_report_mail',true);            
            if((!$ceo_report_status) && (date('Y-m-t',time()) == date('Y-m-d',time()))){               
                $data               = get_ceo_report_html($client);
                $data['client_id']  = $client->ID;                
                $client_user_args = array(
                    'role'          => 'teacher',
                    'number'        => 10,
                    'meta_query'    => array(
                        'relation' => 'AND',
                        array(
                            'key' => 'client',
                            'value' => $client->ID
                        )
                    )
                );                
                $client_users = get_users($client_user_args);                
                foreach($client_users as $client_rep){
                    $data['client_rep'] = $client_rep->ID;    
                    $client_report_status  = get_user_meta($client_rep->ID,'ceo_report_mail',true);  
                    if(!$client_report_status){
                        do_action('ceo_report_mail',$data);
                    }                    
                }                
            }            
        }
      
        $session_ceo_args = array(
            'post_type'         => 'event',
            'post_status'       => 'publish', 
            'posts_per_page'    => -1,      
            'orderby'           => 'meta_value_num',
            'meta_key'          => 'start_date',
            'order'             => 'DESC',
            'meta_query'        => array(
                'relation'      => 'AND', 
                array(
                    'key'       => 'event_type',
                    'value'     => 'Repeat',
                    'compare'   => '!='
                ),                
                array(
                    'key'       => 'start_date',
                    'value'     => date('Ymd',strtotime('Yesterday')),
                    'compare'   => '=='
                )
            )
        );
        
        $months     = array('01' => 'january', '02' => 'february', '03' => 'march', '04' => 'april', 
                      '05' => 'may', '06' => 'june', '07' => 'july', '08' => 'august', '09' => 'september',
                      '10' => 'october', '11' => 'november', '12' => 'december',1 => 'january', 
                        2 => 'february', 3 => 'march', 4 => 'april', 5 => 'may', 6 => 'june', 
                        7 => 'july', 8 => 'august', 9 => 'september', 10 => 'october', 
                        11 => 'november', 12 => 'december');    
        
        $ceo_sessions   = new WP_Query($session_ceo_args);  
        
        $footer_html    = '<table border="0" cellpadding="6">';
        $footer_html   .= '<tr style="height:40px;background-color:#212240;width:100%;margin-top:20px;">';
        $footer_html   .= '<td style="text-align: left; color: #fff;font-size: 12px;width:100%;">Report Generated at '.date('Y-m-d H:i',time()).'.<br/>Copyright 2016, Crestcom International, LLC. All rights reserved.</td>';           
        $footer_html   .= '</tr>';
        $footer_html   .= '</table>';
        
        foreach($ceo_sessions->posts as $session){
            $session_date       = date('d-m-Y',strtotime(get_post_meta($session->ID,'start_date',true))+24*60*60);
            $session_month      = $months[get_post_meta($session->ID,'month',true)];
            $session_end_time   = date('g:i a',strtotime(get_post_meta($session->ID,'end_time',true)));
            $session_group      = get_post_meta($session->ID,'groups');
            $client_ids         = array();
            $users              = array();
            $number             = 999999999;
            
            foreach($session_group as $group){
                $client_ids[]    = get_post_meta($group,'client',true);
            }
            foreach($client_ids as $client){
                $data['groups'] = $session_group;
                
                $client_user_args = array(
                    'role'          => 'teacher',
                    'number'        => $number,
                    'meta_query'    => array(                         
                        array(
                            'key'   => 'client',
                            'value' => $client
                        )
                    )
                );
                $client_users = new WP_User_Query($client_user_args);
                
                foreach($client_users->results as $client_user){
                    $data['client']         = $client_user;
                    $data['client_id']      = $client;
                    $data['template_id']    = get_field("{$session_month}_ceo_summary_notification", 'option');                    
                    $data['html']           = '';
                    
                    $html   = '';
                    $html  .= '<table border="0" cellpadding="6" cellspacing="0">';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">Session:</th>';
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.$session->post_title.'</th>';
                    $html  .= '</tr>';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">When :</th>';
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.get_event_date($session->ID).'</th>';
                    $html  .= '</tr>';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">Venue :</th>';
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.get_post_meta($session->ID,'address',true).'</th>';
                    $html  .= '</tr>';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">Franchisee :</th>';
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.get_franchisee_name_by_id(get_field('franchisee',$session->ID)).'</th>';
                    $html  .= '</tr>';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">Facilitator :</th>';
                    
                    $facilitators  = get_post_meta( $session->ID, 'facilitator'); 
                    $facilitator_names  = array();
                    foreach($facilitators as $facilitator){
                        $facilitator_detail     = get_userdata($facilitator);
                        if(!empty($facilitator)){
                            $facilitator_names[]    = $facilitator_detail->first_name.' '.$facilitator_detail->last_name;
                        }
                    }
                    if(count($facilitator_names) > 1){
                        $event_facilitator  = implode(' , ', $facilitator_names);
                    }else{
                        $event_facilitator  = $facilitator_names[0];
                    }
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.$event_facilitator.'</th>';
                    $html  .= '</tr>';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">Group :</th>';
                    $group_names    = array();
                    if(!empty($session_group)){
                        if(is_array($session_group)){
                            foreach($session_group as $group){
                                $group_names[]  = get_group_name($group);
                            }
                        }else if(!is_array($session_group)){
                            $group_names[]  = get_group_name($session_group);
                        }                                           
                        $event_group  = implode(' , ',$group_names);
                    }else{
                        $event_group  = 'No Group Assigned.';
                    }
                    
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.$event_group.'</th>';
                    $html  .= '</tr>';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">Course :</th>';
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.get_post_field('post_title',$session->post_parent).'</th>';
                    $html  .= '</tr>';
                    $html  .= '<tr>';
                    $html  .= '<th style="width:25%;text-align:left;font-size:10px;">Month :</th>';
                    $html  .= '<th style="width:75%;text-align:left;font-size:10px;">'.ucfirst($session_month).'</th>';
                    $html  .= '</tr>';
                    $html  .= '</table>';
                    $html  .= '<div style="height:40px;"></div>';
                    $html  .= '<table border="1" cellpadding="7" cellspacing="0">';
                    $html  .= '<tr>';
                    $html  .= '<th style="font-size:10px;width:33.33%;text-align: center;">Name</th>';
                    $html  .= '<th style="font-size:10px;width:33.33%;text-align: center;">Email</th>';
                    $html  .= '<th style="font-size:10px;width:33.33%;text-align: center;">Attendance</th>';
                    $html  .= '</tr>';
                    
                    $grp_users = array();       
                    if(is_array($session_group)){
                        foreach($session_group as $group) {
                            # Get group Users
                            $grp_user = Group::get_users_by_group($group, 'contributor');

                            $grp_users = array_merge($grp_users,$grp_user);
                        }
                    }else{
                         $grp_users = Group::get_users_by_group($session_group, 'contributor');
                    }

                    $grp_users  = array_unique($grp_users);  
                    $attendance = array('' => ' --- ','attended' => 'Attended', 'not-attended' => 'Not Attended');
                    if (is_array($grp_users) && count($grp_users) > 0) {                                                         
                        if (have_rows('attendance',$session->ID)) {
                            while (have_rows('attendance',$session->ID)) {
                                the_row();
                                $person = get_sub_field('user',$session->ID);
                                $person_id = $person['ID'];
                                if (($key = array_search($person_id, $grp_users)) !== false) {
                                    unset($grp_users[$key]);
                                }
                                $user = get_userdata($person_id);
                                if ($user) {
                                    $html  .= '<tr>';
                                    $html  .= '<th style="font-size:10px;width:33.33%;text-align: center;">'.$user->first_name . ' ' . $user->last_name .'</th>';
                                    $html  .= '<th style="font-size:10px;width:33.33%;text-align: center;">'.$user->user_email.'</th>';
                                    $html  .= '<th style="font-size:10px;width:33.33%;text-align: center;">'.$attendance[get_sub_field('status',$session->ID)].'</th>';
                                    $html  .= '</tr>';
                                }
                            }
                        }
                    }
                    
                    $html  .= '</table>';
                    $html  .= '<div style="height:40px;"></div>';
                    $html  .= $footer_html;
                    
                    $data['html']       = $html;     
                    $data['file_name']  = 'Session Attendance.pdf';   
                    $session_summary_mail   = get_user_meta($client_user->ID,'session_summary_mail',true);                                            
                    if(!$session_summary_mail){
                        do_action('ceo_session_summary',$data);
                    }
                }
            }     
        }
        
    }
}

new Email();

function get_ceo_report_html($client){
    
    $participants   = array();
    $groups         = get_groups_by_client($client->ID);
    
    foreach ($groups as $group) {
        $participants = array_merge($participants, get_group_users($group['id']));
    }
    
    array_unique($participants);
    $user_args  = array(
        'role'      => 'contributor',
        'include'   => $participants,  
        'meta_key'  => 'last_name',
        'orderby'   => 'meta_value',
        'order'     => 'ASC'
    );

    $participants_details   = get_users($user_args);
    
    $total_ceo = array(
        'attendance'            => 0,
        'action_plan'           => 0,
        'result'                => 0,
        'revenue'               => 0,
        'costsaving'            => 0,
        'productivity'          => 0,
        'customersatisfaction'  => 0,
        'innovation'            => 0,
        'riskmanage'            => 0,
        'attitudinal'           => 0
    );
    
    $ceo    = get_userdata(get_post_meta($client->ID,'client_owner',true));  
    
    $footer_html    = '<table border="0" cellpadding="6">';
    $footer_html   .= '<tr style="height:40px;background-color:#212240;width:100%;margin-top:20px;">';
    $footer_html   .= '<td style="text-align: left; color: #fff;font-size: 12px;width:75%;">Report Generated at '.date('Y-m-d H:i',time()).'.<br/>Copyright 2016, Crestcom International, LLC. All rights reserved.</td>';   
    $footer_html   .= '<td style="text-align: right;color: #fff;font-size: 8px;width:25%;">Powered By <img src="'.$image.'" style="width:40px;height:18px;vertical-align:bottom;" /></td>';                
    $footer_html   .= '</tr>';
    $footer_html   .= '</table>';
    
    $ceo_report_html     = '<table border="0" cellspacing="0" cellpadding="5">';
    $ceo_report_html    .= '<tr>';
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:25%;">Franchise:</th>';    
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:75%;">'.get_post_field('post_title',get_post_meta($client->ID,'franchisee',true)).'</th>';    
    $ceo_report_html    .= '</tr>';
    $ceo_report_html    .= '<tr>';
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:25%;">Company Name:</th>';    
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:75%;">'.$client->post_title.'</th>';    
    $ceo_report_html    .= '</tr>';
    $ceo_report_html    .= '<tr>';
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:25%;">CEO Name:</th>';    
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:75%;">'.$ceo->first_name.' '.$ceo->last_name.'</th>';       
    $ceo_report_html    .= '</tr>';
    $ceo_report_html    .= '<tr>';
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:25%;">Report Date:</th>';    
    $ceo_report_html    .= '<th style="font-size:12px;text-align:left;width:75%;">'.date('F d, Y',time()).'</th>';       
    $ceo_report_html    .= '</tr>';
    $ceo_report_html    .= '</table>';
    
    $ceo_report_html    .= '<div style="height:20px;"></div>';
    
    $ceo_report_html    .= '<table border="0" cellspacing="0" cellpadding="7">';
    $ceo_report_html    .= '<tr style="height:40px;background-color:#212240;color:#fff;">';
    $ceo_report_html    .= '<th style="font-size:8px;width:23%;text-align: center;">Name of Participant</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Attendance <br/>(%)</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:6%;text-align: center;">Action Plan Submitted</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:7%;text-align: center;">Results Submitted</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Revenue<br/>($)</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Cost Saving<br/>($)</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Productivity<br/>(Hours)</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Customer Satisfaction<br/>(%)</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Innovation<br/>(# of New Ideas)</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Risk Mitigation<br/>(# of Risks Averted)</th>';
    $ceo_report_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Attitudinal Metric Increase (%)</th>';   
    $ceo_report_html    .= '</tr>';
    
    $count              = 1;
    foreach($participants_details as $participant){   
        $participant_month_html = '';   
        $total_participant  = array(    
            'revenue'               => 0,
            'costsaving'            => 0,
            'productivity'          => 0,
            'customersatisfaction'  => 0,
            'innovation'            => 0,
            'riskmanage'            => 0,
            'attitudinal'           => 0
        );
       
        if(!empty($participant)){
            $report_details = ceo_report_details($participant->ID);                                   
            $name           = $participant->first_name.' '.$participant->last_name;
            $bpm_obj        = new BPM($participant->ID);
            
            $color  = ($count%2 == 1)?'ccd0d3':'ffffff';
            
            $attendance          = ($report_details['marked_sessions'] == 0)?0:round(((1-((int)$report_details['not-attended']/12))*100),2);
            
            $ceo_report_html    .= '<tr style="background-color:#'.$color.';">';            
            $ceo_report_html    .= '<td style="font-size:8px;width:23%;text-align: center;">'.$name . ' <br/>( '.$participant->user_login.' )'.'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.$attendance.'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:6%;text-align: center;">'.$report_details['action_plan'].'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:7%;text-align: center;">'.$report_details['result'].'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($report_details['revenue']).'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($report_details['costsaving']).'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($report_details['productivity']).'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($report_details['customersatisfaction']).'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($report_details['innovation']).'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($report_details['riskmanage']).'</td>';
            $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($report_details['attitudinal']).'</td>';   
            $ceo_report_html    .= '</tr>';
            
            $total_ceo['attendance']            += $attendance;
            $total_ceo['action_plan']           += $report_details['action_plan'];
            $total_ceo['result']                += $report_details['result'];
            $total_ceo['revenue']               += $report_details['revenue'];
            $total_ceo['costsaving']            += $report_details['costsaving'];
            $total_ceo['productivity']          += $report_details['productivity'];
            $total_ceo['customersatisfaction']  += $report_details['customersatisfaction'];
            $total_ceo['innovation']            += $report_details['innovation'];
            $total_ceo['riskmanage']            += $report_details['riskmanage'];
            $total_ceo['attitudinal']           += $report_details['attitudinal'];
            
            $participant_html    = '<table border="1" cellspacing="0" cellpadding="5" style="margin-bottom:20px;">';
            $participant_html   .= '<tr>';
            $participant_html   .= '<td>Name of participant</td>';
            $participant_html   .= '<td>Attendance</td>';
            $participant_html   .= '<td>Completion Date</td>';
            $participant_html   .= '</tr>';
            $participant_html   .= '<tr>';
            $participant_html   .= '<td>'.$name.'</td>';
            $participant_html   .= '<td>'.$attendance.'</td>';
            $participant_html   .= '<td>'.date('F d, Y',strtotime(str_replace('/', '-', $bpm_obj->group['end']))).'</td>';
            $participant_html   .= '</tr>';
            $participant_html   .= '</table>';
           
            
            $participant_html    .= '<div style="height:20px;"></div>';
            
            $participant_html    .= '<table border="0" cellspacing="0" cellpadding="7" style="width:100%;margin-top:20px;">';
            $participant_html    .= '<tr style="height:40px;background-color:#212240;color:#fff;">';
            $participant_html    .= '<th style="font-size:8px;width:28%;text-align: center;">Module Name</th>';
            $participant_html    .= '<th style="font-size:8px;width:10%;text-align: center;">Action Plan</th>';
            $participant_html    .= '<th style="font-size:8px;width:10%;text-align: center;">Results</th>';            
            $participant_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Revenue<br/>($)</th>';
            $participant_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Cost Saving<br/>($)</th>';
            $participant_html    .= '<th style="font-size:8px;width:8%;text-align: center;">Productivity<br/>(Hours)</th>';
            $participant_html    .= '<th style="font-size:8px;width:7%;text-align: center;">Customer Satisfaction<br/>(%)</th>';
            $participant_html    .= '<th style="font-size:8px;width:7%;text-align: center;">Innovation<br/>(# of New Ideas)</th>';
            $participant_html    .= '<th style="font-size:8px;width:7%;text-align: center;">Risk Mitigation<br/>(# of Risks Averted)</th>';
            $participant_html    .= '<th style="font-size:8px;width:7%;text-align: center;">Attitudinal Metric Increase (%)</th>';   
            $participant_html    .= '</tr>';
            
            $obj_count  = 1;
            
            foreach($bpm_obj->months as $month => $modules){
                if(isset($modules) && !empty($modules)){
                    foreach($modules as $name => $module) {
                        if(!in_array($name,array('event','feedback'))) {
                            
                             foreach($module as $id){
                                $tag = get_field('tag',$id); 
                                if($tag == 'Action Plan'){                               
                                    $action_plan = get_user_meta($participant->ID,'submission_plan_'.$id,true);                                
                                    if($action_plan){
                                         $submissions       = get_submission_by_type('plan',$id,$participant->ID); 
                                         $idea              = get_post_meta($action_plan, 'p_action_plan_idea', true);
                                         $action_plan_idea  = get_excerpt($idea,0,20,get_permalink($submissions[0]->ID));                                                
                                     }else{   
                                         $input  = false;
                                         $action_plan_idea  = '<span class="error-message">Not submitted.</span>';
                                     }
                                }                           
                            } 
                            
                            foreach($module as $id){
                                $tag = get_field('tag',$id); 
                                if($tag == 'Results'){
                                    $result = get_user_meta($participant->ID,'submission_result_'.$id,true); 
                                    if($result){
                                         $submissions   = get_submission_by_type('result',$id,$participant->ID); 
                                         $idea          = get_post_meta($result, 'p_action_plan_idea', true);
                                         $result_idea   = get_excerpt($idea,0,20,get_permalink($submissions[0]->ID));                                                
                                     }else{   
                                         $input  = false;
                                         $result_idea   =  '<span class="error-message">Not submitted.</span>';
                                     }
                                }                           
                            }
                            
                            $revenue_array  = explode(',',get_post_meta( $result, 'i_revenue', true));
                            $revenue = '';
                            foreach($revenue_array as $revenue_a){
                                $revenue .= $revenue_a;
                            }      
                            $revenue    = (int) $revenue;
                            $costsaving_array  = explode(',',get_post_meta( $result, 'i_costsaving', true));
                            $costsaving = '';
                            foreach($costsaving_array as $costsaving_a){
                                $costsaving .= $costsaving_a;
                            }       
                            $costsaving = (int) $costsaving;
                            $productivity_array  = explode(',',get_post_meta( $result, 'i_productivity', true));
                            $productivity = '';
                            foreach($productivity_array as $productivity_a){
                                $productivity .= $productivity_a;
                            }      
                            $productivity   = (int) $productivity;
                            $customer_satisfaction  = get_post_meta( $result, 'i_customersatisfaction', true);
                            $innovation  = get_post_meta( $result, 'i_innovation', true);
                            $riskmanage  = get_post_meta( $result, 'i_riskmanage', true);
                            $attitudinal = get_post_meta( $result, 'i_attitudinal', true);
                            
                            $total_participant['revenue']               += $revenue;
                            $total_participant['costsaving']            += $costsaving;
                            $total_participant['productivity']          += $productivity;
                            $total_participant['customersatisfaction']  += $customer_satisfaction;
                            $total_participant['innovation']            += $innovation;
                            $total_participant['riskmanage']            += $riskmanage;
                            $total_participant['attitudinal']           += $attitudinal;
                            
                            $part_color  = ($obj_count%2 == 1)?'ccd0d3':'ffffff';            
                            
                            if($action_plan){                                              
                                $participant_month_html    .= '<tr style="background-color:#'.$part_color.';">';
                                $participant_month_html    .= '<td style="font-size:8px;width:28%;text-align: left;">'.$name.'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:10%;text-align: center;">'.$action_plan_idea.'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:10%;text-align: center;">'.$result_idea.'</td>';            
                                $participant_month_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($revenue).'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($costsaving).'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:8%;text-align: center;">'.number_format($productivity).'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:7%;text-align: center;">'.number_format($customer_satisfaction).'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:7%;text-align: center;">'.number_format($innovation).'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:7%;text-align: center;">'.number_format($riskmanage).'</td>';
                                $participant_month_html    .= '<td style="font-size:8px;width:7%;text-align: center;">'.number_format($attitudinal).'</td>';   
                                $participant_month_html    .= '</tr>';
                                
                                $obj_count++;
                            }
                        }
                    }
                }
            }
                            
            $part_color  = ($obj_count%2 == 1)?'ccd0d3':'ffffff';    
            
            $participant_html    .= $participant_month_html;
            $participant_html    .= '<tr style="background-color:#'.$part_color.';">';
            $participant_html    .= '<th style="font-size:8px;width:48%;text-align: center;font-weight:bold;">Total</th>';                  
            $participant_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_participant['revenue']).'</td>';
            $participant_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_participant['costsaving']).'</td>';
            $participant_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_participant['productivity']).'</td>';
            $participant_html    .= '<td style="font-size:8px;width:7%;text-align: center;font-weight:bold;">'.number_format($total_participant['customersatisfaction']).'</td>';
            $participant_html    .= '<td style="font-size:8px;width:7%;text-align: center;font-weight:bold;">'.number_format($total_participant['innovation']).'</td>';
            $participant_html    .= '<td style="font-size:8px;width:7%;text-align: center;font-weight:bold;">'.number_format($total_participant['riskmanage']).'</td>';
            $participant_html    .= '<td style="font-size:8px;width:7%;text-align: center;font-weight:bold;">'.number_format($total_participant['attitudinal']).'</td>';   
            $participant_html    .= '</tr>';
            $participant_html    .= '</table>';
            
            $participant_html    .= '<div style="height:20px;"></div>';
            
            $participant_html    .= $footer_html;
            
            if(!empty($participant_month_html)){
                $data['participant_details'][]   = $participant_html;
            }
            
            $count++;
        }
    }
    
    $color  = ($count%2 == 1)?'ccd0d3':'ffffff';
    
    $total_attendance    = ($total_ceo['attendance'] == 0)?0:round(($total_ceo['attendance']/count($participants_details)),2);
    
    $ceo_report_html    .= '<tr style="background-color:#'.$color.';">';            
    $ceo_report_html    .= '<th style="font-size:8px;width:23%;text-align: center;font-weight:bold;">Total</th>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.$total_attendance.'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:6%;text-align: center;font-weight:bold;">'.$total_ceo['action_plan'].'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:7%;text-align: center;font-weight:bold;">'.$total_ceo['result'].'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_ceo['revenue']).'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_ceo['costsaving']).'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_ceo['productivity']).'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_ceo['customersatisfaction']).'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_ceo['innovation']).'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_ceo['riskmanage']).'</td>';
    $ceo_report_html    .= '<td style="font-size:8px;width:8%;text-align: center;font-weight:bold;">'.number_format($total_ceo['attitudinal']).'</td>';   
    $ceo_report_html    .= '</tr>';
    
    $ceo_report_html    .= '</table>';
    
    $ceo_report_html    .= '<div style="height:20px;"></div>';
    
    $ceo_report_html    .= $footer_html;
    
    $data['client_details']['participant']   = $ceo_report_html;
    $data['file_name']                       = 'CEO Report '.date('Y-m-d',time()).'.pdf';
    
    return $data;
}