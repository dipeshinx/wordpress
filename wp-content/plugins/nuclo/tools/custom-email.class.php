<?php

/**
 * @package  Custom_Email
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Custom_Email {
	
	/**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {

    	add_action( 'register_new_franchisee',              array( $this, 'new_franchisee_notification' ),          1, 1);
    	add_action( 'register_new_client',                  array( $this, 'new_client_notification' ),              1, 1);
    	add_action( 'register_new_facilitator',             array( $this, 'new_facilitator_notification' ),         1, 1);
        add_action( 'register_new_hq_admin',                array( $this, 'new_hq_admin_notification' ),            1, 1);
        add_action( 'register_new_participant',             array( $this, 'new_participant_notification'));
        add_action( 'result_supervisor_email',              array( $this, 'result_supervisor_mail' ),               1, 1);
        add_action( 'participant_pre_assesment_mail',       array( $this, 'participant_pre_assesment_mail' ),       1, 1);
        add_action( 'session_feedback_action_plan_alert',   array( $this, 'session_feedback_action_plan_alert' ),   1, 1);
        add_action( 'action_plan_reminder',                 array( $this, 'action_plan_reminder' ),   1, 1);
        add_action( 'result_reminder',                      array( $this, 'result_reminder' ),   1, 1);
        add_action( 'mid_assessment_reminder',              array( $this, 'mid_assessment_reminder' ),   1, 1);
        add_action( 'final_assessment_reminder',            array( $this, 'final_assessment_reminder' ),   1, 1);
        add_action( 'participant_mid_assesment_mail',       array( $this, 'participant_mid_assesment_mail' ),   1, 1);
        add_action( 'participant_final_assesment_mail',     array( $this, 'participant_final_assesment_mail' ),   1, 1);
        add_action( 'session_reminder',                     array( $this, 'session_reminder' ),   1, 1);
        add_action( 'ceo_session_summary',                  array( $this, 'ceo_session_summary' ),   1, 1);
        add_action( 'ceo_report_mail',                      array( $this, 'ceo_report_mail' ),   1, 1);
    }
    
    public function new_hq_admin_notification($data) {
		
            $post_id =  get_field('new_hq_admin_notification', 'option');
            
            if(!empty($post_id)) {

                $email          = get_post($post_id);
                $user_email     = $data['user_email'];
                $user_password  = $data['password'];
                $hq_admin_name  = $data['first_name'].' '.$data['last_name'];
                $_message 	    = $email->post_content;
                $title 	    = get_field('subject', $email->ID );
                $user_id        = $data['user_id'];

                if(!empty($user_email) && !empty($user_password) && !empty($hq_admin_name)) {

                    $message = Nuclo::replace_vars( $_message, $user_id, array(
                            '%email%' 		=> $user_email,
                            '%password%' 	=> $user_password,
                            '%hq_admin_name%'   => $hq_admin_name,
                            '%url%' 		=> site_url( 'wp-login.php', 'login' )
                    ));

                    wp_mail( $user_email, $title, $message);
                }
            }
    }
	
    public function new_franchisee_notification($data) {

            $post_id =  get_field('new_franchisee_notification', 'option');

            if(!empty($post_id)) {

                $email              = get_post($post_id);
                $user_email 	= (isset($data['f_o_email']) && !empty($data['f_o_email']))?$data['f_o_email']:$data['user_email'];
                $user_password 	= $data['password'];
                $franchisee_name    = (isset($data['f_o_email']) && !empty($data['f_o_email']))?$data['f_o_fname'].' '.$data['f_o_lname']:$data['first_name'].' '.$data['last_name'];
                $_message 		= $email->post_content;
                $title 		= get_field('subject', $email->ID );
                $user_id            = $data['user_id'];
                $franchisee         = $data['fran_name'];

                if(!empty($user_email) && !empty($user_password) && !empty($franchisee_name)) {

                    $message = Nuclo::replace_vars( $_message, $user_id, array(
                            '%email%' 		=> $user_email,
                            '%password%' 	=> $user_password,
                            '%franchisee_name%' => $franchisee_name,
                            '%franchisee%'      => $franchisee,
                            '%url%'             => site_url( 'wp-login.php', 'login' )
                    )); 

                    wp_mail( $user_email, $title, $message);
                }
            }
    }

    public function new_client_notification($data) {

            $post_id =  get_field('new_client_notification', 'option');

            if(!empty($post_id)) {
                
                if(isset($data['individual_user']) && ($data['individual_user'])){
                    $user_email     = $data['user_email'];
                    $user_password  = $data['password'];
                    $client_name    = $data['first_name'].' '.$data['last_name'];
                    $user_id        = $data['user_id'];
                }else{
                    $user_email     = (isset($data['ceo_user_id']) && !empty($data['ceo_user_id']))?$data['ceo_email']:$data['c_o_email'];
                    $user_password  = (isset($data['ceo_user_id']) && !empty($data['ceo_user_id']))?$data['ceo_password']:$data['other_password'];
                    $client_name    = (isset($data['ceo_user_id']) && !empty($data['ceo_user_id']))?$data['ceo_fname'].' '.$data['ceo_lname']:$data['c_o_fname'].' '.$data['c_o_lname'];
                    $user_id        = (isset($data['ceo_user_id']) && !empty($data['ceo_user_id']))?$data['ceo_user_id']:$data['other_user_id'];
                }

                $email          = get_post($post_id);

                # Getting Franchisee Details
                $franchisee_id 		= get_user_meta($user_id, 'franchisee', true );
                $franchisee_name            = get_the_title($franchisee_id );
                $franchisee_owner           = get_post_meta($franchisee_id, 'franchise_owner', true );
                $owner_info 		= get_userdata($franchisee_owner);
                $owner_name 		= $owner_info->first_name . ' '. $owner_info->last_name;                    
                $_message 			= $email->post_content;
                $title 			= get_field('subject', $email->ID );

                if(!empty($user_email) && !empty($user_password) && !empty($client_name)) {

                    $message = Nuclo::replace_vars( $_message, $user_id, array(
                            '%email%' 		=> $user_email,
                            '%password%' 	=> $user_password,
                            '%client_name%'	=> $client_name,
                            '%url%' 		=> site_url( 'wp-login.php', 'login' ),
                            '%franchisee%'	=> $franchisee_name,
                            '%franchisee_name%' => $owner_name
                    ));

                    wp_mail( $user_email, $title, $message);
                }
            }
    }

    public function new_facilitator_notification($data) {
		
        $post_id =  get_field('new_facilitator_notification', 'option');

        if(!empty($post_id)) {

            $email              = get_post($post_id);
            $user_email         = $data['user_email'];
            $user_password      = $data['password'];
            $facilitator_name   = $data['first_name'].' '.$data['last_name'];
            $user_id            = $data['user_id'];

            # Getting Franchisee Details
            $franchisee_id 		= get_user_meta( $data['user_id'], 'franchisee', true );
            $franchisee_name	= get_the_title( $franchisee_id );
            $franchisee_owner 	= get_post_meta( $franchisee_id, 'franchise_owner', true );
            $owner_info 		= get_userdata($franchisee_owner);
            $owner_name 		= $owner_info->first_name . ' '. $owner_info->last_name;
            $_message 		= $email->post_content;
            $title 			= get_field('subject', $email->ID );

            if(!empty($user_email) && !empty($user_password) && !empty($facilitator_name)) {

                $message = Nuclo::replace_vars( $_message, $user_id, array(
                    '%email%' 		=> $user_email,
                    '%password%' 		=> $user_password,
                    '%facilitator_name%'	=> $facilitator_name,
                    '%url%' 		=> site_url( 'wp-login.php', 'login' ),
                    '%franchisee%'		=> $franchisee_name,
                    '%franchisee_name%'	=> $owner_name
                ));

                wp_mail( $user_email, $title, $message);
            }
        }
    }
        
    public function new_participant_notification($data){
        $post_id    = get_field('participant_notification', 'option');
        
        if(!empty($post_id)){            
            $email                  = get_post($post_id);
            $_message               = $email->post_content;
            $title                  = get_field('subject', $email->ID );

            if(!empty($data)) {

                    $message = Nuclo::replace_vars( $_message, $data['user_id'], array(                                
                        '%participant_name%'    => $data['name'],          
                        '%username%'            => $data['username'],   
                        '%password%'            => $data['password'],                        
                        '%franchisee_name%'     => $data['franchisee_name'],
                        '%franchisee%'          => $data['franchisee'],
                        '%franchisee_address%'  => $data['franchisee_address'],
                        '%franchisee_no%'       => $data['franchisee_no'],
                        '%event_venue%'         => $data['venue'],
                        '%event_time%'          => $data['time'],
                        '%url%' 		=> site_url( 'wp-login.php', 'login' ),
                    ));
                    
                    wp_mail( $data['user_email'], $title, $message);
            }
        }
    }
        
    public function result_supervisor_mail($data){
        $post_id =  get_field('action_plan_result_supervisor_mail', 'option');
        
        if(!empty($post_id)) {
               
                $participant_details    = get_userdata($data['user_id']);  
                $module_details         = get_post($data['measure_id']);
                $supervisor_email       = $participant_details->supervisor_mail;
                $supervisor_name        = $participant_details->supervisor_name;
                $email                  = get_post($post_id);
                $franchise_id           = get_post_meta(get_user_meta($data['user_id'], 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID ); 
                
                $plugin_dir  = str_replace('tools/','',plugin_dir_path(__FILE__));
        
                require_once($plugin_dir.'classes/thirdparty_lib/tcpdf/xtcpdf.php');
                
                $pdf = new XTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                
                $pdf->participant_name = $participant_details->first_name.' '.$participant_details->last_name.' ('.$participant_details->user_login.')';
                $pdf->module_name      = $module_details->post_title;
                $pdf->header_type      = 'supervisor_mail';
                
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

                // set auto page breaks
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                //// set font
                $pdf->SetFont('dejavusans', '', 8);

                $pdf->AddPage();
                
                $pdf->writeHTML($data['html'], true, false, true, false, '');
                
                $file_content = $pdf->Output($data['file_name'], 'S');      
                
                if(!empty($supervisor_name) && !empty($supervisor_email) && $participant_details) {
                    $attachment             = array();
                    $directory              = wp_upload_dir();
                    $filename               = $directory['path'].'/'.$data['file_name'];                    
                    $myfile                 = fopen($filename, "w") or die("Unable to open file!");
                    fwrite($myfile,$file_content);
                    fclose($myfile);                    
                    $attachment[]   = $filename;
                    
                    $subject        = Nuclo::replace_vars( $title , $data['user_id'],array(
                        '%type%'                    => $data['type'],
                        '%participant_name%'        => $participant_details->first_name.' '.$participant_details->last_name,                                
                    ));
                    
                    $message        = Nuclo::replace_vars( $_message, $data['user_id'], array(                                
                        '%supervisor_name%'         => $supervisor_name,
                        '%module_name%'             => $module_details->post_title,
                        '%participant_name%'        => $participant_details->first_name.' '.$participant_details->last_name.' ('.$participant_details->user_login.')',                                
                        '%url%'                     => site_url( 'wp-login.php', 'login' ),
                        '%type%'                    => $data['type'],
                        '%franchisee_name%'         => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                        '%franchisee%'              => $franchise_details->post_title,
                        '%franchisee_address%'      => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                        '%franchisee_no%'           => $franchise_details->franchisee_contact
                    ));
                   
                    wp_mail( $supervisor_email, $subject, $message , "" , $attachment);
                    unlink($filename);
                }
        }
    }
    
    public function participant_pre_assesment_mail($user_id){
        $post_id =  get_field('pre_assesment_submission', 'option');
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($user_id);                
                $franchise_id           = get_post_meta(get_user_meta($user_id, 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                

                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $user_id, array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact
                        ));
                        
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function session_feedback_action_plan_alert($user_id){
        $post_id =  get_field('session_feedback_and_action_plan_reminder', 'option');
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($user_id);                
                $franchise_id           = get_post_meta(get_user_meta($user_id, 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                

                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $user_id, array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact
                        ));
                        
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function action_plan_reminder($user_id){
        $post_id =  get_field('action_plan_reminder', 'option');
        
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($user_id);   
                $franchise_id           = get_post_meta(get_user_meta($user_id, 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                
                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $user_id, array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact
                        ));
                        
                        update_user_meta($participant_details->ID,'action_plan_reminder_mail','1');
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function result_reminder($user_id){
        $post_id =  get_field('result_reminder', 'option');
        
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($user_id);                
                $franchise_id           = get_post_meta(get_user_meta($user_id, 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                
                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $user_id, array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact
                        ));
                        
                        update_user_meta($participant_details->ID,'result_reminder_mail','1');
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function mid_assessment_reminder($data){
        $post_id =  get_field('mid_assessment_reminder', 'option');        
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($data['user_id']);                     
                $franchise_id           = get_post_meta(get_user_meta($data['user_id'], 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                $url                    = get_permalink($data['module_id']);
               
                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $data['user_id'], array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact,
                            '%url%'                 => $url
                        ));
                        
                        update_user_meta($participant_details->ID,'mid_assessment_reminder_mail','1');
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function final_assessment_reminder($data){
        $post_id =  get_field('final_assessment_reminder', 'option');
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($data['user_id']);                
                $franchise_id           = get_post_meta(get_user_meta($data['user_id'], 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                $url                    = get_permalink($data['module_id']);

                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $data['user_id'], array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact,
                            '%url%'                 => $url
                        ));
                        
                        update_user_meta($participant_details->ID,'final_assessment_reminder_mail','1');                        
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function participant_mid_assesment_mail($user_id){
        $post_id =  get_field('mid_assesment_submission', 'option');
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($user_id);                
                $franchise_id           = get_post_meta(get_user_meta($user_id, 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                

                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $user_id, array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact
                        ));
                       
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function participant_final_assesment_mail($user_id){
        $post_id =  get_field('final_assesment_submission', 'option');
        if(!empty($post_id)) {
            
                $participant_details    = get_userdata($user_id);                
                $franchise_id           = get_post_meta(get_user_meta($user_id, 'client', 'single'), 'franchisee', 'single');
                $franchise_details      = get_post($franchise_id);
                $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
                $franchisee_details     = get_userdata($franchisee_id); 
                $email                  = get_post($post_id);
                $_message               = $email->post_content;
                $title                  = get_field('subject', $email->ID );
                

                if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                        $message = Nuclo::replace_vars( $_message, $user_id, array(                                
                            '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                            '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                            '%franchisee%'          => $franchise_details->post_title,
                            '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                            '%franchisee_no%'       => $franchise_details->franchisee_contact
                        ));
                        
                        wp_mail( $participant_details->user_email, $title, $message);
                }
        }
    }
    
    public function session_reminder($data){
        $post_id =  $data['template_id'];
        if(!empty($post_id)) {
            
            $participant_details    = get_userdata($data['user_id']);                
            $franchise_id           = get_post_meta(get_user_meta($data['user_id'], 'client', 'single'), 'franchisee', 'single');
            $franchise_details      = get_post($franchise_id);
            $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
            $franchisee_details     = get_userdata($franchisee_id); 
            $email                  = get_post($post_id);
            $_message               = $email->post_content;
            $title                  = get_field('subject', $email->ID );
            $attachment_ids         = get_field('attachment',$email->ID);              
            $attachments            = array();

            if(!empty($attachment_ids)){
                foreach($attachment_ids as $attachment){
                    array_push($attachments, get_attached_file( $attachment['files']['ID'] ));
                }
            }                 
            if(!empty($franchise_details) && !empty($franchisee_details) && ($participant_details)) {

                $message = Nuclo::replace_vars( $_message, $data['user_id'], array(     
                    '%date_time%'           => $data['time'],
                    '%venue%'               => $data['venue'],
                    '%participant_name%'    => $participant_details->first_name.' '.$participant_details->last_name,          
                    '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                    '%franchisee%'          => $franchise_details->post_title,
                    '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                    '%franchisee_no%'       => $franchise_details->franchisee_contact
                ));
                update_user_meta($participant_details->ID,'session_reminder_mail','1');                        
                wp_mail( $participant_details->user_email, $title, $message, '' ,$attachments);
            }
        }
    }
    
    public function ceo_session_summary($data){
        $post_id    = $data['template_id'];
        
        if(!empty($post_id)) {
            
            $plugin_dir  = str_replace('tools/','',plugin_dir_path(__FILE__));
        
            require_once($plugin_dir.'classes/thirdparty_lib/tcpdf/xtcpdf.php');

            $pdf = new XTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            $pdf->header_type      = 'A4';

            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            //// set font
            $pdf->SetFont('dejavusans', '', 8);

            $pdf->AddPage();

            $pdf->writeHTML($data['html'], true, false, true, false, '');

            $file_content   = $pdf->Output($data['file_name'], 'S');
            
            $directory              = wp_upload_dir();
            $filename               = $directory['path'].'/'.$data['file_name'];                    
            $myfile                 = fopen($filename, "w") or die("Unable to open file!");
            fwrite($myfile,$file_content);
            fclose($myfile);                    
           
            
            $client_details         = $data['client'];             
            $franchise_id           = get_post_meta($data['client_id'], 'franchisee', 'single');
            $franchise_details      = get_post($franchise_id);            
            $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
            $franchisee_details     = get_userdata($franchisee_id); 
            $email                  = get_post($post_id);
            $_message               = $email->post_content;
            $title                  = get_field('subject', $email->ID );
            $attachment_ids         = get_field('attachment',$email->ID);              
            $attachments            = array($filename);

            if(!empty($attachment_ids)){
                foreach($attachment_ids as $attachment){
                    array_push($attachments, get_attached_file( $attachment['files']['ID'] ));
                }
            }                 
            
            if(!empty($franchise_details) && !empty($franchisee_details) && ($client_details)) {
                $message = Nuclo::replace_vars( $_message, $client_details->ID, array(                        
                    '%ceo_name%'            => $client_details->first_name.' '.$client_details->last_name,          
                    '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                    '%franchisee%'          => $franchise_details->post_title,
                    '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                    '%franchisee_no%'       => $franchise_details->franchisee_contact
                ));     
               
                update_user_meta($client_details->ID,'session_summary_mail','1');                        
                wp_mail( $client_details->user_email, $title, $message, '' ,$attachments);
                unlink($filename);
            }
        }
    }
    
    public function ceo_report_mail($data){
        $post_id =  get_field('ceo_report_mail', 'option');
        if(!empty($post_id)) {
            
            $plugin_dir  = str_replace('tools/','',plugin_dir_path(__FILE__));        
            require_once($plugin_dir.'classes/thirdparty_lib/tcpdf/xtcpdf.php');
            
            $pdf = new XTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A3', true, 'UTF-8', false);
            $pdf->header_type      = 'reports';

            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(6, PDF_MARGIN_TOP, 6);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            //// set font
            $pdf->SetFont('dejavusans', '', 8);

            $pdf->AddPage();
            
            if(isset($data['client_details']['participant']) && !empty($data['client_details']['participant'])){
                $html    = '<html>';
                $html   .= '<body>';
                $html   .= $data['client_details']['participant'];
                $html   .= '</body>';
                $html   .= '</html>';

                $pdf->writeHTML($html, true, false, true, false, '');
            }
            
            if(isset($data['participant_details']) && !empty($data['participant_details'])){
                foreach($data['participant_details'] as $participant_html){
                    $pdf->AddPage();
                    $pdf->writeHTML($participant_html, true, false, true, false, '');        
                }
            }
            
            $file_content   = $pdf->Output($data['file_name'], 'S');
            
            $attachments            = array();
            $directory              = wp_upload_dir();
            $filename               = $directory['path'].'/'.$data['file_name'];                    
            $myfile                 = fopen($filename, "w") or die("Unable to open file!");
            fwrite($myfile,$file_content);
            fclose($myfile);                    
            $attachments[]          = $filename;
            
            $client_details         = $data['client_rep'];             
            $franchise_id           = get_post_meta($data['client_id'], 'franchisee', 'single');
            $franchise_details      = get_post($franchise_id);            
            $franchisee_id          = get_post_meta($franchise_id, 'franchise_owner', 'single');
            $franchisee_details     = get_userdata($franchisee_id); 
            $email                  = get_post($post_id);
            $_message               = $email->post_content;
            $title                  = get_field('subject', $email->ID );
            
            if(!empty($franchise_details) && !empty($franchisee_details) && ($client_details)) {
                $message = Nuclo::replace_vars( $_message, $client_details->ID, array(                        
                    '%ceo_name%'            => $client_details->first_name.' '.$client_details->last_name,          
                    '%franchisee_name%'     => $franchisee_details->first_name.' '.$franchisee_details->last_name,
                    '%franchisee%'          => $franchise_details->post_title,
                    '%franchisee_address%'  => $franchise_details->address.','.$franchise_details->city.', '.nc_state_name($franchise_details->state).', '.nc_country_name($franchise_details->country),
                    '%franchisee_no%'       => $franchise_details->franchisee_contact
                ));     
               
                update_post_meta($data['client_id'],'ceo_report_mail',1);
                update_user_meta($client_details->ID,'ceo_report_mail',1);
                wp_mail( $client_details->user_email, $title, $message, '' ,$attachments);                
                unlink($filename);
            }
        }
    }
    
    
}

new Custom_Email();