<?php

/*
  Plugin Name: A NuCLO
  Plugin URI: http://allnuclo.com
  Description: Powerfull Learning Management System
  Author: Nuveda Pvt. Ltd.
  Version: 1.0.0
  Author URI: http://nuvedalearning.com
 */


/**
 * Class autoloader
 * the theme.
 * @since 1.0
 */
define('NUCLO_BASE', WP_PLUGIN_DIR . '/' . str_replace(basename(__FILE__), '', plugin_basename(__FILE__)));

/**
 * Nuclo basic class
 * @since 1.0
 */
class Nuclo {

    protected $files;

    /**
     * @since 1.0
     */
    public function __construct() {

        foreach ($this->fetch_directory(NUCLO_BASE . 'admin/') as $class_file) {
            if (file_exists($class_file)) {
                include $class_file;
            }
        }

        foreach ($this->fetch_directory(NUCLO_BASE . 'classes/') as $class_file) {
            if (file_exists($class_file)) {
                include $class_file;
            }
        }

        foreach ($this->fetch_directory(NUCLO_BASE . 'modules/') as $class_file) {
            if (file_exists($class_file)) {
                include $class_file;
            }
        }

        foreach ($this->fetch_directory(NUCLO_BASE . 'reports/') as $class_file) {
            if (file_exists($class_file)) {
                include $class_file;
            }
        }

        foreach ($this->fetch_directory(NUCLO_BASE . 'tools/') as $class_file) {
            if (file_exists($class_file)) {
                include $class_file;
            }
        }

        add_image_size('mini-size', 32, 32, array('center', 'center'));
        add_image_size('certi-size', 816, 1056);
        remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
        remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
        remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
        remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
        remove_action('wp_head', 'index_rel_link'); // index link
        remove_action('wp_head', 'parent_post_rel_link', 10, 0); // prev link
        remove_action('wp_head', 'start_post_rel_link', 10, 0); // start link
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
        remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        add_action('admin_head', array($this, 'admin_del_options'));
    }

    /**
     * @since 1.0
     */
    public static function replace_vars($input, $user_id = '', $replacements = array()) {
        $defaults = array(
            '%site_url%' => get_bloginfo('url'),
            '%user_ip%' => $_SERVER['REMOTE_ADDR']
        );
        $replacements = wp_parse_args($replacements, $defaults);

        // Get user data
        $user = false;
        if ($user_id)
            $user = get_user_by('id', $user_id);

        // Get all matches ($matches[0] will be '%value%'; $matches[1] will be 'value')
        preg_match_all('/%([a-zA-Z0-9-_]*)%/', $input, $matches);

        // Iterate through matches
        foreach ($matches[0] as $key => $match) {
            if (!isset($replacements[$match])) {
                if ($user && isset($user->{$matches[1][$key]})) // Replacement from WP_User object
                    $replacements[$match] = $user->{$matches[1][$key]};
                else
                    $replacements[$match] = get_bloginfo($matches[1][$key]); // Replacement from get_bloginfo()
            }
        }

        // Allow replacements to be filtered
        $replacements = apply_filters('tml_replace_vars', $replacements, $user_id);

        if (empty($replacements))
            return $input;

        // Get search values
        $search = array_keys($replacements);

        // Get replacement values
        $replace = array_values($replacements);

        return str_replace($search, $replace, $input);
    }

    /**
     * @since 1.0
     */
    private function fetch_directory($directory) {
        $this->files = array();
        if (function_exists('scandir')) :
            $lists = scandir($directory);
            foreach ($lists as $list) :
                if ($list != '.' && $list != '..') :
                    $extension = pathinfo($list, PATHINFO_EXTENSION);
                    if (!empty($extension)) :
                        array_push($this->files, $directory . $list);
                    endif;
                endif;
            endforeach;
        endif;
        return $this->files;
    }

    /**
     * @since 1.0
     */
    public function admin_del_options() {
        global $_wp_admin_css_colors;
        $_wp_admin_css_colors = 0;
    }

    /**
     * @since 1.0
     */
    public function wpsqt_theme_locations($possibleLocations) {
        return $possibleLocations;
    }

}

new Nuclo();

function nuclo_search_id($id, $array) {
    foreach ($array as $key => $val) {
        if ($val['value'] === $id) {
            return $key;
        }
    }
    return null;
}

/**
 * Generate dynamic selct box
 * @since 1.0
 */
function selectbox($name, $data, $default = '', $param = '', $class = '') {
    $out = '<select class="form-control ' . $class . '" name="' . $name . '"' . (!empty($param) ? ' ' . $param : '') . ">\n";

    foreach ($data as $key => $val) {
        $out.='<option value="' . $key . '"' . ($default == $key ? ' selected="selected"' : '') . '>';
        $out.=$val;
        $out.="</option>\n";
    }
    $out.="</select>\n";

    return $out;
}

/**
 * Get relative for static time 
 * i.e. 1 min ago, 1 hour ago
 * @since 1.0
 */
function relativeTime($time, $short = false) {
    if (!empty($time)) {
        $SECOND = 1;
        $MINUTE = 60 * $SECOND;
        $HOUR = 60 * $MINUTE;
        $DAY = 24 * $HOUR;
        $MONTH = 30 * $DAY;
        $before = time() - $time;

        if ($before < 0) {
            return "not yet";
        }

        if ($short) {
            if ($before < 1 * $MINUTE) {
                return ($before < 5) ? "just now" : $before . " ago";
            }

            if ($before < 2 * $MINUTE) {
                return "1m ago";
            }

            if ($before < 45 * $MINUTE) {
                return floor($before / 60) . "m ago";
            }

            if ($before < 90 * $MINUTE) {
                return "1h ago";
            }

            if ($before < 24 * $HOUR) {

                return floor($before / 60 / 60) . "h ago";
            }

            if ($before < 48 * $HOUR) {
                return "1d ago";
            }

            if ($before < 30 * $DAY) {
                return floor($before / 60 / 60 / 24) . "d ago";
            }


            if ($before < 12 * $MONTH) {
                $months = floor($before / 60 / 60 / 24 / 30);
                return $months <= 1 ? "1mo ago" : $months . "mo ago";
            } else {
                $years = floor($before / 60 / 60 / 24 / 30 / 12);
                return $years <= 1 ? "1y ago" : $years . "y ago";
            }
        }

        if ($before < 1 * $MINUTE) {
            return ($before <= 1) ? "just now" : $before . " seconds ago";
        }

        if ($before < 2 * $MINUTE) {
            return "a minute ago";
        }

        if ($before < 45 * $MINUTE) {
            return floor($before / 60) . " minutes ago";
        }

        if ($before < 90 * $MINUTE) {
            return "an hour ago";
        }

        if ($before < 24 * $HOUR) {

            return (floor($before / 60 / 60) == 1 ? 'about an hour' : floor($before / 60 / 60) . ' hours') . " ago";
        }

        if ($before < 48 * $HOUR) {
            return "yesterday";
        }

        if ($before < 30 * $DAY) {
            return floor($before / 60 / 60 / 24) . " days ago";
        }

        if ($before < 12 * $MONTH) {

            $months = floor($before / 60 / 60 / 24 / 30);
            return $months <= 1 ? "one month ago" : $months . " months ago";
        } else {
            $years = floor($before / 60 / 60 / 24 / 30 / 12);
            return $years <= 1 ? "one year ago" : $years . " years ago";
        }

        return "$time";
    }
}

/**
 * Get timezone list from ACF Theme Setting Field
 * @since 1.0
 */
function nuclo_timezone() {
    $timezones = array();
    if (have_rows('timezone_list', 'option')):
        while (have_rows('timezone_list', 'option')) : the_row();
            $timezones[get_sub_field('sort') . '_' . get_sub_field('utc')] = get_sub_field('full');
        endwhile;
    else :
        $timezones = array();
    endif;
    return $timezones;
}

function nuclo_countries($country_id = '') {

    global $countries;

    if (empty($countries)) {

        $response_array = get_option('restcountries', true);
        $countries = array();

        if (empty($response_array)) {
            $response_array = file_get_contents('https://restcountries.eu/rest/v1/all/');
            update_option('restcountries', $response_array);
        }

        $response_array = json_decode($response_array);

        foreach ($response_array as $country) {
            $countries[$country->alpha2Code] = $country->name;
        }
    }

    if ($country_id != null) {
        if (isset($countries[$country_id])) {
            return $countries[$country_id];
        } else {
            return '';
        }
    }

    return $countries;
}

function nuclo_countries_latlang() {
    global $contries_latlang;

    $response_array = get_option('restcountries', true);
    if (empty($response_array)) {
        $response_array = file_get_contents('https://restcountries.eu/rest/v1/all/');
        update_option('restcountries', $response_array);
    }

    $response_array = json_decode($response_array);

    foreach ($response_array as $response) {
        $contries_latlang[$response->name] = $response->latlng;
    }

    return $contries_latlang;
}

/**
 * Filter to add custom cron interval
 * @since 1.0
 */
function nuclo_minutely_interval($schedules) {

    $schedules['minute'] = array(
        'interval' => 60,
        'display' => __('Per Minute')
    );

    return $schedules;
}

add_filter('cron_schedules', 'nuclo_minutely_interval');

/**
 * Plugin Activation hook
 * @since 1.0
 */
function nuclo_activate() {

    global $wpdb;

    // Check Survey and Assessment Plugin Activated
    if (is_plugin_active('wp-survey-and-quiz-tool/wp-survey-and-quiz-tool.php')) {
        // Alter Survey and Assessment Table
        $user_id_column_exits = $wpdb->get_var("SELECT count(*) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $wpdb->dbname . "' AND TABLE_NAME = " . WPSQT_TABLE_QUIZ_SURVEYS . "  AND COLUMN_NAME = 'user_id'");
        if ($user_id_column_exits == 0) {
            $wpdb->query($wpdb->prepare("ALTER TABLE " . WPSQT_TABLE_QUIZ_SURVEYS . " ADD `user_id` BIGINT(20) NOT NULL"));
        }
        $desc_column_exits = $wpdb->get_var("SELECT count(*) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $wpdb->dbname . "' AND TABLE_NAME = " . WPSQT_TABLE_SECTIONS . "  AND COLUMN_NAME = 'desc'");
        if ($desc_column_exits == 0) {
            $wpdb->query($wpdb->prepare("ALTER TABLE " . WPSQT_TABLE_SECTIONS . " ADD `desc` LONGTEXT NOT NULL"));
        }
    }

    // Schedule cron event for sending emails 
    wp_schedule_event(time(), 'minute', 'nuclo_scheduled_email');
    
    if(class_exists('CSC'))
        CSC::fix();
    
}

register_activation_hook(__FILE__, 'nuclo_activate');

/*
 * 
 */

function update_nuclo_meta($ID = 0, $type = 'post', $meta_key = '', $new_values = array()) {
    $old_values = $diff_values = array();
    switch ($type) {
        case 'post':
            $old_values = get_post_meta($ID, $meta_key);
            if (!empty($new_values)) {
                foreach ($new_values as $key => $value) {
                    if (($old_key = array_search($value, $old_values)) !== false) {
                        unset($old_values[$old_key]);
                        unset($new_values[$key]);
                    }
                }
            }
            if (!empty($old_values)) {
                foreach ($old_values as $value) {
                    delete_post_meta($ID, $meta_key, $value);
                }
            }
            if (!empty($new_values)) {
                foreach ($new_values as $value) {
                    add_post_meta($ID, $meta_key, $value);
                }
            }

            $new_values = (!empty($new_values)) ? array_values($new_values) : array();

            if (!empty($old_values)) {
                $i = 0;
                foreach ($old_values as $key => $value) {
                    if ($i <= count($new_values)) {
                        update_post_meta($ID, $meta_key, $new_values[$i], $value);
                        unset($new_values[$i]);
                        unset($old_values[$key]);
                        $i++;
                    }
                }
            }
            break;
        case 'user':
            $old_values = get_user_meta($ID, $meta_key);
            foreach ($new_values as $key => $value) {
                if (($old_key = array_search($value, $old_values)) !== false) {
                    unset($old_values[$old_key]);
                    unset($new_values[$key]);
                }
            }
            if (!empty($old_values)) {
                $i = 0;
                foreach ($old_values as $key => $value) {
                    if ($i <= count($new_values)) {
                        update_user_meta($ID, $meta_key, $new_values[$i], $value);
                        unset($new_values[$i]);
                        unset($old_values[$key]);
                        $i++;
                    }
                }
            }
            if (!empty($old_values)) {
                foreach ($old_values as $value) {
                    delete_user_meta($ID, $meta_key, $value);
                }
            }
            if (!empty($new_values)) {
                foreach ($new_values as $value) {
                    add_user_meta($ID, $meta_key, $value);
                }
            }
            break;
    }
}