<?php

function bell_notification() {
    global $current_user;
    global $wpdb;

    $userRole = $current_user->roles[0];

    $data = array();

    if ($userRole == 'super-admin' || $userRole == 'editor') {
        
        if($userRole == 'super-admin')
        {
            $args = array(
                'post_type'     => 'notification',
                'post_status'   => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'user_role',
                        'value' => 'franchise'
                    ),
                    array(
                        'key' => 'read',
                        'value' => '0'
                    )
                )
            );
        }
        else{
            
            $args = array(
                'post_type'     => 'notification',
                'post_status'   => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'user_role',
                        'value' => 'participant'
                    ),
                    array(
                        'key' => 'franchisee_id',
                        'value' => $current_user->ID
                    ),
                    array(
                        'key' => 'read',
                        'value' => '0'
                    )
                )
            );
        }

        $getPosts = new WP_Query($args);
        $results = $getPosts->get_posts();

        
        if (!empty($results))
        {
            foreach ($results as $k => $v) {
                
                $post_meta = get_post_meta($v->ID);
                
                $post_time = $v->post_date;

                
                if($userRole == 'super-admin')
                {
                    $franchiseName = get_user_meta($post_meta['created_by'][0], 'first_name', 'single') . ' ' . get_user_meta($post_meta['created_by'][0], 'last_name', 'single');
                    $clientName = get_user_meta($post_meta['user_id'][0], 'first_name', 'single') . ' ' . get_user_meta($post_meta['user_id'][0], 'last_name', 'single');
                    
                    $franchiseuser = get_user_to_edit($post_meta['created_by'][0]);
                    $franchiselink = get_option('siteurl').'/users/'.$franchiseuser->user_nicename.'/edit';
                    
                    $clientuser = get_user_to_edit($post_meta['user_id'][0]);
                    $clientlink = get_option('siteurl').'/users/'.$clientuser->user_nicename.'/edit';
                    
                    $data[$k]['message'] = '<a  href="'.$franchiselink .'"style="padding:0px !important;display:inline;color:#4C4C4C;font-size:12px;"><strong>' . $franchiseName . '</strong></a> has added <a href="'.$clientlink.'" style="padding:0px !important;display:inline;color:#4C4C4C;font-size:12px;"><strong>' . $clientName . '</strong></a> with ' . $post_meta['total_participant'][0] . ' participants';
                }
                else
                {
                    $participantName = get_user_meta($post_meta['user_id'][0], 'first_name', 'single') . ' ' . get_user_meta($post_meta['user_id'][0], 'last_name', 'single');
                    $moduleName = $post_meta['module'][0];
                    
                    $user = get_user_to_edit($post_meta['user_id'][0]);
                    
                    $link = get_option('siteurl').'/users/'.$user->user_nicename.'/edit';
                    
                    if($post_meta['notification_type'][0] == 1)
                    {
                        $data[$k]['message'] = '<a href="'.$link.'" style="padding:0px !important;display:inline;color:#4C4C4C;font-size:12px;"><strong>' . $participantName . '</strong></a>  has posted Action Plan in Module '.$moduleName;
                    }
                    elseif($post_meta['notification_type'][0] == 2)
                    {
                        $data[$k]['message'] = '<a href="'.$link.'" style="padding:0px !important;display:inline;color:#4C4C4C;font-size:12px;"><strong>' . $participantName . '</strong></a>  has updated Results for Module '.$moduleName;
                    }
                    else
                    {
                        $itemId = $post_meta['item_id'][0];
                        
                        $itemData = $wpdb->get_results( 'SELECT * FROM wp_wpsqt_quiz_surveys WHERE id = '.$itemId);
                        $itemName = $itemData[0]->name;                       
                        $data[$k]['message'] = '<a href="'.$link.'" style="padding:0px !important;display:inline;color:#4C4C4C;font-size:12px;"><strong>' . $participantName . '</strong></a>  has submitted session Feedback for '.$itemName;
                    }
                    
                }
                
                $data[$k]['post_id'] = $v->ID;
                
                $data[$k]['time'] = relativeTime(strtotime($post_time));
                $data[$k]['avatar_id'] = $post_meta['user_id'][0];
            }
        }
    }

    $notificationCount = count($data);

    $html = '';

    $html = '<li class="dropdown notification">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-bell"></i>  <span class="label label-primary" id="notification_count" data-count="'.$notificationCount.'">' . $notificationCount . '</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages no-padding " style="padding-left: 0;min-height: 250px;max-height: 250px;overflow: hidden;overflow-y: auto;">';
    if (!empty($data)) {

        foreach ($data as $k => $v) {

            $html .='<li class="border-bottom read_notification" data-id="'.$v['post_id'].'" id="notification'.$v['post_id'].'" >
                    
                        <div class="p-xs clearfix">
                            <div class="pull-left no-padding m-r-sm">
                                '.get_avatar($v['avatar_id'], 25).'
                            </div>
                            <div class="media-body" style="line-height:15px !important;">
                                <small class="pull-right">' . $v['time'] . '</small>
                                ' . $v['message'] . '
                            </div>
                        </div>
                </li>';
        }
//        $html .='<li>
//                        <div class="text-center link-block">
//                            <a href="#">
//                                <strong>See All Alerts</strong>
//                            </a>
//                        </div>
//                    </li>';
    } else {
        $html .='<li>
                        <div class="text-center link-block">
                            <a href="">
                                <strong>No notification</strong>
                            </a>
                        </div>
                    </li>';
    }

    $html .='</ul></li>';

    return $html;
}
