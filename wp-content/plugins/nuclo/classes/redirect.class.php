<?php

function nuclo_referer_field() {
    
    if(is_user_logged_in()) {
        
        if(current_user_can('contributor')) {
            
            global $current_user;
            
            $user_id = $current_user->ID;
            
            if(is_author()) {
                
                 $status  = get_user_meta( $user_id, 'profile_update_after_login', true);
                 
                 if($status == 'yes') {
                 
                     return esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) );
                 
                 } else {
                     
                     $course_id = get_field('bpm','option');
                     
                     if($course_id) {
                         
                         return '';
                         
                     } else {
                         
                         return '';
                         
                     }
                     
                     
                     
                 }
                
            }
            
        }
        
    }
    
}


function program_redirect_to_current_module($course_id=0,$module_id=0) {
    
    if(current_user_can('contributor')) {
        
        if($course_id == 0) {
            global $post;
            $course_id = $post->ID;
        }

        global $current_user;

        $module_args = array(
                    'post_type'      => array('extra', 'discussion', 'survey', 'measure', 'assessment','bundle','event'),
                    'post_parent'    => $course_id,
                    'posts_per_page' => -1,
                    'orderby'        => 'menu_order',
                    'order'          => 'ASC'
                ); 

        $module_query = new WP_Query( $module_args );

        $last_module = get_user_meta($current_user->ID,'bpm_last_module',true);

        $lists = array();

        if ( $module_query->have_posts() ) {
            while ( $module_query->have_posts() ) { 
                $module_query->the_post();
                
                if(get_post_type() == 'bundle') {
                    
                    $bundle_args = array(
                                'post_type'      => array('extra', 'discussion', 'survey', 'measure', 'assessment'),
                                'post_parent'    => get_the_ID(),
                                'posts_per_page' => -1,
                                'orderby'        => 'menu_order',
                                'order'          => 'ASC'
                            ); 

                    $bundle_query = new WP_Query( $bundle_args );
                    
                    if ( $bundle_query->have_posts() ) {
                        while ( $bundle_query->have_posts() ) { 
                            $bundle_query->the_post();
                            $list['id']     = get_the_ID();
                            $list['order']  = get_post_field('menu_order',get_the_ID());
                            $list['url']    = get_permalink();
                            array_push($lists,$list);
                        }
                    }
                    wp_reset_postdata();
                } else {
                    $list['id']     = get_the_ID();
                    $list['order']  = get_post_field('menu_order',get_the_ID());
                    $list['url']    = get_permalink();
                    array_push($lists,$list);
                }
            }
        }

        wp_reset_postdata();
        
        $status = get_user_meta($current_user->ID,'module_result_'.$lists[0]['id'],true);
        
        if($status) {
            
            return $lists;
            
        } else {
            
            if($module_id != 0) {
                if($lists[0]['id'] == $module_id) return;
            }
            
            $url = $lists[0]['url'];

            wp_redirect($url);

            exit;
            
        }

    }
    
}
//add_action('program','program_redirect_to_current_module');

function module_redirect_to_current_module() {
    
    if(current_user_can('contributor')) {

        global $post,$current_user;
    
        $module_id = $post->ID;
        
        //$module = $post->post_type;
        
        $post_type = get_post_type($post->post_parent);
        
        $course_id = 0;
        
        if( $post_type == 'bundle' ) {
            $post_parent = get_post($post->post_parent);
            $course_id = $post_parent->post_parent;
        }
        
        if($course_id == 0) {
            $course_id = $post->post_parent;
        }
        
        $lists = program_redirect_to_current_module($course_id,$module_id);
        
        $status = get_user_meta($current_user->ID,'module_result_'.$module_id,true);
        
        if(isset($_REQUEST['viewed'])) {
            update_user_meta($current_user->ID, 'module_result_'.$module_id, 1);
            wp_redirect(get_permalink($module_id));
            exit;
        }
        
        if($status) {
            $index = array_search($module_id,array_column($lists, 'id'));
            $next = $index+1;
            $url = $lists[$next]['url'];
            wp_redirect($url);
            exit;
        }
           
    }
    
}
//add_action('module','module_redirect_to_current_module');

function bundle_redirect_to_current_module() {
    
   if(current_user_can('contributor')) {

        global $post;
    
        $bundle_id = $post->ID;
        
        $course_id = $post->post_parent; 
        
        //$lists = program_redirect_to_current_module($course_id,$bundle_id);
        
        
    } 
    
}
//add_action('bundle','bundle_redirect_to_current_module');

function get_bundle_status($user_id,$post_id) {
    
    $final_status = false;
    
    $bundle_args = array(
                'post_type'      => array('extra', 'discussion', 'survey', 'measure', 'assessment'),
                'post_parent'    => $post_id,
                'posts_per_page' => -1,
                'orderby'        => 'menu_order',
                'order'          => 'ASC'
            ); 

    $bundle_query = new WP_Query( $bundle_args );

    if ( $bundle_query->have_posts() ) {
        while ( $bundle_query->have_posts() ) { 
            $bundle_query->the_post();
            
            $status = get_user_meta($user_id,'module_result_'.get_the_ID(),true);
            
            if($status) {
                $final_status = true;
            } else {
                $final_status = false;
            }
            
        }
    }
    
    return $final_status;
}