<?php

/**
 * @package  Franchisee
 * @author   Dipak Kumar Pusti <dipak.pusti@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Franchisee {

  public $per_page;
  /**
   * Adds the generic hooks that are required throughout
   * the theme.
   * @since 1.0
   */
  public function __construct() {

    add_action('init', array($this, 'rewrites_init'));
    add_action('init', array($this, 'register_post_type'));
    add_action('init', array($this, 'post_franchisee'));
    add_filter('pre_get_posts', array($this, 'filter_query'), 1, 1);
    $this->per_page = isset($_REQUEST['record_per_page'])?$_REQUEST['record_per_page']:20;
  }

  /**
   * @since 1.0
   */
  public function register_post_type() {
    if (function_exists('get_field')) :
      $labels = array(
          'name' => _x(get_field('franchisee_general_name', 'option'), 'post type general name', 'nuclo'),
          'singular_name' => _x(get_field('franchisee_singular_name', 'option'), 'post type singular name', 'nuclo'),
          'menu_name' => _x(get_field('franchisee_general_name', 'option'), 'admin menu', 'nuclo'),
          'name_admin_bar' => _x(get_field('franchisee_singular_name', 'option'), 'add new on admin bar', 'nuclo'),
          'add_new' => _x('Add New', strtolower(get_field('franchisee_singular_name', 'option')), 'nuclo'),
          'add_new_item' => __('Add New ' . get_field('franchisee_singular_name', 'option'), 'nuclo'),
          'new_item' => __('New ' . get_field('franchisee_singular_name', 'option'), 'nuclo'),
          'edit_item' => __('Edit ' . get_field('franchisee_singular_name', 'option'), 'nuclo'),
          'view_item' => __('View ' . get_field('franchisee_singular_name', 'option'), 'nuclo'),
          'all_items' => __('All ' . get_field('franchisee_general_name', 'option'), 'nuclo'),
          'search_items' => __('Search ' . get_field('franchisee_general_name', 'option'), 'nuclo'),
          'parent_item_colon' => __('Parent ' . get_field('franchisee_general_name', 'option') . ':', 'nuclo'),
          'not_found' => __('No ' . strtolower(get_field('franchisee_general_name', 'option')) . ' found.', 'nuclo'),
          'not_found_in_trash' => __('No ' . strtolower(get_field('franchisee_general_name', 'option')) . ' found in Trash.', 'nuclo')
      );

      $args = array(
          'labels' => $labels,
          'public' => true,
          'publicly_queryable' => true,
          'exclude_from_search' => true,
          'show_ui' => true,
          'show_in_menu' => true,
          'query_var' => true,
          'rewrite' => array('slug' => get_field('franchisee_slug', 'option')),
          'capability_type' => 'post',
          'has_archive' => true,
          'hierarchical' => true,
          'menu_position' => null,
          'supports' => array('title', 'author')
      );

      register_post_type('franchisee', $args);
    endif;
  }

  /**
   * @since  1.0
   */
  public function rewrites_init() {
    add_rewrite_rule('franchisee/page/([0-9]{1,})/?', 'index.php?post_type=franchisee&paged=$matches[1]', 'top');
    add_rewrite_rule('franchisee/(.+?)/([^/]*)/?$', 'index.php?franchisee=$matches[1]&action=$matches[2]', 'top');
  }

  /**
   * @since 1.0
   */
  public function post_franchisee() {

    if (isset($_POST) && !empty($_POST['submit']) && isset($_POST['nuclo_create_franchisee']) && wp_verify_nonce($_POST['nuclo_create_franchisee'], 'nuclo_create_franchisee')) {

      $data = Users::cleandata($_POST);

      $validate = $this->validate('create', $data);

      if ($validate == true && !is_array($validate)) {
        return $this->insert_franchisee($data);
      }
    }

    if (isset($_POST) && !empty($_POST['submit']) && isset($_POST['nuclo_edit_franchisee']) && wp_verify_nonce($_POST['nuclo_edit_franchisee'], 'nuclo_edit_franchisee')) {

      $data = Users::cleandata($_POST);
      $validate = $this->validate('edit', $data);

      if ($validate == true && !is_array($validate)) {
        return $this->update_franchisee($data);
      }
    }
  }

  /**
   * @since  1.0
   */
  public function validate($type, $data) {

    global $fran_messsage;

    if (empty($data['fran_id'])) {
      $error['fran_id'] = 'Franchise ID is required.';
    } else if (!ctype_alnum($data['fran_id'])) {
      $error['fran_id'] = 'Franchise ID is only accepts letters and numbers.';
    } else if (strlen($data['fran_id']) < 2 || strlen($data['fran_id']) > 30) {
      $error['fran_id'] = 'Franchise ID should contain 2 - 30 character long';
    }else if(!empty($data['fran_id']) && get_franchisee_id($data['fran_id']) && ($type != 'edit')){
        $error['fran_id'] = 'Franchise ID already exists.';
    }

    if (empty($data['fran_name'])) {
      $error['fran_name'] = 'Franchise/DBA name is required .';
    }else if (strlen($data['fran_name']) < 2 || strlen($data['fran_name']) > 30) {
      $error['fran_name'] = 'Franchise/DBA name should contain 2 - 30 character long';
    }

    if ($type == 'create') {
      if (empty($data['f_o_fname'])) {
        $error['f_o_fname'] = 'Franchisee first name is required.';
      } else if (strlen($data['f_o_fname']) < 2 || strlen($data['f_o_fname']) > 30) {
        $error['f_o_fname'] = 'Franchisee first name should contain 2 - 30 character long';
        } else if (!ctype_alpha(str_replace('-', '', $data['f_o_fname']))) {
         $error['f_o_fname'] = 'Only letters are allowed in Franchisee first name.';
      }

      if (empty($data['f_o_lname'])) {
        $error['f_o_lname'] = 'Franchisee last name is required.';
      } else if (strlen($data['f_o_lname']) < 2 || strlen($data['f_o_lname']) > 30) {
        $error['f_o_lname'] = 'Franchisee last name should contain 2 - 30 character long.';
          } else if (!ctype_alpha(str_replace(' ', '', $data['f_o_lname']))) {
        $error['f_o_lname'] = 'Only letters are allowed in Franchisee last name.';
      }


      $email_array = explode('.', $data['f_o_email']);
      $last_element = $email_array[count($email_array) - 1];

      if (empty($data['f_o_email'])) {
        $error['f_o_email'] = 'Email address is required.';
      } else if (!is_email($data['f_o_email'])) {
        $error['f_o_email'] = 'Invalid owner email.';
      } else if (email_exists($data['f_o_email'])) {
        $error['f_o_email'] = 'Owner email is already in use.';
      } else if (isset($data['f_o_email']) && strlen($last_element) > 10) {
        $error['f_o_email'] = 'Invalid email address.';
      }
    }

    if (empty($data['f_o_contact'])) {
      $error['f_o_contact'] = 'Phone number is required.';
    } else if (!empty($data['f_o_contact']) && !ctype_digit($data['f_o_contact'])) {
      $error['f_o_contact'] = 'Phone number only accepts numbers.';
    } else if (!empty($data['f_o_contact']) && (strlen($data['f_o_contact']) > 15 || (!ctype_digit($data['f_o_contact'])))) {
      $error['f_o_contact'] = 'Please enter a valid phone number.';
    }

    if (empty($data['address'])) {
      $error['address'] = 'Address is required.';
    }
    
    if (empty($data['country'])) {
      $error['country'] = 'Country is required.';
    }

    if (empty($data['state'])) {
      $error['state'] = 'State is required.';
    }

    if (empty($data['city'])) {
        $error['city'] = 'City is required.';
    } else if (!ctype_alpha(str_replace(' ', '', $data['city']))) {
        $error['city'] = 'City only accepts letters.';
    }
    
    
//    if (empty($data['time_zone'])) {
//      $error['time_zone'] = 'Timezone is required.';
//    }

//        if (empty($data['region'])) {
//            $error['region'] = 'Region is required.';
//        }

    if (!empty($error)) {
      $fran_messsage['error'] = $error;
      return $error;
    } else {
      return true;
    }
  }

  /**
   * @since  1.0
   */
  private function insert_franchisee($data) {

    # Adding Franchisee Owner
    $data['password'] = wp_generate_password();

    $user_id = wp_insert_user(array(
        'user_login' => $data['f_o_email'],
        'user_email' => $data['f_o_email'],
        'user_pass' => $data['password'],
        'role' => 'editor')
    );

    if (!is_wp_error($user_id)) {

        // Updating User Meta
        update_user_meta($user_id, 'first_name', $data['f_o_fname']);
        update_user_meta($user_id, 'last_name', $data['f_o_lname']);
        update_user_meta($user_id, 'contact', $data['f_o_contact']);
        update_user_meta($user_id, 'address', $data['city']);
        update_user_meta($user_id, 'city', $data['city']);
        update_user_meta($user_id, 'state', $data['state']);
        update_user_meta($user_id, 'country', $data['country']);

        // Send Notification after franchisee added
        do_action('register_new_franchisee', $data);
    }

    # Adding Franchisee Post type
    $franchisee = array(
        'post_title' => $data['fran_name'],
        'post_type' => 'franchisee',
        'post_status' => 'publish',
        'post_author' => get_current_user_id()
    );

    $franchisee_id = wp_insert_post($franchisee);


    if (!empty($franchisee_id)) {
      $this->save_meta($franchisee_id, $data);
    }

    if (!is_wp_error($user_id) && !empty($franchisee_id)) {

      # Adding relative user and post meta
      update_post_meta($franchisee_id, 'franchise_owner', $user_id);

      update_user_meta($user_id, 'franchisee', $franchisee_id);
      update_user_meta($user_id, 'is_franchisee_owner', 'yes');

      // Redirect to Create page and Display message
      $url    = strtok($data['_wp_http_referer'], '?');

      wp_safe_redirect($url . '/?msg=success');

      exit();
    } else {

      global $fran_messsage;

      $fran_messsage['error']['create'] = 'Something went wrong!';
    }
  }

  /**
   * @since  1.0
   */
  private function update_franchisee($data) {

    # Updating Franchisee Post type
    $franchisee = array(
        'ID' => $data['post_id'],
        'post_title' => $data['fran_name'],
        'post_type' => 'franchisee'
    );


    $franchisee_id = wp_update_post($franchisee);

    if (!empty($franchisee_id)) {

      $this->save_meta($franchisee_id, $data);

      // Redirect to Create page and Display message
      $url    = strtok($data['_wp_http_referer'], '?');

      wp_safe_redirect($url . '/?msg=success');
      
      exit();
    } else {

      global $fran_messsage;
      $fran_messsage['error']['create'] = 'Something went wrong!';
    }
  }

  /**
   * @since  1.0
   */
  private function save_meta($franchisee_id, $data) {
    # Setting up Franchisee post meta
    update_post_meta($franchisee_id, 'franchisee_id', $data['fran_id']);
    update_post_meta($franchisee_id, 'franchisee_email', $data['f_o_email']);
    update_post_meta($franchisee_id, 'franchisee_contact', $data['f_o_contact']);
    update_post_meta($franchisee_id, 'address', $data['address']);
    update_post_meta($franchisee_id, 'city', $data['city']);
    update_post_meta($franchisee_id, 'state', $data['state']);
    update_post_meta($franchisee_id, 'country', $data['country']);
    update_post_meta($franchisee_id, 'time_zone', $data['time_zone']);
    update_post_meta($franchisee_id, 'region', $data['region']);
    update_post_meta($franchisee_id, 'zip', $data['postal_code']);
    
    
  }

  /**
   * @since  1.0
   */
  public static function get_franchisee() {

    // ACL - Bring franchisee under particular franchisee group
    // under which the logged in user is enrolled
    $user_id = get_current_user_id();
    $user_role = nuclo_get_user_role($user_id);

    $post_array = array();
    if ('editor' == $user_role) {
      // Get franchisee ID
      $parent_franchisee = get_user_meta($user_id, 'franchisee', true);
      if (!empty($parent_franchisee)) {
        array_push($post_array, $parent_franchisee);
      }
    }

    $f_args = array(
        'post_type' => 'franchisee',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'post__in' => $post_array
    );

    $franchisee = get_posts($f_args);

    # formatting data
    $all_franchisee = array();
    foreach ($franchisee as $f) : setup_postdata($f);
      
      $franchise_owner          = get_post_meta($f->ID,'franchise_owner',true);
      $franchise_owner_detail   = get_userdata($franchise_owner);
      $return = array(
          "id"      => $f->ID,
          "name"    => $f->post_title,    
          "owner"   => (!empty($franchise_owner_detail->first_name))?$franchise_owner_detail->first_name.' '.$franchise_owner_detail->last_name:''
      );
      $all_franchisee[] = $return;

    endforeach;

    # Reseting post query
    wp_reset_postdata();

    return $all_franchisee;
  }

  /**
   * @since 1.0
   */
  public function filter_query($query) {

    if ($query->get('post_type') == 'franchisee') {

        $query->query_vars['posts_per_page'] = $this->per_page;
        $query->query_vars['paged'] = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;

        if (isset($_GET) && !empty($_GET['search'])) {
          $search_text = sanitize_text_field($_GET['search']);
          $query->set('s', $search_text);
        }
    }

    return $query;
  }

  /**
   * @since  1.0
   */
  public function get_franchisee_details($franchisee_id) {

    $franchisee = get_post($franchisee_id, ARRAY_A);
    $franchisee['franchisee_email'] = get_post_meta($franchisee_id, 'franchisee_email', true);
    $franchisee['franchisee_contact'] = get_post_meta($franchisee_id, 'franchisee_contact', true);
    $franchisee['address'] = get_post_meta($franchisee_id, 'address', true);
    $franchisee['city'] = get_post_meta($franchisee_id, 'city', true);
    $franchisee['state'] = nc_state_name(get_post_meta($franchisee_id, 'state', true));
    $franchisee['country'] = nc_country_name(get_post_meta($franchisee_id, 'country', true));
    $franchisee['region'] = get_post_meta($franchisee_id, 'region', true);
    $franchisee['zip'] = get_post_meta($franchisee_id, 'zip', true);

    return $franchisee;
  }
  
  public static function select_course_for_franchisee($program = array(),$disabled='')
  {
      $html = '';
      $html .= '<select data-placeholder="Select Program" name="nc_programs[]" class="chosen-select" id="nc_programs" multiple ' . $disabled . '>';
      
      $courses = array();
        $course_args    = array(
            'post_type'         => 'program',
            'numberposts'       => -1            
        );
        
        $courses        = get_posts($course_args);
        $course_list    = array();
        
        foreach($courses as $course){
            
            $selected = '';
            if(in_array($course->ID, $program))
            {
                $selected = 'selected="selected"';
            }
            
            $html .='<option value="'.$course->ID.'" '.$selected.'>'.$course->post_title.'</option>';
        }
      
      $html .='</select>';
      return $html;
  }

}

new Franchisee();

function get_franchisee_name($owner_id) {
  $args = array(
      'posts_per_page' => 1,
      'meta_key' => 'franchise_owner',
      'meta_value' => $owner_id,
      'post_type' => 'franchisee',
      'post_status' => 'publish'
  );
  $franchisees = get_posts($args);

  if (isset($franchisees[0]->post_title))
    return $franchisees[0]->post_title;
  else
    return '';
}

function get_franchisee_name_by_id($franchisee_id){     
     $franchisees   = get_post($franchisee_id);
     
     if(isset($franchisees->post_title)){
         return $franchisees->post_title;
     }else{
         return '';
     }    
}

function get_franchisee_id($franchisee_unique_id) {
  $args = array(
      'posts_per_page' => 1,
      'meta_key' => 'franchisee_id',
      'meta_value' => $franchisee_unique_id,
      'post_type' => 'franchisee',
      'post_status' => 'publish'
  );
  $franchisees = get_posts($args);
  
  if(!empty($franchisees)){
      return $franchisees;
  }else{
      return FALSE;
  }
}

function get_owner() {
  $franchisee_id = get_the_ID();
  $owner_id = get_post_meta($franchisee_id, 'franchise_owner', true);
  $name = get_user_meta($owner_id, 'first_name', true) . ' ' . get_user_meta($owner_id, 'last_name', true);
  return $name;
}

function get_owner_id() {
  $franchisee_id = get_the_ID();
  $owner_id = get_post_meta($franchisee_id, 'franchise_owner', true);
  return $owner_id;
}

function get_admins_count() {
  $admins = get_admins();
  return count($admins);
}

function get_admins() {
  $admins = get_users(array(
      'role' => 'editor', 
      'meta_query'  => array(
          'relation'    => 'AND',
          array(
            'key'      => 'franchisee', 
            'value'    => get_the_ID(), 
            'compare'  => '='
          )
      )
      
    ));
  return $admins;
}

function get_current_user_franchisee_id() {
  $user_id = get_current_user_id();
  $franchisee_id = get_user_meta($user_id, 'franchisee', true);
  return $franchisee_id;
}

function get_franchise_users_count() {

  $users = get_franchise_users();

  return count($users);
}

function get_franchise_users($franchise_id = 0) {

  $clients = array();;

  $args = array('post_type' => 'client', 'post_status' => 'publish', 'posts_per_page' => -1);

  if (current_user_can('super-admin') || current_user_can('administrator')) {
    $args['meta_key'] = 'franchisee';
    $args['meta_value'] = $franchise_id;
  } else if ($franchise_id > 0) {    
    $args['meta_key'] = 'franchisee';
    $args['meta_value'] = $franchise_id;
  }

  $clients_query = new WP_Query($args);
  
  if ($clients_query->have_posts()) {
    while ($clients_query->have_posts()) {
      $clients_query->the_post();
      $clients[] = get_the_ID(); 
    }
  }
  wp_reset_postdata();
  
  $users = get_client_users_ids($clients);

  return $users;
}

function get_franchise_client_admins($franchise_id = 0) {

  $users = array();

  $args = array('post_type' => 'client', 'post_status' => 'publish', 'posts_per_page' => -1);

  if (current_user_can('super-admin') || current_user_can('administrator')) {
    $args['meta_key'] = 'franchisee';
    $args['meta_value'] = $franchise_id;
  } else if ($franchise_id > 0) {
    $args['meta_key'] = 'franchisee';
    $args['meta_value'] = $franchise_id;
  }

  $clients_query = new WP_Query($args);

  if ($clients_query->have_posts()) {
    while ($clients_query->have_posts()) {
      $clients_query->the_post();
      $client_users = get_client_admins_ids();
      $users = array_merge($users, $client_users);
    }
  }
  wp_reset_postdata();

  return $users;
}

function get_franchise_client($franchise_id = 0,$client_id = 0) {

  $users = array();

  $args = array('post_type' => 'client', 'post_status' => 'publish', 'posts_per_page' => -1);

  if (current_user_can('super-admin') || current_user_can('administrator')) {
    $args['meta_key'] = 'franchisee';
    $args['meta_value'] = $franchise_id;
  } else if ($franchise_id > 0) {    
    $args['meta_key'] = 'franchisee';
    $args['meta_value'] = $franchise_id;
  }

    $html = '';
    $html.= '<select data-placeholder="Select Client" id="group_client" name="grp_client" onchange="get_participants_outside_workflow(this.value);" class="chosen-select">';
    $html.='<option value="">Select Client</option>';
  $clients_query = new WP_Query($args);

  if ($clients_query->have_posts()) {
    while ($clients_query->have_posts()) {
      $clients_query->the_post();
      $value = [];
      $c_id= get_the_ID();
      $c_title = get_the_title();
      $selected = '';
      if($c_id == $client_id)
        $selected = 'selected';
      $html.='<option value="'.$c_id.'" '.$selected.' >'.$c_title.'</option>';
    }
  }
  wp_reset_postdata();
  $html.= '</select>';
  return $html;
}

function get_franchise_facilitators($franchise_id = 0) {

    $facilitators = $admins = array();

    $facilitator_users = get_users(array('role' => 'author', 
      'meta_query' => array(
        'relation'  => 'AND',
        array(
            'key'       => 'franchisee', 
            'value'     => $franchise_id, 
            'compare'   => '='
        )        
      )      
    ));
    
    foreach ($facilitator_users as $facilitator_user) {
      $facilitators[] = $facilitator_user->ID;
    }
    
    $admin_users = get_users(array('role' => 'editor',
      'meta_query'  => array(
        'relation'  => 'AND',
        array(
            'key'       => 'franchisee', 
            'value'     => $franchise_id, 
            'compare'   => '='
        )        
      )      
    ));
    
    foreach ($admin_users as $admin) {
      $admins[] = $admin->ID;
    }
   
    $users = array_merge($facilitators, $admins);

    return array_unique($users);
}

function get_franchise_group($franchise_id = 0, $one_year = true) {

  if ($one_year) {
    $months = array('January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0);
  } else {
    $months = array();
  }
  if ($franchise_id == 'all') {
    $meta_query = array('relation' => 'OR',
        array('key' => 'group_start_date',
            'value' => date("Y") . '0101',
            'compare' => '>', 'type' => 'DATE'),
        array('key' => 'group_start_date',
            'value' => (date("Y") + 1) . '0101',
            'compare' => '<', 'type' => 'DATE'));
  } else {
    $meta_query = array(
        'relation' => 'AND',
        array('key' => 'franchisee',
            'value' => $franchise_id),
        array('relation' => 'OR',
            array('key' => 'group_start_date',
                'value' => date("Y") . '0101',
                'compare' => '>', 'type' => 'DATE'),
            array('key' => 'group_start_date',
                'value' => (date("Y") + 1) . '0101',
                'compare' => '<', 'type' => 'DATE')));
  }
  $grp_args = array(
      'post_type' => 'group',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'meta_query' => $meta_query
  );

  $groups = get_posts($grp_args);

  if (!empty($groups)) {
    foreach ($groups as $group) {
      $new_start_date = '';
      $new_end_date = '';
      $users = get_field('users', $group->ID);
      $start_date = get_field('group_start_date', $group->ID);
      if (!empty($start_date))
        $new_start_date = str_replace('/', '-', $start_date);

      $end_date = get_field('group_end_date', $group->ID);
      if (!empty($end_date))
        $new_end_date = str_replace('/', '-', $end_date);

      if (!empty($new_start_date) && !empty($new_end_date)) {
        $start = new DateTime($new_start_date);
        $start->modify('first day of this month');
        $end = new DateTime($new_end_date);
        $end->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        if ($period) {
          foreach ($period as $dt) {
            if ($one_year) {
              if ($dt->format("Y") == date("Y"))
                $months[$dt->format("F")] += count($users);
            } else {
              if (!isset($months[$dt->format("Y")][$dt->format("F")])) {
                $months[$group->ID][$dt->format("Y")][$dt->format("F")] = 0;
              }
              $months[$group->ID][$dt->format("Y")][$dt->format("F")] += count($users);
            }
          }
        }
      }
    }
  }

  return $months;
}

function get_franchise_enrollment($franchise_id = 0) {

  $months = get_franchise_group($franchise_id);

  $return = array();

  if ($months) {
    foreach ($months as $month => $count) {
      $return[] = array('name' => $month, 'y' => $count);
    }
  }

  return json_encode($return);
}

function get_franchise_course_enrollment($franchise_id = 0, $one_year = true) {

  $groups = get_franchise_group($franchise_id, false);

  $month_names = array('January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0);

  $courses = array('bpm' => $month_names, 'csa' => $month_names, 'custom' => $month_names);

  $current_year = date('Y');

  if ($groups) {
    foreach ($groups as $group => $years) {
      $course_types = get_group_courses_type($group);
      foreach ($years as $year => $months) {
        if ($one_year) {
          if ($current_year == $year) {
            foreach ($months as $month => $count) {
              $courses['bpm'][$month] += $course_types['bpm'] * $count;
              $courses['csa'][$month] += $course_types['csa'] * $count;
              $courses['custom'][$month] += $course_types['custom'] * $count;
            }
          }
        } else {
          foreach ($months as $month => $count) {
            $courses['bpm'][$year][$month] += $course_types['bpm'] * $count;
            $courses['csa'][$year][$month] += $course_types['csa'] * $count;
            $courses['custom'][$year][$month] += $course_types['custom'] * $count;
          }
        }
      }
    }
  }

  return $courses;
}

function get_group_courses_type($group_id = 0) {

  $return = array('bpm' => 0, 'csa' => 0, 'custom' => 0);

  $courses = get_post_meta($group_id, 'posts', true);

  if (is_array($courses)) {
    foreach ($courses as $course) {
      $type = get_field('type', $course);
      switch ($type) {
        case 'ecommerce':
          $return['custom'] ++;
          break;
        case 'csa':
          $return['csa'] ++;
          break;
        case 'bpm':
          $return['bpm'] = 1;
          break;
      }
    }
  }

  return $return;
}
