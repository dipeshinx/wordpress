<?php

/**
 * @package  Client
 * @author   Dipak Kumar Pusti <dipak.pusti@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Client {

    public $per_page;

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {

        add_action('init', array($this, 'rewrites_init'));
        add_action('init', array($this, 'register_post_type'));
        add_action('init', array($this, 'post_client'));
        add_action('wp_ajax_load_sales_id', array($this, 'load_client_sales_id'));
        add_filter('pre_get_posts', array($this, 'filter_query'), 1, 1);

        $this->per_page = isset($_REQUEST['record_per_page'])?$_REQUEST['record_per_page']:20;
    }

    /**
     * @since 1.0
     */
    public function register_post_type() {
        if (function_exists('get_field')) :
            $labels = array(
                'name' => _x(get_field('client_general_name', 'option'), 'post type general name', 'nuclo'),
                'singular_name' => _x(get_field('client_singular_name', 'option'), 'post type singular name', 'nuclo'),
                'menu_name' => _x(get_field('client_general_name', 'option'), 'admin menu', 'nuclo'),
                'name_admin_bar' => _x(get_field('client_singular_name', 'option'), 'add new on admin bar', 'nuclo'),
                'add_new' => _x('Add New', strtolower(get_field('client_singular_name', 'option')), 'nuclo'),
                'add_new_item' => __('Add New ' . get_field('client_singular_name', 'option'), 'nuclo'),
                'new_item' => __('New ' . get_field('client_singular_name', 'option'), 'nuclo'),
                'edit_item' => __('Edit ' . get_field('client_singular_name', 'option'), 'nuclo'),
                'view_item' => __('View ' . get_field('client_singular_name', 'option'), 'nuclo'),
                'all_items' => __('All ' . get_field('client_general_name', 'option'), 'nuclo'),
                'search_items' => __('Search ' . get_field('client_general_name', 'option'), 'nuclo'),
                'parent_item_colon' => __('Parent ' . get_field('client_general_name', 'option') . ':', 'nuclo'),
                'not_found' => __('No ' . strtolower(get_field('client_general_name', 'option')) . ' found.', 'nuclo'),
                'not_found_in_trash' => __('No ' . strtolower(get_field('client_general_name', 'option')) . ' found in Trash.', 'nuclo')
            );

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => get_field('client_slug', 'option')),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => true,
                'menu_position' => null,
                'supports' => array('title', 'author')
            );

            register_post_type('client', $args);
        endif;
    }

    /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('client/page/([0-9]{1,})/?','index.php?post_type=client&paged=$matches[1]','top');
        add_rewrite_rule('client/(.+?)/([^/]*)/?$', 'index.php?client=$matches[1]&action=$matches[2]', 'top');
    }

    /**
     * @since 1.0
     */
    public function post_client() {

        if (isset($_POST) && !empty($_POST['submit']) && isset($_POST['nuclo_create_client']) && wp_verify_nonce($_POST['nuclo_create_client'], 'nuclo_create_client')) {

            $data = Users::cleandata($_POST);

            $validate = $this->validate('create', $data);

            if ($validate == true && !is_array($validate)) {
              
                return $this->insert_client($data);
            }
        }

        if (isset($_POST) && !empty($_POST['submit']) && isset($_POST['nuclo_edit_client']) && wp_verify_nonce($_POST['nuclo_edit_client'], 'nuclo_edit_client')) {

            $data = Users::cleandata($_POST);

            $validate = $this->validate('edit', $data);

            if ($validate == true && !is_array($validate)) {  
                return $this->update_client($data);
            }
        }

        if (isset($_POST) && !empty($_POST['submit']) && isset($_POST['nuclo_client_license']) && wp_verify_nonce($_POST['nuclo_client_license'], 'nuclo_client_license')) {

            $data = Users::cleandata($_POST);

            $validate = $this->validate_license($data);

            if ($validate == true && !is_array($validate)) {
                return $this->update_license($data);
            }
        }
    }

    /**
     * @since  1.0
     */
    public function validate($type, $data) {

        global $client_messsage;

        if (empty($data['client_name'])) {
            $error['client_name'] = 'Organization name is required.';
        } 

        if (current_user_can('administrator') || current_user_can('super-admin')) {
            if (empty($data['nc_franchisee'])) {
                $error['nc_franchisee'] = 'Please select Franchise/DBA';
            }
        }

        if ($type == 'create') {

            if (empty($data['ceo_fname'])) {
                $error['ceo_fname'] = 'CEO first name is required.';
            } else if (strlen($data['ceo_fname']) < 2 || strlen($data['ceo_fname']) > 30) {
                $error['ceo_fname'] = 'CEO name should contain 2 - 30 character long';
            //} else if (!ctype_alpha(str_replace('-', '', $data['c_o_fname']))) {
               // $error['c_o_fname'] = 'Only letters are allowed in CEO first name.';
            } 

            if (empty($data['ceo_lname'])) {
                $error['ceo_lname'] = 'CEO last name is required.';
            } else if (strlen($data['ceo_lname']) < 2 || strlen($data['ceo_lname']) > 30) {
                $error['ceo_lname'] = 'CEO last name should contain 2 - 30 character long';
           // } else if (!ctype_alpha(str_replace('-', '', $data['c_o_lname']))) {
                //$error['c_o_lname'] = 'Only letters are allowed in CEO last name.';
            } 
        

            if (empty($data['net_suite_id'])) {
                $error['net_suite_id'] = 'NetSuite Sales ID is required.';
              } else if (strlen($data['net_suite_id']) < 2 || strlen($data['net_suite_id']) > 5) {
                $error['net_suite_id'] = 'NetSuite Sales ID should contain 5 character long';
              } else if (!ctype_alnum(str_replace('-', '', $data['net_suite_id']))) {
                $error['net_suite_id'] = 'Alphanumeric are allowed in NetSuite Sales ID only.';
            }else if(!empty($data['net_suite_id']) && get_netsales_suit_id($data['net_suite_id']) && ($type != 'edit')){
                $error['net_suite_id'] = 'NetSuite Sales Id is already in use.';
            }
            
            if (empty($data['agreement'])) {
                $error['agreement'] = 'Agreement Date is required.';
            }

            if (empty($data['total_participant'])) {
                $error['total_participant'] = 'Number of participant is required.';
            }else if(!empty($data['total_participant']) && (!ctype_digit($data['total_participant']))){
                $error['total_participant'] = 'Please enter digits only.';
            }
            
            if (empty($data['ceo_email'])) {
                $error['ceo_email'] = 'Email address is required.';
            } else if (!is_email($data['ceo_email'])) {
                $error['ceo_email'] = 'Invalid CEO email address.';
            } else if (email_exists($data['ceo_email']) && ($type != 'edit')) {
                $error['ceo_email'] = 'Email address is already in use.';
            }
            
            if (empty($data['product'])) {
                $error['product'] = 'Program Type is required.';
            }
        }
        
        

        if (empty($data['ceo_contact'])) {
            $error['ceo_contact'] = 'Phone number is required.';
        } else if (!empty($data['ceo_contact']) && !ctype_digit($data['ceo_contact'])) {
            $error['ceo_contact'] = 'Phone number only accepts numbers.';
        } else if(!empty($data['ceo_contact']) && (strlen($data['ceo_contact']) > 15 || (!ctype_digit($data['ceo_contact'])))){
            $error['ceo_contact'] = 'Please enter a valid phone number.';
        }

        if (empty($data['sic_code'])) {
            $error['sic_code'] = 'Industry Name is required.';
        }

        

        if (empty($data['address'])) {
            $error['address'] = ' Organization Address is required.';
        }

        if (empty($data['country'])) {
            $error['country'] = 'Country is required.';
        }
        if (empty($data['state'])) {
            $error['state'] = 'State is required.';
        }
        if (empty($data['city'])) {
            $error['city'] = 'City is required.';
        } else if (!ctype_alpha(str_replace(' ', '', $data['city']))) {
            $error['city'] = 'City only accepts letters.';
        }
        

        if (!empty($error)) {
            $client_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }

    /**
     * @since  1.0
     */
    public function validate_license($data) {

        global $client_messsage;

        if (empty($data['client_id'])) {
            $error['client_id'] = 'Client is required to update license.';
        }

        if (empty($data['net_suite_id'])) {
            $error['net_suite_id'] = 'NetSuite Sales ID is required.';
        } else if (strlen($data['net_suite_id']) < 2 || strlen($data['net_suite_id']) > 5) {
            $error['net_suite_id'] = 'NetSuite Sales ID should contain 5 character long';
        } else if (!ctype_alnum(str_replace('-', '', $data['net_suite_id']))) {
            $error['net_suite_id'] = 'Alphanumeric are allowed in NetSuite Sales ID only.';
        }else if(!empty($data['net_suite_id']) && get_netsales_suit_id($data['net_suite_id'])){
            $error['net_suite_id'] = 'NetSuite Sales Id is already in use.';
        }
            
        if (empty($data['agreement'])) {
            $error['agreement'] = 'Agreement Date is required.';
        }

        if (empty($data['total_participant'])) {
            $error['total_participant'] = 'Number of participant is required.';
        }
        
        if (empty($data['product'])) {
            $error['product'] = 'Program Type is required.';
        }

        if (!empty($error)) {
            $client_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }

    /**
     * @since  1.0
     */
    private function insert_client($data) {
        
        global $current_user;
        
        if(nuclo_get_user_role($current_user->ID) == 'editor'){
            $data['nc_franchisee'] = get_current_user_franchisee_id();
        }
        
       
        # Adding Franchisee Owner
        $data['ceo_password'] = wp_generate_password();

        
        $ceo_user_id = wp_insert_user(array(
            'user_login'    => $data['ceo_email'],
            'user_email'    => $data['ceo_email'],
            'user_pass'     => $data['ceo_password'],
            'role'          => 'teacher')
        );
        
        
        if(!empty($data['c_o_email'])){
            
            $data['other_password'] = wp_generate_password();
            
            $other_user_id = wp_insert_user(array(
                'user_login'    => $data['c_o_email'],
                'user_email'    => $data['c_o_email'],
                'user_pass'     => $data['other_password'],
                'role'          => 'teacher')
            );
        }
        
        if (!is_wp_error($ceo_user_id)) {

            $data['ceo_user_id'] = $ceo_user_id;

            // Updating User Meta
            update_user_meta($ceo_user_id, 'first_name', $data['ceo_fname']);
            update_user_meta($ceo_user_id, 'last_name', $data['ceo_lname']);
            update_user_meta($ceo_user_id, 'contact', $data['ceo_contact']);
            update_user_meta($ceo_user_id, 'address', $data['address']);
            update_user_meta($ceo_user_id, 'city', $data['city']);
            update_user_meta($ceo_user_id, 'state', $data['state']);
            update_user_meta($ceo_user_id, 'country', $data['country']);
            update_user_meta($ceo_user_id, 'franchisee', $data['nc_franchisee']);
            
            // Sending Notification after client added
            do_action('register_new_client', $data);            
            $this->add_notifcation_post('1',$ceo_user_id,$data['total_participant'],$data['client_name']);
            
            if(!is_wp_error($other_user_id)){
                unset($data['ceo_user_id']);
                $data['other_user_id'] = $other_user_id;
                
                update_user_meta($other_user_id, 'first_name', $data['c_o_fname']);
                update_user_meta($other_user_id, 'last_name', $data['c_o_lname']);
                update_user_meta($other_user_id, 'contact', $data['c_o_contact']);
                update_user_meta($other_user_id, 'address', $data['address']);
                update_user_meta($other_user_id, 'city', $data['city']);
                update_user_meta($other_user_id, 'state', $data['state']);
                update_user_meta($other_user_id, 'country', $data['country']);
                update_user_meta($other_user_id, 'franchisee', $data['nc_franchisee']);
                
                // Sending Notification after client added
                do_action('register_new_client', $data);
                $this->add_notifcation_post('1',$other_user_id,$data['total_participant'],$data['client_name']);
            }

            
        }

        # Adding Franchisee Post type
        $client = array(
            'post_title' => $data['client_name'],
            'post_type' => 'client',
            'post_status' => 'publish',
            'post_author' => get_current_user_id()
        );

        $client_id = wp_insert_post($client);  

        if (!empty($client_id)) {
            $this->save_meta($client_id, $data, 'create');
            $data['client_id'] = $client_id;
            do_action('create_workflow', $data);
        }
        
        if (!is_wp_error($ceo_user_id) && !empty($client_id)) {

            # Adding relative user and post meta
            update_post_meta($client_id, 'client_owner', $ceo_user_id);

            update_user_meta($ceo_user_id, 'client', $client_id);
            
            if(!is_wp_error($other_user_id)){
                update_user_meta($other_user_id, 'client', $client_id);
            }
            update_user_meta($ceo_user_id, 'is_client_owner', 'yes');

            do_action('create_client');

            // Redirect to Create page and Display message
            wp_safe_redirect($data['_wp_http_referer'] . '/?msg=success');
            exit();
        } else {

            global $client_messsage;
            $client_messsage['error']['create'] = 'Something went wrong!';
        }
    }

    /**
     * @since  1.0
     */
    private function update_client($data) {

        # Updating Franchisee Post type
        $client = array(
            'ID' => $data['post_id'],
            'post_title' => $data['client_name'],
            'post_type' => 'client'
        );

        $client_id = wp_update_post($client);

        if (!empty($client_id)) {

            $this->save_meta($client_id, $data, 'update');

            // Redirect to Create page and Display message
            $url    = strtok($data['_wp_http_referer'], '?');

            wp_safe_redirect($url . '/?msg=success');
            exit();
        } else {

            global $client_messsage;
            $client_messsage['error']['create'] = 'Something went wrong!';
        }
    }

    /**
     * @since  1.0
     */
    private function update_license($data) {

        $client_id = $data['client_id'];
        $program_type = $data['product'];

        $license_row = array(
            'net_suite_sales_id' => $data['net_suite_id'],
            'agreement_date' => strtotime($data['agreement']),
            'total_participants' => $data['total_participant'],
            'program_type' => $data['product'],
        );
        add_row('field_56d7e3fadd48c', $license_row, $client_id);
        
        $workflow_program_type = array();
        $workflow_program_type = Workflow::get_workflow_program_type_by_client($client_id);
        
        if(in_array($program_type, $workflow_program_type))
        {        
            $workflow_id = array_search($program_type, $workflow_program_type);
            $update_data['workflow_id'] = $workflow_id;
            $update_data['total_participant'] = $data['total_participant'];
            do_action('update_participant_workflow', $update_data);
            $_SESSION['WORKFLOW']['SUCCESS'] = 'The training agreement has been successfully updated. You may now add participants.';            
        }
        else
        {
            $insert_data['client_name'] = get_the_title($client_id);
            $insert_data['client_id'] = $client_id;
            $insert_data['product'] = $program_type;
            $insert_data['total_participant'] = $data['total_participant'];
            $insert_data['nc_franchisee'] = get_post_meta($client_id,'franchisee',true);
            do_action('create_workflow', $insert_data);
            $_SESSION['WORKFLOW']['SUCCESS'] = 'The training agreement has been successfully created. You may now add participants.';
        }
        // Redirect to Create page and Display message
//        $url    = strtok($data['_wp_http_referer'], '?');
//        wp_safe_redirect($url . '/?nc_id='.$client_id.'&msg=success');
        wp_redirect(home_url('create-user'));
        exit();
    }

    /**
     * @since  1.0
     */
    private function save_meta($client_id, $data, $type) {

        # Setting up client post meta
        if (current_user_can('administrator') || current_user_can('super-admin')) {
            update_post_meta($client_id, 'franchisee', $data['nc_franchisee']);
        } else {
            $franchisee_id = get_current_user_franchisee_id();
            update_post_meta($client_id, 'franchisee', $franchisee_id);
        }
        update_post_meta($client_id, 'client_email', $data['ceo_email']);
        update_post_meta($client_id, 'client_contact', $data['ceo_contact']);
        update_post_meta($client_id, 'hr_email', $data['c_o_email']);
        update_post_meta($client_id, 'hr_contact', $data['c_o_contact']);       
        update_post_meta($client_id, 'address', $data['address']);
        update_post_meta($client_id, 'city', $data['city']);
        update_post_meta($client_id, 'state', $data['state']);
        update_post_meta($client_id, 'country', $data['country']);
        //update_post_meta($client_id, 'region', $data['region']);
        update_post_meta($client_id, 'zip', $data['postal_code']);
        update_post_meta($client_id, 'sic_code', $data['sic_code']);
        //update_post_meta($client_id, 'product', $data['product']);
        update_post_meta($client_id, 'coemployee', $data['coemployee']);
        update_post_meta($client_id, 'g_presence', $data['g_presence']);

        if ('create' === $type) {
            $license_row = array(
                'net_suite_sales_id' => $data['net_suite_id'],
                'agreement_date' => strtotime($data['agreement']),
                'total_participants' => $data['total_participant'],
                'program_type' => $data['product'],
            );
            add_row('field_56d7e3fadd48c', $license_row, $client_id);
        }
    }

    /**
     * @since  1.0
     */
    public static function get_clients($franchisee_id = 0 ) {

        // ACL - Bring franchisee under particular franchisee group
        // under which the logged in user is enrolled
        $user_id = get_current_user_id();
        $user_role = nuclo_get_user_role($user_id);

        if ('editor' == $user_role) {
            // Get franchisee ID
            //$parent_franchisee = get_user_meta($user_id, 'franchisee', true);
            $c_args = array(
                'post_type' => 'client',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_key' => 'franchisee',
                'meta_value' => get_current_user_franchisee_id()
            );
        } else if($franchisee_id != 0){
            $c_args = array(
                'post_type' => 'client',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_key' => 'franchisee',
                'meta_value' => $franchisee_id
            );
        } else {
            $c_args = array(
                'post_type' => 'client',
                'post_status' => 'publish',
                'posts_per_page' => -1
            );
        }

        $clients = get_posts($c_args);
        
        
        # formatting data
        $all_clients = array();
        foreach ($clients as $c) : setup_postdata($c);

            $return = array(
                "id" => $c->ID,
                "name" => $c->post_title
            );
            $all_clients[] = $return;

        endforeach;

        # Reseting post query
        wp_reset_postdata();
      
        return $all_clients;
    }

    /**
     * @since  1.0
     */
    public static function load_client_sales_id($client = NULL,$default = NULL) {
        
        $return = array();
        
        if (defined('DOING_AJAX') && DOING_AJAX && $client == NULL) {
            $data = $_REQUEST;
            $client = $data['client']; 
        }
        
        $rows   = get_field('license', $client);
        
        foreach($rows as $row){
             $return['data'][]   = $row['net_suite_sales_id'];
        }
        
        
        if (defined('DOING_AJAX') && DOING_AJAX) {
            
            if($return['data']) {
                $return['status'] = 'success';
            } else {
                $return['status'] = 'error';
                $return['data'] = 'No sales id found';
            }
            
            echo json_encode($return);            
            exit();
            
        }else{
            
            $html   = '<option value="">--Select Netsuite Sales id--</option>';
            $count  = 0;
            foreach($return['data'] as $sale_id){
                $selected   = ($sale_id == $default)?'selected':($count == 0)?'selected':'';
                $html .= '<option value="'.$sale_id.'" '.$selected.' >'.$sale_id.'</option>';
                $count++;
            }
            
            return $html;
        }
    }

    /**
     * @since 1.0
     */
        
    public function filter_query($query) {
        
        if (isset($query->query_vars['post_type'])) {

            if ($query->query_vars['post_type'] == 'client' && !is_admin()) {
                
                $query->query_vars['posts_per_page'] = $this->per_page;
                $query->query_vars['paged'] = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;
                
                if (current_user_can('editor')) {
                    $franchisee_id = get_current_user_franchisee_id();
                    $query->set('meta_key', 'franchisee');
                    $query->set('meta_value', $franchisee_id);
                }
                if(get_current_user_role()=='author')
                {
                    $franchisee_id = get_current_user_franchisee_id();
                    global $current_user;
                    $id_array = array();
                    $args = array(
                        'post_type'     => 'group',
                        'post_status'   => 'publish',
                        'meta_query' => array(
                            array(
                                'key' => 'franchisee',
                                'value' => $franchisee_id
                            )
                        )
                    );
                    
                    $getPosts = new WP_Query($args);
                    $results = $getPosts->get_posts();
                    if ($results) {
                        foreach ($results as $k => $v) {
                            $g_id = $v->ID;
                            $facilitator = get_post_meta($g_id,'facilitators');
                            if(in_array($current_user->ID, $facilitator))
                            {
                                $id_array[] = get_post_meta($g_id,'client',true);
                            }
                        }
                    }
                    
                    if (empty($id_array)) {
                        $id_array = array('0');
                    }
                    $query->set('post__in', $id_array);
                }

                if (isset($_GET) && !empty($_GET['search'])) {
                    $search_text = sanitize_text_field($_GET['search']);
                    $query->set('s', $search_text);
                }
                
                $query->query_vars['orderby']   = 'title';
                $query->query_vars['order']     = 'asc';
            }
        }
    }

    /**
     * @since 1.0
     */
    public function get_client_details($client_id) {

        $client = get_post($client_id, ARRAY_A);
        $client['franchisee'] = get_post_meta($client_id, 'franchisee', true);
        $client['client_email'] = get_post_meta($client_id, 'client_email', true);
        $client['client_contact'] = get_post_meta($client_id, 'client_contact', true);
        $client['ceo_email'] = get_post_meta($client_id, 'ceo_email', true);
        $client['ceo_contact'] = get_post_meta($client_id, 'ceo_contact', true);
        $client['ceo_fname'] = get_post_meta($client_id, 'ceo_fname', true);
        $client['ceo_lname'] = get_post_meta($client_id, 'ceo_lname', true);
        $client['address'] = get_post_meta($client_id, 'address', true);
        $client['city'] = get_post_meta($client_id, 'city', true);
        $client['state'] = nc_state_name(get_post_meta($client_id, 'state', true));
        $client['country'] = nc_country_name(get_post_meta($client_id, 'country', true));
        $client['region'] = get_post_meta($client_id, 'region', true);
        $client['zip'] = get_post_meta($client_id, 'zip', true);
        $client['sic_code'] = get_post_meta($client_id, 'sic_code', true);
        $client['product'] = get_post_meta($client_id, 'product', true);

        return $client;
    }

    /**
     * @since 1.0
     */
    private function add_notifcation_post($type, $user_id, $total_participant,$client_name) {
        $client = array(
            'post_title' => $client_name,
            'post_type' => 'notification',
            'post_status' => 'publish',
            'post_author' => get_current_user_id()
        );

        $client_id = wp_insert_post($client);

        if (!empty($client_id)) {

            # Adding relative notification and post meta
            update_post_meta($client_id, 'notification_type', $type);
            update_post_meta($client_id, 'user_role', 'franchise');
            update_post_meta($client_id, 'user_id', $user_id);
            update_post_meta($client_id, 'total_participant', $total_participant);
            update_post_meta($client_id, 'created_by', get_current_user_id());
            update_post_meta($client_id, 'read', 0);
        }
    }
    
    public static function get_program_type_for_facilitator()
    {
        $franchisee_id = get_current_user_franchisee_id();
        global $current_user;
        $p_type = array();
        $args = array(
            'post_type'     => 'group',
            'post_status'   => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'franchisee',
                    'value' => $franchisee_id
                )
            )
        );

        $getPosts = new WP_Query($args);
        $results = $getPosts->get_posts();
        if ($results) {
            foreach ($results as $k => $v) {
                $g_id = $v->ID;
                $facilitator = get_post_meta($g_id,'facilitators');
                if(in_array($current_user->ID, $facilitator))
                {
                    $workflow_id = Workflow::get_workflow_by_group($g_id);
                    if($workflow_id!=0)
                    {
                        $program_type = get_post_meta($workflow_id,'program_type',true);
                        $p_type[$workflow_id] = $program_type;
                    }
                }
            }
        }
        return $p_type;
    }

}	


new Client();

function get_client_users($client_id = NULL) {
    $id     = (is_null($client_id))?get_the_ID():$client_id;

    if(is_array($id)) {
        $users = get_users(array(
            'role' => 'contributor',
            'meta_query'    => array(
                array(
                    'key'       => 'client', 
                    'value'     => $id, 
                    'compare'   => 'IN'
                )            
            )        
        ));
    } else {
        $users = get_users(array(
        'role' => 'contributor',
        'meta_query'    => array(
            array(
                'key'       => 'client', 
                'value'     => $id, 
                'compare'   => '='
            )            
        )        
    ));
    }
    
   
    return $users;
}

function get_client_users_count() {
    $users = get_client_users();
    return count($users);
}

function get_client_users_ids($client_ids = NULL) {
    $id  = (is_null($client_ids))?get_the_ID():$client_ids;
    $ids = array();
    $users = get_client_users($id);
    foreach ($users as $user) {
        $ids[] = $user->ID;
    }
    return array_unique($ids);
}

function get_client_admins($client_id = 0) {
    
    if($client_id == 0)
    {
        $id = get_the_ID();
    }
    else
    {
        $id = $client_id;
    }
    
    $users = get_users(array(
        'role' => 'teacher', 
        'meta_query'    => array(
            'relation'  => 'AND',
            array(
                'key'       => 'client',
                'value'     => $id,
                'compare'   => '='
            )
        )        
    ));
    return $users;
}

function get_client_admins_count($client_id = 0) {
    $users = get_client_admins($client_id);
    return count($users);
}

function get_client_admins_ids() {
    $ids = array();
    $users = get_client_admins();
    foreach ($users as $user) {
        $ids[] = $user->ID;
    }
    return array_unique($ids);
}

function total_license_count($client_id = NULL) {

    $client_id = (is_null($client_id))?get_the_ID():$client_id;

    $total = 0;

    if (have_rows('license', $client_id)):
        while (have_rows('license', $client_id)) : the_row();
            if (get_sub_field('total_participants') > 0)
                $total += get_sub_field('total_participants');
        endwhile;
    endif;
    
    return $total;
}

function get_netsales_suit_id($sales_id){
    
    global $wpdb;
    
    $query      = "SELECT DISTINCT ID FROM $wpdb->posts WHERE post_type = 'client' AND post_status = 'publish'";
    $clients    = $wpdb->get_results($query);
    
    
    foreach($clients as $client){    
        if( have_rows('license',$client->ID) ):
            while ( have_rows('license',$client->ID) ) : the_row();
                $id = get_sub_field('net_suite_sales_id');                   
                if($sales_id == $id){
                    return TRUE;                
                }
            endwhile;        
        else:
            return FALSE;
        endif;
    }
}

function get_license_user($sales_id){
    
    
    $participant_user_args    = array(
        'role'          => 'contributor',
        'number'        => 99999999
    );
    $participant_user_args['meta_query'][]  = array(
        'key'   => 'sales_id',
        'value' => $sales_id
    );
    $participant_users  = new WP_User_Query($participant_user_args);
    
    return count($participant_users->results);
    
}