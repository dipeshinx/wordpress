<?php

function wf_client_start() {

    if (is_page('create-client')) {
        if (!isset($_SESSION['WF_CLIENT']['start'])) {
            $_SESSION['WF_CLIENT']['start'] = 1;
        } else {
            if ($_SESSION['WF_CLIENT']['end'] == 1) {
                $_SESSION['WF_CLIENT']['start'] = 1;
                unset($_SESSION['WF_CLIENT']['client']);
            }
        }
    } elseif (is_page('create-user') || is_page('import-user')) {
        if (isset($_SESSION['WF_CLIENT']['end']) && $_SESSION['WF_CLIENT']['end'] == 1) {
            unset($_SESSION['WF_CLIENT']['user']);
        }
    } elseif (is_page('create-group')) {
        if (isset($_SESSION['WF_CLIENT']['end']) && $_SESSION['WF_CLIENT']['end'] == 1) {
            unset($_SESSION['WF_CLIENT']['group']);
        }
    } elseif (is_page('compose-email')) {
        if (isset($_SESSION['WF_CLIENT']['end']) && $_SESSION['WF_CLIENT']['end'] == 1) {
            unset($_SESSION['WF_CLIENT']['email']);
        }
    } elseif (is_post_type_archive('client')) {

        if (isset($_SESSION['WF_CLIENT']['email']) && $_SESSION['WF_CLIENT']['email'] == 1) {
            $_SESSION['WF_CLIENT']['end'] = 1;
        } else {
            unset($_SESSION['WF_CLIENT']);
        }
        if (isset($_SESSION['WF_CLIENT']['end']) && $_SESSION['WF_CLIENT']['end'] == 1) {
            unset($_SESSION['WF_CLIENT']);
        }
    }
}

add_action('wp_head', 'wf_client_start', 10);

function wf_client_client() {

    if (isset($_SESSION['WF_CLIENT']['start']) && $_SESSION['WF_CLIENT']['start'] == 1) {
        unset($_SESSION['CLIENT_DATA']);
        $_SESSION['WF_CLIENT']['client'] = 1;
        $_SESSION['WORKFLOW']['SUCCESS'] = 'Client successfully created.';
        wp_redirect(home_url('create-user'));
        exit;
    } else if (isset($_SESSION['WF_CLIENT']['end']) && $_SESSION['WF_CLIENT']['end'] == 1) {
        unset($_SESSION['WF_CLIENT']['client']);
    }
}

add_action('create_client', 'wf_client_client');

function wf_client_user() {

    if (isset($_POST['continue'])) {
        $group_id = isset($_SESSION['WORKFLOW']['GROUP_ID']) ? $_SESSION['WORKFLOW']['GROUP_ID'] : '';
        if ($group_id != '') {
            $_SESSION['WORKFLOW']['SUCCESS'] = 'Participant successfully created.';
            $post_data = get_post($group_id);
            $url = '/group/' . $post_data->post_name . '/edit';
            wp_redirect(home_url($url));
            exit;
        } else {
            $_SESSION['WORKFLOW']['SUCCESS'] = 'Participant successfully created.';
            wp_redirect(home_url('create-group'));
            exit;
        }
    }
}

function wf_client_user_import() {
    if (isset($_POST['continue'])) {
        $group_id = isset($_SESSION['WORKFLOW']['GROUP_ID']) ? $_SESSION['WORKFLOW']['GROUP_ID'] : '';
        if ($group_id != '') {
            $post_data = get_post($group_id);
            $url = '/group/' . $post_data->post_name . '/edit';
            wp_redirect(home_url($url));
            exit;
        } else {
            wp_redirect(home_url('create-group'));
            exit;
        }
    }
}

add_action('create_user', 'wf_client_user');
add_action('import_user', 'wf_client_user_import');

function wf_client_group() {

    if (isset($_POST['continue'])) {
        wp_redirect(home_url('compose-email'));
        exit;
    }
}

add_action('create_group', 'wf_client_group');

function wf_client_email() {

    if (isset($_POST['continue'])) {
        wp_redirect(home_url('client'));
        exit;
    }
}

add_action('compose_email', 'wf_client_email');

function wf_client_reset() {
    unset($_SESSION['WF_CLIENT']);
    unset($_SESSION['WF_COURSE']);
}

add_action('wp_logout', 'wf_client_reset');

function hq_workflow_html($when) {

    if (isset($_SESSION['WF_COURSE']) && $_SESSION['WF_COURSE']['start']) {

        $hq_workflow = array('course' => 'Courses', 'pre' => 'Pre Assessment', 'extra' => 'Extra', 'feedback' => 'Training Feedback', 'plan' => 'Action Plan', 'debrief' => 'Monthly Debrief');

        if (in_array($_SESSION['WF_COURSE']['current'], $when)) {

            $count = 1;

            echo '<div class="wrapper m-t-sm"><div class="ibox"><div class="ibox-content" style="padding-left:0;padding-right:0;"><div class="row bs-wizard no-borders">';

            foreach ($hq_workflow as $id => $workflow) {

                $class = ($_SESSION['WF_COURSE']['current'] == $id) ? 'active' : ($_SESSION['WF_COURSE'][$id] == 1 ? 'complete' : 'disabled');
                if ($count == 1 || $count == 7) {
                    $col = 'col-xs-1';
                } else {
                    $col = 'col-xs-2';
                }

                echo '<div class="' . $col . ' bs-wizard-step ' . $class . '"><div class="text-center bs-wizard-stepnum">' . $workflow . '</div><div class="progress"><div class="progress-bar"></div></div><a href="#" class="bs-wizard-dot"></a></div>';

                $count++;
            }

            echo '</div></div></div></div>';
        } else if ($_SESSION['WF_COURSE']['current'] == 'end') {
            unset($_SESSION['WF_COURSE']);
            echo '<div class="alert alert-success">Congrats! Your course components are set up now. Now you may choose to create a bundle and duplicate components as required.</div>';
        }
    }
}

function wf_course_start() {
    unset($_SESSION['WF_COURSE']);
    $_SESSION['WF_COURSE']['start'] = 1;
    $_SESSION['WF_COURSE']['current'] = 'course';
    $_SESSION['WF_COURSE']['course_id'] = get_the_ID();
}

add_action('create_program', 'wf_course_start');

function wf_course_content($data) {

    if ($_SESSION['WF_COURSE']['current'] && $_SESSION['WF_COURSE']['current'] == 'course') {

        $_SESSION['WF_COURSE']['current'] = 'pre';

        $_SESSION['WF_COURSE']['course'] = 1;

        $_SESSION['WF_COURSE']['course_id'] = $data['course_id'];

        $url = ($data['submit'] == 'next') ? get_permalink($data['course_id']) . 'modules/survey/' : get_permalink($data['course_id']) . 'content';

        wp_redirect($url);

        exit;
    }
}

add_action('course_content_updated', 'wf_course_content', 10, 1);

function wf_course_pre_feedback($course_id) {

    if ($_SESSION['WF_COURSE']['course'] && $_SESSION['WF_COURSE']['current'] == 'pre') {

        $_SESSION['WF_COURSE']['current'] = 'extra';

        $_SESSION['WF_COURSE']['pre'] = 1;

        $url = get_permalink($course_id) . 'modules/extra/';

        wp_redirect($url);

        exit;
    } else if ($_SESSION['WF_COURSE']['extra'] && $_SESSION['WF_COURSE']['current'] == 'feedback') {

        $_SESSION['WF_COURSE']['current'] = 'plan';

        $_SESSION['WF_COURSE']['feedback'] = 1;

        $url = get_permalink($course_id) . 'modules/plan/';

        wp_redirect($url);

        exit;
    }
}

add_action('survey_created', 'wf_course_pre_feedback', 10, 1);

function wf_course_extra($course_id) {

    if ($_SESSION['WF_COURSE']['pre'] && $_SESSION['WF_COURSE']['current'] == 'extra') {

        $_SESSION['WF_COURSE']['current'] = 'feedback';

        $_SESSION['WF_COURSE']['extra'] = 1;

        $url = get_permalink($course_id) . 'modules/survey/';

        wp_redirect($url);

        exit;
    }
}

add_action('course_extra_created', 'wf_course_extra', 10, 1);

function wf_course_plan($course_id) {

    if ($_SESSION['WF_COURSE']['feedback'] && $_SESSION['WF_COURSE']['current'] == 'plan') {

        $_SESSION['WF_COURSE']['current'] = 'debrief';

        $_SESSION['WF_COURSE']['plan'] = 1;

        $url = home_url('compose-email');

        wp_redirect($url);

        exit;
    }
}

add_action('course_plan_created', 'wf_course_plan', 10, 1);

function wf_course_debrief() {

    if ($_SESSION['WF_COURSE']['plan'] && $_SESSION['WF_COURSE']['current'] == 'debrief') {

        $_SESSION['WF_COURSE']['current'] = 'result';

        $_SESSION['WF_COURSE']['debrief'] = 1;

        $course_id = $_SESSION['WF_COURSE']['course_id'];

        $url = get_permalink($course_id) . 'modules/result/';

        wp_redirect($url);

        exit;
    }
}

add_action('compose_email', 'wf_course_debrief');

function wf_course_result($course_id) {

    if ($_SESSION['WF_COURSE']['debrief'] && $_SESSION['WF_COURSE']['current'] == 'result') {

        $_SESSION['WF_COURSE']['current'] = 'end';

        $_SESSION['WF_COURSE']['result'] = 1;

        $_SESSION['WF_COURSE']['$course_id'] = $course_id;

        $url = get_permalink($course_id) . 'modules';

        wp_redirect($url);

        exit;
    }
}

add_action('course_plan_result', 'wf_course_result', 10, 1);

function participant_workflow() {

    global $post;

    if ($post->post_type == 'bundle') {
        $bundle_id = $post->ID;
        $course_id = $post->post_parent;
    } else {
        $bundle_id = $post->post_parent;
        $bundle_post = get_post($bundle_id);
        $course_id = $bundle_post->post_parent;
    }

    if ($post->post_type == 'bundle' || $post->post_parent != $course_id) {

        $lists = array();
        $lists['current'] = $post->ID;

        $bundle_args = array(
            'post_type' => array('extra', 'discussion', 'survey', 'measure', 'assessment'),
            'post_parent' => $bundle_id,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        $bundle_query = new WP_Query($bundle_args);

        if ($bundle_query->have_posts()) {
            while ($bundle_query->have_posts()) {
                $bundle_query->the_post();
                $list['id'] = get_the_ID();
                $list['name'] = empty(get_field('tag')) ? ucfirst(get_post_type()) : get_field('tag');
                $list['url'] = get_permalink();
                array_push($lists, $list);
            }
        }

        wp_reset_postdata();

        $query = new WP_Query(array('post_type' => 'bundle', 'post__in' => array($bundle_id)));

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $next = mod_get_adjacent_post('next', array('survey', 'extra', 'measure', 'discussion', 'assessment', 'bundle'));
                $list['id'] = $next->ID;
                $list['name'] = empty(get_field('tag', $next->ID)) ? ucfirst(get_post_type($next->ID)) : get_field('tag', $next->ID);
                $list['url'] = get_permalink($next->ID);
                array_push($lists, $list);
            }
        }

        wp_reset_postdata();
    }


    return $lists;
}

function participant_workflow_html() {

    global $post, $current_user;

    if ($post->post_type == 'bundle') {
        $bundle_id = $post->ID;
        $course_id = $post->post_parent;
    } else {
        $bundle_id = $post->post_parent;
        $bundle_post = get_post($bundle_id);
        if ($bundle_post->post_type == 'program') {
            $course_id = $bundle_post->post_parent;
            $bundle_id = 0;
        }
    }

    if ($post->post_type == 'bundle' || $post->post_parent == $bundle_id) {

        $lists = participant_workflow();

        echo '<div class="wrapper m-t-sm"><div class="ibox"><div class="ibox-content" style="padding-left:0;padding-right:0;"><div class="row bs-wizard no-borders">';

        $count = count($lists) - 1;

        $m_count = 1;

        $current = $lists['current'];

        unset($lists['current']);

        foreach ($lists as $key => $list) {

            $class = ($current == $list['id']) ? 'active' : 'disabled';

            $status = get_user_meta($current_user->ID, 'module_result_' . $list['id'], true);

            if ($status) {
                $class = 'complete';
            }

            if ($count == 2) {
                $col = 'col-xs-6';
            } else if ($count == 3) {
                $col = 'col-xs-4';
            } else if ($count == 4) {
                $col = 'col-xs-3';
            } else if ($count == 5) {
                $col = 'col-xs-2';
                $offset = 'col-xs-offest-2';
            } else if ($count == 6) {
                $col = 'col-xs-2';
            }

            if ($m_count == 1) {
                $offset = 'col-xs-offest-2';
            } else {
                $offset = '';
            }

//            $workflow_html   = "";
//            $workflow_html  .= '<div class="'.$col.' bs-wizard-step ' . $offset .' '. $class . '">';
//            $workflow_html  .= '<div class="text-center bs-wizard-stepnum">' . $list['name'] . '</div><div class="progress"><div class="progress-bar"></div></div>';
//            $workflow_html  .= '<a href="'.$list['url'].'" class="bs-wizard-dot">';
//            if($status){
//                $workflow_html  .= '<i class="fa fa-check"></i>';
//            }
//            $workflow_html  .= '</a>';
//            $workflow_html  .= '</div>';
//            
//            echo $workflow_html;
//            
            echo '<div class="' . $col . ' bs-wizard-step ' . $offset . ' ' . $class . '">'
            . '<div class="text-center bs-wizard-stepnum">' . $list['name'] . '</div><div class="progress"><div class="progress-bar"></div></div>'
            . '<a href="' . $list['url'] . '" class="bs-wizard-dot">'
            . '</a>'
            . '</div>';

            $m_count++;
        }

        echo '</div></div></div></div>';
    }
}

function mod_get_adjacent_post($direction = 'prev', $post_types = 'post') {
    global $post, $wpdb;

    if (empty($post))
        return NULL;
    if (!$post_types)
        return NULL;

    if (is_array($post_types)) {
        $txt = '';
        for ($i = 0; $i <= count($post_types) - 1; $i++) {
            $txt .= "'" . $post_types[$i] . "'";
            if ($i != count($post_types) - 1)
                $txt .= ', ';
        }
        $post_types = $txt;
    }

    $current_post_order = $post->menu_order;
    $current_post_parent = $post->post_parent;

    $join = '';
    $in_same_cat = FALSE;
    $excluded_categories = '';
    $adjacent = $direction == 'prev' ? 'previous' : 'next';
    $op = $direction == 'prev' ? ($current_post_order - 1) : ($current_post_order + 1);
    $order = $direction == 'prev' ? 'DESC' : 'ASC';

    $join = apply_filters("get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories);
    $where = apply_filters("get_{$adjacent}_post_where", $wpdb->prepare("WHERE p.menu_order = %d AND p.post_type IN({$post_types}) AND p.post_status = 'publish' AND post_parent = %d", $op, $current_post_parent), $in_same_cat, $excluded_categories);
    $sort = apply_filters("get_{$adjacent}_post_sort", "ORDER BY p.menu_order $order LIMIT 1");

    $query = "SELECT p.* FROM $wpdb->posts AS p $join $where $sort";
    $query_key = 'adjacent_post_' . md5($query);
    $result = wp_cache_get($query_key, 'counts');
    if (false !== $result)
        return $result;

    $result = $wpdb->get_row("SELECT p.* FROM $wpdb->posts AS p $join $where $sort");
    if (null === $result)
        $result = '';

    wp_cache_set($query_key, $result, 'counts');
    return $result;
}

class Workflow {

    public function __construct() {

        add_action('init', array($this, 'register_post_type'));
        add_action('pre_get_posts', array($this, 'filter_query'), 1, 1);
        add_action('create_workflow', array($this, 'save_post'));
        add_action('save_participant_workflow', array($this, 'save_participant'),2,1);
        add_action('save_group_workflow', array($this, 'save_group'),3,1);
        add_action('save_session_workflow', array($this, 'save_session'),4,1);
        add_action('save_meeting_workflow', array($this, 'save_meeting'),5,1);
        add_action('update_participant_workflow', array($this, 'update_total_participants'),6,1);
    }

    public function register_post_type() {
        if (function_exists('get_field')) :
            $labels = array(
                'name' => _x(get_field('workflow_general_name', 'option'), 'post type general name', 'nuclo'),
                'singular_name' => _x(get_field('workflow_singular_name', 'option'), 'post type singular name', 'nuclo'),
                'menu_name' => _x(get_field('workflow_general_name', 'option'), 'admin menu', 'nuclo'),
                'name_admin_bar' => _x(get_field('workflow_singular_name', 'option'), 'add new on admin bar', 'nuclo'),
                'add_new' => _x('Add New', strtolower(get_field('workflow_singular_name', 'option')), 'nuclo'),
                'add_new_item' => __('Add New ' . get_field('workflow_singular_name', 'option'), 'nuclo'),
                'new_item' => __('New ' . get_field('workflow_singular_name', 'option'), 'nuclo'),
                'edit_item' => __('Edit ' . get_field('workflow_singular_name', 'option'), 'nuclo'),
                'view_item' => __('View ' . get_field('workflow_singular_name', 'option'), 'nuclo'),
                'all_items' => __('All ' . get_field('workflow_general_name', 'option'), 'nuclo'),
                'search_items' => __('Search ' . get_field('workflow_general_name', 'option'), 'nuclo'),
                'parent_item_colon' => __('Parent ' . get_field('workflow_general_name', 'option') . ':', 'nuclo'),
                'not_found' => __('No ' . strtolower(get_field('workflow_general_name', 'option')) . ' found.', 'nuclo'),
                'not_found_in_trash' => __('No ' . strtolower(get_field('workflow_general_name', 'option')) . ' found in Trash.', 'nuclo')
            );

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => 'workflow'),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => true,
                'menu_position' => null,
                'supports' => array('title', 'author')
            );

            register_post_type('workflow', $args);
        endif;
    }

    public function save_post($data) {
        $client_id = $data['client_id'];
        $workflow_data = array(
            'post_title' => $data['client_name'] . ' - Workflow',
            'post_type' => 'workflow',
            'post_status' => 'publish',
            'post_author' => get_current_user_id()
        );

        $workflow_id = wp_insert_post($workflow_data);
        $data['client_id'] = $client_id;
        $data['program_type'] = $data['product'];
        $this->save_meta($workflow_id, $data);

        $session_data = [];
        $session_data['workflow_id'] = $workflow_id;
        $session_data['client_id'] = $client_id;
        $session_data['program_type'] = $data['program_type'];
        $session_data['status'] = '1';
        do_action('session_workflow', $session_data);

        return $workflow_id;
    }

    public function save_meta($id, $data) {

        update_post_meta($id, 'number_of_participant', $data['total_participant']);
        update_post_meta($id, 'franchisee', $data['nc_franchisee']);
        update_post_meta($id, 'client_id', $data['client_id']);
        update_post_meta($id, '_client_id', 'field_5739d6a655df5');
        update_post_meta($id, 'program_type', $data['program_type']);
        update_post_meta($id, 'status', '1');
        update_post_meta($id, 'current_added_participant', '0');
    }

    public static function save_participant($participant_id) {
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {

            $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];

            $post_data = get_post_meta($workflow_id);

            $number_of_participant = $post_data['number_of_participant'][0];
            $current_added_particpiant = $post_data['current_added_participant'][0];

            if ($current_added_particpiant < $number_of_participant) {
                $added_particpiant = $post_data['current_added_participant'][0] + 1;

                update_post_meta($workflow_id, 'current_added_participant', $added_particpiant);
                $total_participant = $added_particpiant;
                update_post_meta($workflow_id, 'participants_id_' . $total_participant, $participant_id);
                update_post_meta($workflow_id, '_participants_id_' . $total_participant, 'field_5739d6d755df6');

                update_post_meta($workflow_id, 'status', '2');
                $_SESSION['WORKFLOW']['STATUS'] = '2';
            }
        }
    }

    public static function save_group($group_id) {
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
            $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];

            $post_data = get_post_meta($workflow_id);

            $total_group = isset($post_data['group'][0]) ? $post_data['group'][0] + 1 : 1;

            update_post_meta($workflow_id, 'group', $total_group);
            update_post_meta($workflow_id, 'group_id_' . $total_group, $group_id);
            update_post_meta($workflow_id, '_group_id_' . $total_group, 'field_573c75b607e89');
            update_post_meta($workflow_id, 'status', '3');
            $_SESSION['WORKFLOW']['STATUS'] = '3';
            $_SESSION['WORKFLOW']['GROUP_ID'] = $group_id;
            update_post_meta($workflow_id, 'work_position_' . $total_group, '0');
        }
    }

    public static function save_session($session_id) {
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
            $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];

            $post_data = get_post_meta($workflow_id);

            $total_group = $post_data['group'][0];

            update_post_meta($workflow_id, 'session_id_' . $total_group, $session_id);
            update_post_meta($workflow_id, 'status', '4');
            $_SESSION['WORKFLOW']['STATUS'] = '4';
            $_SESSION['WORKFLOW']['EVENT_ID'] = $session_id;
            update_post_meta($workflow_id, 'work_position_' . $total_group, '1');
        }
    }

    public static function save_meeting($meeting_id) {
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
            $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];

            $post_data = get_post_meta($workflow_id);

            $total_group = $post_data['group'][0];

            update_post_meta($workflow_id, 'meeting_id_' . $total_group, $meeting_id);

            update_post_meta($workflow_id, 'work_position_' . $total_group, '2');
            $remaining_participant = Workflow::check_workflow_status();

            $total_participant = $post_data['number_of_participant']['0'];
            $current_added_participant = $post_data['current_added_participant']['0'];
            if ($current_added_participant != $total_participant) {
                update_post_meta($workflow_id, 'status', '7');
                $_SESSION['WORKFLOW']['STATUS'] = '7';
            } else {
                if ($remaining_participant == 0) {
                    update_post_meta($workflow_id, 'status', '6');
                    $_SESSION['WORKFLOW']['STATUS'] = '6';
                } else {
                    update_post_meta($workflow_id, 'status', '5');
                    $_SESSION['WORKFLOW']['STATUS'] = '5';
                }
            }
        }
    }

    public static function check_workflow_status() {
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID'])) {
            $participant_data = Group::remaining_participant_group();
            $remaining_data = count($participant_data);
            return $remaining_data;
        }
    }

    public function filter_query($query) {
        global $wpdb;

        global $current_user;

        $userRole = $current_user->roles[0];

        if (is_post_type_archive('workflow') && $query->query_vars['post_type'] == 'workflow' && ( $userRole == 'editor' || $userRole == 'teacher')) {

            $user_id = $current_user->ID;
            if ($userRole == 'editor') {
                $franchisee = get_user_meta($user_id, 'franchisee', true);
                $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee AND wp.post_type='workflow'");
            } elseif ($userRole == 'teacher') {
                $client = get_user_meta($user_id, 'client', true);
                $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'client_id' AND wpm.meta_value = $client AND wp.post_type='workflow'");
            }

            $workflow = array();

            if ($results) {
                foreach ($results as $k => $v) {
                    $workflow[] = $v->ID;
                }
            }
            if (empty($workflow)) {
                $workflow = array('0');
            }
            $query->set('post__in', $workflow);

            if (isset($_GET) && !empty($_GET['search'])) {
                $search_text = sanitize_text_field($_GET['search']);
                $query->set('s', $search_text);
            }
        }
    }

    public static function update_total_participants($data) {
        $workflow_id = $data['workflow_id'];
        $total_participant = $data['total_participant'];
        $current_number_of_participant = get_post_meta($workflow_id, 'number_of_participant', true);
        $status = get_post_meta($workflow_id, 'status', true);
        if($status == '6')
        {
            update_post_meta($workflow_id, 'status', 7);
            $status = '7';
        }
        $number_of_participant = $current_number_of_participant + $total_participant;
        update_post_meta($workflow_id, 'number_of_participant', $number_of_participant);
        
        $session_data = [];
        $session_data['workflow_id'] = $workflow_id;
        $session_data['client_id'] = get_post_meta($workflow_id, 'client_id', true);;
        $session_data['program_type'] = get_post_meta($workflow_id, 'program_type', true);
        $session_data['status'] = $status;
        do_action('session_workflow', $session_data);
               
    }
    
    public static function get_client_workflow($workflow_id) {
    
        $workflow_post_meta = get_post_meta($workflow_id);
        if ($workflow_post_meta['status'][0] == 1 || $workflow_post_meta['status'][0] == 7) {
            $workflow_link = home_url('create-user');
            return 'onclick="redirect_workflow(\'' . $workflow_id . '\',\'' . $workflow_link . '\')"';
        }
        if ($workflow_post_meta['status'][0] == 2 || $workflow_post_meta['status'][0] == 5) {
            $workflow_link = home_url('create-group');
            return 'onclick="redirect_workflow(\'' . $workflow_id . '\',\'' . $workflow_link . '\')"';
        }
        if ($workflow_post_meta['status'][0] == 3) {
            $workflow_link = home_url('create-event');
            return 'onclick="redirect_workflow(\'' . $workflow_id . '\',\'' . $workflow_link . '\')"';
        }
        if ($workflow_post_meta['status'][0] == 4) {
            $workflow_link = home_url('compose-email');
            return 'onclick="redirect_workflow(\'' . $workflow_id . '\',\'' . $workflow_link . '\')"';
        }
        if ($workflow_post_meta['status'][0] == 6) {
            return '0';
        }

        return '0';
    }
    
    public static function get_client_workflow_status($workflow_id) {
    
        $workflow_postmeta = get_post_meta($workflow_id);
        if(!empty($workflow_postmeta))
        {
            $total_participant = $workflow_postmeta['number_of_participant']['0'];
            $current_added_participant = $workflow_postmeta['current_added_participant']['0'];
            $client_id = $workflow_postmeta['client_id']['0'];

            $percentage = 20;

            $total_group = $workflow_postmeta['group']['0'];
            $sigle_group = isset($workflow_postmeta['group_id_1'][0]) ? $workflow_postmeta['group_id_1'][0] : 0;
            $sigle_session = isset($workflow_postmeta['session_id_1'][0]) ? $workflow_postmeta['session_id_1'][0] : 0;
            $sigle_meeting = isset($workflow_postmeta['meeting_id_1'][0]) ? $workflow_postmeta['meeting_id_1'][0] : 0;

            $added_user_group = array();

            if ($sigle_group != 0 || $sigle_group != '') {
                $percentage += 20;
            }
            if ($sigle_session != 0 || $sigle_session != '') {
                $percentage += 20;
            }
            if ($sigle_meeting != 0 || $sigle_meeting != '') {
                $percentage += 20;
            }

            if ($total_participant != 0 && $current_added_participant != 0) {
                $percentage += ceil(($current_added_participant * 20) / $total_participant);
            } else {
                $percentage += 0;
            }

            return $percentage . '%';
        }
        return '-';
    }
    
    public static function get_workflow_by_group($group_id) {
    
        $client = get_post_meta($group_id,'client',true);
        
        $args = array(
                    'post_type'     => 'workflow',
                    'post_status'   => 'publish',
                    'meta_query' => array(
                        array(
                            'key' => 'client_id',
                            'value' => $client
                        )
                    )
                );

        $getPosts = new WP_Query($args);
        $results = $getPosts->get_posts();
        
        if (!empty($results)) {
            foreach ($results as $k => $v) {
                $workflow_id = $v->ID;
                $workflow_postmeta = get_post_meta($workflow_id);
                $total_group = get_field('group', $workflow_id);
                
                for ($i = 1; $i <= $total_group; $i++) {
                    $group_workflow_id = $workflow_postmeta['group_id_' . $i][0];
                    if($group_workflow_id == $group_id)
                    {
                        return $workflow_id;
                    }
                }
                
                
            }
        }
        return '0';
    }
    
    public static function get_workflow_id_by_client($client) {
    
        $args = array(
                    'post_type'     => 'workflow',
                    'post_status'   => 'publish',
                    'meta_query' => array(
                        array(
                            'key' => 'client_id',
                            'value' => $client
                        )
                    )
                );

        $getPosts = new WP_Query($args);
        $results = $getPosts->get_posts();
        
        $id_array = array();
        if (!empty($results)) {
            foreach ($results as $k => $v) {
                $id_array[] = $v->ID;
            }
        }
        return $id_array;
    }
    
    public static function get_workflow_program_type_by_client($client) {
    
        $args = array(
                    'post_type'     => 'workflow',
                    'post_status'   => 'publish',
                    'meta_query' => array(
                        array(
                            'key' => 'client_id',
                            'value' => $client
                        )
                    )
                );

        $getPosts = new WP_Query($args);
        $results = $getPosts->get_posts();
        
        $id_array = array();
        if (!empty($results)) {
            foreach ($results as $k => $v) {
                $id_array[$v->ID] = get_post_meta($v->ID,'program_type',true);
            }
        }
        return $id_array;
    }
    
    public static function get_workflow_user($workflow_id)
    {
        $workflow_postmeta = get_post_meta($workflow_id);
        $total_participant = get_field('number_of_participant', $workflow_id);
        $wf_participant = array();

        for ($i = 1; $i <= $total_participant; $i++) {
            if (isset($workflow_postmeta['participants_id_' . $i][0]) && $workflow_postmeta['participants_id_' . $i][0] != '')
                $wf_participant[] = $workflow_postmeta['participants_id_' . $i][0];
        }
        return $wf_participant;
    }

}

new Workflow();

function add_value_workflow_session($data) {
    unset($_SESSION['WORKFLOW']);
    $_SESSION['WORKFLOW']['WORKFLOW_ID'] = $data['workflow_id'];
    $_SESSION['WORKFLOW']['CLIENT_ID'] = $data['client_id'];
    $_SESSION['WORKFLOW']['PROGRAM_TYPE'] = $data['program_type'];
    $_SESSION['WORKFLOW']['STATUS'] = $data['status'];
    $post_meta = get_post_meta($data['workflow_id']);
    if(isset($post_meta['group'][0]))
    {
        $total_group = $post_meta['group'][0];
        if($post_meta['work_position_'.$total_group][0]!='2')
        {
            $_SESSION['WORKFLOW']['GROUP_ID'] = $post_meta['group_id_'.$total_group][0];
            $_SESSION['WORKFLOW']['EVENT_ID'] = $post_meta['session_id_'.$total_group][0];
        }
    }
}
add_action('session_workflow','add_value_workflow_session',1,1);