<?php 

/**
 * @package  NucloApi
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class NucloApi {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'rest_api_init', array($this,'api_init') );
        add_action( 'init', array($this,'init'));
    }

    /**
     * @since 1.0
     */
    public function init() {
        $cookie_name = 'nuclouser';
        if(!isset($_COOKIE[$cookie_name])) {
            //echo "Cookie named '" . $cookie_name . "' is not set!";
        } else {
            //echo "Cookie '" . $cookie_name . "' is set!<br>";
            //echo "Value is: " . $_COOKIE[$cookie_name];
        }
    }
    
    /**
     * @since 1.0
     */
    public function api_init() {
        require_once WP_PLUGIN_DIR . '/rest-api/lib/endpoints/class-nuclo-users-controller.php';
    }

    
}

new NucloApi();