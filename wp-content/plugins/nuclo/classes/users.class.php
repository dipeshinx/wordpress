<?php

/**
 * @package  Users
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Users {

    public $results;
    public $total;
    public $total_pages;
    public $per_page;
    public $offset;
    public $role;
    public $search;
    public $filter;

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action('init', array($this, 'rewrites_init'), 1, 0);
        add_action('init', array($this, 'create'), 1, 0);
        add_action('get_header', array($this, 'redirect_init'), 10, 0);
        add_action('wp_login', array($this, 'wp_login'), 10, 2);
        add_action('author_init', array($this, 'edit'), 1, 0);
        add_filter('get_avatar', array($this, 'custom_avatar'), 1, 5);
        add_action('wp_ajax_delete_user', array($this, 'delete'));
        add_action('wp_ajax_lock_user', array($this, 'lock'));
        add_action('wp_ajax_unlock_user', array($this, 'lock'));
        add_action('wp_ajax_load_address_details', array($this, 'load_address_details'));
        add_action('wp_ajax_get_timezone_dropdown', 'get_timezone_dropdown');
        add_action('wp_ajax_get_user_counts', 'get_user_counts');
        add_action('wp_ajax_get_modal_country_timezone', 'get_modal_country_timezone');
        add_action('edit_user_profile', array($this, 'hide_personal_options'));
        add_filter('user_contactmethods', array($this, 'hide_profile_fields'), 10, 1);

        add_shortcode('username', 'username_shortcode');
        if (!is_admin())
            add_action('pre_get_users', array($this, 'filter_query'), 1, 1);

        $this->per_page = isset($_REQUEST['record_per_page'])?$_REQUEST['record_per_page']:20;
    }

    /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('^users/([^/]*)/?([^/]*)/?$', 'index.php?author_name=$matches[1]&action=$matches[2]', 'top');
    }

    /**
     * @since  1.0
     */
    public function redirect_init() {

        if (is_user_logged_in()) {

            if (isset($_GET['loginas']) && current_user_can('administrator')) {
                $user_id = $_GET['loginas'];
                $user = get_user_by('id', $user_id);
                if ($user) {
                    wp_set_current_user($user_id, $user->user_login);
                    wp_set_auth_cookie($user_id);
                    do_action('wp_login', $user->user_login, $user);
                }
            }

            global $current_user;

            $user_id = $current_user->ID;

            if (isset($_REQUEST['skip']) && is_page('welcome')) {
                update_user_meta($user_id, 'welcome', 'yes');
                wp_redirect(home_url('users/') . $current_user->user_login . '/edit');
                exit;
            }

            $welcome = get_user_meta($user_id, 'welcome', true);
            $status = get_user_meta($user_id, 'profile_update_after_login', true);
            $author = get_query_var('author_name');

            // Checking pre assessment status
            $pre_a_status = get_user_meta($user_id, 'status_survey_10329', true);

            if (empty($welcome) && !is_page('welcome')) {
                wp_redirect(home_url('welcome'));
                exit();
            } else if ($welcome == 'yes' && empty($status) && empty($author)) {
                wp_redirect(home_url('users/') . $current_user->user_login . '/edit');
                exit();
            } else if ((!isset($pre_a_status) || $pre_a_status != 'complete') && current_user_can('participant') && !is_single('10329')) {
                //wp_redirect( get_permalink('10329'));
            }
        }

        if (is_page('users') && !current_user_can('list_users')) {
            die();
        }
    }

    /**
     * @since  1.0
     */
    public function wp_login($user_login, $user) {

        $welcome = get_user_meta($user->ID, 'welcome', true);
        $status = get_user_meta($user->ID, 'profile_update_after_login', true);
        update_user_meta($user->ID, 'last_login', time());

        if (empty($welcome)) {
            wp_redirect(home_url('welcome'));
            exit();
        } else if ($welcome == 'yes' && empty($status)) {
            wp_redirect(home_url('users/') . $user->user_login . '/edit');
            exit();
        } else if ($welcome == 'yes' && $status == 'yes') {
            wp_redirect(home_url('/'));
//            if(in_array(nuclo_get_user_role($user->ID),array('administrator','super-admin','editor'))){
//                wp_redirect(home_url('/courseware/'));
//            }else{
//                wp_redirect(home_url('/'));
//            }
            exit();
        }
    }

    /**
     * @since 1.0
     */
    public function custom_avatar($avatar, $id_or_email, $size, $default, $alt) {
        $user = false;

        if (is_numeric($id_or_email)) {

            $id = (int) $id_or_email;
            $user = get_user_by('id', $id);
        } elseif (is_object($id_or_email)) {

            if (!empty($id_or_email->user_id)) {
                $id = (int) $id_or_email->user_id;
                $user = get_user_by('id', $id);
            }
        } else {
            $user = get_user_by('email', $id_or_email);
        }

        if ($user && is_object($user)) {
            $image = get_field('image', 'user_' . $user->data->ID);
            if (!empty($image)) {
                if ($size == 32) {
                    // mini-size
                    $size = 'mini-size';
                    $thumb = $image['sizes'][$size];
                    $width = $image['sizes'][$size . '-width'];
                    $height = $image['sizes'][$size . '-height'];
                } else {
                    // thumbnail
                    $size = 'thumbnail';
                    $thumb = $image['sizes'][$size];
                    $width = $image['sizes'][$size . '-width'];
                    $height = $image['sizes'][$size . '-height'];
                }
                $avatar = "<img alt='{$alt}' src='{$thumb}' class='avatar avatar-{$size} photo img-circle' height='{$width}' width='{$height}' />";
            } else {
                $avatar = $avatar;
            }
        }

        return $avatar;
    }

    /**
     * @since  1.0
     */
    public function create() {

        global $current_user;

        if (isset($_POST) && (!empty($_POST['submit']) || !empty($_POST['continue'])) && isset($_POST['nuclo_create_user']) && wp_verify_nonce($_POST['nuclo_create_user'], 'nuclo_create_user')) {

            $data = $this->cleandata($_POST);
              
            $current_role = nuclo_get_user_role($current_user->ID);
            
            if($data['franchisee_group']=='')
            {
               $data['franchisee_group'] = isset($_GET['franchisee_group'])?$_GET['franchisee_group']:'';
            }
            
            if ($current_role == 'editor') {
                $data['franchisee_group'] = get_current_user_franchisee_id();
            }
            $validate = $this->validate('create', $data);

            if ($validate == true && !is_array($validate)) {
                return $this->insert($data);
            }
        }

        if (isset($_POST) && (!empty($_POST['submit']) || !empty($_POST['continue'])) && isset($_POST['nuclo_import_user']) && wp_verify_nonce($_POST['nuclo_import_user'], 'nuclo_import_user')) {

            $data = $_POST;
            $files = $_FILES;

            $validate = $this->import_validate($data, $files);

            if ($validate == true && !is_array($validate)) {
                return $this->import_user($data, $files);
            }
        }
    }

    /**
     * @since  1.0
     */
    public function edit() {
        
        if (isset($_POST) && !empty($_POST['submit']) && wp_verify_nonce($_POST['nuclo'], 'nuclo')) {
            $data = $this->cleandata($_POST);
            $files = $_FILES;
            $author = get_user_by('slug', get_query_var('author_name'));
            $user_id = $author->ID;
            
            
            if (current_user_can('editor')) {
                $data['franchisee_group'] = get_current_user_franchisee_id();
            }

            $data['role'] = nuclo_get_user_role($user_id);
            $data['choose_client'] = get_user_meta($user_id, 'client', TRUE);
            if(!empty(get_user_meta($user_id, 'sales_id', TRUE))){
                $data['sales_id'] = get_user_meta($user_id, 'sales_id', TRUE);
            }
            
            $validate = $this->validate('edit', $data, $files);

            if ($validate == true && !is_array($validate)) {

                return $this->update($user_id, $data, $files);
            }
        }
    }

    /**
     * @since  1.0
     */
    private function update($user_id, $data, $files) {


        if (!empty($data['password'])) {
            $userdata = array(
                'ID' => $user_id,
                'user_pass' => $data['password']
            );
            wp_update_user($userdata);
            //wp_set_password( $data['password'], $user_id );
        }

        update_user_meta($user_id, 'first_name', $data['first_name']);
        update_user_meta($user_id, 'last_name', $data['last_name']);
        update_user_meta($user_id, 'contact', $data['contact']);
        update_user_meta($user_id, 'timezone', $data['timezone']);
//        if($data['role'] == 'contributor'){
//            update_user_meta($user_id, 'address'        , $data['address']);
//            update_user_meta($user_id, 'region'         , $data['region']);
//            update_user_meta($user_id, 'city'           , $data['city']);
//            update_user_meta($user_id, 'state'          , $data['state']);
//            update_user_meta($user_id, 'country'        , $data['country']);
//            update_user_meta($user_id, 'postal_code'    , $data['postal_code']);
//            update_user_meta($user_id, 'phone_number'   , $data['phone_number']);
//        }
        update_user_meta($user_id, 'gender', $data['gender']);
        update_user_meta($user_id, 'city_search', $data['city_search']);
        update_user_meta($user_id, 'level', $data['p_level']);
        update_user_meta($user_id, 'function', $data['function']);
        update_user_meta($user_id, 'salary', $data['p_salary']);
        update_user_meta($user_id, 'education', $data['p_edu']);
        update_user_meta($user_id, 'dob', $data['dob']);
        update_user_meta($user_id, 'coemployee', $data['coemployee']);
        update_user_meta($user_id, 'g_presence', $data['g_presence']);

        $this->user_role_meta($user_id, $data);

        if (!empty($data['nc_groups']) && ($data['role'] == 'contributor')) {
            Group::asign('user', $user_id, $data['nc_groups']);
        }
//        update_user_meta($user_id, 'linkedin'       , $data['linkedin'] );
//        update_user_meta($user_id, 'google_plus'    , $data['google_plus'] );
//        update_user_meta($user_id, 'twitter'        , $data['twitter'] );
//        update_user_meta($user_id, 'facebook'       , $data['facebook'] );

        $this->upload_avatar($user_id, $files);

        $additional_string = (isset($_GET['msg'])) ? "" : '/?msg=success';

        if (get_current_user_id() == $user_id) {

            $status = get_user_meta($user_id, 'profile_update_after_login', true);
            if ($status == 'yes') {
                wp_safe_redirect($data['_wp_http_referer'] . $additional_string);
                exit();
            } else {
                update_user_meta($user_id, 'profile_update_after_login', 'yes');
                $additional_string .= "&first_login=1";
                wp_safe_redirect($data['_wp_http_referer'] . $additional_string);
                exit();
            }
        } else {
            wp_safe_redirect($data['_wp_http_referer'] . $additional_string);
            exit();
        }
    }

    /**
     * @since  1.0
     */
    private function upload_avatar($user_id, $files) {

        if (!empty($files['files']['name'])) {
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            $avatar_id = media_handle_upload('files', 0);

            if ($avatar_id) :
                $avatar = get_field('image', 'user_' . $user_id);
                update_user_meta($user_id, 'image', $avatar_id);
                update_user_meta($user_id, '_image', 'field_55c2f68f82665');
                if ($avatar['ID'])
                    wp_delete_attachment($avatar['ID']);

            endif;
        }
    }

    /**
     * @since  1.0
     */
    public function cleandata($data) {

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (!empty($value) && !in_array($key,array('nc_groups','nc_programs'))) {
                    $value = trim($value);
                    $value = stripslashes($value);
                    $value = htmlspecialchars($value);
                }
                $data[$key] = $value;
            }
        } else {
            if (!empty($data)) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
            }
        }
        return $data;
    }

    /**
     * @since  1.0
     */
    public function validate($type, $data, $files = array()) {

        global $author, $user_messsage, $current_user;
        
        if (empty($data['first_name'])) {
            $error['first_name'] = 'First name is required.';
        } else if (strlen($data['first_name']) < 2 || strlen($data['first_name']) > 30) {
            $error['first_name'] = 'First name should contain 2 - 30 character long.';
        } else if (!ctype_alpha(str_replace('-', '', $data['first_name']))) {
            $error['first_name'] = 'Only Letters and dashes are allowed.';
        }

        if (empty($data['last_name'])) {
            $error['last_name'] = 'Last name is required.';
        } else if (strlen($data['last_name']) < 2 || strlen($data['last_name']) > 30) {
            $error['last_name'] = 'Last name should contain 2 - 30 character long.';
        } else if (!ctype_alpha(str_replace('-', '', $data['last_name']))) {
            $error['last_name'] = 'Only Letters and  dashes are allowed.';
        }


        $email_array = explode('.', $data['user_email']);
        $last_element = $email_array[count($email_array) - 1];

        if (isset($data['user_email']) && empty($data['user_email'])) {
            $error['user_email'] = 'Email address is required.';
        } else if (isset($data['user_email']) && !is_email($data['user_email'])) {
            $error['user_email'] = 'Invalid email address.';
        } else if (isset($data['user_email']) && email_exists($data['user_email'])) {
            $error['user_email'] = 'Email address already in use.';
        } else if (isset($data['user_email']) && strlen($data['user_email']) > 50) {
            $error['user_email'] = 'Email address should contain maximum 50 characters.';
        } else if (isset($data['user_email']) && strlen($last_element) > 10) {
            $error['user_email'] = 'Invalid email address.';
        }

        $roles = array();
        foreach (get_editable_roles() as $role_name => $role_info):
            $roles[$role_name] = $role_name;
        endforeach;

        if (empty($data['role'])) {
            $error['role'] = 'Role is required.';
        } else if (!in_array($data['role'], $roles) && ($data['role'] != 'administrator')) {
            $error['role'] = 'The role selected is invalid';
        } else {
            switch ($data['role']) {

                case 'editor':
                case 'author':
                    if (empty($data['franchisee_group'])) {
                        $error['franchise_name'] = 'Franchisee is required.';
                    }
                    break;

                case 'teacher':
                    if (empty($data['client_group'])) {
                        $error['client_group'] = 'Client is required.';
                    }
                    break;

                case 'contributor':
                    if (!empty($data['sup_name']) && !ctype_alpha(str_replace(' ', '', $data['sup_name']))) {
                        $error['sup_name'] = 'Supervisor name only accepts alphabets.';
                    }
                    if (!empty($data['sup_email']) && !is_email($data['sup_email'])) {
                        $error['sup_email'] = 'Supervisor email is invalid.';
                    }
                    if (empty($data['choose_client']))
                        $error['choose_client'] = 'Client is required.';
                    if (empty($data['sales_id']) && ($type == 'create'))
                        $error['sales_id'] = 'NetSuite Sales ID is required.';

//                    $users = get_client_users($data['choose_client']);
//
//                    if (!empty($data['choose_client']) && ($type != 'edit')) {
//                        if (have_rows('license', $data['choose_client'])):
//                            while (have_rows('license', $data['choose_client'])) : the_row();
//                                if (get_sub_field('net_suite_sales_id') == $data['sales_id']) {
//                                    if (get_license_user($data['sales_id']) >= (int) get_sub_field('total_participants')) {
//                                        $error['choose_client'] = 'More participants cannot be added to the selected license.';
//                                        break;
//                                    }
//                                }
//                            endwhile;
//                        endif;
//                    }
                    
                    if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
          
                        $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];
                        $client_id = $_SESSION['WORKFLOW']['CLIENT_ID'];
                        $workflow_postmeta = get_post_meta($workflow_id);
                        $total_participant = $workflow_postmeta['number_of_participant']['0'];
                        $current_added_participant = $workflow_postmeta['current_added_participant']['0'];
                        $remaining_participant = $total_participant - $current_added_participant;
                        if(($remaining_participant == 0) && ($type == 'create'))
                        {
                            $error['choose_client'] = 'More participants cannot be added to the selected license.';
                        }
                    }

                    if (($type == 'edit') && in_array(nuclo_get_user_role($current_user->ID), array('super-admin', 'administrator'))) {
                        if (empty($data['nc_groups']))
                            $error['nc_groups'] = 'Please Select group for participant.';
                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
        
//        if(($type == 'edit') && in_array(nuclo_get_user_role($current_user->ID),array('editor','contributor','author'))){
//            if(empty($data['time_zone'])){
//                $error['time_zone'] = 'Please select timezone.';
//            }
//        }
        
        if (isset($data['password'])) {
            $containsLetter = preg_match('/[a-zA-Z]/', $data['password']);
            $containsDigit = preg_match('/\d/', $data['password']);
            $containsSpecial = preg_match('/[^a-zA-Z\d]/', $data['password']);

            if (!empty($data['password']) && empty($data['repeat_password'])) {
                $error['password'] = 'Repeat the password you want to change';
            } else if (!empty($data['password']) && !empty($data['repeat_password']) && $data['repeat_password'] != $data['password']) {
                $error['password'] = 'Password do not match. Please enter same value in both fields';
            } else if (!empty($data['password']) && !empty($data['repeat_password']) && strlen($data['password']) < 7 || strlen($data['password']) > 15) {
                $error['password'] = 'Password length should be 7 to 15 characters long.';
            }
//            else if (!empty($data['password']) && !empty($data['repeat_password']) && ((!$containsLetter) || (!$containsDigit) || (!$containsSpecial))){                
//               $error['password'] = 'Password should contain atleast one character and number.';                
//            }
        }

        if (isset($data['contact'])) {
            if (!empty($data['contact']) && (strlen($data['contact']) > 15 || (!ctype_digit($data['contact'])))) {
                $error['contact'] = 'Please enter a valid phone number';
            }
        }




        $allowed = array('jpg', 'jpeg', 'gif', 'png');
        $upfilename = isset($files['files']['name']) ? $files['files']['name'] : '';
        $extallowed = pathinfo($upfilename, PATHINFO_EXTENSION);

        if (!empty($upfilename) && !in_array($extallowed, $allowed)) {
            $error['profile_image'] = 'Invalid image.';
        }


        if (!empty($error)) {
            $user_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }

    /**
     * @since  1.0
     */
    public function import_validate($data, $files) {

        global $author, $user_messsage;

        $allowed = array('csv');
        $upfilename = $files['import_csv']['name'];
        $extallowed = pathinfo($upfilename, PATHINFO_EXTENSION);

        if ($files['import_csv']['error']) {
            $error['import'] = 'Import file is required.';
        } elseif (!empty($upfilename) && !in_array($extallowed, $allowed)) {
            $error['import'] = 'Invalid file. Please upload a csv file.';
        }

        if (isset($error['import'])) {
            $user_messsage['error'] = $error;
            return $error;
        }

        $imported = fopen($files['import_csv']['tmp_name'], "r");
        $incr = 0;
        $import_data = array();

        while (($file = fgetcsv($imported) ) !== FALSE) {
            $import_data[] = $file;
        }

        if (count($import_data) == 1) {
            $error['sheet_validation'] = 'There is no data in the file.';
        } else {
            unset($import_data[0]);
            $f_required_line = $f_character_line = $f_allowed_line = $l_required_line = $l_character_line = $l_allowed_line = array();
            $e_required_line = $e_invalid_line = $e_already_use_line = $e_character_line = $s_character_line = $s_invalid_line =  array();
            $error_sheet_validation_first_name_required = $error_sheet_validation_first_name_character = $error_sheet_validation_first_name_allowed = '';
            $error_sheet_validation_last_name_required = $error_sheet_validation_last_name_character = $error_sheet_validation_last_name_allowed = '';
            $error_sheet_validation_user_email_required = $error_sheet_validation_user_email_invalid = $error_sheet_validation_user_email_already_use = $error_sheet_validation_user_email_character = '';
            $error_sheet_validation_sup_name = $error_sheet_validation_sup_email = '';
            $i=1;
            foreach ($import_data as $file_data) {
                $i++;
                if (empty($file_data['0'])) {
                    $f_required_line[] = $i;
                    $error_sheet_validation_first_name_required = 'First name is required';
                } else if (strlen($file_data['0']) < 2 || strlen($file_data['0']) > 30) {
                    $f_character_line[] = $i;
                    $error_sheet_validation_first_name_character = 'First name should contain 2 - 30 character long';
                } else if (!ctype_alpha(str_replace('-', '', $file_data['0']))) {
                    $f_allowed_line[] = $i;
                    $error_sheet_validation_first_name_allowed = 'Only Letters and dashes are allowed';
                }
                
                if (empty($file_data['1'])) {
                    $l_required_line[] = $i;
                    $error_sheet_validation_last_name_required = 'Last name is required';
                } else if (strlen($file_data['1']) < 2 || strlen($file_data['1']) > 30) {
                    $l_character_line[] = $i;
                    $error_sheet_validation_last_name_character = 'Last name should contain 2 - 30 character long';
                } else if (!ctype_alpha(str_replace('-', '', $file_data['1']))) {
                    $l_allowed_line[] = $i;
                    $error_sheet_validation_last_name_allowed = 'Only Letters and dashes are allowed';
                }
                
                $email_array = explode('.', $file_data['2']);
                $last_element = $email_array[count($email_array) - 1];
                
                if (isset($file_data['2']) && empty($file_data['2'])) {
                    $e_required_line [] = $i;
                    $error_sheet_validation_user_email_required = 'Email address is required';
                } else if (isset($file_data['2']) && !is_email($file_data['2'])) {
                    $e_invalid_line [] = $i;
                    $error_sheet_validation_user_email_invalid = 'Invalid email address';
                } else if (isset($file_data['2']) && email_exists($file_data['2'])) {
                    $e_already_use_line [] = $i;
                    $error_sheet_validation_user_email_already_use = 'Email address already in use';
                } else if (isset($file_data['2']) && strlen($file_data['2']) > 50) {
                    $e_character_line [] = $i;
                    $error_sheet_validation_user_email_character = 'Email address should contain maximum 50 characters';
                } else if (isset($file_data['2']) && strlen($last_element) > 10) {
                    $e_required_line [] = $i;
                    $error_sheet_validation_user_email_invalid = 'Invalid email address';
                }
                
            
                if (!empty($file_data['3']) && !ctype_alpha(str_replace(' ', '', $file_data['3']))) {
                    $s_character_line[] = $i;
                    $error_sheet_validation_sup_name = 'Supervisor name only accepts alphabets';
                }
                if (!empty($file_data['4']) && !is_email($file_data['4'])) {
                    $s_invalid_line[] = $i;
                    $error_sheet_validation_sup_email = 'Supervisor email is invalid';
                }
                
            }
            
            if(!empty($f_required_line) && $error_sheet_validation_first_name_required!='')
            {
                $line_number = implode(', ',$f_required_line);
                $error['sheet_validation_first_name_required'] = $error_sheet_validation_first_name_required.' on line number '.$line_number.'.';
            }
            elseif(!empty($f_character_line) && $error_sheet_validation_first_name_character!='')
            {
                $line_number = implode(', ',$f_character_line);
                $error['sheet_validation_first_name_character'] = $error_sheet_validation_first_name_character.' on line number '.$line_number.'.';
            }
            elseif(!empty($f_allowed_line) && $error_sheet_validation_first_name_allowed!='')
            {
                $line_number = implode(', ',$f_allowed_line);
                $error['sheet_validation_first_name_allowed'] = $error_sheet_validation_first_name_allowed.' on line number '.$line_number.'.';
            }
            if(!empty($l_required_line) && $error_sheet_validation_last_name_required!='')
            {
                $line_number = implode(', ',$l_required_line);
                $error['sheet_validation_last_name_required'] = $error_sheet_validation_last_name_required.' on line number '.$line_number.'.';
            }
            elseif(!empty($l_character_line) && $error_sheet_validation_last_name_character!='')
            {
                $line_number = implode(', ',$l_character_line);
                $error['sheet_validation_last_name_character'] = $error_sheet_validation_last_name_character.' on line number '.$line_number.'.';
            }
            elseif(!empty($l_allowed_line) && $error_sheet_validation_last_name_allowed!='')
            {
                $line_number = implode(', ',$l_allowed_line);
                $error['sheet_validation_last_name_allowed'] = $error_sheet_validation_last_name_allowed.' on line number '.$line_number.'.';
            }
            
            if(!empty($e_required_line) && $error_sheet_validation_user_email_required!='')
            {
                $line_number = implode(', ',$e_required_line);
                $error['sheet_validation_user_email_required'] = $error_sheet_validation_user_email_required.' on line number '.$line_number.'.';
            }
            elseif(!empty($e_invalid_line) && $error_sheet_validation_user_email_invalid!='')
            {
                $line_number = implode(', ',$e_invalid_line);
                $error['sheet_validation_user_email_invalid'] = $error_sheet_validation_user_email_invalid.' on line number '.$line_number.'.';
            }
            elseif(!empty($e_already_use_line) && $error_sheet_validation_user_email_already_use!='')
            {
                $line_number = implode(', ',$e_already_use_line);
                $error['sheet_validation_user_email_already_use'] = $error_sheet_validation_user_email_already_use.' on line number '.$line_number.'.';
            }
            elseif(!empty($e_character_line) && $error_sheet_validation_user_email_character!='')
            {
                $line_number = implode(', ',$e_character_line);
                $error['sheet_validation_user_email_character'] = $error_sheet_validation_user_email_character.' on line number '.$line_number.'.';
            }
            
            if(!empty($s_character_line) && $error_sheet_validation_sup_name!='')
            {
                $line_number = implode(', ',$s_character_line);
                $error['sheet_validation_sup_name'] = $error_sheet_validation_sup_name.' on line number '.$line_number.'.';
            }
            if(!empty($s_invalid_line) && $error_sheet_validation_sup_email!='')
            {
                $line_number = implode(', ',$s_invalid_line);
                $error['sheet_validation_sup_email'] = $error_sheet_validation_sup_email.' on line number '.$line_number.'.';
            }
            
        }


        if (empty($data['choose_client']))
            $error['choose_client'] = 'Client is required.';
        if (!isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && !isset($_SESSION['WORKFLOW']['STATUS'])) {
            if (empty($data['sales_id']))
                $error['sales_id'] = 'NetSuite Sales ID is required.';
//            if (empty($data['usr_group']))
//                $error['usr_group'] = 'Group is required.';
        }


        if (!empty($error)) {
            $user_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }

    /**
     * @since  1.0
     */
    public function import_user($data, $files) {
        $imported = fopen($files['import_csv']['tmp_name'], "r");
        $total_record = 0;
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
          
            $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];
            $client_id = $_SESSION['WORKFLOW']['CLIENT_ID'];
            $workflow_postmeta = get_post_meta($workflow_id);
            $total_participant = $workflow_postmeta['number_of_participant']['0'];
            $current_added_participant = $workflow_postmeta['current_added_participant']['0'];
            $remaining_participant = $total_participant - $current_added_participant;  
        } 
        else
        {
            $client_id = $data['choose_client'];
            $client_user = count(get_client_users($client_id));
            $total_license_count = total_license_count($client_id);
            $remaining_participant = $total_license_count - $client_user;
        }
        
        $added_recrod = $remaining_record = 0;
        $remaining_array = [];
        while (($file = fgetcsv($imported) ) !== FALSE) {
            if (is_email($file['2']) && !email_exists($file['2'])) {

                $total_record++;

                if ($total_record <= $remaining_participant) {

                    $added_recrod++;
                    $data['password'] = wp_generate_password();

                    $user_id = wp_insert_user(array(
                        'user_login' => $file['2'],
                        'user_email' => $file['2'],
                        'user_pass' => $data['password'],
                        'role' => 'contributor')
                    );

                    if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
                        do_action('save_participant_workflow', $user_id);
                    }

                    if (!is_wp_error($user_id)) {

                        update_user_meta($user_id, 'first_name', $file['0']);
                        update_user_meta($user_id, 'last_name', $file['1']);
                        update_user_meta($user_id, 'client', $data['choose_client']);

                        $data['role'] = 'contributor';
                        $data['sup_email'] = $file['4'];
                        $data['sup_name'] = $file['3'];

                        // Updating Based On Role
                        $this->user_role_meta($user_id, $data);

                        if (!isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && !isset($_SESSION['WORKFLOW']['STATUS'])) {
                        // Adding group to the user
                            $groups = array($data['usr_group']);
                            Group::asign('user', $user_id, $groups);
                        }
                    }
                } else {
                    $remaining_record++;
                    $remaining_array[] = $file['2'];
                }
            }
        }
        
        $_SESSION['WORKFLOW']['SUCCESS'] = $_SESSION['WORKFLOW']['ERROR'] = '';
        if ($added_recrod != 0) {
            $_SESSION['WORKFLOW']['SUCCESS'] = $added_recrod . ' Participants added.';
        }
        if ($remaining_record != 0) {
            $remaing_email = implode(', ', $remaining_array);
            $_SESSION['WORKFLOW']['ERROR'] = $remaining_record . ' Participants ignored : ' . $remaing_email . '.';
        }
        do_action('import_user');
        wp_safe_redirect($data['_wp_http_referer']);
        exit();
    }

    /**
     * @since  1.0
     */
    private function insert($data) {

        $data['password'] = wp_generate_password();

        $user_id = wp_insert_user(array('user_login' => $data['user_email'],
            'user_email' => $data['user_email'],
            'user_pass' => $data['password'],
            'role' => $data['role']));

        if ($data['role'] == 'contributor') {
            if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
                do_action('save_participant_workflow', $user_id);
            }
        }

        if (!is_wp_error($user_id)) {

            // Updating User Meta
            update_user_meta($user_id, 'first_name', $data['first_name']);
            update_user_meta($user_id, 'last_name', $data['last_name']);
//            update_user_meta( $user_id, 'state',         $data['state']);
//            update_user_meta( $user_id, 'country',       $data['country']);
            // Updating Based On Role
            $this->user_role_meta($user_id, $data);

            $data['user_id'] = $user_id;

            // Sending Notification to user and admin

            switch ($data['role']) {
                case 'teacher':
                    $data['individual_user'] = TRUE;
                    do_action('register_new_client', $data);
                    break;
                case 'author':
                    do_action('register_new_facilitator', $data);
                    break;
                case 'editor':
                    $franchisee_obj = new Franchisee();
                    $franchisee_details = $franchisee_obj->get_franchisee_details($data['franchisee_group']);
                    $data['fran_name'] = $franchisee_details['post_title'];

                    do_action('register_new_franchisee', $data);
                    break;
                case 'super-admin':
                    do_action('register_new_hq_admin', $data);
                    break;
            }

            do_action('create_user', $user_id);

            // Redirect to Create page and Display message
            wp_safe_redirect($data['_wp_http_referer'] . '/?msg=success&nc_role=' . $data['role']);
            exit();
        } else {
            global $user_messsage;
            $user_messsage['error']['create'] = 'Something went wrong!';
        }
    }

    /**
     * @since 1.0
     */
    private function user_role_meta($user_id, $data) {

        switch ($data['role']) {

            case 'editor':
            case 'author':

                $franchisee_obj = new Franchisee();
                $franchisee = $franchisee_obj->get_franchisee_details($data['franchisee_group']);

                update_user_meta($user_id, 'franchisee', $data['franchisee_group']);
                if(isset($data['contact']) && !empty($data['contact'])){
                    update_user_meta($user_id, 'contact', $data['contact']);
                }else{
                    update_user_meta($user_id, 'contact', $franchisee['franchisee_contact']);
                }
                update_user_meta($user_id, 'address', $franchisee['address']);                
                update_user_meta($user_id, 'city', $franchisee['city']);
                update_user_meta($user_id, 'state', $franchisee['state']);
                update_user_meta($user_id, 'country', $franchisee['country']);
                update_user_meta($user_id, 'time_zone', $data['time_zone']);          
                break;

            case 'teacher':

                $client_obj = new Client();
                $client = $client_obj->get_client_details($data['client_group']);

                update_user_meta($user_id, 'franchisee', $client['franchisee']); 
                if(isset($data['contact']) && !empty($data['contact'])){
                    update_user_meta($user_id, 'contact', $data['contact']);
                }else{
                    update_user_meta($user_id, 'contact', $client['client_contact']);
                }
                update_user_meta($user_id, 'client', $data['client_group']);
                update_user_meta($user_id, 'address', $client['address']);
                update_user_meta($user_id, 'city', $client['city']);
                update_user_meta($user_id, 'state', $client['state']);
                update_user_meta($user_id, 'country', $client['country']);
                update_user_meta($user_id, 'time_zone', $data['time_zone']);
                
                break;

            case 'contributor':
                update_user_meta($user_id, 'supervisor_name', $data['sup_name']);
                update_user_meta($user_id, 'supervisor_mail', $data['sup_email']);
                update_user_meta($user_id, 'client', $data['choose_client']);
                update_user_meta($user_id, 'sales_id', $data['sales_id']);
                update_user_meta($user_id, 'city', $data['city']);
                update_user_meta($user_id, 'state', $data['state']);
                update_user_meta($user_id, 'country', $data['country']);
                update_user_meta($user_id, 'time_zone', $data['time_zone']);
                update_user_meta($user_id, 'address', $data['address']);

                break;

            default:
                # code...
                break;
        }
    }

    /**
     * @since 1.0
     */
    public function get_users($args = null) {


        if (current_user_can('create_hq_admin')) {
            $hqadmin = array(
                'key' => "{$GLOBALS['wpdb']->prefix}capabilities",
                'value' => 'super-admin',
                'compare' => 'like'
            );
        } else {
            $hqadmin = array();
        }

        if (current_user_can('create_franchisee')) {
            $franchis = array(
                'key' => "{$GLOBALS['wpdb']->prefix}capabilities",
                'value' => 'editor',
                'compare' => 'like'
            );
        } else {
            $franchis = array();
        }

        if (current_user_can('create_facilitator')) {
            $facilita = array(
                'key' => "{$GLOBALS['wpdb']->prefix}capabilities",
                'value' => 'author',
                'compare' => 'like'
            );
        } else {
            $facilita = array();
        }

        if (current_user_can('create_participant')) {
            $particip = array(
                'key' => "{$GLOBALS['wpdb']->prefix}capabilities",
                'value' => 'contributor',
                'compare' => 'like'
            );
        } else {
            $particip = array();
        }

        if (current_user_can('create_ceo')) {
            $teacher = array(
                'key' => "{$GLOBALS['wpdb']->prefix}capabilities",
                'value' => 'teacher',
                'compare' => 'like'
            );
        } else {
            $teacher = array();
        }

        if (current_user_can('editor') && (is_page('users'))) {
            $franchise_id = get_current_user_franchisee_id();
            $clients = get_franchise_client_admins($franchise_id);
            $others = get_franchise_users($franchise_id);
            $facilitator = get_facilitator($franchise_id);
            $all_users = array_merge($clients, $others);
            $all_users = array_merge($all_users, $facilitator);
            if (!empty($all_users)) {
                $args['include'] = $all_users;
            } else {
                $args['include'] = array(1);
            }
        }

        $args['orderby'] = 'meta_value';
        $args['meta_key'] = 'first_name';

        $meta_query = array('meta_query' => array(
                'relation' => 'OR', $hqadmin, $franchis, $facilita, $particip, $teacher,
        ));

        $args = array_merge($args, $meta_query);

        $results = new WP_User_Query($args);

        $this->results = $results->results;
        $this->total = $results->total_users;
        $this->total_pages = ceil($this->total / $this->per_page);
    }

    /**
     * @since 1.0
     */
    public function filter_query($query) {

        global $current_user;

        $exclude = array();
        $search_users = array();

        $users = get_field('hide_users', 'option');

        foreach ($users as $user) {
            array_push($exclude, $user['ID']);
        }

        $query->query_vars['exclude'] = $exclude;
        if(is_page('users')){
            $query->query_vars['include'] = array();
        }
        
        if (current_user_can('editor') && is_page('users') && (!is_page('group')) && (!is_page('event')) ) {
            $query->query_vars['include'] = $this->franchise_users(get_current_user_franchisee_id());
        }
        if (get_current_user_role()=='author' && is_page('users') && (!is_page('group')) && (!is_page('event'))) {
            $query->query_vars['include'] = $this->franchise_users_for_facilitator(get_current_user_franchisee_id());
        }
        if (get_current_user_role()=='teacher' && is_page('users') && (!is_page('group')) && (!is_page('event'))) {
            $query->query_vars['include'] = $this->client_users(get_client_id());
        }

        if (is_page('users')) {

            $data = $_GET;

            $query->query_vars['number'] = $this->per_page;
            $query->query_vars['paged'] = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;

            if (isset($data) && !empty($data['role']) && $data['role'] == 'all') {
                $this->filter = true;
            }

            if (isset($data) && !empty($data['role']) && $data['role'] != 'all') {
                $this->role = $this->cleandata($data['role']);
                $query->query_vars['role'] = $this->role;
                $this->filter = true;
            }

            if (isset($data) && !empty($data['search'])) {
                $this->search = esc_attr(trim($data['search']));

                $search_users = $this->search_by_name(sanitize_text_field($data['search']));
                $search_users = array_merge($search_users, $this->search_by_meta($data['search']));
                $search_users = array_merge($search_users, $this->search_client_name($data['search']));

                if (empty($search_users)) {
                    $search_users[] = 0;
                }

                $query->query_vars['include'] = $search_users;
                $this->filter = true;
            }
        }
       
        $query->query_vars['orderby'] = 'meta_value';
        $query->query_vars['meta_key'] = 'last_name';
    }

    /**
     * @since 1.0
     */
    private function search_by_name($name) {

        global $wpdb;

        $users = array();

        if (!empty($name)) {
            $filter_users = $wpdb->get_results(""
                    . "SELECT ID FROM $wpdb->users WHERE (user_login LIKE '%" . $name . "%' OR user_email LIKE '%" . $name . "%') order by ((case when user_login like '%" . $name . "%' then 1 else 0 end) - (case when user_email like '%" . $name . "%' then 1 else 0 end)) desc");

            foreach ($filter_users as $filter_user) {
                array_push($users, $filter_user->ID);
            }
        }

        return $users;
    }

    private function search_by_meta($name) {
        global $wpdb;

        $users = array();

        if (!empty($name)) {

            $filter_users = $wpdb->get_results("SELECT DISTINCT ID FROM $wpdb->users INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE ((um2.meta_key = 'first_name' OR um2.meta_key = 'last_name') AND um2.meta_value LIKE '%" . $name . "%');");

            foreach ($filter_users as $filter_user) {
                array_push($users, $filter_user->ID);
            }
        }

        return $users;
    }

    private function franchise_users($franchise_id) {

        $users = array();

        $args = array('post_type' => 'client', 'post_status' => 'publish', 'posts_per_page' => -1);

        if (current_user_can('super-admin') || current_user_can('administrator')) {
            $args['meta_key'] = 'franchisee';
            $args['meta_value'] = $franchise_id;
        } else if ($franchise_id > 0) {
            $args['meta_key'] = 'franchisee';
            $args['meta_value'] = $franchise_id;
        }

        $clients_query = new WP_Query($args);
        $client_ids = array();

        if ($clients_query->have_posts()) {
            while ($clients_query->have_posts()) {
                $clients_query->the_post();
                $client_ids[] = get_the_ID();
            }
        }

        global $wpdb;

        if (!empty($clients_query)) {
            $query = "SELECT DISTINCT ID FROM $wpdb->users";
            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE "
                    . "(";

            $meta_query = array();

            foreach ($client_ids as $client) {
                $meta_query[] = "(um2.meta_key LIKE 'client' AND um2.meta_value LIKE '$client')";
            }

            $query .= implode(' OR ', $meta_query) . ")";

            $client_users = $wpdb->get_results($query);
            $c_users = array();

            foreach ($client_users as $client_user) {
                $c_users[$client_user->ID] = $client_user->ID;
            }


            $query = "SELECT DISTINCT ID FROM $wpdb->users";
            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE ";
            $query .= " (um2.meta_key = 'wp_capabilities' AND um2.meta_value LIKE '%contributor%')";

            $contributor_users = $wpdb->get_results($query);
            $participant_users = array();

            foreach ($contributor_users as $contributor_user) {
                $participant_users[$contributor_user->ID] = $contributor_user->ID;
            }

            $query = "SELECT DISTINCT ID FROM $wpdb->users";
            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE ";
            $query .= " (um2.meta_key = 'wp_capabilities' AND um2.meta_value LIKE '%teacher%')";

            $teacher_users = $wpdb->get_results($query);
            $client_rep_users = array();

            foreach ($teacher_users as $teacher_user) {
                $client_rep_users[$teacher_user->ID] = $teacher_user->ID;
            }

            $participant_users = array_intersect($participant_users, $c_users);
            $client_rep_users = array_intersect($client_rep_users, $c_users);
            $filter_users = array_merge($participant_users, $client_rep_users);

            $query = "SELECT DISTINCT ID FROM $wpdb->users";
            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE ";
            $query .= " (um2.meta_key = 'wp_capabilities' AND um2.meta_value LIKE '%author%') AND ";
            $query .= " (um1.meta_key = 'franchisee' AND um1.meta_value = $franchise_id)";

            $author_users = $wpdb->get_results($query);
            $facilitator_users = array();

            foreach ($author_users as $author_user) {
                $facilitator_users[$author_user->ID] = $author_user->ID;
            }

            $filter_users = array_merge($filter_users, $facilitator_users);

            foreach ($filter_users as $filter_user) {
                array_push($users, $filter_user);
            }
        }

        wp_reset_postdata();



        if (empty($users)) {
            $users[] = 0;
        }
        return $users;
    }
    
    private function franchise_users_for_facilitator($franchise_id) {

        $users = array();

        $franchisee_id = get_current_user_franchisee_id();
        $client_ids = array();
        global $wpdb;
        $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");

        global $current_user;
        if ($results) {
            foreach ($results as $k => $v) {
                $g_id = $v->ID;
                $facilitator = get_post_meta($g_id,'facilitators');
                if(in_array($current_user->ID, $facilitator))
                {
                    $client_ids[] = get_post_meta($g_id,'client',true);
                }
            }
        }
        if (!empty($results)) {
            $query = "SELECT DISTINCT ID FROM $wpdb->users";
            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE "
                    . "(";

            $meta_query = array();

            foreach ($client_ids as $client) {
                $meta_query[] = "(um2.meta_key LIKE 'client' AND um2.meta_value LIKE '$client')";
            }

            $query .= implode(' OR ', $meta_query) . ")";

            $client_users = $wpdb->get_results($query);
            
            $c_users = array();

            foreach ($client_users as $client_user) {
                $c_users[$client_user->ID] = $client_user->ID;
            }


//            $query = "SELECT DISTINCT ID FROM $wpdb->users";
//            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
//            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE ";
//            $query .= " (um2.meta_key = 'wp_capabilities' AND um2.meta_value LIKE '%contributor%')";
//
//            $contributor_users = $wpdb->get_results($query);
//            $participant_users = array();
//
//            foreach ($contributor_users as $contributor_user) {
//                $participant_users[$contributor_user->ID] = $contributor_user->ID;
//            }
            
            $franchisee_id = get_current_user_franchisee_id();
            $participant_users = array();
            global $wpdb;
            $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");

            global $current_user;
            if ($results) {
                foreach ($results as $k => $v) {
                    $g_id = $v->ID;
                    $facilitator = get_post_meta($g_id,'facilitators');
                    if(in_array($current_user->ID, $facilitator))
                    {
                        $users = get_post_meta($g_id, 'users', true);
                        foreach ($users as $user) {
                            if (!in_array($user, $participant_users)) {
                                $participant_users[$user] = $user;
                            }
                        }
                    }
                }
            }

            $query = "SELECT DISTINCT ID FROM $wpdb->users";
            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE ";
            $query .= " (um2.meta_key = 'wp_capabilities' AND um2.meta_value LIKE '%teacher%')";

            $teacher_users = $wpdb->get_results($query);
            $client_rep_users = array();

            foreach ($teacher_users as $teacher_user) {
                $client_rep_users[$teacher_user->ID] = $teacher_user->ID;
            }

            $participant_users = array_intersect($participant_users, $c_users);
            $client_rep_users = array_intersect($client_rep_users, $c_users);
            $filter_users = array_merge($participant_users, $client_rep_users);

//            $query = "SELECT DISTINCT ID FROM $wpdb->users";
//            $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID";
//            $query .= " JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE ";
//            $query .= " (um2.meta_key = 'wp_capabilities' AND um2.meta_value LIKE '%author%') AND ";
//            $query .= " (um1.meta_key = 'franchisee' AND um1.meta_value = $franchise_id)";
//
//            $author_users = $wpdb->get_results($query);
//            $facilitator_users = array();
//
//            foreach ($author_users as $author_user) {
//                $facilitator_users[$author_user->ID] = $author_user->ID;
//            }
//
//            $filter_users = array_merge($filter_users, $facilitator_users);

            foreach ($filter_users as $filter_user) {
                array_push($users, $filter_user);
            }
        }

        wp_reset_postdata();



        if (empty($users)) {
            $users[] = 0;
        }
        return $users;
    }
    public static function client_users($client_id) {

        $users = array();

        global $wpdb;
        
        $query = "SELECT $wpdb->users.ID FROM $wpdb->users";
        $query .= " INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID ";
        $query .= "  WHERE  (um1.meta_key = 'client' AND um1.meta_value = $client_id)";
        

        $client_users = $wpdb->get_results($query);
        
        $c_users = array();

        foreach ($client_users as $client_user) {
            $role = nuclo_get_user_role($client_user->ID);
            if($role == 'contributor')
            $c_users[$client_user->ID] = $client_user->ID;
        }
        foreach ($c_users as $c_user) {
            array_push($users, $c_user);
        }
        wp_reset_postdata();



        if (empty($users)) {
            $users[] = 0;
        }
        return $users;
    }

    private function search_client_name($client_name) {

        global $wpdb;

        $client_details = $wpdb->get_results("SELECT DISTINCT ID , post_title FROM $wpdb->posts WHERE post_title LIKE '%{$client_name}%' AND post_type = 'client'");
        $client_array = array();

        foreach ($client_details as $details) {
            $client_array[] = $details->ID;
        }

        $users = array();

        if (!empty($client_array)) {

            foreach ($client_array as $client) {
                $meta[] = "(um2.meta_key = 'client' AND um2.meta_value LIKE '%{$client}%')";
            }

            $filter_users = $wpdb->get_results("SELECT DISTINCT ID FROM $wpdb->users INNER JOIN $wpdb->usermeta um1 ON um1.user_id = $wpdb->users.ID "
                    . "JOIN $wpdb->usermeta um2 ON um2.user_id = $wpdb->users.ID WHERE "
                    . implode('OR', $meta));

            foreach ($filter_users as $filter_user) {
                array_push($users, $filter_user->ID);
            }
        }

        return $users;
    }

    /**
     * @since 1.0
     */
    public function action_links($user_id) {
        //$view = '<a href="' . get_author_posts_url($user_id) . '" class="btn btn-warning btn-xs m-r-sm" data-placement="top" data-original-title="View" title="" data-toggle="tooltip"><i class="fa fa-eye"></i></a>';
        $login = '';
        if (current_user_can('administrator'))
            $login = '<a href="?loginas=' . $user_id . '" class="btn btn-default btn-xs m-r-sm" data-placement="top" data-original-title="Switch" title="" data-toggle="tooltip"><i class="fa fa-exchange"></i></a>';

        $edit = '<a href="' . get_author_posts_url($user_id) . 'edit" class="btn btn-success btn-xs m-r-sm unset_workflow" data-placement="top" data-original-title="Edit" title="" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>';
        $lock = '';
        //$lock = '<a href="#" class="btn btn-default btn-xs m-r-sm" data-placement="top" data-original-title="Lock" title="" data-toggle="tooltip" disabled><i class="fa fa-unlock-alt"></i></a>';
        $delete = '<a href="javascript:" class="btn btn-danger btn-xs m-r-sm" data-placement="top" data-id="' . $user_id . '" data-original-title="Delete" title="" data-toggle="tooltip" onclick="deleteUr(this);"><i class="fa fa-trash-o"></i></a>';

        echo '<td class="text-center">' . $login . $edit . $lock . $delete . '</td>';
    }

    /**
     * @since 1.0
     */
    public function delete() {

        if (!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX) && current_user_can('delete_users')) {
            $return['status'] = 'error';
            $return['data'] = 'Something went wrong';
            echo json_encode($return);
            exit;
        }

        $data = $_POST;

        if (!empty($data['id'])) {
            $user_id = (int) $data['id'];
            wp_delete_user($user_id);
            $return['status'] = 'success';
            $return['data'] = 'Successfully deleted.';
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data'] = 'you can not delete this.';
            echo json_encode($return);
        }
        exit;
    }

    /**
     * @since 1.0
     */
    public function username_shortcode() {
        global $current_user;
        return $current_user->user_login;
    }

    public function load_address_details() {

        $data = $_REQUEST;

        $return = array();

        if (!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX)) {
            $return['status'] = 'error';
            $return['data'] = 'Something went wrong';
            echo json_encode($return);
            exit;
        }

        if (!empty($data['value'])) {

            $country = get_post_meta($data['value'], 'country', true);
            $state   = get_post_meta($data['value'], 'state', true);
            $city    = get_post_meta($data['value'], 'city', true);

            $return['status'] = 'success';
            $return['data']['country'] = nc_country_name($country);
            $return['data']['state'] = nc_state_name($state);
            $return['data']['city'] = $city;
            $return['data']['country_id'] = $country;
            $return['data']['state_id'] = $state;
            $return['data']['city_id'] = $city;
            echo json_encode($return);
            exit;
        } else {
            $return['status'] = 'error';
            $return['data'] = 'Something went wrong';
            echo json_encode($return);
            exit;
        }
    }

    /**
     * @since 1.0
     */
    public function hide_personal_options() {
        echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) {
                $(\'form#your-profile > h3\').hide();
                $(\'form#your-profile\').show();
                $(\'form#your-profile > h3:first\').hide();
                $(\'form#your-profile > table:first\').hide();
                $(\'form#your-profile label[for=url], form#your-profile input#url\').hide();
                $(\'p.description\').hide();
                $(\'form#createuser label[for=role], form#createuser select#role\').hide();
                $(\'form#createuser label[for=url], form#createuser input#url\').hide();
                });
                </script>' . "\n";
    }

    /**
     * @since 1.0
     */
    public function hide_profile_fields($contactmethods) {
        unset($contactmethods['aim']);
        unset($contactmethods['jabber']);
        unset($contactmethods['yim']);
        unset($contactmethods['url']);
        return $contactmethods;
    }

}

new Users();

function get_user_groups_dropdown($user_id) {

    $grp_args = array(
        'post_type' => 'group',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    # Getting all programs
    $groups = get_posts($grp_args);

    # formatting data
    $group_array = array();
    foreach ($groups as $group) : setup_postdata($group);

        $return = array(
            "id" => $group->ID,
            "name" => $group->post_title
        );
        $group_array[] = $return;

    endforeach;

    # Reseting post query
    wp_reset_postdata();

    if (!empty($user_id)) {
        $post_groups = get_user_meta($user_id, 'groups', true);
    } else {
        $post_groups = array();
    }

    $html = '';
    $html.= '<select data-placeholder="Select Group" name="nc_groups[]" class="chosen-select" multiple>';
    foreach ($group_array as $group) {
        $selected = in_array($group['id'], $post_groups) ? 'selected' : '';
        $html.= '<option value="' . $group['id'] . '" ' . $selected . '>' . $group['name'] . '</option>';
    }
    $html.= '</select>';

    return $html;
}

function other_role_users($users) {
    $count = 0;
    foreach ($users['avail_roles'] as $role => $user) {
        $roles = new Roles();
        $roles_name = $roles->default_roles();
        if (!in_array($role, $roles_name) && $role != 'administrator') {
            $count = $count + $user;
        }
    }
    return $count;
}

function nuclo_get_user_role($user_id) {
    $user = get_userdata($user_id);
    $user_roles = $user->roles;
    if (is_array($user_roles)) {
        $user_role = array_shift($user_roles);
    } else {
        $user_role = $user_roles;
    }
    return $user_role;
}

function get_facilitator($franchisee_id) {

    $users = get_users(array(
        'role' => 'author',
        'meta_key' => 'franchisee',
        'meta_value' => $franchisee_id
    ));

    $filter_users = array();

    foreach ($users as $user) {
        $filter_users[] = $user->ID;
    }

    return $filter_users;
}

function get_user_counts($franchisee_id = NULL) {

    global $current_user;

    if (nuclo_get_user_role($current_user->ID) == 'editor' || get_current_user_role()=='author') {
        $franchisee_id = get_current_user_franchisee_id();
    }

    $user_counts = array();
    $number = 999999999;

    $franchise_user_args = array(
        'role' => 'editor',
        'number' => $number
    );

    if (!empty($franchisee_id)) {
        $franchise_user_args['meta_query'] = array(
            'relation' => 'AND',
            array(
                'key' => 'franchisee',
                'value' => $franchisee_id
            )
        );
    }

    $franchise_users = get_users($franchise_user_args);
    $user_counts['franchisee'] = count($franchise_users);

    $franchise_args = array(
        'post_type' => 'franchisee',
        'post_status' => 'publish',
        'posts_per_page' => -1
    );

    if (!empty($franchisee_id)) {
        $franchise_args['include'] = array($franchisee_id);
    }

    $franchisee_post = get_posts($franchise_args);

    $user_counts['franchisee_post'] = count($franchisee_post);

    $client_user_args = array(
        'role' => 'teacher',
        'number' => $number
    );

    if (!empty($franchisee_id)) {
        $client_user_args['meta_query'] = array(
            'relation' => 'AND',
            array(
                'key' => 'franchisee',
                'value' => $franchisee_id
            )
        );
    }

    $client_users = new WP_User_Query($client_user_args);
    //echo "Last SQL-Query: {$client_users->request}";exit;
    $user_counts['client'] = count($client_users->results);
    
    
    $client_args = array(
        'post_type' => 'client',
        'post_status' => 'publish',
        'posts_per_page' => -1
    );

    $client_args['meta_key'] = 'franchisee';
    $client_args['meta_value'] = $franchisee_id;

    $clients_query = get_posts($client_args);

    $user_counts['client_post'] = count($clients_query);
    
    if(get_current_user_role() == 'author')
    {
        $user_counts['client_post'] = count(client_for_facilitator());
        $count = 0;
        foreach($client_users->results as $val)
        {
            $client_id = get_user_meta($val->ID,'client',true);
            $client_id_array = client_for_facilitator();
            if(in_array($client_id, $client_id_array))
            {
                $count++;
            }
        }
        $user_counts['client'] = $count;
        
    }

    $facilitator_user_args = array(
        'role' => 'author',
        'number' => $number
    );

    if (!empty($franchisee_id)) {
        $facilitator_user_args['meta_query'] = array(
            'relation' => 'AND',
            array(
                'key' => 'franchisee',
                'value' => $franchisee_id
            )
        );
    }

    $facilitator_users = new WP_User_Query($facilitator_user_args);
    $user_counts['facilitator'] = count($facilitator_users->results);

    $participant_user_args = array(
        'role' => 'contributor',
        'number' => $number
    );

    if (!empty($franchisee_id)) {

        $participant_user_args['meta_query'] = array(
            'relation' => 'OR'
        );

        if(get_current_user_role() == 'author')
        {
            $client_id = client_for_facilitator();
            
            foreach ($client_id as $client) {
                $participant_user_args['meta_query'][] = array(
                    'key' => 'client',
                    'value' => $client
                );
            }
        }
        else
        {
            foreach ($clients_query as $client) {
                $participant_user_args['meta_query'][] = array(
                    'key' => 'client',
                    'value' => $client->ID
                );
            }
        }
    }

    $participant_users = ($user_counts['client_post'] == 0) ? 0 : new WP_User_Query($participant_user_args);
    
    
    $user_counts['participant'] = count($participant_users->results);

    if(get_current_user_role() == 'teacher')
    {
        $user_counts['participant'] = count(Users::client_users(get_client_id()));
    }
    if(get_current_user_role() == 'author')
    {
        $user_counts['participant'] = count(facilitator_users());
    }
    
    $hq_admin_args = array(
        'role' => 'super-admin',
        'number' => $number
    );

    $hq_admin_users = new WP_User_Query($hq_admin_args);
    $user_counts['hq_admin'] = count($hq_admin_users->results);

    if (($_POST) && defined('DOING_AJAX') && DOING_AJAX) {
        echo json_encode($user_counts);
        exit();
    } else {
        return $user_counts;
    }
}

function get_timezones($country = NULL) {

    $response_array = get_option('restcountries', true);

    if (empty($response_array)) {
        $response_array = file_get_contents('https://restcountries.eu/rest/v1/all/');
        update_option('restcountries', $response_array);
    }

    $response_array = json_decode($response_array);

    foreach ($response_array as $response) {
        $timezone_list[$response->name] = $response->timezones;
    }

    if (!is_null($country)) {
        return $timezone_list[$country];
    } else {
        return $timezone_list;
    }
}

function get_timezone_dropdown($country = NULL, $value = NULL) {

    global $current_user;

    if (isset($_POST) && defined('DOING_AJAX') && DOING_AJAX) {
        $country = $_POST['country'];
    }

    $timezones = get_timezones($country);

    $html = '<select class="form-control" id="time_zone" name="time_zone">';
    $html .= ' <option value="">-- Select Timezone --</option>';
    foreach ($timezones as $key => $time_zone) {
        if (is_array($time_zone)) {
            foreach ($time_zone as $k => $zone) {
                $selected = ($value == $zone) ? 'selected' : '';
                if (!empty($zone)) {
                    $html .= '<option value="' . $zone . '" ' . $selected . '>' . $zone . '</option>';
                }
            }
        } else {
            if (!empty($time_zone)) {
                $selected = ($value == $zone) ? 'selected' : '';
                $html .= '<option value="' . $time_zone . '" ' . $selected . '>' . $time_zone . '</option>';
            }
        }
    }
    $html .= '</select>';

    if (isset($_POST) && defined('DOING_AJAX') && DOING_AJAX) {
        echo $html;
        exit();
    } else {
        return $html;
    }
}

function get_modal_country_timezone() {

    $countries = nuclo_countries();

    if (isset($_POST) && defined('DOING_AJAX') && DOING_AJAX) {
        $country_sl = $_POST['country'];
    }
    ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">Country</label>                                
                <select class="form-control" id="country" name="country" <?php echo $field_disable; ?> onchange="get_modal_country_timezone(this)">
                    <option value="">-- Select Country --</option>
    <?php foreach ($countries as $key => $country) { ?>
                        <option value="<?php echo $country; ?>" <?php if ($country == $country_sl) { ?> selected <?php } ?>><?php echo $country; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>                        
    </div>                    
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">Timezone</label>
    <?php $time_zones = get_timezones($country_sl); ?>
                <select class="form-control" id="time_zone" name="time_zone">
                    <option value="">-- Select Timezone --</option>
                <?php foreach ($time_zones as $key => $time_zone) { ?>
                        <option value="<?php echo $time_zone; ?>"><?php echo $time_zone; ?></option>
                    <?php } ?>                                    
                </select>
                <div class="select-loader"><img src="<?php echo get_template_directory_uri(); ?>/images/loader-20x20.gif"></div>
            </div>
        </div>
    </div>

    <?php
    exit();
}

function get_current_user_role()
{
    global $current_user;
    return nuclo_get_user_role($current_user->ID);
}

function get_client_id()
{
    global $current_user;
    
    $role = nuclo_get_user_role($current_user->ID);
    $client_id = '0';
    if($role == 'teacher')
    {
        $client_id = get_user_meta($current_user->ID,'client',true);
    }
    
    return $client_id;
    
}

function client_for_facilitator()
{
    $franchisee_id = get_current_user_franchisee_id();
    $client_ids = array();
    global $wpdb;
    $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");

    global $current_user;
    if ($results) {
        foreach ($results as $k => $v) {
            $g_id = $v->ID;
            $facilitator = get_post_meta($g_id,'facilitators');
            if(in_array($current_user->ID, $facilitator))
            {
                $client_ids[] = get_post_meta($g_id,'client',true);
            }
        }
    }
    return $client_ids;
}
function facilitator_users()
{
    $franchisee_id = get_current_user_franchisee_id();
    $participant_users = array();
    global $wpdb;
    $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");

    global $current_user;
    if ($results) {
        foreach ($results as $k => $v) {
            $g_id = $v->ID;
            $facilitator = get_post_meta($g_id,'facilitators');
            if(in_array($current_user->ID, $facilitator))
            {
                $users = get_post_meta($g_id, 'users', true);
                foreach ($users as $user) {
                    if (!in_array($user, $participant_users)) {
                        $participant_users[$user] = $user;
                    }
                }
            }
        }
    }
    return $participant_users;
}