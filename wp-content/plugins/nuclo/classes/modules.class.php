<?php

/**
 * @package  Modules
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Modules {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this, 'rewrites_init'), 1, 0);
        add_action( 'init', array($this, 'rewrite_tag'), 10, 0);
        add_action( 'program', array($this, 'action'),10, 0);
        add_action( 'module_menu', array($this,'module_menu'));
        add_filter( 'query_vars', array($this, 'query_vars'));
    
    }
    
    /**
     * @since  1.0
     */
    public function rewrites_init() {  
        add_rewrite_rule('courseware/page/([0-9]{1,})/?','index.php?post_type=program&paged=$matches[1]','top');
        add_rewrite_rule('courseware/(.+?)/([^/]*)/([^/]*)/([^/]*)/([^/]*)/?$', 'index.php?program=$matches[1]&action=$matches[2]&module=$matches[3]&modid=$matches[4]&section=$matches[5]', 'top');
        add_rewrite_rule('courseware/(.+?)/([^/]*)/([^/]*)/([^/]*)/?$', 'index.php?program=$matches[1]&action=$matches[2]&module=$matches[3]&modid=$matches[4]', 'top');
        add_rewrite_rule('courseware/(.+?)/([^/]*)/([^/]*)/?$', 'index.php?program=$matches[1]&action=$matches[2]&module=$matches[3]', 'top');
    }
    
    /**
     * @since  1.0
     */
    public function query_vars($query_vars) {
        $query_vars[] = 'module';
        $query_vars[] = 'modid';
        $query_vars[] = 'section';
        return $query_vars;
    }

    /**
     * @since  1.0
     */
    public function rewrite_tag() {
        add_rewrite_tag('%module%', '([^&]+)');
        add_rewrite_tag('%modid%', '([^&]+)');
        add_rewrite_tag('%section%', '([^&]+)');
    }
    
    /**
     * @since  1.0
     */
    public function module_menu() {

        $modules = array(
            'survey'        => 'Survey', 
            'assessment'    => 'Assessment', 
            'extra'         => 'Extras',
            'plan'          => 'Action Plan',
           //'result'        => 'Results',
            'discussion'    => 'Discussion',
            'media'         => 'Media',
        );

        $current_module  = get_query_var('module');

        $class = empty($current_module)?'btn btn-default btn-sm text-capitalize menutab ':'btn btn-white btn-sm text-capitalize menutab ';

        echo '<div class="btn-group">';// btn-group-justified
        echo '<a class="'.$class.'" href="'.get_the_permalink().'modules/"><i class="fa fa-list"></i> Modules & Components</a>';

        foreach($modules as $key => $module) :
            $class = ($key == $current_module)?'btn btn-default btn-sm active text-capitalize menutab':'btn btn-white btn-sm text-capitalize menutab';  
            echo '<a class="'.$class.'" href="'.get_the_permalink().'modules/'.$key.'">'.$module.'</a>';
        endforeach;
        echo '</div>';
    }
    
    /**
     * @since  1.0
     */
    public function action() { 
        $action  = get_query_var('action');
        switch ($action) {
             case "type":
                $this->add_type();
                break;
            case "content":
                $this->add_content();
                break;
            case "modules":
                $this->add_module();
                break;
            case "assign":
                $this->assign_to_group();
                break;
            case "settings":
                $this->add_settings();
                break;
            default:
        }
    }
    
    /**
     * @since  1.0
     */
    private function add_type() {
        
        $data = $_POST;
        
        if(isset($data) && !empty($data['nc_type']) && wp_verify_nonce( $data['nc_nuclo'], 'nc_nuclo' ) && current_user_can( 'create_courseware') && is_integer(get_the_ID()) && in_array($data['nc_type'], array('csa','bpm', 'ecommerce'))) {

            update_post_meta(get_the_ID(), 'type', $data['nc_type']);
            
            do_action('course_type_updated',get_the_ID(),$data);
            
            $url = get_permalink(get_the_ID()).'content';
            
            wp_redirect($url);
            
            exit;

        }
        
    }
    
    /**
     * @since  1.0
     */
    private function add_content() {
        
        $data = $_POST;
      
        if(isset($data) && !empty($data['submit']) && wp_verify_nonce( $data['nc_nuclo'], 'nc_nuclo' ) && current_user_can('create_courseware') && is_integer(get_the_ID())) {

            $my_program = array( 'ID' => get_the_ID(),'post_content' => $data['nc_desc']);
            
            wp_update_post( $my_program );
            
            $url    = ($data['submit'] == 'next')?get_permalink(get_the_ID()).'modules/survey/':get_permalink(get_the_ID()).'content';
            
            $data['course_id']  = get_the_ID();
            
            do_action('course_content_updated',$data);
           
            wp_redirect($url);

            exit();

        }
    }

    /**
     * @since  1.0
     */
    private function assign_to_group() {

        $data = $_POST;

        if(isset($data['nc_nuclo']) && wp_verify_nonce( $data['nc_nuclo'], 'nc_nuclo' ) && is_integer(get_the_ID())) {

            $groups = $data['nc_groups'];
            
            $program = get_the_ID();
            
            if(current_user_can('administrator') || current_user_can('super-admin')) {
                $franchises = $data['nc_franchises'];
                Group::asign('franchises', $program, $franchises);
            }

            // Adding group to post
            Group::asign('post', $program, $groups);
            
            do_action('course_assign_group',get_the_ID(),$groups);

            if($data['submit'] == 'next') {

                $url = get_permalink(get_the_ID()).'settings';

                wp_redirect($url);
                
                exit();

            } else { 

                $url = get_permalink(get_the_ID()).'assign';   
                
                wp_redirect($url);
                
                exit();
            }
        }

    }
    
    /**
     * @since  1.0
     */
    private function add_settings() {
        
        $data = $_POST;
        
        if(isset($data) && !empty($data['nc_submit']) && wp_verify_nonce( $data['nc_nuclo'], 'nc_nuclo' ) && current_user_can( 'create_courseware') && is_integer(get_the_ID())) {
            
            update_post_meta(get_the_ID(),'disabled',$data['nc_disabled']);
            update_post_meta(get_the_ID(),'_disabled','field_55c9dae554fee');

            update_post_meta(get_the_ID(),'certificate',$data['nc_certificate']);
            update_post_meta(get_the_ID(),'_certificate','field_56123a2874144');

            update_post_meta(get_the_ID(),'announcements',$data['nc_announcement']);
            update_post_meta(get_the_ID(),'_announcements','field_56235b0d2f499');

            $my_program = array( 'ID' => get_the_ID(),'post_title' => $data['nc_title']);
            
            wp_update_post( $my_program );   
            
            do_action('course_setting_updated',get_the_ID(),$data);
            
            $url = get_permalink(get_the_ID()).'settings?msg=success';

            wp_redirect($url);
            
            exit();
            
            
        } else {      
            
            //$error->get_error('type', __( "Somthing went wrong", "nuclo" ));
            
        }
    }

    
    /**
     *  @since 1.0
     */
    public function add_module() {
        
        $data    = $_POST;
        $module  = get_query_var('module');
        $id      = get_query_var('modid');
       
        
        if(isset($data) && !empty($data['submit']) && wp_verify_nonce( $data['nc_nuclo'], 'nc_nuclo' ) && current_user_can( 'create_courseware') && is_integer(get_the_ID())) {
                    
            switch ($module) {
                case "bundle":
                        $program_id = Bundle::save_post($id,$data); 
                        
                        if(!empty($program_id)) {
                            
                            $url = get_permalink(get_the_ID()).'modules';
                            
                            wp_redirect($url);
                            
                            exit;
                            
                        }
                    break;
                case "extra":               
                        $program_id = Content::save_post($id, $data); 
                        if(!empty($program_id)) {
                            $url = get_permalink(get_the_ID()).'modules';                        
                            wp_redirect($url);
                            exit;
                        }
                    break;
                case "discussion":
                        $program_id = Discussion::save_post($id, $data);                    
                        if(!empty($program_id)) {
                            $url = get_permalink(get_the_ID()).'modules';                        
                            wp_redirect($url);
                            exit;
                        }
                    break;
                case "survey":
                        $section = get_query_var('section');
                        if($section == 'settings') {
                            $survey = new WPSQT('',$id);
                            $survey->create_survey('survey');

                            if ($data['submit'] == 'continue') {
                                $url = get_permalink(get_the_ID()).'modules/survey/'.$id.'/sections';
                                wp_redirect($url);
                                exit();
                            }

                        } else if($section == 'sections') {
                            $survey = new WPSQT('',$id);
                            $survey->save_sections($data);

                            if ($data['submit'] == 'continue') {
                                $url = get_permalink(get_the_ID()).'modules/survey/'.$id.'/questions';
                                wp_redirect($url);
                                exit();
                            }

                        } else if($section == 'questions') {
                            
                            do_action('survey_created',get_the_ID());
                            do_action('survey_upddated',get_the_ID());

                            if ($data['submit'] == 'save') {
                                $url = get_permalink(get_the_ID()).'modules/';
                                wp_redirect($url);
                                exit();
                            }

                        } else {
                            $program_id = Survey::save_post($id, $data);                    
                            if(!empty($program_id)) {
                                $url = get_permalink(get_the_ID()).'modules/survey/'.$program_id.'/settings';
                                wp_redirect($url);
                                exit;
                            }
                        }
                    break;
                case "assessment":
                        $section = get_query_var('section');
                        if($section == 'settings') {
                            $survey = new WPSQT('',$id);
                            $survey->create_survey('quiz');

                            if ($data['submit'] == 'continue') {
                                $url = get_permalink(get_the_ID()).'modules/assessment/'.$id.'/sections';
                                wp_redirect($url);
                                exit();
                            }

                        } else if($section == 'sections') {
                            $survey = new WPSQT('',$id);
                            $survey->save_sections($data);

                            if ($data['submit'] == 'continue') {
                                $url = get_permalink(get_the_ID()).'modules/assessment/'.$id.'/questions';
                                wp_redirect($url);
                                exit();
                            }

                        } else if($section == 'questions') {
                            
                            do_action('assessment_created',get_the_ID());
                            do_action('assessment_upddated',get_the_ID());

                            if ($data['submit'] == 'save') {
                                $url = get_permalink(get_the_ID()).'modules/';
                                wp_redirect($url);
                                exit();
                            }

                        }else {
                            $program_id = Assessment::save_post($id, $data);                    
                            if(!empty($program_id)) {
                                $url = get_permalink(get_the_ID()).'modules/assessment/'.$program_id.'/settings';
                                wp_redirect($url);
                                exit();
                            }
                        }
                    break;
                case "event":
                        $section = get_query_var('section');
                        
						$program_id = Event::save_post($id,$data);                    
						if(!empty($program_id)) {
							$url = get_permalink(get_the_ID()).'modules/';            
							wp_redirect($url);
							exit;
						}
                        
                    break;
                case "webinar":
                        $program_id = Webinar::save_post($id,$data);                    
                        if(!empty($program_id)) {
                            $url = get_permalink(get_the_ID()).'modules';                        
                            wp_redirect($url);
                            exit;
                        }
                    break;                    
                case "webcast":
                        $program_id = Webcast::save_post($id,$data);                    
                        if(!empty($program_id)) {
                            $url = get_permalink(get_the_ID()).'modules';                        
                            wp_redirect($url);
                            exit;
                        }
                    break;
                case "media":
                        $program_id = Media::save_post($id, $data);                    
                        if(!empty($program_id)) {
                            $url = get_permalink(get_the_ID()).'modules';                        
                            wp_redirect($url);
                            exit;
                        }
                    break;
                default:
            }
        }
    }
    
    /**
     *  @since 1.0
     */
    public static function save_meta($id,$data) {
        if(!empty($data['semester'])) 
            update_post_meta($id,'my_semester','Semester '.$data['semester']);  
        
        if(!empty($data['week'])) 
            update_post_meta($id,'my_week', 'Week '.$data['week']); 
        
        if(!empty($data['date'])) {
            $originalDate = $data['date'];
            $newDate = date("Ymd", strtotime($originalDate));
            update_post_meta($id,'date', $newDate);  
        }

        if(!empty($data['time'])) {          
            update_post_meta($id,'time', $data['time']);
        } else if(!empty($data['starttime'])) {
            update_post_meta($id,'time', $data['starttime']);
        }
        
        update_post_meta($id,'_my_semester','field_55ccf6120a3fd');
        update_post_meta($id,'_my_week'    ,'field_55ccf6200a3fe');
        update_post_meta($id,'_date'       ,'field_55ccf6270a3ff');
        update_post_meta($id,'_time'       ,'field_55d332804aa36');

        update_post_meta($id,'required',$data['nc_required']); 
        update_post_meta($id,'tag',$data['nc_tag']); 
        
        update_post_meta( $id, 'month_of_module', $data['nc_month']);
        update_post_meta( $id, '_month_of_module', 'field_56f64f266d989');
    }

}

new Modules();

function get_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			return mb_substr( $subex, 0, $excut );
		} else {
			return $subex;
		}
	} else {
		return $excerpt;
	}
}

function get_title_max_charlength($charlength) {
	$excerpt = get_the_title();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			return mb_substr( $subex, 0, $excut );
		} else {
			return $subex;
		}
	} else {
		return $excerpt;
	}
}