<?php

function download_excel($values, $file_name = 'export.xls', $type, $franchisee_name, $message, $user_type = 'hqadmin') {
    require_once 'thirdparty_lib/PHPExcel.php';
    require_once 'thirdparty_lib/PHPExcel/IOFactory.php';

    $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
    $rendererLibrary = 'tcpdf';
    $rendererLibraryPath = dirname(__FILE__) . '/thirdparty_lib/' . $rendererLibrary;

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setTitle(str_replace('.xls', '', $file_name));
    $rowcount = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objNucloDrawing = new PHPExcel_Worksheet_Drawing();

    $report_name = str_replace('.xls', '', $file_name);
    
    $document_root = get_home_path();

    $path = parse_url(get_field('logo', 'option'), PHP_URL_PATH);
    //$logo_path = $document_root . $path;
    $logo_path = 'logo_excel.png';
    $nuclo_logo_path = 'nuclowhite.png';


    $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);


    if ($type == '1') {

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":D" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $objRichText = new PHPExcel_RichText();
        //$objRichText->createText($message);

        $objPayable = $objRichText->createTextRun($message.$franchisee_name);
        $objPayable->getFont()->setBold(true);
        //$objPayable->getFont()->setItalic(true);
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A7:D7')->getFont()->setBold(true);

        
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $rowcount, 'Month')
                ->setCellValue('B' . $rowcount, 'Module')
                ->setCellValue('C' . $rowcount, 'Participants')
                ->setCellValue('D' . $rowcount, 'Feedback');



        $rowcount++;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FFCCD0D3'),
                ),
            ),
        );
        
        $i = 1;

        foreach ($values as $data) {
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['month'])
                    ->setCellValue('B' . $rowcount, $data['name'])
                    ->setCellValue('C' . $rowcount, $data['participant'])
                    ->setCellValue('D' . $rowcount, $data['feedback']);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':D' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
            
        }
        $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('C7:C' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('D7:D' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    } elseif ($type == '2') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":Y" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A' . $rowcount, ':Y' . $rowcount);
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(10);

        if ($franchisee_name != '' && $message != '') {
            $objRichText = new PHPExcel_RichText();
            //$objRichText->createText($message);

            $objPayable = $objRichText->createTextRun($message.$franchisee_name);
            $objPayable->getFont()->setBold(true);
            $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

            $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        }

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A7:Y8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A7:A8');
        $objPHPExcel->getActiveSheet()->mergeCells('B7:M7');
        $objPHPExcel->getActiveSheet()->mergeCells('N7:Y7');

        $current = date('Y');
        $next = date('Y', strtotime('+1 year'));
        if ($user_type == 'hqadmin') {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'Franchisee')
                    ->setCellValue('B7', $current)
                    ->setCellValue('N7', $next);
        } else {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'Group')
                    ->setCellValue('B7', $current)
                    ->setCellValue('N7', $next);
        }


        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B8', 'Jan')
                ->setCellValue('C8', 'Feb')
                ->setCellValue('D8', 'Mar')
                ->setCellValue('E8', 'Apr')
                ->setCellValue('F8', 'May')
                ->setCellValue('G8', 'June')
                ->setCellValue('H8', 'July')
                ->setCellValue('I8', 'Aug')
                ->setCellValue('J8', 'Sept')
                ->setCellValue('K8', 'Oct')
                ->setCellValue('L8', 'Nov')
                ->setCellValue('M8', 'Dec')
                ->setCellValue('N8', 'Jan')
                ->setCellValue('O8', 'Feb')
                ->setCellValue('P8', 'Mar')
                ->setCellValue('Q8', 'Apr')
                ->setCellValue('R8', 'May')
                ->setCellValue('S8', 'June')
                ->setCellValue('T8', 'July')
                ->setCellValue('U8', 'Aug')
                ->setCellValue('V8', 'Sept')
                ->setCellValue('W8', 'Oct')
                ->setCellValue('X8', 'Nov')
                ->setCellValue('Y8', 'Dec');



        $rowcount = 9;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            
            $i++;
            
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data[0])
                    ->setCellValue('C' . $rowcount, $data[1])
                    ->setCellValue('D' . $rowcount, $data[2])
                    ->setCellValue('E' . $rowcount, $data[3])
                    ->setCellValue('F' . $rowcount, $data[4])
                    ->setCellValue('G' . $rowcount, $data[5])
                    ->setCellValue('H' . $rowcount, $data[6])
                    ->setCellValue('I' . $rowcount, $data[7])
                    ->setCellValue('J' . $rowcount, $data[8])
                    ->setCellValue('K' . $rowcount, $data[9])
                    ->setCellValue('L' . $rowcount, $data[10])
                    ->setCellValue('M' . $rowcount, $data[11])
                    ->setCellValue('N' . $rowcount, $data[12])
                    ->setCellValue('O' . $rowcount, $data[13])
                    ->setCellValue('P' . $rowcount, $data[14])
                    ->setCellValue('Q' . $rowcount, $data[15])
                    ->setCellValue('R' . $rowcount, $data[16])
                    ->setCellValue('S' . $rowcount, $data[17])
                    ->setCellValue('T' . $rowcount, $data[18])
                    ->setCellValue('U' . $rowcount, $data[19])
                    ->setCellValue('V' . $rowcount, $data[20])
                    ->setCellValue('W' . $rowcount, $data[21])
                    ->setCellValue('X' . $rowcount, $data[22])
                    ->setCellValue('Y' . $rowcount, $data[23]);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A7:A' . $rowcount)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1))->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:Y' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    } elseif ($type == '3') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":I" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A' . $rowcount, ':I' . $rowcount);
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

        $objRichText = new PHPExcel_RichText();
        //$objRichText->createText($message);

        $objPayable = $objRichText->createTextRun($message.$franchisee_name);
        $objPayable->getFont()->setBold(true);
        //$objPayable->getFont()->setItalic(true);
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A7:I7')->getFont()->setBold(true);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $rowcount, 'Month')
                ->setCellValue('B' . $rowcount, 'Module')
                ->setCellValue('C' . $rowcount, 'Participants')
                ->setCellValue('D' . $rowcount, 'Revenue ($)')
                ->setCellValue('E' . $rowcount, 'Cost Saving ($)')
                ->setCellValue('F' . $rowcount, 'Productivity (Hours)')
                ->setCellValue('G' . $rowcount, 'Customer Satisfaction (%)')
                ->setCellValue('H' . $rowcount, 'Innovation (# of New Ideas)')
                ->setCellValue('I' . $rowcount, 'Risk Mitigation (# of Risks Averted)');



        $rowcount++;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i++;
        foreach ($values as $data) {
            
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            
            $i++;
            
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['month'])
                    ->setCellValue('B' . $rowcount, $data['name'])
                    ->setCellValue('C' . $rowcount, $data['users'])
                    ->setCellValue('D' . $rowcount, number_format($data['revenue']))
                    ->setCellValue('E' . $rowcount, number_format($data['costsaving']))
                    ->setCellValue('F' . $rowcount, number_format($data['productivity']))
                    ->setCellValue('G' . $rowcount, number_format($data['customersatisfaction']))
                    ->setCellValue('H' . $rowcount, number_format($data['innovation']))
                    ->setCellValue('I' . $rowcount, number_format($data['riskmanage']));

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':I' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }
        
        $objPHPExcel->getActiveSheet()->getStyle('A7:A' . $rowcount)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':I' . ($rowcount - 1))->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('C7:I' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A7:A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:B' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

//        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
//        $objRichText = new PHPExcel_RichText();
//        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
//        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
//
//        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
//
//
//        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
//                ->getFont()
//                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
//
//
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
//                ->getFill()
//                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
//                ->getStartColor()
//                ->setARGB('FF212240');
        
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(20);
        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':G' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':G' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':G' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objRichText1 = new PHPExcel_RichText();
        $objPayable = $objRichText1->createTextRun('Powered by');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('H' . $rowcount)->setValue($objRichText1);
        $objPHPExcel->getActiveSheet()->getStyle('H' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('H'. $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objNucloDrawing->setPath($nuclo_logo_path);
        $objNucloDrawing->setCoordinates('I'.$rowcount);
        $objNucloDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex());

        
        
        $objPHPExcel->getActiveSheet()->getStyle('I'.$rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
    } elseif ($type == '4') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":Y" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A' . $rowcount, ':Y' . $rowcount);
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));
        
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(10);

        $objRichText = new PHPExcel_RichText();
        //$objRichText->createText($message);

        $objPayable = $objRichText->createTextRun($message.$franchisee_name);
        $objPayable->getFont()->setBold(true);
        //$objPayable->getFont()->setItalic(true);
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A7:Y8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A7:A8');
        $objPHPExcel->getActiveSheet()->mergeCells('B7:M7');
        $objPHPExcel->getActiveSheet()->mergeCells('N7:Y7');

        $current = date('Y');
        $next = date('Y', strtotime('+1 year'));

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A7', 'Program Type')
                ->setCellValue('B7', $current)
                ->setCellValue('N7', $next);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B8', 'Jan')
                ->setCellValue('C8', 'Feb')
                ->setCellValue('D8', 'Mar')
                ->setCellValue('E8', 'Apr')
                ->setCellValue('F8', 'May')
                ->setCellValue('G8', 'June')
                ->setCellValue('H8', 'July')
                ->setCellValue('I8', 'Aug')
                ->setCellValue('J8', 'Sept')
                ->setCellValue('K8', 'Oct')
                ->setCellValue('L8', 'Nov')
                ->setCellValue('M8', 'Dec')
                ->setCellValue('N8', 'Jan')
                ->setCellValue('O8', 'Feb')
                ->setCellValue('P8', 'Mar')
                ->setCellValue('Q8', 'Apr')
                ->setCellValue('R8', 'May')
                ->setCellValue('S8', 'June')
                ->setCellValue('T8', 'July')
                ->setCellValue('U8', 'Aug')
                ->setCellValue('V8', 'Sept')
                ->setCellValue('W8', 'Oct')
                ->setCellValue('X8', 'Nov')
                ->setCellValue('Y8', 'Dec');



        $rowcount = 9;

        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data[0])
                    ->setCellValue('C' . $rowcount, $data[1])
                    ->setCellValue('D' . $rowcount, $data[2])
                    ->setCellValue('E' . $rowcount, $data[3])
                    ->setCellValue('F' . $rowcount, $data[4])
                    ->setCellValue('G' . $rowcount, $data[5])
                    ->setCellValue('H' . $rowcount, $data[6])
                    ->setCellValue('I' . $rowcount, $data[7])
                    ->setCellValue('J' . $rowcount, $data[8])
                    ->setCellValue('K' . $rowcount, $data[9])
                    ->setCellValue('L' . $rowcount, $data[10])
                    ->setCellValue('M' . $rowcount, $data[11])
                    ->setCellValue('N' . $rowcount, $data[12])
                    ->setCellValue('O' . $rowcount, $data[13])
                    ->setCellValue('P' . $rowcount, $data[14])
                    ->setCellValue('Q' . $rowcount, $data[15])
                    ->setCellValue('R' . $rowcount, $data[16])
                    ->setCellValue('S' . $rowcount, $data[17])
                    ->setCellValue('T' . $rowcount, $data[18])
                    ->setCellValue('U' . $rowcount, $data[19])
                    ->setCellValue('V' . $rowcount, $data[20])
                    ->setCellValue('W' . $rowcount, $data[21])
                    ->setCellValue('X' . $rowcount, $data[22])
                    ->setCellValue('Y' . $rowcount, $data[23]);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A7:A' . $rowcount)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1))->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:Y' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    } elseif ($type == '5') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":M" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        
        global $current_user;
        $participant_name = 'Particpant Name: '.ucfirst($current_user->user_firstname).' '.ucfirst($current_user->user_lastname);
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($participant_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':M' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':M' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objPHPExcel->getActiveSheet()->getStyle('A7:M8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A7:A8');
        $objPHPExcel->getActiveSheet()->mergeCells('B7:C7');
        $objPHPExcel->getActiveSheet()->mergeCells('D7:E7');
        $objPHPExcel->getActiveSheet()->mergeCells('F7:G7');
        $objPHPExcel->getActiveSheet()->mergeCells('H7:I7');
        $objPHPExcel->getActiveSheet()->mergeCells('J7:K7');
        $objPHPExcel->getActiveSheet()->mergeCells('L7:M7');

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A7', 'Module')
                ->setCellValue('B7', 'Revenue ($)')
                ->setCellValue('D7', 'Cost Saving ($)')
                ->setCellValue('F7', 'Productivity (Hours)')
                ->setCellValue('H7', 'Customer Satisfaction (%)')
                ->setCellValue('J7', 'Innovation (# of New Ideas)')
                ->setCellValue('L7', 'Risk Mitigation (# of Risks Averted)');

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B8', 'Action Plan')
                ->setCellValue('C8', 'Result')
                ->setCellValue('D8', 'Action Plan')
                ->setCellValue('E8', 'Result')
                ->setCellValue('F8', 'Action Plan')
                ->setCellValue('G8', 'Result')
                ->setCellValue('H8', 'Action Plan')
                ->setCellValue('I8', 'Result')
                ->setCellValue('J8', 'Action Plan')
                ->setCellValue('K8', 'Result')
                ->setCellValue('L8', 'Action Plan')
                ->setCellValue('M8', 'Result');

        $rowcount = 9;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data[0])
                    ->setCellValue('C' . $rowcount, $data[1])
                    ->setCellValue('D' . $rowcount, $data[2])
                    ->setCellValue('E' . $rowcount, $data[3])
                    ->setCellValue('F' . $rowcount, $data[4])
                    ->setCellValue('G' . $rowcount, $data[5])
                    ->setCellValue('H' . $rowcount, $data[6])
                    ->setCellValue('I' . $rowcount, $data[7])
                    ->setCellValue('J' . $rowcount, $data[8])
                    ->setCellValue('K' . $rowcount, $data[9])
                    ->setCellValue('L' . $rowcount, $data[10])
                    ->setCellValue('M' . $rowcount, $data[11]);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':M' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':M' . ($rowcount - 1))->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:M7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B8:M8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B9:M'.$rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':M' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':M' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    }
    elseif ($type == '6') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":F" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        
        global $current_user;
        $participant_name = 'Particpant Name: '.ucfirst($current_user->user_firstname).' '.ucfirst($current_user->user_lastname);
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($participant_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objPHPExcel->getActiveSheet()->getStyle('A7:F7')->getFont()->setBold(true);


        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A7', 'Module Name')
                ->setCellValue('B7', 'Session')
                ->setCellValue('C7', 'Action Plan')
                ->setCellValue('D7', 'Results')
                ->setCellValue('E7', 'Best Idea')
                ->setCellValue('F7', 'Total');

        $rowcount = 8;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data['session'])
                    ->setCellValue('C' . $rowcount, $data['action_plan'])
                    ->setCellValue('D' . $rowcount, $data['results'])
                    ->setCellValue('E' . $rowcount, $data['best_idea'])
                    ->setCellValue('F' . $rowcount, $data['total']);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':F' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':F' . ($rowcount - 1))->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:F8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B8:F'.$rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

//        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
//        $objRichText = new PHPExcel_RichText();
//        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
//        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
//
//        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
//
//
//        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
//                ->getFont()
//                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
//
//
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
//                ->getFill()
//                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
//                ->getStartColor()
//                ->setARGB('FF212240');
        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(20);
        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objRichText1 = new PHPExcel_RichText();
        $objPayable = $objRichText1->createTextRun('Powered by');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('E' . $rowcount)->setValue($objRichText1);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('E'. $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objNucloDrawing->setPath($nuclo_logo_path);
        $objNucloDrawing->setCoordinates('F'.$rowcount);
        $objNucloDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex());

        
        
        $objPHPExcel->getActiveSheet()->getStyle('F'.$rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    }
    elseif ($type == '7') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":C" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':C' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        
        global $current_user;
        $participant_name = 'Particpant Name: '.ucfirst($current_user->user_firstname).' '.ucfirst($current_user->user_lastname);
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($participant_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':C' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':C' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $rowcount = 8;
        // Set thin black border outline around column
        
        $i = 1;
        foreach ($values as $key=>$val) {
            
            foreach($val as $k=>$v)
            {
                if($k==0)
                {
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $v['A'])
                    ->setCellValue('B' . $rowcount, $v['B']);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowcount.':B'.$rowcount)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':C' . $rowcount);
                    $rowcount++;
                }
            }
            

            $rowcount++;
        }
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':F' . ($rowcount - 1))->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:F8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B8:F'.$rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':C' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':C' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':C' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;


        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(20);
        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objRichText1 = new PHPExcel_RichText();
        $objPayable = $objRichText1->createTextRun('Powered by');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('E' . $rowcount)->setValue($objRichText1);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('E'. $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objNucloDrawing->setPath($nuclo_logo_path);
        $objNucloDrawing->setCoordinates('F'.$rowcount);
        $objNucloDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex());

        
        
        $objPHPExcel->getActiveSheet()->getStyle('F'.$rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    }



    $objPHPExcel->getActiveSheet()->setShowGridLines(true);

    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

    $objPHPExcel->setActiveSheetIndex(0);


    if (!PHPExcel_Settings::setPdfRenderer(
                    $rendererName, $rendererLibraryPath
            )) {
        die(
                'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
                EOL .
                'at the top of this script as appropriate for your directory structure'
        );
    }



// Redirect output to a clientÃ¢â‚¬â„¢s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $file_name . '"');
    header('Cache-Control: max-age=0');

    ob_end_clean();
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    //$objWriter->save(str_replace('.php', '.xls', __FILE__));
    exit;
}

function download_pdf($values, $file_name = 'export.pdf', $type, $franchisee_name, $message, $user_type = 'hqadmin',$other_array = array()) {

    require_once 'thirdparty_lib/PHPExcel.php';
    require_once 'thirdparty_lib/PHPExcel/IOFactory.php';

    $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
    $rendererLibrary = 'tcpdf';
    $rendererLibraryPath = dirname(__FILE__) . '/thirdparty_lib/' . $rendererLibrary;

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setTitle(str_replace('.pdf', '', $file_name));
    $rowcount = 1;
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objNucloDrawing = new PHPExcel_Worksheet_Drawing();
    $report_name = str_replace('.pdf', '', $file_name);

    $document_root = get_home_path();

    $path = parse_url(get_field('logo', 'option'), PHP_URL_PATH);
    //$logo_path = $document_root . $path;
    $logo_path = 'logo_pdf.png';
    $nuclo_logo_path = 'nuclowhite_new.png';

    $objPHPExcel->getDefaultStyle()->getFont()->setName('heroregular');
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
    
//    $objPHPExcel->getActiveSheet()->getPageMargins()->setTop(1);
//    $objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.50);
//    $objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.50);
//    $objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(1);
    


    if ($type == '1') {

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":D" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);

        $objRichText = new PHPExcel_RichText();
        //$objRichText->createText($message);

        $objPayable = $objRichText->createTextRun($message.$franchisee_name);
        $objPayable->getFont()->setBold(true);
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $start_point = '7';
        
        if(isset($other_array) && !empty($other_array))
        {
            $objRichText = new PHPExcel_RichText();
            $objPayable = $objRichText->createTextRun($other_array[0]);
            $objPayable->getFont()->setBold(true);
            $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

            $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);

            $rowcount++;
            $rowcount++;

            $objRichText = new PHPExcel_RichText();
            //$objRichText->createText($message);

            $objPayable = $objRichText->createTextRun($other_array[1]);
            $objPayable->getFont()->setBold(true);
            $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

            $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);

            $rowcount++;
            $rowcount++;
            
            $start_point = '11';
        
        }
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A'.$start_point.':D'.$start_point)->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->setShowGridLines(true);
        
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $rowcount, 'Month')
                ->setCellValue('B' . $rowcount, 'Module')
                ->setCellValue('C' . $rowcount, 'Participants')
                ->setCellValue('D' . $rowcount, 'Feedback');



        $rowcount++;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {

            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            $i++;
            
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['month'])
                    ->setCellValue('B' . $rowcount, $data['name'])
                    ->setCellValue('C' . $rowcount, $data['participant'])
                    ->setCellValue('D' . $rowcount, $data['feedback']);

            $rowcount++;
        }
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start_point)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$start_point.':C' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('D'.$start_point.':D' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        //$objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':B' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':B' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':B' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objRichText1 = new PHPExcel_RichText();
        $objPayable = $objRichText1->createTextRun('Powered by');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('C' . $rowcount)->setValue($objRichText1);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('C'. $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objNucloDrawing->setPath($nuclo_logo_path);
        $objNucloDrawing->setCoordinates('D'.$rowcount);
        $objNucloDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex());

        $objPHPExcel->getActiveSheet()->getStyle('D'.$rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(6);
        $objPHPExcel->getActiveSheet()->getStyle('H' . $rowcount)->getFont()->setSize(6);
    } elseif ($type == '2') {

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":Y" . ($rowcount));
        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(5);

        if ($franchisee_name != '' && $message != '') {
            $objRichText = new PHPExcel_RichText();
            //$objRichText->createText($message);

            $objPayable = $objRichText->createTextRun($message.$franchisee_name);
            $objPayable->getFont()->setBold(true);
            //$objPayable->getFont()->setItalic(true);
            $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

            $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        }

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A7:Y8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A7:A8');
        $objPHPExcel->getActiveSheet()->mergeCells('B7:M7');
        $objPHPExcel->getActiveSheet()->mergeCells('N7:Y7');

        $current = date('Y');
        $next = date('Y', strtotime('+1 year'));
        if ($user_type == 'hqadmin') {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'Franchisee')
                    ->setCellValue('B7', $current)
                    ->setCellValue('N7', $next);
        } else {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'Group')
                    ->setCellValue('B7', $current)
                    ->setCellValue('N7', $next);
        }


        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B8', 'Jan')
                ->setCellValue('C8', 'Feb')
                ->setCellValue('D8', 'Mar')
                ->setCellValue('E8', 'Apr')
                ->setCellValue('F8', 'May')
                ->setCellValue('G8', 'June')
                ->setCellValue('H8', 'July')
                ->setCellValue('I8', 'Aug')
                ->setCellValue('J8', 'Sept')
                ->setCellValue('K8', 'Oct')
                ->setCellValue('L8', 'Nov')
                ->setCellValue('M8', 'Dec')
                ->setCellValue('N8', 'Jan')
                ->setCellValue('O8', 'Feb')
                ->setCellValue('P8', 'Mar')
                ->setCellValue('Q8', 'Apr')
                ->setCellValue('R8', 'May')
                ->setCellValue('S8', 'June')
                ->setCellValue('T8', 'July')
                ->setCellValue('U8', 'Aug')
                ->setCellValue('V8', 'Sept')
                ->setCellValue('W8', 'Oct')
                ->setCellValue('X8', 'Nov')
                ->setCellValue('Y8', 'Dec');



        $rowcount = 9;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data[0])
                    ->setCellValue('C' . $rowcount, $data[1])
                    ->setCellValue('D' . $rowcount, $data[2])
                    ->setCellValue('E' . $rowcount, $data[3])
                    ->setCellValue('F' . $rowcount, $data[4])
                    ->setCellValue('G' . $rowcount, $data[5])
                    ->setCellValue('H' . $rowcount, $data[6])
                    ->setCellValue('I' . $rowcount, $data[7])
                    ->setCellValue('J' . $rowcount, $data[8])
                    ->setCellValue('K' . $rowcount, $data[9])
                    ->setCellValue('L' . $rowcount, $data[10])
                    ->setCellValue('M' . $rowcount, $data[11])
                    ->setCellValue('N' . $rowcount, $data[12])
                    ->setCellValue('O' . $rowcount, $data[13])
                    ->setCellValue('P' . $rowcount, $data[14])
                    ->setCellValue('Q' . $rowcount, $data[15])
                    ->setCellValue('R' . $rowcount, $data[16])
                    ->setCellValue('S' . $rowcount, $data[17])
                    ->setCellValue('T' . $rowcount, $data[18])
                    ->setCellValue('U' . $rowcount, $data[19])
                    ->setCellValue('V' . $rowcount, $data[20])
                    ->setCellValue('W' . $rowcount, $data[21])
                    ->setCellValue('X' . $rowcount, $data[22])
                    ->setCellValue('Y' . $rowcount, $data[23]);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A7:A' . $rowcount)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1))->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B7:Y' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    } elseif ($type == '3') {
        
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
        
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":I" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(63);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

        $objRichText = new PHPExcel_RichText();
        //$objRichText->createText($message);

        $objPayable = $objRichText->createTextRun($message.$franchisee_name);
        $objPayable->getFont()->setBold(true);
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $start_point = '7';
        
        if(isset($other_array) && !empty($other_array))
        {
            $objRichText = new PHPExcel_RichText();
            $objPayable = $objRichText->createTextRun($other_array[0]);
            $objPayable->getFont()->setBold(true);
            $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

            $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);

            $rowcount++;
            $rowcount++;

            $objRichText = new PHPExcel_RichText();
            //$objRichText->createText($message);

            $objPayable = $objRichText->createTextRun($other_array[1]);
            $objPayable->getFont()->setBold(true);
            $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

            $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);

            $rowcount++;
            $rowcount++;
            
            $start_point = '11';
        
        }
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':I' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A'.$start_point.':I'.$start_point)->getFont()->setBold(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_GRADIENT_PATH)
                ->getStartColor()
                ->setARGB('FF212240');

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $rowcount, 'Month')
                ->setCellValue('B' . $rowcount, 'Module')
                ->setCellValue('C' . $rowcount, 'Participants')
                ->setCellValue('D' . $rowcount, 'Revenue ($)')
                ->setCellValue('E' . $rowcount, 'Cost Saving ($)')
                ->setCellValue('F' . $rowcount, 'Productivity (Hours)')
                ->setCellValue('G' . $rowcount, 'Customer Satisfaction (%)')
                ->setCellValue('H' . $rowcount, 'Innovation (# of New Ideas)')
                ->setCellValue('I' . $rowcount, 'Risk Mitigation (# of Risks Averted)');

        $rowcount++;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':I' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['month'])
                    ->setCellValue('B' . $rowcount, $data['name'])
                    ->setCellValue('C' . $rowcount, $data['users'])
                    ->setCellValue('D' . $rowcount, number_format($data['revenue']))
                    ->setCellValue('E' . $rowcount, number_format($data['costsaving']))
                    ->setCellValue('F' . $rowcount, number_format($data['productivity']))
                    ->setCellValue('G' . $rowcount, number_format($data['customersatisfaction']))
                    ->setCellValue('H' . $rowcount, number_format($data['innovation']))
                    ->setCellValue('I' . $rowcount, number_format($data['riskmanage']));
            $objPHPExcel->getActiveSheet()->getRowDimension($rowcount)->setRowHeight(22);            
            
            $rowcount++;
            //$objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':I' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }
        //$objPHPExcel->getActiveSheet()->getStyle('A7:I' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        //$objPHPExcel->getActiveSheet()->getStyle('A7:A' . $rowcount)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':I' . ($rowcount - 1))->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('C'.($start_point + 1).':I' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //$objPHPExcel->getActiveSheet()->getStyle('A'.($start_point + 1).':I' . $rowcount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':H' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':H' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':H' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_GRADIENT_PATH)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $rowcount++;

        //$objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(+2);
        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objRichText1 = new PHPExcel_RichText();
        $objPayable = $objRichText1->createTextRun('Powered by');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
        $objPHPExcel->getActiveSheet()->mergeCells('G' . $rowcount. ':H' . $rowcount);
        $objPHPExcel->getActiveSheet()->getCell('G' . $rowcount)->setValue($objRichText1);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('G'. $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objNucloDrawing->setPath($nuclo_logo_path);
        $objNucloDrawing->setCoordinates('I'.($rowcount-1));
        $objNucloDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex());
        $objPHPExcel->getActiveSheet()->mergeCells('I' . ($rowcount -1) . ':I' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('I'.($rowcount-1))
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        //$objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(6);
        //$objPHPExcel->getActiveSheet()->getStyle('H' . $rowcount)->getFont()->setSize(6);
        
    } elseif ($type == '4') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":Y" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(5);

        $objRichText = new PHPExcel_RichText();
        //$objRichText->createText($message);

        $objPayable = $objRichText->createTextRun($message.$franchisee_name);
        $objPayable->getFont()->setBold(true);
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color('FF212240'));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getStyle('A7:Y8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A7:A8');
        $objPHPExcel->getActiveSheet()->mergeCells('B7:M7');
        $objPHPExcel->getActiveSheet()->mergeCells('N7:Y7');

        $current = date('Y');
        $next = date('Y', strtotime('+1 year'));

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A7', 'Program Type')
                ->setCellValue('B7', $current)
                ->setCellValue('N7', $next);
        
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B8', 'Jan')
                ->setCellValue('C8', 'Feb')
                ->setCellValue('D8', 'Mar')
                ->setCellValue('E8', 'Apr')
                ->setCellValue('F8', 'May')
                ->setCellValue('G8', 'June')
                ->setCellValue('H8', 'July')
                ->setCellValue('I8', 'Aug')
                ->setCellValue('J8', 'Sept')
                ->setCellValue('K8', 'Oct')
                ->setCellValue('L8', 'Nov')
                ->setCellValue('M8', 'Dec')
                ->setCellValue('N8', 'Jan')
                ->setCellValue('O8', 'Feb')
                ->setCellValue('P8', 'Mar')
                ->setCellValue('Q8', 'Apr')
                ->setCellValue('R8', 'May')
                ->setCellValue('S8', 'June')
                ->setCellValue('T8', 'July')
                ->setCellValue('U8', 'Aug')
                ->setCellValue('V8', 'Sept')
                ->setCellValue('W8', 'Oct')
                ->setCellValue('X8', 'Nov')
                ->setCellValue('Y8', 'Dec');



        $rowcount = 9;

        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data[0])
                    ->setCellValue('C' . $rowcount, $data[1])
                    ->setCellValue('D' . $rowcount, $data[2])
                    ->setCellValue('E' . $rowcount, $data[3])
                    ->setCellValue('F' . $rowcount, $data[4])
                    ->setCellValue('G' . $rowcount, $data[5])
                    ->setCellValue('H' . $rowcount, $data[6])
                    ->setCellValue('I' . $rowcount, $data[7])
                    ->setCellValue('J' . $rowcount, $data[8])
                    ->setCellValue('K' . $rowcount, $data[9])
                    ->setCellValue('L' . $rowcount, $data[10])
                    ->setCellValue('M' . $rowcount, $data[11])
                    ->setCellValue('N' . $rowcount, $data[12])
                    ->setCellValue('O' . $rowcount, $data[13])
                    ->setCellValue('P' . $rowcount, $data[14])
                    ->setCellValue('Q' . $rowcount, $data[15])
                    ->setCellValue('R' . $rowcount, $data[16])
                    ->setCellValue('S' . $rowcount, $data[17])
                    ->setCellValue('T' . $rowcount, $data[18])
                    ->setCellValue('U' . $rowcount, $data[19])
                    ->setCellValue('V' . $rowcount, $data[20])
                    ->setCellValue('W' . $rowcount, $data[21])
                    ->setCellValue('X' . $rowcount, $data[22])
                    ->setCellValue('Y' . $rowcount, $data[23]);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A7:A' . $rowcount)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':Y' . ($rowcount - 1))->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B7:Y' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));



        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':Y' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    } elseif ($type == '5') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":M" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);

        global $current_user;
        $participant_name = 'Particpant Name: '.ucfirst($current_user->user_firstname).' '.ucfirst($current_user->user_lastname);
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($participant_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':M' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':Y' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objPHPExcel->getActiveSheet()->getStyle('A7:M8')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->mergeCells('A7:A8');
        $objPHPExcel->getActiveSheet()->mergeCells('B7:C7');
        $objPHPExcel->getActiveSheet()->mergeCells('D7:E7');
        $objPHPExcel->getActiveSheet()->mergeCells('F7:G7');
        $objPHPExcel->getActiveSheet()->mergeCells('H7:I7');
        $objPHPExcel->getActiveSheet()->mergeCells('J7:K7');
        $objPHPExcel->getActiveSheet()->mergeCells('L7:M7');

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A7', 'Module')
                ->setCellValue('B7', 'Revenue ($)')
                ->setCellValue('D7', 'Cost Saving ($)')
                ->setCellValue('F7', 'Productivity (Hours)')
                ->setCellValue('H7', 'Customer Satisfaction (%)')
                ->setCellValue('J7', 'Innovation (# of New Ideas)')
                ->setCellValue('L7', 'Risk Mitigation (# of Risks Averted)');

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B8', 'Action Plan')
                ->setCellValue('C8', 'Result')
                ->setCellValue('D8', 'Action Plan')
                ->setCellValue('E8', 'Result')
                ->setCellValue('F8', 'Action Plan')
                ->setCellValue('G8', 'Result')
                ->setCellValue('H8', 'Action Plan')
                ->setCellValue('I8', 'Result')
                ->setCellValue('J8', 'Action Plan')
                ->setCellValue('K8', 'Result')
                ->setCellValue('L8', 'Action Plan')
                ->setCellValue('M8', 'Result');

        $rowcount = 9;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data[0])
                    ->setCellValue('C' . $rowcount, $data[1])
                    ->setCellValue('D' . $rowcount, $data[2])
                    ->setCellValue('E' . $rowcount, $data[3])
                    ->setCellValue('F' . $rowcount, $data[4])
                    ->setCellValue('G' . $rowcount, $data[5])
                    ->setCellValue('H' . $rowcount, $data[6])
                    ->setCellValue('I' . $rowcount, $data[7])
                    ->setCellValue('J' . $rowcount, $data[8])
                    ->setCellValue('K' . $rowcount, $data[9])
                    ->setCellValue('L' . $rowcount, $data[10])
                    ->setCellValue('M' . $rowcount, $data[11]);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':M' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':M' . ($rowcount - 1))->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:M' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':M' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':M' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));



        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':M' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
    }
    elseif ($type == '6') {
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A" . ($rowcount) . ":F" . ($rowcount));

        $objDrawing->setPath($logo_path);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex(0));

        $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($rowcount)->setRowHeight(25);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;
        $rowcount++;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(65);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        
        global $current_user;
        $participant_name = 'Particpant Name: '.ucfirst($current_user->user_firstname).' '.ucfirst($current_user->user_lastname);
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($participant_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objRichText = new PHPExcel_RichText();
        
        $objPayable = $objRichText->createTextRun($report_name);
        $objPayable->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);

        $rowcount++;
        $rowcount++;
        
        $objPHPExcel->getActiveSheet()->getStyle('A7:F7')->getFont()->setBold(true);


        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A7', 'Module Name')
                ->setCellValue('B7', 'Session')
                ->setCellValue('C7', 'Action Plan')
                ->setCellValue('D7', 'Results')
                ->setCellValue('E7', 'Best Idea')
                ->setCellValue('F7', 'Total');

        $rowcount = 8;
        // Set thin black border outline around column
        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allBorders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $i = 1;
        foreach ($values as $data) {
            
            if($i%2 ==1)
            {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FFCCD0D3');
            }
            
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowcount, $data['name'])
                    ->setCellValue('B' . $rowcount, $data['session'])
                    ->setCellValue('C' . $rowcount, $data['action_plan'])
                    ->setCellValue('D' . $rowcount, $data['results'])
                    ->setCellValue('E' . $rowcount, $data['best_idea'])
                    ->setCellValue('F' . $rowcount, $data['total']);

            $rowcount++;
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':F' . ($rowcount - 1) . '')->applyFromArray($styleThinBlackBorderOutline);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($rowcount - 1) . ':F' . ($rowcount - 1))->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
        $objPHPExcel->getActiveSheet()->getStyle('B7:F8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B8:F'.$rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rowcount++;
        $report_text = 'Report generated at ' . date('Y-m-d H:i');
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Report generated at ' . date('Y-m-d H:i') . '.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':E' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':E' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':E' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');

        $rowcount++;

//        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
//        $objRichText = new PHPExcel_RichText();
//        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
//        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
//
//        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
//
//
//        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':F' . $rowcount);
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
//                ->getFont()
//                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
//
//
//        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':F' . $rowcount)
//                ->getFill()
//                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
//                ->getStartColor()
//                ->setARGB('FF212240');
        $footer_text = 'Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.';
        $objRichText = new PHPExcel_RichText();
        $objPayable = $objRichText->createTextRun('Copyright ' . date('Y') . ', Crestcom International, LLC. All rights reserved.');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));

        $objPHPExcel->getActiveSheet()->getCell('A' . $rowcount)->setValue($objRichText);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);


        $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowcount . ':D' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFont()
                ->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));


        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowcount . ':D' . $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objRichText1 = new PHPExcel_RichText();
        $objPayable = $objRichText1->createTextRun('Powered by');
        $objPayable->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
        $objPHPExcel->getActiveSheet()->getCell('E' . $rowcount)->setValue($objRichText1);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $rowcount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objPHPExcel->getActiveSheet()->getStyle('E'. $rowcount)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
        $objNucloDrawing->setPath($nuclo_logo_path);
        $objNucloDrawing->setCoordinates('F'.($rowcount-1));
        $objNucloDrawing->setWorksheet($objPHPExcel->setActiveSheetIndex());
        $objPHPExcel->getActiveSheet()->mergeCells('F' . ($rowcount -1) . ':F' . $rowcount);
        $objPHPExcel->getActiveSheet()->getStyle('F'.($rowcount-1))
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('FF212240');
        
    }

    $objPHPExcel->getActiveSheet()->setShowGridLines(false);

    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

    $objPHPExcel->setActiveSheetIndex(0);


    if (!PHPExcel_Settings::setPdfRenderer(
                    $rendererName, $rendererLibraryPath
            )) {
        die(
                'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
                EOL .
                'at the top of this script as appropriate for your directory structure'
        );
    }

    // Redirect output to a clientÃ¢â‚¬â„¢s web browser (PDF)
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment;filename="' . $file_name . '"');
    header('Cache-Control: max-age=0');

    ob_end_clean();
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
    $objWriter->save('php://output');
    exit;
}

function download_tcpdf($pdf_details){    
    
    $plugin_dir  = str_replace('classes/','',plugin_dir_path(__FILE__));
    require_once($plugin_dir.'classes/thirdparty_lib/tcpdf/xtcpdf.php');
    
    $pdf = new XTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A3', true, 'UTF-8', false);
    $pdf->header_type      = 'reports';
    
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(6, PDF_MARGIN_TOP, 6);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    //// set font
    $pdf->SetFont('dejavusans', '', 8);

    $pdf->AddPage();
    
    switch($pdf_details['type']){
        case 'ceo-report':
         
            if(isset($pdf_details['client_details']['participant']) && !empty($pdf_details['client_details']['participant'])){
                $html    = '<html>';
                $html   .= '<body>';
                $html   .= $pdf_details['client_details']['participant'];
                $html   .= '</body>';
                $html   .= '</html>';

                $pdf->writeHTML($html, true, false, true, false, '');
            }

            foreach($pdf_details['participant_details'] as $participant_html){
                $pdf->AddPage();
                $pdf->writeHTML($participant_html, true, false, true, false, '');        
            }
            break;
        
        case 'gamification-report':
            
            if(isset($pdf_details['html']) && !empty($pdf_details['html'])){
                $pdf->writeHTML($pdf_details['html'], true, false, true, false, '');
            }
            
            break;
    }
    
    

    $pdf->Output($pdf_details['file_name'], 'D');      
}
