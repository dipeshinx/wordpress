<?php
/**
 * @package  Reports
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Reports {

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action('init', array($this, 'select_best_idea'));
    }
    
    /**
     * @since  1.0
     */
    public function rewrites_init() {  
        
    }
    
    /**
     * @since  1.0
     */
    public function query_vars($query_vars) {
        
    }

    /**
     * @since  1.0
     */
    public function rewrite_tag() {
        
    }
    
    /**
     * @since  1.0
     */
    
    public function select_best_idea(){
        
        global $report_message;
        
        if(isset($_POST) && (isset($_POST['nuclo_select_best_idea'])) && wp_verify_nonce($_POST['nuclo_select_best_idea'], 'nuclo_select_best_idea')){
            
            $data = $_POST;
                        
            if(!isset($data['best_idea'])){
                $error['best_idea'] = 'Please select best idea and then submit.';
                $report_message['error']    = $error;
                return $error;
            }
            
            $array  = explode('_', $data['nc_module']);
            $module = $array[0];
            $key    = $array[1]+1;
            
            update_user_meta($data['best_idea'], 'best_idea_'.$data['nc_event'].'_'.$key, $module);
            
            update_post_meta($data['nc_event'],'module_'.$key,true);
            update_post_meta($data['nc_event'],'best_idea_module_'.$key,$data['best_idea']);
        
            wp_safe_redirect(home_url('select-best-idea').'/?msg=success&msg_event='.$data['nc_event'].'&msg_module='.$key);
            exit();
        }
        
    }
    
  
}

new Reports();

function get_excerpt($str, $start_pos = 0, $max_length = 100, $url = '') {
	if(strlen($str) > $max_length) {
		$excerpt   = substr($str, $start_pos, $max_length-3);
		$lastSpace = strrpos($excerpt, ' ');
		$excerpt   = substr($excerpt, 0, $lastSpace);
		$excerpt  .= '...';
	} else {
		$excerpt =  $str;
	}
	
	return '<a href="'.$url.'" target="blank">'.$excerpt.'</a>';
}

