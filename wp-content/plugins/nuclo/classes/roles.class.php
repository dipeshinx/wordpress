<?php
/**
 * @package  Roles
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Roles {

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    function __construct() { 
        add_action('init',                  array($this, 'rewrites_init'), 1, 0);
        add_action('init',                  array($this, 'rewrite_tag'), 10, 0);
        add_action('wp_head',               array($this, 'update_role'),1,0);
        add_action('wp_head',               array($this, 'update_role_fields'),1,0);
        add_action('wp_ajax_remove_role',   array($this, 'remove_role'));
        add_action('wp_ajax_create_role',   array($this, 'create_role'));

        add_filter('query_vars',            array($this, 'query_vars'));
        add_filter('editable_roles',        array($this, 'remove_editable_roles'));
        
        add_action('wp_ajax_read_notification',   array($this, 'read_notification'));
        add_action('wp_ajax_change_workflow',   array($this, 'change_workflow'));
        add_action('wp_ajax_session_client_data',   array($this, 'session_client_data'));
        add_action('wp_ajax_unset_workflow',   array($this, 'unset_workflow'));
        
    }
        
    public function read_notification()
    {
        $post_id = $_GET['post_id'];
        update_post_meta($post_id, 'read', 1);
        echo $_GET['post_id'];exit;
    }
    
    public function change_workflow()
    {
        $post_id = $_GET['workflow_id'];
        $post_meta = get_post_meta($post_id);
        $_SESSION['WORKFLOW']['WORKFLOW_ID'] = $post_id;
        $_SESSION['WORKFLOW']['CLIENT_ID'] = $post_meta['client_id'][0];
        $_SESSION['WORKFLOW']['STATUS'] = $post_meta['status'][0];
        $_SESSION['WORKFLOW']['PROGRAM_TYPE'] = $post_meta['program_type'][0];
        if(isset($post_meta['group'][0]))
        {
            $total_group = $post_meta['group'][0];
            if($post_meta['work_position_'.$total_group][0]!='2')
            {
                $_SESSION['WORKFLOW']['GROUP_ID'] = $post_meta['group_id_'.$total_group][0];
                $_SESSION['WORKFLOW']['EVENT_ID'] = $post_meta['session_id_'.$total_group][0];
            }
        }
        echo '1';exit;
        
    }
    
    public function session_client_data()
    {
        $data = $_POST;
        if(!empty($data))
        {
            unset($_SESSION['CLIENT_DATA']);
            $_SESSION['CLIENT_DATA'] = $data; 
        }
    }

    public function unset_workflow()
    {
        if(isset($_SESSION['WORKFLOW']))
        {
            unset($_SESSION['WORKFLOW']);
            echo '1';exit;
        }
    }
    /**
     * @since  1.0
     */
    public static function default_roles() {
        $roles = array('super-admin', 'admin', 'editor', 'teacher', 'author', 'contributor');
        return $roles;
    }

    /**
     * @since  1.0
     */
    public function editable_roles() {
        return get_editable_roles();
    }

    /**
     * @since  1.0
     */
    public static function editable_roles_name() {
        $names = array();
        foreach (get_editable_roles() as $role => $value) {
            array_push($names, $role);
        }
        return $names;
    }

    /**
     * @since  1.0
     */
    public function rewrites_init() {

        add_rewrite_rule('^manage/roles/([^/]*)/?$ ', 'index.php?page_id=9155&role_id=$matches[1]', 'top');
        add_rewrite_rule('^manage/roles/([^/]*)/?([^/]*)/?$', 'index.php?page_id=9155&role_id=$matches[1]&action=$matches[2]', 'top');
        
    }

    /**
     * @since  1.0
     */
    public function query_vars($query_vars) {
        $query_vars[] = 'role_id';
        $query_vars[] = 'action';
        return $query_vars;
    }

    /**
     * @since  1.0
     */
    public function rewrite_tag() {
        add_rewrite_tag('%role_id%', '([^&]+)');
        add_rewrite_tag('%action%', '([^&]+)');
    }

    /**
     * @since  1.0
     */
    public function remove_editable_roles($all_roles) {
        unset($all_roles['subscriber']);
        unset($all_roles['administrator']);
        return $all_roles;
    }

    /**
     * @since  1.0
     */
    public function remove_role() {

        // Validate ajax
        if (!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && current_user_can('remove_users')))
            return;

        // Set variables 
        $remove_role = sanitize_text_field($_POST['role']);
        $add_role = sanitize_text_field($_POST['move']);

        // Validate role
        if (in_array($remove_role, $this->default_roles()) && $add_role == $remove_role && !in_array($add_role, Roles::editable_roles_name()))
            return;

        $this->modify_role($remove_role, $add_role);

        exit;
    }

    /**
     * @since  1.0
     */
    protected function modify_role($remove_role, $add_role) {
        // Change user role
        $users = get_users('fields=ID&role=' . $remove_role);

        if (!empty($users)) :
            foreach ($users as $user) :
                $u = new WP_User($user);
                // Remove role
                $u->remove_role($remove_role);
                // Add role
                $u->add_role($add_role);
            endforeach;
        endif;

        // Delete role
        remove_role($remove_role);
    }

    /**
     * @since  1.0
     */
    public function create_role() {

        // Validate ajax
        if (!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && current_user_can('remove_users')))
            return;

        // Set variables 
        $role_title = sanitize_text_field($_POST['role_name']);
        $role_name = sanitize_title($role_title);
        $copy_of = sanitize_text_field($_POST['copy_of']);

        // Validate role
        if (array_key_exists($role_name, Roles::editable_roles_name()))
            return;

        if (!empty($copy_of) || $copy_of != 'none') :

            if (in_array($copy_of, Roles::editable_roles_name()))
                $copy = get_role($copy_of);

        endif;

        $this->add_role($role_name, $role_title, $copy->capabilities);

        exit;
    }

    /**
     * @since  1.0
     */
    protected function add_role($name, $title, $capabilities) {
        add_role($name, $title, $capabilities);
    }
    
    /**
     * @since  1.0
     */
    public function update_role() {
        
        if (current_user_can('remove_users') && isset($_POST['update-role']) && !empty($_POST['update-role'])) {
            
            $role_title = sanitize_text_field($_POST['role_name']);
            $role_name = sanitize_title($role_title);
            $role_id = get_query_var('role_id');
            
            if(empty($role_name) && in_array($role_id,$this->default_roles())) {
                return;
            }
            
            $type = array('short_online_courses', 'tao_communicator', 'dhhh');
            
            $courseware_caps = array('read_courseware', 'read_others_courseware', 'create_courseware', 'edit_courseware', 'edit_others_courseware', 'delete_courseware', 'delete_others_courseware');

            $certificates_caps = array('read_certificates', 'read_others_certificates', 'create_certificates', 'edit_certificates', 'edit_others_certificates', 'delete_certificates', 'delete_others_certificates');

            $reports_caps = array('read_reports', 'read_others_reports', 'edit_reports', 'edit_others_reports');

            $grade_caps = array('read_grade', 'read_others_grade', 'edit_grade', 'edit_others_grade');

            $attendance_caps = array('read_attendance', 'read_others_attendance', 'edit_attendance', 'edit_others_attendance');

            $approve_caps = array('can_approve');

            $default_caps = array_merge($type,$courseware_caps, $certificates_caps, $reports_caps, $grade_caps, $attendance_caps, $approve_caps);

            $add_caps = array_map('esc_attr', $_POST);

            $remove_caps = array_diff($default_caps, $add_caps);  
            
            $role = get_role($role_id);           

            if (!empty($remove_caps)) :
                foreach ($remove_caps as $remove_cap) :
                    $role->remove_cap($remove_cap);
                endforeach;
            endif;

            if (!empty($add_caps)) :
                foreach ($add_caps as $add_cap) :
                    $role->add_cap($add_cap);
                endforeach;
            endif;

            if ($role_name != $role_id && !in_array($role_id,$this->default_roles())) {
                add_role($role_name, $role_title, $role->capabilities);

                $users = get_users('fields=ID&role=' . $role_id);

                if (!empty($users)) :
                    foreach ($users as $user) :
                        $u = new WP_User($user);
                        // Remove role
                        $u->remove_role($role_id);
                        // Add role
                        $u->add_role($role_name);
                    endforeach;
                endif;

                remove_role($role_id);
            }

            $url = get_permalink(get_page_by_path('roles')) . $role_name.'?msg=success';
            
            ob_clean();
            
            wp_redirect($url);

            exit;
            
        }
    }
    
    /**
     * @since  1.0
     */
    public function update_role_fields() {
        if (current_user_can('remove_users') && isset($_POST['update-role-fields']) && !empty($_POST['update-role-fields'])) {
            $default_caps = array();
            if (have_rows('fields', 'option')) {
                while (have_rows('fields', 'option')){ the_row();
                    array_push($default_caps, 'read_'.get_sub_field('id'));
                    array_push($default_caps, 'edit_'.get_sub_field('id'));
                    array_push($default_caps, 'hide_'.get_sub_field('id'));
                }
            }
            $add_caps = array_map('esc_attr', $_POST);
            $remove_caps = array_diff($default_caps, $add_caps);
            $role_id = get_query_var('role_id');
            $role = get_role($role_id);
            if(!empty($role)) {
                // Remove Caps
                if (!empty($remove_caps)) :
                    foreach ($remove_caps as $remove_cap) :
                        $role->remove_cap($remove_cap);
                    endforeach;
                endif;
                // Add Caps
                if (!empty($add_caps)) :
                    foreach ($add_caps as $add_cap) :
                        $role->add_cap($add_cap);
                    endforeach;
                endif;
            }
            $url = get_permalink(get_page_by_path('roles')) . $role->name.'/fields/?msg=success';
            
            ob_clean();
            
            wp_redirect($url);

            exit;
        }
    }
    
    /**
     * @since  1.0
     */
    public static function display_role($role) {
        global $wp_roles;
        if(is_numeric($role)) {
            $user = get_user_by('id',$role);
            $role = $user->roles[0];
        } 
        
        if(in_array($role,Roles::default_roles())) {
            if($role == 'super-admin') {
                return '<span class="label label-purple">'.$wp_roles->roles[$role]['name'].'</span>';
            } if($role == 'admin') {
                return '<span class="label label-danger">'.$wp_roles->roles[$role]['name'].'</span>';
            } else if($role == 'editor') {
                return '<span class="label label-warning">'.$wp_roles->roles[$role]['name'].'</span>';
            } else if($role == 'author') {
                return '<span class="label label-success">'.$wp_roles->roles[$role]['name'].'</span>';
            } else if($role == 'contributor') {
                return '<span class="label label-info">'.$wp_roles->roles[$role]['name'].'</span>';
            } else if($role == 'teacher') {
                return '<span class="label label-primary">'.$wp_roles->roles[$role]['name'].'</span>';
            }
        } else {
            return '<span class="label">'.$wp_roles->roles[$role]['name'].'</span>';
        }
    }

}
new Roles();