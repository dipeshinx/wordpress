<?php

function help_page(){
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">                
        <div class="col-sm-12">
            <form type="post" id="faq_search">
                <div class="form-group">
                    <div class="col-sm-12">                                
                        <input type="text" name="faq_search_text" id="faq_search_text" placeHolder="Search for Help" class="form-control">
                        <div id="help-search-loader"><img src="<?php echo get_template_directory_uri();?>/images/loader-20x20.gif"></div>
                    </div>
                </div>                        
            </form>
        </div>
    </div>
</div>
<?php
        echo '<div class="wrapper wrapper-content" id="help-text-scroll">';
        
        /*
         *  Here Help Text differenciate by user's role when user is logged in.
         *  If user is not logged in by default it takes Participant's Help File.
         */
        
        global $current_user;
        
        $faq_shortcode  = "[ultimate-faqs ";
        
        if(is_user_logged_in()){            
            $faq_shortcode  .= "include_category = '".  nuclo_get_user_role($current_user->ID)."'";            
        }else{
            $faq_shortcode  .= "include_category = 'contributor'";
        }
        
        $faq_shortcode  .= "]";
        
        echo do_shortcode($faq_shortcode);

        echo '</div>';
    
        exit();
}

function search_faq(){
    global $wpdb;
    global $current_user;

    $users_role = 'contributor';
    if(is_user_logged_in())
        $users_role = nuclo_get_user_role($current_user->ID);

    $querystr = "
        SELECT ID,post_title FROM $wpdb->posts	
        LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
        LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
        LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
        WHERE $wpdb->terms.slug = '".$users_role."'
        AND $wpdb->term_taxonomy.taxonomy = 'ufaq-category'
        AND $wpdb->posts.post_status = 'publish'
        AND $wpdb->posts.post_type = 'ufaq'
        AND $wpdb->posts.post_title LIKE '%".$_REQUEST['term']."%'
    ";

    $faqs_title = $wpdb->get_results($querystr,ARRAY_A); 

    echo json_encode($faqs_title);
    exit();
}

function search_faq_result(){
    global $post;
    $post = get_post($_REQUEST['id']);    
?>    
<div class="wrapper wrapper-content  animated fadeInRight article" id="help-text-scroll">
    
    <div class="row">
        
        <div class="col-lg-10 col-lg-offset-1">
            <div class="clearfix"><a class="pull-right btn btn-xs btn-danger" id="back-to-help" style="margin-bottom: 10px;" href="javascript:"><i class="fa fa-mail-reply"></i> Back to Help</a></div>
            <div class="ibox">
                
                <div class="ibox-content p-sm">
                    
                    <div class="text-center m-b-xl">
                        <h1>
                            <?php echo $post->post_title; ?>
                        </h1>                                        
                    </div>
                    
                    <div class="m-t-lg text-justify">
                        <?php echo $post->post_content; ?>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
    </div>
    
</div>   
<?php
exit();
}

add_action('wp_ajax_search_faq_result','search_faq_result');
add_action('wp_ajax_help_page','help_page');
add_action('wp_ajax_search_faq','search_faq');
