<?php

/**
 * @package  Programs
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Programs {

    public $per_page;
    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this, 'register_post_type') );  
        add_action( 'init', array($this, 'rewrites_init'), 1, 0);
        add_action( 'wp_ajax_create_program', array($this,'create'));
        add_action( 'wp_ajax_delete_program', array($this,'delete'));
        add_action( 'wp_ajax_reorder_module', array($this,'reorder'));
        add_action( 'wp_ajax_load_course_modules', array($this,'load_course_modules'));
        add_action( 'wp_ajax_load_courses', array($this,'get_all_courses_dropdown'));
        add_action( 'pre_get_posts', array($this, 'filter_query'), 1, 1);
        
        if (is_post_type_archive('program') && !current_user_can('hq_admin')) {
            $this->per_page = isset($_REQUEST['record_per_page'])?$_REQUEST['record_per_page']:-1;            
        }else{
            $this->per_page = -1;
        }
    }

    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('programs_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('programs_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('programs_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('programs_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('programs_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('programs_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('programs_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('programs_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('programs_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('programs_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('programs_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('programs_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('programs_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('programs_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => get_field('programs_slug', 'option')),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => true,
                    'menu_position'      => null,
                    'supports'           => array( 'title', 'author', 'thumbnail' )
            );

            register_post_type( 'program', $args );
        endif;
    }
    
    /**
     * @since  1.0
     */
    public function rewrites_init() {         
       add_rewrite_rule('courseware/(.+?)/([^/]*)/?$', 'index.php?program=$matches[1]&action=$matches[2]', 'top');
    }
    
    /**
     * @since 1.0
     */
    public function filter_query($query) {
        
        $query->query_vars['posts_per_page'] = $this->per_page;
        $query->query_vars['paged'] = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;
        
        if (is_post_type_archive('program') && $query->query_vars['post_type'] == 'program' && !current_user_can('hq_admin')) {
            if(current_user_can('franchisee')) {                
                $programs = get_post_meta(get_current_user_franchisee_id() ,'courses',true);                
            } else if ( current_user_can('participant')) {
                $programs = Group::get_programs( 'user', get_current_user_id());
            }
            
            if (get_current_user_role()=='author') {
                
                $franchisee_id = get_current_user_franchisee_id();
                $programs = array();
                global $wpdb;
                $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");

                global $current_user;
                if ($results) {
                    foreach ($results as $k => $v) {
                        $g_id = $v->ID;
                        $facilitator = get_post_meta($g_id,'facilitators');
                        if(in_array($current_user->ID, $facilitator))
                        {
                            $posts = get_post_meta($g_id, 'posts', true);
                            foreach ($posts as $post) {
                                if (!in_array($post, $programs)) {
                                    array_push($programs, $post);
                                }
                            }
                        }
                    }
                }
                if (empty($programs)) {
                    $programs = array('0');
                }
            }
          

            if(empty($programs)) {
                $programs = array('0');
            }
            $query->query_vars['post__in'] = $programs;
            
            $query->set( 'post_parent', '0' );

        }       
    }
    
    /**
     * @since 1.0
     */
    public static function action_links($id, $module=null, $program_url='', $course_id=null) {
        $action = '';
        if(current_user_can('edit_courseware')) {       
            
            if(!empty($module)) { 
                
                if($module != 'bundle') {                    
                    $action .= '<a href="#" class="btn btn-info btn-xs m-r-sm bundle" data-course="'.$course_id.'"  data-module="'.$id.'"><i class="fa fa-list-alt" data-toggle="tooltip" data-placement="top" data-original-title="Bundle" title="" ></i></a>';
                }
                
                $action .= '<a href="'.$program_url.'modules/'.$module.'/'.$id.'" class="btn btn-success btn-xs m-r-sm" data-placement="top" data-original-title="Edit" title="" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>';
                
            } else {
                $action .= '<a href="'.  get_permalink($id).'modules" class="btn btn-success btn-xs m-r-sm" data-placement="top" data-original-title="Edit" title="" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>';
            }
            
            $action .= '<button type="button" class="btn btn-default btn-xs m-r-sm btn-duplicate" data-placement="top" data-original-title="Duplicate" title="" data-toggle="tooltip" data-id="'.$id.'" data-action="duplicate"><i class="fa fa-copy"></i></button>';
            
        }
        if(current_user_can('delete_courseware')) {
            $action .= '<a href="javascript:" class="btn btn-danger btn-xs m-r-sm" data-id="'.$id.'" data-placement="top" data-original-title="Delete" title="" data-toggle="tooltip" onclick="deletePr(this);"><i class="fa fa-trash-o"></i></a>';
        }
        if(!empty($action)) {
            echo '<td width="20%">' . $action . '</td>';
        }
    }
    
    /**
     * @since 1.0
     */
    public static function get_status($program_id) {
        $status = get_field('disabled',$program_id);
        if($status == 'yes') {
            echo '<td><span class="label label-danger">Disabled</span></td>';
        } else {
            echo '<td><span class="label label-success">Enabled</span></td>';
        }
    }
    
    /**
     * @since 1.0
     */
    public static function get_type($program_id) {

        $status = get_field('type',$program_id);
        
        if($status == 'csa') {
            echo '<td><span class="text-default">CSA</span></td>';
        } else if($status == 'bpm') {
            echo '<td><span class="text-primary">BPM</span></td>';
        } else if($status == 'ecommerce') {
            echo '<td><span class="text-success">E-Commerce</span></td>';
        } else {
            echo '<td></td>';
        }
    }
    
    /**
     * @since 1.0
     */
    public function create() {
        
        $data = $_POST;
        
        $return = array();

        if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && current_user_can( 'create_courseware') && wp_verify_nonce( $data['nc_nonce'], 'nc_nonce' ))) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong';                
            echo json_encode($return);
            exit;
        }
        
        if(!empty($data['nc_title'])) {
            
            $my_program = array(
                        'post_title'   => $data['nc_title'],
                        'post_type'    => 'program',
                        'post_status'  => 'publish'
            );

            $program_id = wp_insert_post( $my_program );
            
            do_action('create_program');

            if(!empty($program_id)) {
                $return['status'] = 'success';
                $return['data']   = get_the_permalink($program_id).'type';                
                echo json_encode($return);
            } else {
                $return['status'] = 'error';
                $return['data']   = 'Something went wrong';                
                echo json_encode($return);
            }
        } else {
            $return['status'] = 'error';
            $return['data']   = 'Please enter title';                
            echo json_encode($return);
        }
        exit;
    }
    
    /**
     * @since 1.0
     */
    public function delete() {
        
        if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && current_user_can( 'delete_courseware'))) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong';                
            echo json_encode($return);
            exit;
        }
        
        $data = $_POST;
        
        if(get_post_type($data['id']) == 'bundle') {
        
            global $wpdb;

            $parent_id = $wpdb->get_var( "SELECT post_parent FROM $wpdb->posts WHERE ID=".$data['id'] );

            $modules = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_parent=".$data['id'] );

            if(!empty($modules)) {
                foreach($modules as $module) {
                    $module_data = array('post_parent' => $parent_id);
                    $wpdb->update( $wpdb->posts, $module_data, array('ID' => $module->ID) );
                }
                $return['url'] = 'reload';
            }
        
        }
        
        if(!empty($data['id'])) {
            $program_id = (int) $data['id'];
            wp_delete_post($program_id);
            $return['status'] = 'success';
            $return['data']   = 'Successfully deleted.';                
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data']   = 'you can not delete this.';                
            echo json_encode($return);
        }
        exit;
        
    }

    /**
     * @since 1.0
     */
    public function reorder() {
        
        $data = $_POST;
        
        $return = array();

        if(!(is_array($data) && defined('DOING_AJAX') && DOING_AJAX && current_user_can( 'edit_courseware'))) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong!';                
            echo json_encode($return);
            exit;
        }
        
        global $wpdb;
        
        $values = $data['item'];
        
        if(!empty($values)) {
            foreach( $values as $position => $id ) {            
                $data = array('menu_order' => $position);
                $wpdb->update( $wpdb->posts, $data, array('ID' => $id) );
            }

            $return['status'] = 'success';
            $return['data']   = 'yes';                
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong!';                
            echo json_encode($return);
        }
        exit;
    }
    
    /**
     * @since 1.0
     */
    public function load_course_modules() {

        $data   = $_REQUEST;
        $return = array();

        if(defined('DOING_AJAX') && DOING_AJAX) { 
            
            $course_id = $data['course'];

            $args = array(
                'post_type'=> 'bundle',
                'post_parent'=> $course_id,
                'posts_per_page'=> -1,
                'orderby'=> 'menu_order',
                'order'=> 'ASC',
            );
            
            $the_query = new WP_Query( $args );

            if( $the_query->have_posts() ):

                while ($the_query->have_posts()) : $the_query->the_post();

                    $return['data'][get_the_id()] = get_the_title();

                endwhile;

                $return['status'] = 'success';
                 
                echo json_encode($return);
                exit();

            else :

                $return['status'] = 'error';
                $return['data']   = 'No sales id found';                
                echo json_encode($return);
                exit();

            endif;

            wp_reset_postdata();
        }
    }
    
    /**
     * @since 1.0
     */
    public static function get_all_courses($type = '',$id = ''){
        
        $courses = array();
        $course_args    = array(
            'post_type'         => 'program',
            'numberposts'       => -1            
        );
        
        $courses        = get_posts($course_args);
        $course_list    = array();
        
        foreach($courses as $course){
            switch($type){
                case 'franchisee':
                    $franchises = get_post_meta($course->ID,'franchises',true);                          
                    if(!empty($franchises) && in_array($id,$franchises)){                       
                        $course_list[$course->ID]   = $course->post_title;
                    }
                    break;
                case 'groups':
                    $groups = get_post_meta($course->ID,'groups',true);
                    
                    if(is_array($id)){
                        foreach($id as $value){
                            if(!empty($groups) && in_array($id,$groups))
                                $course_list[$course->ID]   = $course->post_title;                            
                        } 
                    }else{
                        if(!empty($groups) && in_array($id,$groups))
                            $course_list[$course->ID]   = $course->post_title;                        
                    }
                    break;
                default:
                    $course_list[$course->ID]   = $course->post_title;    
            }
        }
        
        return $course_list;
    }
    
    public static function get_all_courses_for_survey($type = ''){
        
        $courses = array();
        $course_args    = array(
            'post_type'         => 'program',
            'numberposts'       => -1            
        );
        
        $courses        = get_posts($course_args);
        $course_list    = array();
        
        foreach($courses as $course){
            switch($type){
                case 'franchisee':
                    $franchisee_id = get_current_user_franchisee_id();
                    $franchises = get_post_meta($course->ID,'franchises',true);                          
                    if(!empty($franchises) && in_array($franchisee_id,$franchises)){                       
                        $course_list[]   = $course->ID;
                    }
                    break;
                case 'author':
                        $franchisee_id = get_current_user_franchisee_id();
                        $course_list = array();
                        global $wpdb;
                        $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");

                        global $current_user;
                        if ($results) {
                            foreach ($results as $k => $v) {
                                $g_id = $v->ID;
                                $facilitator = get_post_meta($g_id,'facilitators');
                                if(in_array($current_user->ID, $facilitator))
                                {
                                    $posts = get_post_meta($g_id, 'posts', true);
                                    foreach ($posts as $post) {
                                        if (!in_array($post, $course_list)) {
                                            array_push($course_list, $post);
                                        }
                                    }
                                }
                            }
                        }
                    break;
                default:
                    $course_list[]   = $course->ID;    
            }
        }
        
        return $course_list;
    }
    
    public static function get_all_courses_dropdown($type = '',$id = '' , $disabled = '',$multiple = true,$name = '',$course_ids = array()){
        if(defined('DOING_AJAX') && DOING_AJAX && isset($_POST) && !empty($_POST)){
            $type   = $_POST['type'];
            $id     = $_POST['id'];
        }
        
        $name   = ($name)?$name:'grp_posts[]';
        
        $course_list    = self::get_all_courses($type,$id);
        $multiple       = ($multiple)?'multiple':'';
        
        $html = '';
        $html.= '<select id="post_courses" data-placeholder="Select Course" name="'.$name.'" class="chosen-select" '.$multiple.' '.$disabled.'>';
        foreach ($course_list as $key => $program) { 
            if(is_array($course_ids)){
                $selected   = (in_array($key,$course_ids))?'selected':'';
            }else{
                $selected   = ($key == $course_ids)?'selected':'';
            }
            
            $html.= '<option value="'.$key.'" '.$selected.'>'.$program.'</option>';
        }
        $html.= '</select>';
        
        if(defined('DOING_AJAX') && DOING_AJAX){
            echo $html;
            exit();
        }else{
            return $html;
        }
        
    }

}

new Programs();

