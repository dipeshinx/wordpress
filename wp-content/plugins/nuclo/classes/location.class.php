<?php
global $location;

/**
 * @package  Location
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Location {
    
    private $country_db;
    private $city_db;
    
    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        global $wpdb;
        $this->country_db = $wpdb->prefix . 'country';
        $this->city_db = $wpdb->prefix . 'city';
        add_action('admin_init',array($this,'create_table'));
        add_action('wp_ajax_ajax_city_dropdown',array($this,'ajax_city_dropdown'));
    }
    
    /**
     * @since 1.0
     */
    public function create_table() {
        global $wpdb;
        if($wpdb->get_var("show tables like '$this->country_db'") != $this->country_db) {
            $country_sql = "CREATE TABLE `".$this->country_db."` (`id` tinyint(4) NOT NULL auto_increment,`country` varchar(20) NOT NULL default '', PRIMARY KEY  (`id`))";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($country_sql);
        }
        if($wpdb->get_var("show tables like '$this->city_db'") != $this->city_db) {
            $city_sql = "CREATE TABLE `".$this->city_db."` ( `id` tinyint(4) NOT NULL auto_increment, `city` varchar(50) default NULL, `stateid` tinyint(4) default NULL, `countryid` tinyint(4) NOT NULL, PRIMARY KEY  (`id`))";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($city_sql);
        }
    }
    
    /**
     * @since 1.0
     */
    public function get_country() {
        global $wpdb;
        $countries = $wpdb->get_results( "SELECT * FROM $this->country_db" );
        $countries_array = array();
        foreach ($countries as $country) {
            $countries_array[$country->id] = $country->country;
        }
        return $countries_array;
    }
    
    /**
     * @since 1.0
     */
    public function get_country_name($country_id) {
        global $wpdb;
        if(!empty($country_id)) {
            $country_name = $wpdb->get_var( "SELECT country FROM $this->country_db where id=$country_id" );
        }
        return $country_name;
    }
    
    /**
     * @since 1.0
     */
    public function get_city($country_id) {
        global $wpdb;
        if(!empty($country_id)) {
            $cities = $wpdb->get_results( "SELECT * FROM $this->city_db where countryid=$country_id" );
        }
        $cities_array = array();
        if(!empty($cities)) {
            foreach ($cities as $city) {
                $cities_array[$city->id] = $city->city;
            }
        }
        return $cities_array;
    }
    
    /**
     * @since 1.0
     */
    public function get_city_name($city_id) {
        global $wpdb;
        if(!empty($city_id)) {
            $city_name = $wpdb->get_var( "SELECT city FROM $this->city_db where id=$city_id" );
        }
        return $city_name;
    }
    
    /**
     * @since 1.0
     */
    public function get_city_dropdown($country_id,$default='') {
        $cities = $this->get_city($country_id);
        
        if($cities) {
            $select = selectbox('venue[0][city]',$cities,$default,'','city');
        } else {
            $select = 'No city found';
        }
        
        return $select;        
    }
    
    /**
     * @since 1.0
     */
    public function ajax_city_dropdown() {
        $country_id = $_POST['country_id'];
        echo $this->get_city_dropdown($country_id);
        exit;
    }
   
}
new Location();