<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once($plugin_dir.'classes/thirdparty_lib/tcpdf/tcpdf.php');

class XTCPDF extends TCPDF {
    
    var $participant_name   = '';
    var $module_name        = '';
    var $header_type        = '';
    
    //Page header
    public function Header() {
        
        switch($this->header_type){
            case 'supervisor_mail':
                $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        
                $this->SetFont('dejavusans', 'B', 10);
                $this->Text(15,8,'Participant Name: '.$this->participant_name ); 
                $this->SetFont('dejavusans', 'B', 10);
                $this->Text(15,13,'Module Name: '.$this->module_name ); 

                $style = array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
                $this->Line(15, 20, 195, 20, $style);
                
                break;
            
            case 'reports':
//                $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                $image  = get_field('logo','option');
                
                $html    = '<table border="0" cellpadding="6">';
                $html   .= '<tr style="height:40px;background-color:#212240;width:100%">';
                $html   .= '<td><img src="'.$image.'" style="width:176px;height:19px;"/></td>';                
                $html   .= '</tr>';
                $html   .= '</table>';
                
                $this->writeHTMLCell(285, 20, 6, 6, $html);                
                break;
            
            case 'A4':
                $image  = get_field('logo','option');
                
                $html    = '<table border="0" cellpadding="6">';
                $html   .= '<tr style="height:40px;background-color:#212240;width:100%">';
                $html   .= '<td><img src="'.$image.'" style="width:176px;height:19px;"/></td>';                
                $html   .= '</tr>';
                $html   .= '</table>';
                
                $this->writeHTMLCell(198, 20, 6, 6, $html);                
                break;
        }
        
    }

    // Page footer
    public function Footer() {
        
        switch($this->header_type){
            case 'supervisor_mail':                
                
                break;
            
            case 'reports':
                
                $this->SetY(-15);
                $this->SetFont('dejavusans', '', 8);
                // Page number
                $style = array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
                $this->Line(5, 405, 290, 405, $style);
                $this->MultiCell(55, 15, 'Page: '.$this->getAliasNumPage().' Of '.$this->getAliasNbPages(), 0, 'R', 0, 0, 250, 408, true, 0, false, true, 0);
                
                $this->SetY(-15);
                
                break;
        }
    }
}