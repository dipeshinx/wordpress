<?php

/**
 * @package  BPM
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class BPM {
    
    var $months;
    
    var $others;
    
    var $results;
    
    var $challenges;
    
    var $group;
    
    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct($user_id) {
        $this->course_id = get_field('bpm','option');
        $this->user_id = $user_id;
        $this->months = array('January' => '', 'February' => '', 'March' => '', 'April' => '', 'May' => '', 'June' => '', 'July' => '', 'August' => '', 'September' => '', 'October' => '', 'November' => '', 'December' => '');
        $this->group = $this->get_group();   
        $this->others = array(); 
        $this->challenges = array();
        $this->get_months();
    }

    private function get_group() {
        $course_groups = Group::get_groups('post', $this->course_id, 'ids');          
        $user_groups = Group::get_groups('user', $this->user_id, 'ids');  
        if(empty($user_groups))
            $user_groups = array();
        
        if(empty($course_groups))
            $course_groups = array();    
        
        $groups     = $user_groups;
        $group_id   = current($groups);
        $group      = array();
        $group['id'] = $group_id;
        $group['start'] = get_field('group_start_date', $group_id);
        $group['end'] = get_field('group_end_date', $group_id);
        return $group;
    }

    private function get_events() { 
        $user_groups = get_user_meta($this->user_id, 'groups', true);
        $event_args = array(
            'post_type' => array('event'),
            'post_parent' => $this->course_id,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array('key' => 'start_date', 'value' => date("Ymd"), 'compare' => '<=', 'type' => 'DATE'),
                array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!='),
                array('key' => 'groups', 'value' => $user_groups, 'compare' => 'IN')
            )
        );
        $event_query = new WP_Query($event_args);
       
        if ($event_query->have_posts()) {
            while ($event_query->have_posts()) {
                $event_query->the_post();
        
                $month_num = (int)get_field('month');                
                $month_name = date('F', mktime(0, 0, 0, $month_num, 10));
                $attended = $this->check_user_attend_session_status(get_the_ID());
                
                if($attended =='attended'){
                    if (empty($this->months[$month_name]['event']))
                        $this->months[$month_name]['event'] = get_the_ID();
                }
            }            
        }
        
        $event_args1 = array(
            'post_type' => array('event'),
            'post_parent' => $this->course_id,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array('key' => 'start_date', 'value' => date("Ymd"), 'compare' => '>=', 'type' => 'DATE'),
                array('key' => 'event_type', 'value' => 'Repeat', 'compare' => '!='),
                array('key' => 'groups', 'value' => $user_groups, 'compare' => 'IN')
            )
        );
        $event_query1 = new WP_Query($event_args1);
        
        if ($event_query1->have_posts()) {
            while ($event_query1->have_posts()) {
                $event_query1->the_post();
        
                $month_num = get_field('month');
                $month_name = date('F', mktime(0, 0, 0, $month_num, 10));
    
                if (empty($this->months[$month_name]['event']))
                    $this->months[$month_name]['event'] = get_the_ID();
                
            }                     
        }           
        wp_reset_postdata();
        
    }

    public function check_user_attend_session_status($module_id=0,$user_id = 0,$ceo = false)
    {
        if($user_id == 0){
            global $current_user; 
            $user_id = $current_user->ID;
        }
        //foreach  
        if($module_id != 0 && $user_id != 0) {
            
            $metadata = get_field('attendance', $module_id);
            if($metadata)
            {
                foreach($metadata as $key=>$val)
                {
                    if($val['user']['ID'] == $user_id && $val['status']=='attended')
                    {
                        return 'attended';
                    }
                    elseif($val['user']['ID'] == $user_id && $val['status']=='not-attended')
                    {
                        return 'not-attended';
                    }
                }
            }
            
        }
        
        if($ceo){
            return 'not-attended';
        }else{
            return 'attended';
        }
        
    }
    
    private function get_modules() {
        $module_args = array(
            'post_type' => array('survey', 'extra', 'measure', 'discussion', 'assessment', 'bundle','media'),
            'post_parent' => $this->course_id,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC',
        );
        $module_query = new WP_Query($module_args);
        if ($module_query->have_posts()) {
            while ($module_query->have_posts()) {
                $module_query->the_post();
        
                $month_num = get_field('month_of_module');
                $menu_order = get_post_field('menu_order', get_the_ID());
        
                if (!empty($month_num)) {
                    $month_name = date('F', mktime(0, 0, 0, $month_num, 10));
                    if (get_post_type() == 'survey') {
                        $this->months[$month_name]['feedback'] = get_the_ID();
                    } else if (get_post_type() == 'bundle') {
                        $bundle_args = array(
                            'post_type' => array('survey', 'extra', 'measure', 'discussion', 'assessment','media'),
                            'post_parent' => get_the_ID(),
                            'posts_per_page' => -1,
                            'orderby' => 'menu_order',
                            'order' => 'ASC',
                        );
                        $bundle_query = new WP_Query($bundle_args);
                        $bundle_title = get_the_title();
                        if ($bundle_query->have_posts()) {
                            while ($bundle_query->have_posts()) {
                                $bundle_query->the_post();
                                $menu_order = get_post_field('menu_order', get_the_ID());
                                $this->months[$month_name][$bundle_title][$menu_order] = get_the_ID();
                            }                            
                        }
                        wp_reset_postdata();
                    } else {
                        $this->months[$month_name][$menu_order] = get_the_ID();
                    }
                } else {
                    $this->others[] = get_the_ID();
                    $this->get_survey_data(get_the_ID());
                }
            }
        }
        wp_reset_postdata();
    }

    private function get_months() {        
        $this->get_events();
        $this->get_modules();
        $date = $this->group['start'];
        $start = str_replace('/', '-', $date);
        $start_month = date('F',strtotime($start));

        foreach($this->months as $key => $month) {
           if($key == $start_month) {
              break;
           } else {
              $item = $this->months[$key];
              unset($this->months[$key]);
              $this->months[$key] = $item;
           }   
        }
    }
    
    public function get_feedacks() {
        $feedbaks = array();
        foreach($this->months as $month => $modules) {
            $feedbaks[$month] = $this->months[$month]['feedback'];
        }
        return $feedbaks;
    }
    
    private function get_survey_data($id){
        $survey_id = get_post_meta($id, 'survey_id', true);
        if(!empty($survey_id)){
            
            global $wpdb;
            
            $current_user = wp_get_current_user();
            $current_email = $current_user->user_email;
            $results = $wpdb->get_results('SELECT * FROM `'.WPSQT_TABLE_RESULTS. '` WHERE `person_name` = "'.$current_email.'" AND `item_id` = "'.$survey_id.'"', ARRAY_A);
                        
            if (count($results) != 0) {
                foreach ($results as $key => $s_result) {
                    $survey = new SurveyReport('', $s_result['id']);
                    $result = $survey->single_result();                     
                    foreach($result['sections'] as $section){
                        if($section['name'] == 'Individual Challenges'){
                            foreach($section['questions'] as $question){
                                if($question['order']   == 12){
                                    $this->challenges['individual'] = $section['answers'][$question['id']]['given'];
                                }
                            }                            
                        }else if($section['name'] == 'Team Challenges'){
                            foreach($section['questions'] as $question){
                                if($question['order']   == 16){
                                    $this->challenges['team'] = $section['answers'][$question['id']]['given'];
                                }
                            }                                                        
                        }else if($section['name'] == 'Level of Engagement'){
                            foreach($section['questions'] as $question){                                      
                                if($question['order']   == 19){                                    
                                    $this->challenges['goal'] = $question['answers'][$section['answers'][$question['id']]['given'][0]]['text'];
                                }
                            }                                                        
                        }
                    }
                }                  
            }
        }
    }
    
    public function get_assessment_status($type='') {
        switch ($type) {
            case 'pre':                
                    $status = get_user_meta($this->user_id,'module_result_'.$this->others[0],true);
                    return $status==1?'dot-ebonyclay':'dot-milanored';
                break;
            case 'mid':
                    $status             = get_user_meta($this->user_id,'module_result_'.$this->others[1],true);
                    return $status==1?'dot-ebonyclay':'dot-milanored';
                break;
            case 'final':
                    $status             = get_user_meta($this->user_id,'module_result_'.$this->others[2],true);
                    return $status==1?'dot-ebonyclay':'dot-milanored';
                break;
        }
    }
    
    private function get_results($submission='') {
        $this->results = array();
        $submission = ($submission=='plan')?'plan':'result';
        foreach($this->months as $modules) {
            if(isset($modules) && !empty($modules)){
                foreach($modules as $name => $ids) {  
                    if(!in_array($name,array('event','feedback'))) { 
                        foreach($ids as $id) {                       
                            if($id) {
                                $type = get_field('measure_type',$id);
                                if($type == $submission) {
                                     $post = get_user_meta($this->user_id, 'submission_'.$submission.'_' . $id, true);
                                     $this->results[] = $post;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public function get_impact($type='',$submission=''){
        $submission = ($submission=='plan')?'plan':'result';
        $this->get_results($submission);  
        $value = 0;
        switch ($type) {
            case 'revenue':
                foreach($this->results as $this->result) {
                    $revenue_array  = explode(',',get_post_meta($this->result, 'i_revenue', true));
                    $revenue = '';
                    foreach($revenue_array as $revenue_a){
                        $revenue .= $revenue_a;
                    }
                    $value += (int) $revenue;
                }
                break;
            case 'costsaving':
                foreach($this->results as $this->result) {
                    $costsaving_array  = explode(',',get_post_meta( $this->result, 'i_costsaving', true));
                    $costsaving = '';
                    foreach($costsaving_array as $costsaving_a){
                        $costsaving .= $costsaving_a;
                    }
                    $value += (int) $costsaving;
                }
                break;
            case 'productivity':                
                foreach($this->results as $this->result) {
                    $productivity_array  = explode(',',get_post_meta( $this->result, 'i_productivity', true));
                    $productivity = '';
                    foreach($productivity_array as $productivity_a){
                        $productivity .= $productivity_a;
                    }
                    $value += (int) $productivity;
                }
                break;
            case 'customersatisfaction':
                foreach($this->results as $this->result) {
                    $value += (int) get_post_meta( $this->result, 'i_customersatisfaction', true);
                }                
                break;
            case 'riskmanage':
                foreach($this->results as $this->result) {
                    $value += (int) get_post_meta( $this->result, 'i_riskmanage', true);
                }
                break;
            case 'innovation':
                foreach($this->results as $this->result) {
                    $value += (int) get_post_meta( $this->result, 'i_innovation', true);
                }
                break;
        }
        return $value;
    }
    
    public function get_single_impact($module_id,$submission='',$type='') {
        
        $submission = ($submission=='plan')?'plan':'result';
        
        $submission_id = get_user_meta($this->user_id, 'submission_'.$submission.'_' . $module_id, true);
        
        switch ($type) {
            case 'revenue':
                $revenue_array  = explode(',',get_post_meta( $submission_id, 'i_revenue', true));
                $revenue = '';
                foreach($revenue_array as $revenue_a){
                    $revenue .= $revenue_a;
                }
                $value = (int) $revenue;                        
                break;
            case 'costsaving':
                $costsaving_array  = explode(',',get_post_meta( $submission_id, 'i_costsaving', true));
                $costsaving = '';
                foreach($costsaving_array as $costsaving_a){
                    $costsaving .= $costsaving_a;
                }
                $value = (int) $costsaving;                
                break;
            case 'productivity':
                $productivity_array  = explode(',',get_post_meta( $submission_id, 'i_productivity', true));
                $productivity = '';
                foreach($productivity_array as $productivity_a){
                    $productivity .= $productivity_a;
                }
                $value = (int) $productivity;               
                break;
            case 'customersatisfaction':
                $value = get_post_meta( $submission_id, 'i_customersatisfaction', true);
                break;
            case 'innovation':
                $value = get_post_meta( $submission_id, 'i_innovation', true);
                break;
            case 'riskmanage':
                $value = get_post_meta( $submission_id, 'i_riskmanage', true);
                break;
        }
        
        return number_format($value);
    }
    
    public function get_medal() {
        $total_count            = 0;
        $complete               = 0;
        $best_idea              = 0;
        $best_idea_total_count  = 0;
        
        $return = array();
        $events = array();     
      
        $report_array   = array();        
        
        foreach($this->months as $month => $modules) {
            foreach($modules as $name => $module) {  
                if(!in_array($name,array('event','feedback'))){
                    $report_array[$name]   = array(
                        'session'      => 0,
                        'action_plan'  => 0,
                        'results'      => 0,
                        'best_idea'    => 0
                    );
                }
            }
        }
     
        foreach($this->months as $month => $modules) {
            $key = 1;
            foreach($modules as $name => $module) {  
                 if(!in_array($name,array('event','feedback')) && !empty($module) && is_array($module)){
                     foreach($module as $id) {                  
                        if(!empty($modules['event'])){
                            $report_array[$name]['session'] = (get_user_meta($this->user_id,'module_result_'.$modules['event'],true))?50:0;
                            $selected_best_idea             = get_user_meta($this->user_id,'best_idea_'.$modules['event'].'_'.($key),true);
                            $best_idea_total_count++;
                            if($selected_best_idea){   
                                $report_array[$name]['best_idea']    = 100;
                                $best_idea++;
                            }
                        }
                        if(!isset($events[$month]) && !empty($modules['event'])){
                            $total_count    = $total_count+2;
                            $events[$month] = $modules['event'];
                            $status = get_user_meta($this->user_id,'module_result_'.$modules['event'],true);         
                            if($status) {                                
                                $complete = $complete+2;                               
                            }
                        }
                        
                        $type = get_field('measure_type',$id);  
                        $status = get_user_meta($this->user_id,'module_result_'.$id,true);
                        if(in_array($type,array('plan','result'))) {
                            $total_count++;
                            if($status) {
                                if($type == 'plan'){
                                    $report_array[$name]['action_plan'] = 50;
                                }else{
                                    $report_array[$name]['results'] = 50;
                                }
                                $complete++;
                            }
                        }                       
                        
                    }
                    $key++;
                }  
            }
           
        }
       
        $return['height']   = (($total_count+$best_idea_total_count)>0)?(90*($complete+$best_idea))/($total_count+$best_idea_total_count):0; 
        $return['point']    = round($complete*50,0) + round($best_idea*100, 0);
        $return['total']    = round($total_count*50,0)+round($best_idea_total_count*100,0);        
        $return['report']   = $report_array;
        
        return $return;
    }
    
}

function module_status($module_id=0,$user_id=0) {
    
    if($user_id == 0)
        global $current_user; $user_id = $current_user->ID;
        
    if($module_id != 0 && $user_id != 0) {
        $status = get_user_meta($user_id,'module_result_'.$module_id,true);        
        $link   = get_permalink($module_id);
        $title  = get_post_field('post_title',$module_id);
        if($title == 'Pre Assessment'){
            $link .= '?pre_assessment_completed=1';
        }
        if($status)
            return '<a href="'.$link.'" class="btn btn-blue pull-right"><i class="fa fa-eye"></i></a><b style="color:#3c763d;float:right">Completed</b>';
        else 
            return FALSE;
    }
    
    return '';
}
function check_module_status_completed($module_id=0,$user_id=0) {
    
    if($user_id == 0){
        global $current_user; 
        $user_id = $current_user->ID;
    }
    if($module_id != 0 && $user_id != 0) {
        
        $status = get_user_meta($user_id,'module_result_'.$module_id,true);
        if($status)
            return 1;
        else 
            return 0;
    }
    
    return 0;
}

function participant_road_map($month = NULL){
    global $current_user,$post;    
    $bpm_obj        = new BPM($current_user->ID);    
    $current_month  = (is_null($month) && empty($month))?date('F',time()):$month;   
    
    $attandance_class   = $feedback_class = $action_plan_class = $result_class = '';
    $event_status       = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['event'],true);
    if(!empty($bpm_obj->months[$current_month]['event']) && (!$event_status)){
        $attandance_class   = 'road_map_highlight';
    }
    
    $feedback_status    = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['feedback'],true);                            
    if(!empty($bpm_obj->months[$current_month]['feedback']) && ($event_status) && (!$feedback_status)){
        $feedback_class     = 'road_map_highlight';
    }
    $tag                = get_field('tag',$bpm_obj->months[$current_month]['feedback']);                            
    $current_post_tag   = get_field('tag',$post->ID);
    
    if(($tag == $current_post_tag) && (nuclo_get_user_role($current_user->ID) == 'contributor')){                                  
        $current_id = $_SESSION['wpsqt']['item_id'];   
        if (count($_SESSION['wpsqt'][$_SESSION['wpsqt']['item_id']]['sections']) == $_POST['step']){                                    
            $feedback_class = '';
        }
    }      
    
    foreach($bpm_obj->months[$current_month] as $key => $module){ 
       if(!in_array($key,array('event','feedback'))){
           
            foreach($module as $id) { 
                $type = get_field('tag',$id);                                 
                if($type    ==  'Action Plan') { 
                    $action_plan_id = $id;
                    $action_plan_status = get_user_meta($current_user->ID,'module_result_'.$id,true);
                    if(($event_status) && ($feedback_status) && (!$action_plan_status)){
                        $action_plan_class     = 'road_map_highlight';
                    }
                    if(($tag == $current_post_tag) && (nuclo_get_user_role($current_user->ID) == 'contributor')){                                  
                        $current_id = $_SESSION['wpsqt']['item_id'];                                       
                        if (count($_SESSION['wpsqt'][$_SESSION['wpsqt']['item_id']]['sections']) == $_POST['step']){                       
                            $action_plan_class     = 'road_map_highlight';
                        }
                    }           
                }
            }

            foreach($module as $id) { 
                $type = get_field('tag',$id);   
                if($type    ==  'Results'){
                    $result_status = get_user_meta($current_user->ID,'module_result_'.$id,true);
                    if(($event_status) && ($feedback_status) && ($action_plan_status) && (!$result_status)){
                        $result_class     = 'road_map_highlight';
                    }
                }
            }
       }
    }   
    
?>
<div class="dashboard table-responsive <?php  if(get_current_user_role()=='contributor' && is_page('dashboard') && $count == 0) echo 'tour_step_1';?>" style="margin-bottom: 20px;">
    <table class="table table-condensed table-bordered responsive-font" style="margin-bottom: 0px;">
        <thead>
            <tr>
                <th class="text-center" style="vertical-align: inherit !important;"><?php echo $current_month.' Modules'?></th>
                <th class="text-center hidden-md hidden-lg visible-sm visible-xs" style="vertical-align: inherit !important;">Media</th>
                <th class="text-center <?php echo $attandance_class; ?>">1 <br/> Attendance</th>
                <th class="text-center <?php echo $feedback_class; ?>">2 <br/> Feedback</th>
                <th class="text-center <?php echo $action_plan_class; ?>">3 <br/> Action Plan</th>
                <th class="text-center <?php echo $result_class; ?>">4 <br/> Results</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $count = 0;                               
                foreach($bpm_obj->months[$current_month] as $key => $module){ 
                    if(!in_array($key,array('event','feedback'))){
                ?>
                <tr>
                    <td>
                        <span class="col-xs-12 col-md-10">
                        <?php echo $key; ?>
                        </span>
                        <span class="col-md-2 hidden-xs hidden-sm">
                            <?php
                                $i = 0;
                                foreach($module as $id){
                                    $event_status   = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['event'],true);
                                    $post_type      = get_post_type($id);
                                    if($post_type == 'media'){
                                        if($event_status){
                            ?>
                                        <a href="<?php echo get_permalink($id); ?>" class="media-icon media-green col-xs-6"><i class="fa fa-play-circle"  data-toggle="tooltip" data-placement="top" data-original-title="Media"></i></a>
                            <?php                         
                                        }else{
                            ?>
                                        <p class="media-icon media-grey col-xs-6" style="margin-bottom:0px;"><i class="fa fa-play-circle" data-toggle="tooltip" data-placement="top" data-original-title="Media"></i></p>
                            <?php 
                                        }  
                                        break;
                                        $i++;
                                    }
                                    
                                }
                                $i = 0;
                                foreach($module as $id){
                                    $event_status   = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['event'],true);
                                    $post_type      = get_post_type($id);
                                    if($post_type == 'extra'){
                                        
                                        if($event_status){
                            ?>
                                        <a href="<?php echo get_permalink($id); ?>" class="media-icon media-green media-extra col-xs-6"><i class="fa fa-file-text-o"  data-toggle="tooltip" data-placement="top" data-original-title="Resources"></i></a>
                            <?php
                                        }else{
                            ?>
                                        <p class="media-icon media-grey media-extra col-xs-6" style="margin-bottom:0px;"><i class="fa fa-file-text-o" data-toggle="tooltip" data-placement="top" data-original-title="Resources"></i></p>
                            <?php 
                                        }
                                        $i++;
                                    }
                                }
                            ?>
                        </span>
                    </td>
                    <td class="hidden-md hidden-lg visible-sm visible-xs media-icon-responsive">
                        <?php
                            $i = 0;
                            foreach($module as $id){
                                $event_status   = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['event'],true);
                                $post_type      = get_post_type($id);
                                if($post_type == 'media'){
                                    if($event_status){
                        ?>
                                    <a href="<?php echo get_permalink($id); ?>" class="media-icon media-green col-xs-6"><i class="fa fa-play-circle"  data-toggle="tooltip" data-placement="top" data-original-title="Media"></i></a>
                        <?php                         
                                    }else{
                        ?>
                                    <p class="media-icon media-grey col-xs-6" style="margin-bottom:0px;"><i class="fa fa-play-circle" data-toggle="tooltip" data-placement="top" data-original-title="Media"></i></p>
                        <?php 
                                    }  
                                    break;
                                    $i++;
                                }

                            }
                            $i = 0;
                            foreach($module as $id){
                                $event_status   = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['event'],true);
                                $post_type      = get_post_type($id);
                                if($post_type == 'extra'){

                                    if($event_status){
                        ?>
                                    <a href="<?php echo get_permalink($id); ?>" class="media-icon media-green media-extra col-xs-6"><i class="fa fa-file-text-o"  data-toggle="tooltip" data-placement="top" data-original-title="Resources"></i></a>
                        <?php
                                    }else{
                        ?>
                                    <p class="media-icon media-grey media-extra col-xs-6" style="margin-bottom:0px;"><i class="fa fa-file-text-o" data-toggle="tooltip" data-placement="top" data-original-title="Resources"></i></p>
                        <?php 
                                    }
                                    $i++;
                                }
                            }
                        ?>
                    </td>
                    <?php if($count == 0){ ?>
                    <td class="text-center" rowspan="2">
                        <?php
                            $status = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['event'],true);                            
                            if($status){
                        ?>
                                <a href="<?php echo get_permalink($bpm_obj->months[$current_month]['event']); ?>" class="tickmark tickmark-big" data-toggle="tooltip" data-placement="top" data-original-title="Attended"><i class="fa fa-check"></i></a>
                        <?php
                            }else{ 
                                if(empty($bpm_obj->months[$current_month]['event'])){
                        ?>
                                <p class="dot dot-milanogrey" style="margin-bottom:0px;" data-toggle="tooltip" data-placement="top" data-original-title="Not Attended"></p>
                        <?php  
                                }else{
                        ?>
                                <a href="<?php echo get_permalink($bpm_obj->months[$current_month]['event']); ?>" class="dot dot-milanored" data-toggle="tooltip" data-placement="top" data-original-title="Not Attended"></a>
                        <?php            
                                }
                            } 
                        ?>
                        <?php  if(get_current_user_role()=='contributor' && is_page('dashboard') && $count == 0)
                            {
                                echo '<div id="step_tour_1"></div>';
                            }
                        ?>
                    </td>
                    <td class="text-center" rowspan="2">
                        <?php
                            $status             = get_user_meta($current_user->ID,'module_result_'.$bpm_obj->months[$current_month]['feedback'],true);       
                            $tag                = get_field('tag',$bpm_obj->months[$current_month]['feedback']);                                                                
                            $current_post_tag   = get_field('tag',$post->ID);
                            if(($tag == $current_post_tag) && (nuclo_get_user_role($current_user->ID) == 'contributor')){                                                                            
                                if (count($_SESSION['wpsqt'][$_SESSION['wpsqt']['item_id']]['sections']) == $_POST['step']){
                                    $status = 1;
                                }
                            }                            
                            if($status){
                        ?>
                                <a href="<?php echo get_permalink($bpm_obj->months[$current_month]['feedback']); ?>" class="tickmark tickmark-big" data-toggle="tooltip" data-placement="top" data-original-title="Submitted"><i class="fa fa-check"></i></a>
                        <?php
                            }else{ 
                                $event_status   = check_module_status_completed($bpm_obj->months[$current_month]['event']);
                                if(empty($bpm_obj->months[$current_month]['feedback'])){
                        ?>
                                <p class="dot dot-milanogrey" style="margin-bottom:0px;" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></p>
                        <?php  
                                }else{
                                    if(!empty($bpm_obj->months[$current_month]['feedback']) && (!$event_status)){
                        ?>
                                <p class="dot dot-milanogrey" style="margin-bottom:0px;" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></p>
                        <?php  
                                    }else{
                        ?>
                                <a href="<?php echo get_permalink($bpm_obj->months[$current_month]['feedback']); ?>" class="dot dot-milanored" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></a>
                        <?php            
                                    }
                                } 
                            }
                        ?>
                    </td>
                    <?php } ?>
                    <td class="text-center">
                        <?php
                            foreach($module as $id) { 
                                $type = get_field('tag',$id);                                 
                                if($type=='Action Plan') { 
                                    $action_plan_id = $id;
                                    $status = get_user_meta($current_user->ID,'module_result_'.$id,true);
                                    if($status){
                        ?>
                                <a href="<?php echo get_permalink($id); ?>" class="tickmark tickmark-big" data-toggle="tooltip" data-placement="top" data-original-title="Submitted"><i class="fa fa-check"></i></a>
                        <?php
                                    }else{
                                        if(empty($bpm_obj->months[$current_month]['event'])){
                         ?>
                                <p class="dot dot-milanogrey" style="margin-bottom:0px;" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></p>
                        <?php   
                                        }else{
                                            $event_status       = check_module_status_completed($bpm_obj->months[$current_month]['event']);
                                            $feedback_status    = check_module_status_completed($bpm_obj->months[$current_month]['feedback']);                                            
                                            $tag                = get_field('tag',$bpm_obj->months[$current_month]['feedback']);                            
                                            $current_post_tag   = get_field('tag',$post->ID);
                                            if(($tag == $current_post_tag) && (nuclo_get_user_role($current_user->ID) == 'contributor')){                                                                                                                      
                                                if (count($_SESSION['wpsqt'][$_SESSION['wpsqt']['item_id']]['sections']) == $_POST['step']){                               
                                                    $feedback_status = 1;
                                                }
                                            }                            
                                            if(!empty($bpm_obj->months[$current_month]['event']) && ((!$event_status) || (!$feedback_status))){
                        ?>
                                <p class="dot dot-milanogrey" style="margin-bottom:0px;" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></p>
                        <?php  
                                            }else{
                        ?>
                                <a href="<?php echo get_permalink($id); ?>" class="dot dot-milanored" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></a>
                        <?php                          
                                            }
                                        }
                                    }
                                }
                            }
                        ?>                       
                    </td>
                    <td class="text-center">
                        <?php
                            foreach($module as $id) { 
                                $type               = get_field('tag',$id);                                
                                
                                if($type=='Results') { 
                                    $status = get_user_meta($current_user->ID,'module_result_'.$id,true);
                                    if($status){
                        ?>
                                <a href="<?php echo get_permalink($id); ?>" class="tickmark tickmark-big" data-toggle="tooltip" data-placement="top" data-original-title="Submitted"><i class="fa fa-check"></i></a>
                        <?php
                                    }else{
                                        if(empty($bpm_obj->months[$current_month]['event'])){
                         ?>
                                <p class="dot dot-milanogrey" style="margin-bottom:0px;" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></p>
                        <?php   
                                        }else{
                                            $event_status       = check_module_status_completed($bpm_obj->months[$current_month]['event']);
                                            $feedback_status    = check_module_status_completed($bpm_obj->months[$current_month]['feedback']);
                                            $plan_status        = check_module_status_completed($action_plan_id);
                                            
                                            if(!empty($bpm_obj->months[$current_month]['event']) && ((!$event_status) || (!$feedback_status) || (!$plan_status))){
                        ?>
                                <p class="dot dot-milanogrey" style="margin-bottom:0px;" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></p>
                        <?php  
                                            }else{
                        ?>
                                <a href="<?php echo get_permalink($id); ?>" class="dot dot-milanored" data-toggle="tooltip" data-placement="top" data-original-title="Not Submitted"></a>
                        <?php                          
                                            }
                                        }
                                    }
                                }
                            }
                        ?>            
                    </td>
                </tr>
            <?php 
                    $count++;
                    }
                } 
            ?>
        </tbody>
    </table>
</div>
<?php   
}


function ceo_report_details($user_id){
    
    $bpm_obj                = new BPM($user_id);    
    
    $return                 = array(            
        'not-attended'          => 0,
        'marked_sessions'       => 0,
        'action_plan'           => 0,
        'result'                => 0,
        'revenue'               => 0,
        'costsaving'            => 0,
        'productivity'          => 0,
        'customersatisfaction'  => 0,
        'innovation'            => 0,
        'riskmanage'            => 0,
        'attitudinal'           => 0,
        'total'                 => 0
    );
    
    foreach($bpm_obj->months as $month => $modules){        
        foreach($modules as $key => $module){                       
            if(($key == 'event')){                
                $attendance_status  = get_post_meta($module,'attendance_status',true);                                
                if(($attendance_status == 'marked')){
                    $return['marked_sessions']  += 1;
                }
                if(($attendance_status == 'marked') && ($bpm_obj->check_user_attend_session_status($module,$user_id,true) == 'not-attended')){
                    $return['not-attended']  =+ 1;  
                }
            }

            if(!in_array($key,array('event','feedback'))){
                foreach ($module as $id) {
                    $tag = get_field('tag', $id);
                    if(($tag == 'Action Plan') && (check_module_status_completed($id,$user_id))){
                        $return['action_plan']++;
                    }

                    if(($tag == 'Results') && (check_module_status_completed($id,$user_id))){ 
                        $submission_id = get_user_meta($user_id, 'submission_result_' . $id, true);                         
                        $return['result']++;        
                        $revenue_array  = explode(',',get_post_meta( $submission_id, 'i_revenue', true));
                        $revenue = '';
                        foreach($revenue_array as $revenue_a){
                            $revenue .= $revenue_a;
                        }                        
                        $return['revenue']              += (int) $revenue;
                        $costsaving_array  = explode(',',get_post_meta( $submission_id, 'i_costsaving', true));
                        $costsaving = '';
                        foreach($costsaving_array as $costsaving_a){
                            $costsaving .= $costsaving_a;
                        }                            
                        $return['costsaving']           += (int) $costsaving;
                        $productivity_array  = explode(',',get_post_meta( $submission_id, 'i_productivity', true));
                        $productivity = '';
                        foreach($productivity_array as $productivity_a){
                            $productivity .= $productivity_a;
                        }                        
                        $return['productivity']         += (int) $productivity;
                        $return['customersatisfaction'] += get_post_meta( $submission_id, 'i_customersatisfaction', true);
                        $return['innovation']           += get_post_meta( $submission_id, 'i_innovation', true);
                        $return['riskmanage']           += get_post_meta( $submission_id, 'i_riskmanage', true);                       
                        $return['attitudinal']          += get_post_meta( $submission_id, 'i_attitudinal', true);
                    }

                }
            }            

        }
    }
           
    return $return;
}

function participant_gamification_for_module($user_id,$re_module = ''){
    $bpm_obj            = new BPM($user_id);
    $report_array       = $bpm_obj->get_medal();    
    if(!empty($re_module)){
        $participant_score  = $report_array['report'][$re_module];
    }else{
        $participant_score  =  current($report_array['report']);
    }
    return $participant_score;
}