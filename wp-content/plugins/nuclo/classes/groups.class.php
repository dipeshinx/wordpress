<?php

/**
 * @package  Group
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Group {

    public $per_page;
    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action('init', array(&$this, 'register_post_type'));
        add_action('init', array(&$this, 'rewrites_init'), 1);
        add_action('init', array(&$this, 'create_group'), 1);
        add_action('save_post', array(&$this, 'admin_save_post'), 2);
        add_action('personal_options_update', array(&$this, 'admin_save_user'), 1);
        add_action('edit_user_profile_update', array(&$this, 'admin_save_user'), 1);
        add_action('wp_ajax_delete_group', array(&$this, 'delete_group'), 1);
        add_action('pre_get_posts', array(&$this, 'filter_query'), 1, 1);
        add_action('wp_ajax_get_participants', array(&$this, 'get_participants'));
        add_action('wp_ajax_get_facilitators', array(&$this, 'get_facilitators'));
        add_action('wp_ajax_get_clients', array(&$this, 'get_clients'));
        add_action('wp_ajax_get_franchisee_group', 'get_groups_by_franchise_dropdown');
        add_action('wp_ajax_get_groups_by_client', array(&$this, 'get_groups_by_client'));
        add_action('wp_ajax_get_participants_outsideworkflow', array(&$this, 'get_participants_outsideworkflow'));

        $this->per_page = isset($_REQUEST['record_per_page'])?$_REQUEST['record_per_page']:20;
    }

    /**
     * @since 1.0
     */
    public function register_post_type() {
        if (function_exists('get_field')) :
            $labels = array(
                'name' => _x(get_field('group_general_name', 'option'), 'post type general name', 'nuclo'),
                'singular_name' => _x(get_field('group_singular_name', 'option'), 'post type singular name', 'nuclo'),
                'menu_name' => _x(get_field('group_general_name', 'option'), 'admin menu', 'nuclo'),
                'name_admin_bar' => _x(get_field('group_singular_name', 'option'), 'add new on admin bar', 'nuclo'),
                'add_new' => _x('Add New', strtolower(get_field('group_singular_name', 'option')), 'nuclo'),
                'add_new_item' => __('Add New ' . get_field('group_singular_name', 'option'), 'nuclo'),
                'new_item' => __('New ' . get_field('group_singular_name', 'option'), 'nuclo'),
                'edit_item' => __('Edit ' . get_field('group_singular_name', 'option'), 'nuclo'),
                'view_item' => __('View ' . get_field('group_singular_name', 'option'), 'nuclo'),
                'all_items' => __('All ' . get_field('group_general_name', 'option'), 'nuclo'),
                'search_items' => __('Search ' . get_field('group_general_name', 'option'), 'nuclo'),
                'parent_item_colon' => __('Parent ' . get_field('group_general_name', 'option') . ':', 'nuclo'),
                'not_found' => __('No ' . strtolower(get_field('group_general_name', 'option')) . ' found.', 'nuclo'),
                'not_found_in_trash' => __('No ' . strtolower(get_field('group_general_name', 'option')) . ' found in Trash.', 'nuclo')
            );

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => 'group'),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array('title', 'author', 'editor')
            );

            register_post_type('group', $args);
        endif;
    }

    /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('group/page/([0-9]{1,})/?', 'index.php?post_type=group&paged=$matches[1]', 'top');
        add_rewrite_rule('group/(.+?)/([^/]*)/?$', 'index.php?group=$matches[1]&action=$matches[2]', 'top');
    }

    /**
     * @since 1.0
     */
    public function admin_save_post($post_id) {

        if (isset($_REQUEST['acf']) && is_array($_REQUEST['acf'])) {

            $request = $_REQUEST['acf'];

            if (isset($request['field_56ae48f22676e'])) {

                $groups = $request['field_56ae48f22676e'];
                $this->asign('post', $post_id, $groups);
            }

            if (isset($request['field_56d5d21653c38'])) {

                $groups = $request['field_56d5d21653c38'];
                $this->asign('franchisee', $post_id, $groups);
            }

            if (isset($request['field_56b05a535033e']) || isset($request['field_56b05a6d5033f']) || isset($request['field_56d5cfe4606d7'])) {

                $posts = $request['field_56b05a535033e'];

                $users = $request['field_56b05a6d5033f'];

                $franchisee = $request['field_56d5cfe4606d7'];

                $groups = array();

                if (!empty($posts)) {

                    $groups['post'] = $posts;
                } else {

                    $groups['post'] = array();
                }

                if (!empty($franchisee)) {

                    $groups['franchisee'] = $franchisee;
                } else {

                    $groups['franchisee'] = array();
                }

                if (!empty($users)) {

                    $groups['user'] = $users;
                } else {

                    $groups['user'] = array();
                }

                $this->asign('group', $post_id, $groups);
            }
        }
    }

    /**
     * @since 1.0
     */
    public function admin_save_user($user_id) {

        $request = $_REQUEST['acf'];

        if (array($request)) {

            if (isset($request['field_56ae488c49d32'])) {

                $groups = $request['field_56ae488c49d32'];

                $this->asign('user', $user_id, $groups);
            }
        }
    }

    /**
     * @since 1.0
     */
    public function create_group() {

        global $group_messsage;
        if (isset($_POST['nuclo_create_group']) && wp_verify_nonce($_POST['nuclo_create_group'], 'nuclo_create_group')) {
            
            if (current_user_can('create_group')) {

                $data       = $_POST;
                
                $type       = (isset($data['group_id']) && !empty($data['group_id']))?'edit':'create';
                $validate   = $this->validate($data,$type);
                
                if ($validate == true && !is_array($validate)) {

                    if (isset($data['group_id']) && !empty($data['group_id'])) {

                        $group_post = array(
                            'ID' => $data['group_id'],
                            'post_title' => $data['grp_name'],
                            'post_content' => $data['grp_desc'],
                        );

                        wp_update_post($group_post);

                        $group_id = $data['group_id'];
                        
                        do_action('update_group',$group_id,$data);
                        
                        $this->save_meta($group_id, $data);

                        if (isset($_SESSION['WORKFLOW']['GROUP_ID']) && $_SESSION['WORKFLOW']['GROUP_ID'] == $group_id) {
                            $position = $data['position'];
                            $event_id = isset($data['event_id']) ? $data['event_id'] : '0';
                            $_SESSION['WORKFLOW']['SUCCESS'] = '';
                            $_SESSION['WORKFLOW']['SUCCESS'] = 'Group successfully updated.';
                            if ($position == 'event' && $event_id == 0) {
                                wp_safe_redirect(home_url('create-event'));
                                exit;
                            }
                            if ($position == 'event' && $event_id != 0) {
                                $post_data = get_post($event_id);
                                $url = $post_data->guid . 'edit';

                                wp_safe_redirect($url);
                                exit;
                            }
                            if ($position == 'meeting') {
                                wp_safe_redirect(home_url('compose-email'));
                                exit;
                            }
                            if ($_SESSION['WORKFLOW']['GROUP_ID'] == $group_id) {
                                wp_safe_redirect(home_url('create-event'));
                                exit;
                            }
                        }
                    } else {

                        $user_id = get_current_user_id();

                        // Create group post
                        $group_post = array(
                            'post_title' => $data['grp_name'],
                            'post_content' => $data['grp_desc'],
                            'post_type' => 'group',
                            'post_status' => 'publish',
                            'post_author' => $user_id
                        );

                        // Insert the post into the database
                        $group_id = wp_insert_post($group_post);
                        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
                            //Workflow::save_group($group_id);
                            do_action('save_group_workflow', $group_id);
                        }
                        
                        if (isset($data['continue'])) {
                            $_SESSION['WF_CLIENT']['group_id'] = $group_id;
                            $_SESSION['WF_CLIENT']['franchisee_id'] = $data['grp_franchisee'];
                        }

                        $this->save_meta($group_id, $data);

                        //do_action('create_group',$group_id);

                        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
                            $_SESSION['WORKFLOW']['SUCCESS'] = '';
                            $_SESSION['WORKFLOW']['SUCCESS'] = 'Group successfully created.';
                            wp_safe_redirect('/create-event/');
                            exit;
                        }
                    }
                    $url    = strtok($data['_wp_http_referer'], '?');

                    wp_safe_redirect($url . '/?msg=success');
                    exit;
                }
            } else {
                $group_messsage['error']['create'] = 'You don\'t have permission to create group';
            }
        }
    }

    /**
     * @since  1.0
     */
    public function validate($data,$type) {

        global $group_messsage;

        if (empty($data['grp_name'])) {
            $error['grp_name'] = 'Please enter group name';
        }

        if(!empty($data['grp_start_date']) || !empty($data['grp_end_date']))
        {
            $start_date_data = explode('/',$data['grp_start_date']);
            $end_date_data = explode('/',$data['grp_end_date']);
        }
        
        if (empty($data['grp_start_date']) || empty($data['grp_end_date'])) {
            $error['grp_date'] = 'Start date & End date are required.';
        }
        else if (!checkdate($start_date_data[0],$start_date_data[1],$start_date_data[2]) || !checkdate($end_date_data[0],$end_date_data[1],$end_date_data[2])) {
            $error['grp_date'] = 'Start date & End date are not valid.';
        }
        else if ((strtotime($data['grp_start_date']) < strtotime(date('m/d/Y',strtotime("-1 days")))) && ($type != 'edit')) {
            $error['grp_date'] = 'Startdate is not less than today.';
        }
        else if (strtotime($data['grp_start_date']) > strtotime($data['grp_end_date'])) {
            $error['grp_date'] = 'End date can not be before start date.';
        }
        
        if (current_user_can('administrator') || current_user_can('super-admin')) {

            if (empty($data['grp_franchisee'])) {
                $error['grp_franchisee'] = 'Franchisee is required.';
            }
        }
        if (empty($data['grp_client'])) {
                $error['grp_client'] = 'Client is required.';
            }

        if (empty($data['grp_users'])) {
            $error['grp_users'] = 'Participants is required.';
        }

        if (empty($data['group_id']) && empty($data['grp_facilitators'])) {
            $error['grp_facilitators'] = 'Facilitator is required.';
        }

        if (empty($data['grp_posts'])) {
            $error['grp_posts'] = 'Program is required.';
        }

        if (!empty($error)) {
            $group_messsage['error'] = $error;
            return $error;
        } else {
            return true;
        }
    }

    /**
     * @since  1.0
     */
    public function save_meta($group_id, $data) {

        // Creating Group meta
        update_post_meta($group_id, 'group_start_date', date("Ymd", strtotime($data['grp_start_date'])));
        update_post_meta($group_id, '_group_start_date', 'field_56b8e28634870');
        update_post_meta($group_id, 'group_end_date', date("Ymd", strtotime($data['grp_end_date'])));
        update_post_meta($group_id, '_group_end_date', 'field_56b8e2b434871');

        // Assigning posts and usersmeta for the group
        $groups = array();

        if (current_user_can('editor')) {
            $data['grp_franchisee'] = get_current_user_franchisee_id();
        }

        $groups['facilitator'] = $data['grp_facilitators'];
        $groups['post'] = $data['grp_posts'];
        $groups['user'] = $data['grp_users'];

        $this->asign('group', $group_id, $groups);

        // Adding group meta for the users, posts & Franchisee
        update_post_meta($group_id, 'posts', $data['grp_posts']);
        update_post_meta($group_id, '_posts', 'field_56b05a535033e');

        update_post_meta($group_id, 'franchisee', $data['grp_franchisee']);
        update_post_meta($group_id, '_franchisee', 'field_56d5cfe4606d7');

        update_post_meta($group_id, 'users', $data['grp_users']);
        update_post_meta($group_id, '_users', 'field_56b05a6d5033f');
        update_post_meta($group_id, 'reschedule_group', 0);
        
        if(empty($data['group_id'])) {        
            update_nuclo_meta($group_id, 'post', 'facilitators', $data['grp_facilitators']);
        }

        update_post_meta($group_id, 'client', $data['grp_client']);

        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
            $client_id = $_SESSION['WORKFLOW']['CLIENT_ID'];
            update_post_meta($group_id, 'client', $client_id);
        }
    }

    /**
     * @since 1.0
     */
    public static function asign($type, $id, $groups) {

        switch ($type) {
            case 'post':
                // Set default
                $post = get_post($id, ARRAY_A);
                if (is_null($post))
                    return 0;
                if (!is_array($groups))
                    $groups = array();
                $post_groups = get_post_meta($id, 'groups', true);
                if (!is_array($post_groups))
                    $post_groups = array();

                // REMOVE Group
                if (array($post_groups))
                    $remove_groups = array_diff($post_groups, $groups);
                else
                    $remove_groups = array();

                $merge_groups = array_merge($post_groups, $groups);
                $add_groups = array_diff($merge_groups, $remove_groups);

                update_post_meta($id, 'groups', $add_groups);
                update_post_meta($id, '_groups', 'field_56ae48f22676e');

                if (!empty($remove_groups)) {
                    foreach ($remove_groups as $group_id) {
                        $group_posts = get_post_meta($group_id, 'posts', true);
                        if (!empty($group_posts)) {
                            $index = array_search($id, $group_posts);
                            if ($index !== FALSE) {
                                unset($group_posts[$index]);
                            }
                            update_post_meta($group_id, 'posts', $group_posts);
                            update_post_meta($group_id, '_posts', 'field_56b05a535033e');
                        }
                    }
                }

                if (!empty($add_groups)) {
                    foreach ($add_groups as $group_id) {
                        $group_posts = get_post_meta($group_id, 'posts', true);
                        if (!empty($group_posts)) {
                            $group_posts = array();
                        }
                        $index = array_search($id, $group_posts);
                        if ($index === FALSE) {
                            $group_posts[] = $id;
                        }
                        update_post_meta($group_id, 'posts', $group_posts);
                        update_post_meta($group_id, '_posts', 'field_56b05a535033e');
                    }
                }

                return 1;

                break;

            case 'franchises':
                // Set default
                $post = get_post($id, ARRAY_A);
                if (is_null($post))
                    return 0;

                $franchises = $groups;

                if (!is_array($franchises))
                    $franchises = array();

                $post_franchises = get_post_meta($id, 'franchises', true);

                if (!is_array($post_franchises))
                    $post_franchises = array();

                // REMOVE franchises
                if (array($post_franchises))
                    $remove_franchises = array_diff($post_franchises, $franchises);
                else
                    $remove_franchises = array();

                $merge_franchises = array_merge($post_franchises, $franchises);
                $add_franchises = array_diff($merge_franchises, $remove_franchises);

                $remove_franchises = array_unique($remove_franchises);
                $add_franchises = array_unique($add_franchises);

                update_post_meta($id, 'franchises', $add_franchises);
                update_post_meta($id, '_franchises', 'field_570b72e72f43c');

                if (!empty($remove_franchises)) {
                    foreach ($remove_franchises as $franchisee_id) {
                        $franchisee_courses = get_post_meta($franchisee_id, 'courses', true);
                        if (!empty($franchisee_courses)) {
                            $index = array_search($id, $franchisee_courses);
                            if ($index !== FALSE) {
                                unset($franchisee_courses[$index]);
                            }
                            update_post_meta($franchisee_id, 'courses', $franchisee_courses);
                            update_post_meta($franchisee_id, '_courses', 'field_570b7d979b4d1');
                        }
                    }
                }

                if (!empty($add_franchises)) {
                    foreach ($add_franchises as $franchisee_id) {
                        $franchisee_courses = get_post_meta($franchisee_id, 'courses', true);

                        if (empty($franchisee_courses)) {
                            $franchisee_courses = array();
                        }

                        $index = array_search($id, $franchisee_courses);

                        if ($index === FALSE) {
                            $franchisee_courses[] = $id;
                        }
                        update_post_meta($franchisee_id, 'courses', $franchisee_courses);
                        update_post_meta($franchisee_id, '_courses', 'field_570b7d979b4d1');
                    }
                }

                return 1;

                break;

            case 'user':

                $user = get_user_by('ID', $id);

                if (is_null($user)) {
                    return 0;
                }

                if (!is_array($groups)) {
                    $groups = array();
                }

                $user_groups = get_user_meta($id, 'groups', true);

                if (empty($user_groups)) {
                    $user_groups = array();
                } else {
                    $user_groups = array($user_groups);
                }

                if ($user_groups) {
                    $remove_groups = array_diff($user_groups, $groups);
                } else {
                    $remove_groups = array();
                }


                if (empty($user_groups)) {
                    $user_groups = array();
                }

                $merge_groups = array_merge($user_groups, $groups);

                $add_groups = array_diff($merge_groups, $remove_groups);

                update_user_meta($id, 'groups', $add_groups);

                update_user_meta($id, '_groups', 'field_56ae488c49d32');

                if (!empty($remove_groups)) {
                    foreach ($remove_groups as $group_id) {
                        $group_users = get_post_meta($group_id, 'users', true);
                        if (!empty($group_users)) {
                            $index = array_search($id, $group_users);
                            if ($index !== FALSE) {
                                unset($group_users[$index]);
                            }
                            update_post_meta($group_id, 'users', $group_users);
                            update_post_meta($group_id, '_users', 'field_56b05a6d5033f');
                        }
                    }
                }

                if (!empty($add_groups)) {
                    foreach ($add_groups as $group_id) {
                        $group_users = get_post_meta($group_id, 'users', true);
                        if (!is_array($group_users)) {
                            $group_users = array();
                        }
                        $index = array_search($id, $group_users);
                        if ($index === FALSE) {
                            $group_users[] = $id;
                        }
                        update_post_meta($group_id, 'users', $group_users);
                        update_post_meta($group_id, '_users', 'field_56b05a6d5033f');
                    }
                }

                return 1;

                break;

            case 'group':

                $post = get_post($id, ARRAY_A);

                if (is_null($post)) {
                    return 0;
                }

                if (!is_array($groups)) {
                    $groups = array();
                }

                // Get default data
                $group_posts = get_post_meta($id, 'posts', true);
                $group_users = get_post_meta($id, 'users', true);
                $group_facilitators = get_post_meta($id, 'facilitators');

                // Set default array
                if (!is_array($group_posts))
                    $group_posts = array();
                if (!is_array($group_users))
                    $group_users = array();
                if (!is_array($group_facilitators))
                    $group_facilitators = array();



                /**  Course  * */
                // REMOVE Courses
                if (array($group_posts))
                    $remove_posts = array_diff($group_posts, $groups['post']);
                else
                    $remove_posts = array();

                // ADD Courses
                $merge_posts = array_merge($group_posts, $groups['post']);
                $add_posts = array_diff($merge_posts, $remove_posts);

                if (!empty($remove_posts)) {
                    foreach ($remove_posts as $post_id) {
                        $post_groups = get_post_meta($post_id, 'groups', true);
                        if (empty($post_groups)) {
                            $post_groups = array();
                        }
                        if (!empty($post_groups)) {
                            $index = array_search($id, $post_groups);
                            if ($index !== FALSE) {
                                unset($post_groups[$index]);
                            }
                            update_post_meta($post_id, 'groups', $post_groups);
                            update_post_meta($post_id, '_groups', 'field_56ae48f22676e');
                        }
                    }
                }
                if (!empty($add_posts)) {
                    foreach ($add_posts as $post_id) {
                        $post_groups = get_post_meta($post_id, 'groups', true);
                        if (empty($post_groups) || !is_array($post_groups)) {
                            $post_groups = array();
                        }
                        $index = array_search($id, $post_groups);
                        if ($index === FALSE) {
                            $post_groups[] = $id;
                        }
                        update_post_meta($post_id, 'groups', $post_groups);
                        update_post_meta($post_id, '_groups', 'field_56ae48f22676e');
                    }
                }



                /**  Participant  * */
                // REMOVE Participants
                if (array($group_users))
                    $remove_users = array_diff($group_users, $groups['user']);
                else
                    $remove_users = array();

                // ADD Participants
                $merge_users = array_merge($group_users, $groups['user']);
                $add_users = array_diff($merge_users, $remove_users);

                if (!empty($remove_users)) {
                    foreach ($remove_users as $user_id) {
                        $user_groups = get_user_meta($user_id, 'groups', true);
                        if (empty($user_groups)) {
                            $user_groups = array();
                        }
                        if (!empty($user_groups)) {
                            $index = array_search($id, $user_groups);
                            if ($index !== FALSE) {
                                unset($user_groups[$index]);
                            }
                            update_user_meta($user_id, 'groups', $user_groups);
                            update_user_meta($user_id, '_groups', 'field_56ae488c49d32');
                        }
                    }
                }

                if (!empty($add_users)) {
                    foreach ($add_users as $user_id) {
                        $user_groups = get_user_meta($user_id, 'groups', true);
                        if (!is_array($user_groups)) {
                            $user_groups = array();
                        }
                        $index = array_search($id, $user_groups);
                        if ($index === FALSE) {
                            $user_groups[] = $id;
                        }
                        update_user_meta($user_id, 'groups', $user_groups);
                        update_user_meta($user_id, '_groups', 'field_56ae488c49d32');
                    }
                }


                /**  Facilitator  * */
                // REMOVE facilitators
                if (array($group_facilitators))
                    $remove_facilitators = array_diff($group_facilitators, $groups['facilitator']);
                else
                    $remove_facilitators = array();

                // ADD facilitators
                $merge_facilitators = array_merge($group_facilitators, $groups['facilitator']);
                $add_facilitators = array_diff($merge_facilitators, $remove_facilitators);

                if (!empty($remove_facilitators)) {
                    foreach ($remove_facilitators as $user_id) {
                        $user_groups = get_user_meta($user_id, 'groups', true);
                        if (empty($user_groups)) {
                            $user_groups = array();
                        }
                        if (array($user_groups)) {
                            $index = array_search($id, $user_groups);
                            if ($index !== FALSE) {
                                unset($user_groups[$index]);
                            }
                            update_user_meta($user_id, 'groups', $user_groups);
                            update_user_meta($user_id, '_groups', 'field_56ae488c49d32');
                        }
                    }
                }
                if (!empty($add_facilitators)) {
                    foreach ($add_facilitators as $user_id) {
                        $user_groups = get_user_meta($user_id, 'groups', true);
                        if (!is_array($user_groups)) {
                            $user_groups = array();
                        }
                        $index = array_search($id, $user_groups);
                        if ($index === FALSE) {
                            $user_groups[] = $id;
                        }
                        update_user_meta($user_id, 'groups', $user_groups);
                        update_user_meta($user_id, '_groups', 'field_56ae488c49d32');
                    }
                }
                break;
        }
    }

    /**
     * `type` should be blank to get all groups
     * `user` and `user_id` for getting groups assigned to specific user
     * `post` and `post_id` for getting groups assigned to specific post
     * `will return array of groups with group id and group name'
     * @since 1.0
     */
    public static function get_groups($type = '', $id = '', $fields = '') {

        switch ($type) {

            case 'user':

                # Validating user input parameter
                if (get_user_by('ID', $id) === false) {
                    return false;
                }

                # Getting user groups
                $user_groups = get_user_meta($id, 'groups', true);

                # validating meta vlaue
                if (!is_array($user_groups)) {
                    $user_groups = array();
                }

                # setting up group argument
                $group_args = array(
                    'post_type' => 'group',
                    'post_status' => 'publish',
                    'post__in' => $user_groups,
                    'posts_per_page' => -1
                );

                break;

            case 'post':

                # Validating post input parameter
                if (get_post_status($id) === false) {
                    return false;
                }

                # Getting post groups
                $post_groups = get_post_meta($id, 'groups', true);

                # validating meta vlaue
                if (!array($post_groups)) {
                    $post_groups = array();
                }

                # setting up group argument
                $group_args = array(
                    'post_type' => 'group',
                    'post_status' => 'publish',
                    'post__in' => $post_groups,
                    'posts_per_page' => -1
                );

                break;

            case 'franchisee':

                # Validating post input parameter
                if (get_post_status($id) === false) {
                    return false;
                }

                # Getting post groups
                $post_groups = get_post_meta($id, 'groups', true);

                # validating meta vlaue
                if (!array($post_groups)) {
                    $post_groups = array();
                }

                # setting up group argument
                $group_args = array(
                    'post_type' => 'group',
                    'post_status' => 'publish',
                    'post__in' => $post_groups,
                    'posts_per_page' => -1
                );

                break;

            default:

                # setting up group argument
                $group_args = array(
                    'post_type' => 'group',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                );

                break;
        }

        # Getting groups now
        $groups = get_posts($group_args);

        # formatting data
        $all_groups = array();
        foreach ($groups as $group) : setup_postdata($group);

            if ($fields == 'ids') {
                $all_groups[] = $group->ID;
            } else {

                $return = array(
                    "id" => $group->ID,
                    "name" => $group->post_title
                );
                $all_groups[] = $return;
            }

        endforeach;

        # Reseting post query
        wp_reset_postdata();

        if (!empty($all_groups)) {
            return $all_groups;
        } else {
            return '';
        }
    }

    /**
     * Both the parameters will be mandatory here
     * `user` and `user_id` for getting posts assigned to specific user
     * `group` and `group_id` for getting posts assigned to specific group
     * `will return array of posts with post id
     * @since 1.0
     */
    public static function get_programs($type, $id) {

        switch ($type) {

            case 'user':

                # Validating user input parameter
                if (get_user_by('ID', $id) === false) {
                    return false;
                }

                # Getting user groups
                $user_groups = get_user_meta($id, 'groups', true);

                if (is_array($user_groups) && !empty($user_groups)) {

                    $programs = array();

                    # Extracting posts from groups
                    foreach ($user_groups as $key => $group) {
                        $posts = get_post_meta($group, 'posts', true);
                        foreach ($posts as $post) {
                            if (!in_array($post, $programs)) {
                                array_push($programs, $post);
                            }
                        }
                    }
                } else {
                    $programs = array();
                }

                return $programs;

                break;

            case 'franchisee_user':

                # Validating user input parameter
                if (get_user_by('ID', $id) === false) {
                    return false;
                }

                # Getting Franchisee ID
                $franchisee = get_user_meta($id, 'franchisee', true);

                # Getting Franchisee groups
                $franchisee_groups = get_post_meta($franchisee, 'groups', true);

                if (is_array($franchisee_groups) && !empty($franchisee_groups)) {

                    $programs = array();

                    # Extracting posts from groups
                    foreach ($franchisee_groups as $key => $group) {
                        $posts = get_post_meta($group, 'posts', true);
                        if ($posts) {
                            foreach ($posts as $post) {
                                if (!in_array($post, $programs)) {
                                    array_push($programs, $post);
                                }
                            }
                        }
                    }
                } else {
                    $programs = array();
                }

                return $programs;

                break;

            case 'franchisee':

                # Validating franchisee input parameter
                if (get_post_status($id) === false) {
                    return false;
                }

                # Getting Franchisee groups
                $franchisee_groups = get_post_meta($id, 'groups', true);

                if (is_array($franchisee_groups) && !empty($franchisee_groups)) {

                    $programs = array();

                    # Extracting posts from groups
                    foreach ($franchisee_groups as $key => $group) {
                        $posts = get_post_meta($group, 'posts', true);
                        foreach ($posts as $post) {
                            if (!in_array($post, $programs)) {
                                array_push($programs, $post);
                            }
                        }
                    }
                } else {
                    $programs = array();
                }

                return $programs;

                break;

            case 'group':

                # Validating post input parameter
                if (get_post_status($id) === false) {
                    return false;
                }

                # Get progrms under this group
                $group_posts = get_post_meta($id, 'posts', true);

                # validate meta value
                if (!is_array($group_posts)) {
                    return array();
                } else {
                    return $group_posts;
                }

                break;

            default:
                return array();
                break;
        }
    }

    /**
     * `user_role` if given only specific user to that role will be returned
     * `will return array of user ids
     * @since 1.0
     */
    public static function get_users_by_group($group_id, $user_role = '') {
        $users = array();

        # validating requested group ID
        if (get_post_status($group_id) === false) {
            return false;
        }

        # Getting all users under that group
        $group_users = get_post_meta($group_id, 'users', true);

        # validating returned meta
        if (!is_array($group_users)) {
            $group_users = array();
        }

        # filtering with role given
        if (($user_role != '') && (!empty($group_users))) {

            $users = $group_users;

            return $users;
        } else {
            return $group_users;
        }
    }

    /**
     * @since 1.0
     */
    public static function action_links($group_id) {

        $edit = '<a href="' . get_permalink($group_id) . 'edit" class="btn btn-success btn-xs m-r-sm unset_workflow" data-placement="top" data-original-title="Edit" title="" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>';
        $delete = '<a href="javascript:" class="btn btn-danger btn-xs m-r-sm" data-placement="top" data-id="' . $group_id . '" data-original-title="Delete Group" title="Delete Group" data-toggle="tooltip" onclick="deleteGroup(this);"><i class="fa fa-trash-o"></i></a>';

        echo '<td>' . $edit . $delete . '</td>';
    }

    /**
     * @since 1.0
     */
    public function delete_group() {

        if (!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX) && current_user_can('delete_group')) {
            $return['status'] = 'error';
            $return['data'] = 'Something went wrong';
            echo json_encode($return);
            exit;
        }

        $data = $_POST;

        if (!empty($data['id'])) {
            $group_id = (int) $data['id'];
            $event_id = get_post_meta($group_id, 'reschedule_group_session', true);
            if ($event_id) {
                delete_post_meta($event_id, 'reschedule_session_group', $group_id);
            }
            wp_delete_post($group_id);
            $return['status'] = 'success';
            $return['data'] = 'Successfully deleted.';
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data'] = 'you can not delete this.';
            echo json_encode($return);
        }
        exit;
    }

    /**
     * @since 1.0
     */
    public static function group_users_dropdown($franchisee_id = NULL, $disabled = '', $group_id = NULL) {

        $html = '<select data-placeholder="Select Participants" name="grp_users[]" class="chosen-select" id="participants" multiple ' . $disabled . '>';

        $html .= self::get_participants($franchisee_id, $disabled, $group_id);

        $html .= '</select>';

        return $html;
    }

    public static function remaining_participant_group() {
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
            $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];
            $workflow_postmeta = get_post_meta($workflow_id);
            $total_participant = get_field('number_of_participant', $workflow_id);
            $total_group = get_field('group', $workflow_id);
            $wf_participant = $wf_group = $remaining_participant = array();

            for ($i = 1; $i <= $total_participant; $i++) {
                if (isset($workflow_postmeta['participants_id_' . $i][0]) && $workflow_postmeta['participants_id_' . $i][0] != '')
                    $wf_participant[] = $workflow_postmeta['participants_id_' . $i][0];
            }
            for ($i = 1; $i <= $total_group; $i++) {
                $group_id = $workflow_postmeta['group_id_' . $i][0];
                $group_user = get_group_users($group_id);
                if (!empty($group_user)) {
                    foreach ($group_user as $k => $v) {
                        $wf_group[] = $v;
                    }
                }
            }

            $remaining_participant = $wf_participant;
            if (!empty($wf_participant) && !empty($wf_group)) {
                $remaining_participant = array_diff($wf_participant, $wf_group);
            }
            return $remaining_participant;
        }
    }
    public static function remaining_participant_group_outside_workflow($workflow_id) {
        
            $workflow_postmeta = get_post_meta($workflow_id);
            $total_participant = get_field('number_of_participant', $workflow_id);
            $total_group = get_field('group', $workflow_id);
            $wf_participant = $wf_group = $remaining_participant = array();

            for ($i = 1; $i <= $total_participant; $i++) {
                if (isset($workflow_postmeta['participants_id_' . $i][0]) && $workflow_postmeta['participants_id_' . $i][0] != '')
                    $wf_participant[] = $workflow_postmeta['participants_id_' . $i][0];
            }
            for ($i = 1; $i <= $total_group; $i++) {
                $group_id = $workflow_postmeta['group_id_' . $i][0];
                $group_user = get_group_users($group_id);
                if (!empty($group_user)) {
                    foreach ($group_user as $k => $v) {
                        $wf_group[] = $v;
                    }
                }
            }
            
            $remaining_participant = $wf_participant;
            if (!empty($wf_participant) && !empty($wf_group)) {
                $remaining_participant = array_diff($wf_participant, $wf_group);
            }
            return $remaining_participant;
    }

    public static function added_participant_group() {
        if (isset($_SESSION['WORKFLOW']['WORKFLOW_ID']) && isset($_SESSION['WORKFLOW']['STATUS'])) {
            $workflow_id = $_SESSION['WORKFLOW']['WORKFLOW_ID'];
            $workflow_postmeta = get_post_meta($workflow_id);
            $total_group = get_field('group', $workflow_id);
            $wf_group = array();

            for ($i = 1; $i <= $total_group; $i++) {
                $group_id = $workflow_postmeta['group_id_' . $i][0];
                $group_user = get_group_users($group_id);
                if (!empty($group_user)) {
                    foreach ($group_user as $k => $v) {
                        $wf_group[] = $v;
                    }
                }
            }
            return $wf_group;
        }
    }

    public static function group_users_dropdown_for_meeting($franchisee_id = NULL, $disabled = '', $group_id = NULL) {

        $html = '<select data-placeholder="Select Participants" style="display:none;" name="grp_users[]" class="chosen-select" id="participants" multiple ' . $disabled . '>';

        $html .= self::get_participants($franchisee_id, $disabled, $group_id);

        $html .= '</select>';

        return $html;
    }

    public static function group_users_dropdown_workflow($client_id = NULL, $disabled = '', $group_id = NULL) {

        $html = '<select data-placeholder="Select Participants" name="grp_users[]" class="chosen-select" id="participants" multiple ' . $disabled . '>';

        $html .= self::get_participants_workflow($client_id, $disabled, $group_id);

        $html .= '</select>';

        return $html;
    }

    public static function get_participants_workflow($client_id = 0, $disabled = '', $group_id = 0) {

        if ($client_id > 0 && ($group_id == 0)) {
            $data['id'] = $client_id;
        } else {
            $data = $_POST;
        }

        $my_particpants = array();

        if (isset($_POST['nuclo_create_group']) && !empty($_POST['grp_users'])) {
            $my_particpants = $_POST['grp_users'];
        } elseif (is_singular('group')) {
            $group_id = get_the_ID();
            $my_particpants = get_post_meta($group_id, 'users', true);
        } elseif (defined('DOING_AJAX') && DOING_AJAX) {
            $my_particpants = array();
        }

        $return = '';

        if (isset($data['id'])) {

            //$particpants = get_client_users($data['id']);   
            $particpants = self::remaining_participant_group();

            if (!empty($particpants)) {
                
                foreach ($particpants as $particpant) {
                    $particpant_details = get_userdata($particpant);
                    $selected = in_array($particpant_details->ID, $my_particpants) ? 'selected' : '';
                    $groups = is_array($particpant_details->groups) ? $particpant_details->groups : array();

                    if (!empty($groups)) {
                        foreach ($groups as $key => $group) {
                            if (!get_post($group))
                                unset($particpant_details->groups[$key]);
                        }
                    }

                    $disabled = !empty($groups) ? 'disabled' : '';

                    if (in_array($group_id, $groups))
                        $disabled = '';

                    $return .= '<option value="' . $particpant_details->ID . '" ' . $selected . ' ' . $disabled . '>' . $particpant_details->first_name . ' ' . $particpant_details->last_name . '</option>';
                }
            }
        }else if ((isset($data['group_id']) && !empty($data['group_id'])) || (($group_id > 0))) {
            //$selected   = 'selected';
            if ((isset($data['group_id']) && !empty($data['group_id']))) {
                $group_id = $data['group_id'];
            }
            $added_particpant = get_group_users($group_id);
            $remaing_particpants = self::remaining_participant_group();
            $particpants = array_merge($added_particpant, $remaing_particpants);
            $particpants = array_unique($particpants);
            
            foreach ($particpants as $particpant) {
                $participant_details = get_userdata($particpant);
                $selected = in_array($particpant, $added_particpant) ? 'selected' : '';
                $return .= '<option value="' . $participant_details->ID . '" ' . $selected . ' ' . $disabled . '>' . $participant_details->first_name . ' ' . $participant_details->last_name . '</option>';
            }
        } else {
            $particpants = get_users();
            
            foreach ($particpants as $particpant) {
                $return .= '<option value="' . $particpant->ID . '" ' . $disabled . '>' . $particpant->first_name . ' ' . $particpant->last_name . '</option>';
            }
        }

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $return;
            exit;
        } else {
            return $return;
        }
    }
    
    public static function group_users_dropdown_outsideworkflow($client_id = NULL,$workflow_id=0, $disabled = '', $group_id = NULL) {

        $html = '<select data-placeholder="Select Participants" name="grp_users[]" class="chosen-select" id="participants" multiple ' . $disabled . '>';

        $html .= self::get_participants_outsideworkflow($client_id,$workflow_id, $disabled, $group_id);

        $html .= '</select>';

        return $html;
    }

    public static function get_participants_outsideworkflow($client_id = 0,$workflow_id=0, $disabled = '', $group_id = 0) {

        if ($client_id > 0 && ($group_id == 0)) {
            $data['id'] = $client_id;
        } 
        elseif($client_id > 0 && $group_id>0)
        {
            
        }
        else {
            $data = $_POST;
            $client_id = $data['id'];
        }
        
        if($workflow_id == 0)
        {   
            $workflow_id = Workflow::get_workflow_id_by_client($client_id);
        }
        
        $my_particpants = array();

        if (isset($_POST['nuclo_create_group']) && !empty($_POST['grp_users'])) {
            $my_particpants = $_POST['grp_users'];
        } elseif (is_singular('group')) {
            $group_id = get_the_ID();
            $my_particpants = get_post_meta($group_id, 'users', true);
        } elseif (defined('DOING_AJAX') && DOING_AJAX) {
            $my_particpants = array();
        }
        $return = '';

        if (isset($data['id'])) {

            //$particpants = get_client_users($data['id']);   
            $particpants = self::remaining_participant_group_outside_workflow($workflow_id);

            if (!empty($particpants)) {
                
                foreach ($particpants as $particpant) {
                    $particpant_details = get_userdata($particpant);
                    $selected = in_array($particpant_details->ID, $my_particpants) ? 'selected' : '';
                    $groups = is_array($particpant_details->groups) ? $particpant_details->groups : array();

                    if (!empty($groups)) {
                        foreach ($groups as $key => $group) {
                            if (!get_post($group))
                                unset($particpant_details->groups[$key]);
                        }
                    }

                    $disabled = !empty($groups) ? 'disabled' : '';

                    if (in_array($group_id, $groups))
                        $disabled = '';

                    $return .= '<option value="' . $particpant_details->ID . '" ' . $selected . ' ' . $disabled . '>' . $particpant_details->first_name . ' ' . $particpant_details->last_name . '</option>';
                }
            }
        }else if ((isset($data['group_id']) && !empty($data['group_id'])) || (($group_id > 0))) {
            //$selected   = 'selected';
            if ((isset($data['group_id']) && !empty($data['group_id']))) {
                $group_id = $data['group_id'];
            }
            $added_particpant = get_group_users($group_id);
            $remaing_particpants = self::remaining_participant_group_outside_workflow($workflow_id);
            $particpants = array_merge($added_particpant, $remaing_particpants);
            $particpants = array_unique($particpants);
            
            foreach ($particpants as $particpant) {
                $participant_details = get_userdata($particpant);
                $selected = in_array($particpant, $added_particpant) ? 'selected' : '';
                $return .= '<option value="' . $participant_details->ID . '" ' . $selected . ' ' . $disabled . '>' . $participant_details->first_name . ' ' . $participant_details->last_name . '</option>';
            }
        } else {
            $particpants = get_users();
            
            foreach ($particpants as $particpant) {
                $return .= '<option value="' . $particpant->ID . '" ' . $disabled . '>' . $particpant->first_name . ' ' . $particpant->last_name . '</option>';
            }
        }

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $return;
            exit;
        } else {
            return $return;
        }
    }

    public static function group_facilitators_dropdown_session($my_facilitators = array()) {

        $multiple = 'multiple';

        $html = '<select data-placeholder="Select Facilitators" name="grp_facilitators[]" class="chosen-select" id="facilitators" ' . $multiple . ' ' . $disabled . '>';

        $html .= self::get_facilitators_session($my_facilitators);

        $html .= '</select>';

        return $html;
    }

    public static function get_facilitators_session($my_facilitators) {

        $users = (!empty($my_facilitators)) ? $my_facilitators : array();

        $return = '';

        if (!empty($users)) {
            foreach ($users as $user) {
                $user_details = get_userdata($user);
                $return .= '<option value="' . $user_details->ID . '" selected >' . $user_details->first_name . ' ' . $user_details->last_name . '</option>';
            }
        }

        return $return;
    }

    /**
     * @since 1.0
     */
    public static function group_facilitators_dropdown($franchisee_id = NULL, $disabled = '', $multiple = true, $my_facilitators = array()) {
       
        $multiple = ($multiple) ? 'multiple' : '';

        $html = '<select data-placeholder="Select Facilitators" name="grp_facilitators[]" class="chosen-select" id="facilitators" ' . $multiple . ' ' . $disabled . '>';

        $html .= self::get_facilitators($franchisee_id, $my_facilitators);

        $html .= '</select>';

        return $html;
    }

    /**
     * @since 1.0
     */
    public static function group_franchisee_dropdown($group_id = NULL, $franchisee_id = NULL, $function = '') {

        $post_args = array(
            'post_type' => 'franchisee',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        # Getting all franchisee
        $franchisee = get_posts($post_args);

        # formatting data
        $f_array = array();
        foreach ($franchisee as $f) : setup_postdata($f);

            $return = array(
                "id" => $f->ID,
                "name" => $f->post_title
            );
            $f_array[] = $return;

        endforeach;

        # Reseting post query
        wp_reset_postdata();

        $group_posts = array();

        if (!empty($group_id)) {
            $group_posts = get_field('franchisee', $group_id);
        }
        
        $function = (!empty($function)) ? $function : 'get_participants(this);get_clients(this)';
        $html = '';
        $html.= '<select name="grp_franchisee" class="chosen-select" onchange="' . $function . '" id="select_franchisee"><option value="0" >-- Select Franchisee --</option>';
        foreach ($f_array as $program) {
            $selected = '';
            if (!empty($group_posts)) {
                $selected = ($program['id'] == $group_posts->ID) ? 'selected' : '';
            } else if (!empty($franchisee_id)) {
                $selected = ($program['id'] == $franchisee_id) ? 'selected' : '';
            }

            $html.= '<option value="' . $program['id'] . '" ' . $selected . '>' . $program['name'] . '</option>';
        }
        $html.= '</select>';

        return $html;
    }

    /**
     * @since 1.0
     */
    public static function group_posts_dropdown($group_id = NULL) {

        $post_args = array(
            'post_type' => 'program',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        # Getting all programs
        $programs = get_posts($post_args);

        # formatting data
        $program_array = array();
        foreach ($programs as $program) : setup_postdata($program);

            $return = array(
                "id" => $program->ID,
                "name" => $program->post_title
            );
            $program_array[] = $return;

        endforeach;

        # Reseting post query
        wp_reset_postdata();

        if (!empty($group_id)) {
            $group_posts = get_post_meta($group_id, 'posts', true);
        } else {
            $group_posts = array();
        }

        $html = '';
        $html.= '<select data-placeholder="Select Course" name="grp_posts[]" class="chosen-select" multiple>';
        foreach ($program_array as $key => $program) {
            $selected = in_array($program['id'], $group_posts) ? 'selected' : '';
            $html.= '<option value="' . $program['id'] . '" ' . $selected . '>' . $program['name'] . '</option>';
        }
        $html.= '</select>';

        return $html;
    }

    /**
     * @since 1.0
     */
    public static function post_franchises_dropdown($post_id = NULL, $function = false) {

        $args = array(
            'post_type' => 'franchisee',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        # Getting all programs
        $franchises = get_posts($args);

        $course_franchises = get_post_meta($post_id, 'franchises', true);
        $function_str = '';
        if ($function) {
            $function_str = 'onchange="get_franchisee_group(this)"';
        }

        $html = '';
        $html .= '<select data-placeholder="Select Franchisee" name="nc_franchises[]" ' . $function_str . ' class="chosen-select" multiple data-id="' . $post_id . '">';
        foreach ($franchises as $franchisee) {
            $selected = (!empty($course_franchises) && in_array($franchisee->ID, $course_franchises)) ? 'selected' : '';
            $html.= '<option value="' . $franchisee->ID . '" ' . $selected . '>' . $franchisee->post_title . '</option>';
        }

        $html.= '</select>';

        return $html;
    }

    /**
     * @since 1.0
     */
    public static function post_groups_dropdown($post_id = NULL, $disabled = ' ', $group_id = NULL, $ajax = false, $multiple = true) {

        $grp_args = array(
            'post_type' => 'group',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        # Getting all programs
        $groups = get_posts($grp_args);

        # formatting data
        $group_array = array();
        foreach ($groups as $group) : setup_postdata($group);

            $return = array(
                "id" => $group->ID,
                "name" => $group->post_title
            );
            $group_array[] = $return;

        endforeach;

        # Reseting post query
        wp_reset_postdata();

        if (!empty($post_id)) {
            $post_groups = get_post_meta($post_id, 'groups', true);
        } else if (!is_null($group_id)) {
            $post_groups = array($group_id);
        } else {
            $post_groups = array();
        }

        $function = ($ajax) ? 'onchange="get_participants(this)"' : '';
        $multiple = ($multiple) ? 'multiple' : '';
        $html = '';
        $html.= '<select data-placeholder="Select Group" id="select_group" name="nc_groups[]" ' . $function . ' class="chosen-select" ' . $multiple . ' ' . $disabled . '>';
        $html.= '<option value="">Select Group</option>';
        foreach ($group_array as $key => $group) {
            $selected = (!empty($post_groups) && in_array($group['id'], $post_groups)) ? 'selected' : '';
            $html.= '<option value="' . $group['id'] . '" ' . $selected . '>' . $group['name'] . '</option>';
        }
        $html.= '</select>';

        return $html;
    }

    /**
     * @since 1.0
     */
    public static function user_groups_dropdown($user_id = NULL) {

        $grp_args = array(
            'post_type' => 'group',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

        # Getting all programs
        $groups = get_posts($grp_args);

        # Formatting data
        $group_array = array();
        foreach ($groups as $group) : setup_postdata($group);

            $return = array(
                "id" => $group->ID,
                "name" => $group->post_title
            );
            $group_array[] = $return;

        endforeach;

        # Reseting post query
        wp_reset_postdata();

        if (!empty($user_id)) {
            $user_groups = get_user_meta($user_id, 'groups', true);
        } else {
            $user_groups = array();
        }

        $html = '';
        $html.= '<select data-placeholder="Select Group" id="usr_group" name="usr_group" class="chosen-select">';
        $html.= '<option value="">Select Group</option>';
        foreach ($group_array as $key => $group) {
            $selected = in_array($group['id'], $user_groups) ? 'selected' : '';
            $html.= '<option value="' . $group['id'] . '" ' . $selected . '>' . $group['name'] . '</option>';
        }
        $html.= '</select>';

        return $html;
    }

    /**
     * @since 1.0
     */
    public function filter_query($query) {
       
        if (isset($query->query_vars['post_type'])) {
            
            if ($query->query_vars['post_type'] == 'group' && !is_admin()) {
                $query->query_vars['posts_per_page'] = $this->per_page;
                $query->query_vars['paged'] = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;
                if (current_user_can('editor')) {
//                    $current_user = get_current_user_id();                
//                    $query->set('author', $current_user);
                    $query->set('meta_key', 'franchisee');
                    $query->set('meta_value', get_current_user_franchisee_id());
                }
                if (get_current_user_role() == 'teacher') {
                    
                    $query->set('meta_key', 'client');
                    $query->set('meta_value', get_client_id());
                }
                if (get_current_user_role()=='author') {
                    $franchisee_id = get_current_user_franchisee_id();
                    $id_array = array();
                    global $wpdb;
                    $results = $wpdb->get_results("SELECT wp.ID FROM $wpdb->posts AS wp INNER JOIN $wpdb->postmeta AS wpm ON wp.ID = wpm.post_id  WHERE wpm.meta_key = 'franchisee' AND wpm.meta_value = $franchisee_id AND wp.post_type='group'");
                    
                    global $current_user;
                    if ($results) {
                        foreach ($results as $k => $v) {
                            $g_id = $v->ID;
                            $facilitator = get_post_meta($g_id,'facilitators');
                            if(in_array($current_user->ID, $facilitator))
                            {
                                $id_array[] = $g_id;
                            }
                        }
                    }
                    if (empty($id_array)) {
                        $id_array = array('0');
                    }
                    $query->set('post__in', $id_array);
                }

                if (isset($_GET) && !empty($_GET['search'])) {
                    $search_text = sanitize_text_field($_GET['search']);
                    $query->set('s', $search_text);
                }
            }
        }
    }

    /**
     * @since 1.0
     */
    public static function get_monthly_user($year = 2) {

        $return = array();

        $start = date("Y") . '0101';

        $end = date("Y", strtotime('+' . $year . ' year')) . '0101';

        $grp_args = array(
            'post_type' => 'group',
            'post_status' => 'publish',
            'posts_per_page' => -1
        );

        if (current_user_can('editor')) {
            $grp_args['franchisee'] = get_current_user_franchisee_id();
        }

        $groups = get_posts($grp_args);

        if ($groups) {
            foreach ($groups as $group) {
                $users = get_field('users', $group->ID);

                $start_date = get_field('group_start_date', $group->ID);

                if (!empty($start_date))
                    $new_start_date = str_replace('/', '-', $start_date);

                $end_date = get_field('group_end_date', $group->ID);
                if (!empty($end_date))
                    $new_end_date = str_replace('/', '-', $end_date);

                if (!empty($new_start_date) && !empty($new_end_date)) {

                    $start = new DateTime($new_start_date);

                    $start->modify('first day of this month');

                    $end = new DateTime($new_end_date);

                    $end->modify('first day of next month');

                    $interval = DateInterval::createFromDateString('1 month');

                    $period = new DatePeriod($start, $interval, $end);
                    foreach ($users as $user) {
                        $client = get_user_meta($user['ID'], 'client', true);
                        if ($period) {
                            foreach ($period as $dt) {
                                if ($client) {
                                    if (!isset($return[$group->ID][$dt->format("Y")][$dt->format("F")])) {
                                        $return[$group->ID][$dt->format("Y")][$dt->format("F")] = 1;
                                    } else {
                                        $return[$group->ID][$dt->format("Y")][$dt->format("F")]++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $return;
    }

    public static function get_participants($franchises_id = 0, $disabled = '', $group_id = 0) {

        if ($franchises_id > 0 && ($group_id == 0)) {
            $data['id'] = $franchises_id;
        } else {
            $data = $_POST;
        }

        $my_particpants = array();

        if (isset($_POST['nuclo_create_group']) && !empty($_POST['grp_users'])) {
            $my_particpants = $_POST['grp_users'];
        } elseif (is_singular('group')) {
            $group_id = get_the_ID();
            $my_particpants = get_post_meta($group_id, 'users', true);
            if ((get_post_meta(get_the_ID(), 'reschedule_group', true))) {
                $event_id = get_post_meta(get_the_ID(), 'reschedule_group_session', true);
                $event_groups = get_post_meta($event_id, 'groups');
                $grp_users = array();
                if (is_array($event_groups)) {
                    foreach ($event_groups as $group) {
                        $grp_user = Group::get_users_by_group($group, 'contributor');
                        $grp_users = array_merge($grp_users, $grp_user);
                    }
                } else {
                    $grp_users = Group::get_users_by_group($event_groups, 'contributor');
                }

                $grp_users = array_unique($grp_users);
            }
        } elseif(get_post_field('post_type',get_the_ID()) == 'email'){        
            $groups             = get_post_meta(get_the_ID(),'sent_to_groups',true);
            $data['group_id']   = $groups[0]; 
            $grp_users          = get_post_meta(get_the_ID(),'sent_to_users',true);
        }elseif (defined('DOING_AJAX') && DOING_AJAX) {
            $my_particpants = array();
        }

        $return = '';

        if (isset($data['id'])) {

            $particpants = get_franchise_users($data['id']);

            if (!empty($particpants)) {

                foreach ($particpants as $particpant) {
                    $particpant_details = get_userdata($particpant);
                    $selected = in_array($particpant_details->ID, $my_particpants) ? 'selected' : '';
                    $groups = is_array($particpant_details->groups) ? $particpant_details->groups : array();

                    if (!empty($groups)) {
                        foreach ($groups as $key => $group) {
                            if (!get_post($group))
                                unset($particpant_details->groups[$key]);
                        }
                    }
                    $disabled = '';

                    if (!(get_post_meta(get_the_ID(), 'reschedule_group', true))) {
                        $disabled = (!empty($groups)) ? 'disabled' : '';
                    } else if ((get_post_meta(get_the_ID(), 'reschedule_group', true)) && is_singular('group')) {
                        if (in_array($particpant, $grp_users) && (get_user_meta($particpant, 'module_result_' . $event_id, true))) {
                            $disabled = 'disabled';
                        }
                    }
                    if (in_array($group_id, $groups))
                        $disabled = '';
                    $return .= '<option value="' . $particpant_details->ID . '" ' . $selected . ' ' . $disabled . '>' . $particpant_details->first_name . ' ' . $particpant_details->last_name . '</option>';
                }
            }
        }else if ((isset($data['group_id']) && !empty($data['group_id'])) || (($group_id > 0))) {
            $selected = 'selected';
            if ((isset($data['group_id']) && !empty($data['group_id']))) {
                $group_id = $data['group_id'];
            }
            $particpants = get_group_users($group_id);
            foreach ($particpants as $particpant) {
                if(isset($grp_users) && !in_array($particpant,$grp_users)){
                    $selected   = '';
                }
                $participant_details = get_userdata($particpant);
                $return .= '<option value="' . $participant_details->ID . '" ' . $selected . ' ' . $disabled . '>' . $participant_details->first_name . ' ' . $participant_details->last_name . '</option>';
            }
        } else {
            $particpants = get_users();
            
            foreach ($particpants as $particpant) {
                $return .= '<option value="' . $particpant->ID . '" ' . $disabled . '>' . $particpant->first_name . ' ' . $particpant->last_name . '</option>';
            }
        }

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $return;
            exit;
        } else {
            return $return;
        }
    }

    public static function get_clients() {

        $data = $_POST;
        
        $franchise_id = $data['id'];
        $users = array();

        $args = array('post_type' => 'client', 'post_status' => 'publish', 'posts_per_page' => -1);

        if (current_user_can('super-admin') || current_user_can('administrator')) {
            $args['meta_key'] = 'franchisee';
            $args['meta_value'] = $franchise_id;
        } else if ($franchise_id > 0) {
            $args['meta_key'] = 'franchisee';
            $args['meta_value'] = $franchise_id;
        }

        $return = '';
        $return .= '<option value="">Select Client</option>';
        $clients_query = new WP_Query($args);

        if ($clients_query->have_posts()) {
            while ($clients_query->have_posts()) {
                $clients_query->the_post();
                $value = [];
                $c_id = get_the_ID();
                $c_title = get_the_title();
                
                $return.='<option value="' . $c_id . '" >' . $c_title . '</option>';
            }
        }
        wp_reset_postdata();

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $return;
            exit;
        } else {
            return $return;
        }
    }

    public static function get_facilitators($franchises_id = 0, $my_facilitators) {

        if ($franchises_id > 0) {
            $data['id'] = $franchises_id;
        } else {
            $data = $_POST;
        }

        if (isset($_POST['nuclo_create_group']) && !empty($_POST['grp_facilitators'])) {
            $my_facilitators = $_POST['grp_facilitators'];
        } elseif (is_singular('group')) {
            $group_id = get_the_ID();
            $my_facilitators = get_post_meta($group_id, 'facilitators');
        } elseif (defined('DOING_AJAX') && DOING_AJAX) {
            $my_facilitators = 0;
        } elseif (is_singular('event')) {
            $event_id = get_the_ID();
            $my_facilitators = get_post_meta($event_id, 'facilitator');                        
        }

        $my_facilitators = (!empty($my_facilitators)) ? $my_facilitators : array();

        $return = '';
        
        if (isset($data['id'])) {
            $users = get_franchise_facilitators($data['id']);
            
            if (!empty($users)) {
                foreach ($users as $user) {
                    $user_details = get_userdata($user);
                    $selected = in_array($user_details->ID, $my_facilitators) ? 'selected' : '';
                    $return .= '<option value="' . $user_details->ID . '" ' . $selected . '>' . $user_details->first_name . ' ' . $user_details->last_name . '</option>';
                }
            }
        }

        if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $return;
            exit;
        } else {
            return $return;
        }
    }
    
    public static function get_all_courses_dropdown($type = '',$id = '' , $disabled = '',$multiple = true,$name = '',$course_ids = array()){
        if(defined('DOING_AJAX') && DOING_AJAX && isset($_POST) && !empty($_POST)){
            $type   = $_POST['type'];
            $id     = $_POST['id'];
        }
        
        $name   = ($name)?$name:'grp_posts[]';
        
        $course_list    = Programs::get_all_courses($type,$id);
        $multiple       = ($multiple)?'multiple':'';
        
        $html = '';
        $html.= '<select id="post_courses" data-placeholder="Select Program" name="'.$name.'" class="chosen-select" '.$multiple.' '.$disabled.'>';
        foreach ($course_list as $key => $program) { 
            if(is_array($course_ids)){
                $selected   = (in_array($key,$course_ids))?'selected':'';
            }else{
                $selected   = ($key == $course_ids)?'selected':'';
            }
            
            $html.= '<option value="'.$key.'" '.$selected.'>'.$program.'</option>';
        }
        $html.= '</select>';
        
        if(defined('DOING_AJAX') && DOING_AJAX){
            echo $html;
            exit();
        }else{
            return $html;
        }
        
    }
    
    public function get_groups_by_client($client_id = NULL) {
    
    $data = $_POST;
    if($client_id == NULL)    
        $client_id = $data['id'];
      
    $all_groups = array();

    $group_args = array(
        'post_type' => 'group',
        'posts_per_page' => -1,
        'post_status' => 'publish',
    );

    if (!is_null($client_id) && !is_array($client_id)) {
        $group_args['meta_key'] = 'client';
        $group_args['meta_value'] = $client_id;
    } 

    $groups = get_posts($group_args, ARRAY_A);
    $return = '';
    $return = '<option value="">Select Group</option>';
    foreach ($groups as $group) {
        $all_groups[] = array(
            'id' => $group->ID,
            'name' => $group->post_title
        );
        $return .='<option value="'.$group->ID.'">'.$group->post_title.'</option>';
    }

    if (defined('DOING_AJAX') && DOING_AJAX) {
            echo $return;
            exit;
        } else {
            return $all_groups;
        }
}

}

new Group();

function get_group_users($group_id) {
    $participants = get_post_meta($group_id, 'users', true);
    return $participants;
}

function get_group_name($group_id) {
    $group = get_post($group_id);
    return $group->post_title;
}

function get_groups_by_client($client_id = NULL){
  
    $all_groups = array();

    $group_args = array(
        'post_type' => 'group',
        'posts_per_page' => -1,
        'post_status' => 'publish',
    );
    
    if (!is_null($client_id) && !is_array($client_id)) {
        $group_args['meta_query'][] = array(
            'key' => 'client',
            'value' => $client_id
        );
    } else if (!is_null($client_id) && is_array($client_id)) {
        $group_args['meta_query'] = array(
            'relation' => 'OR'
        );
        foreach ($client_id as $id) {
            $group_args['meta_query'][] = array(
                'key' => 'client',
                'value' => $id
            );
        }
    }

    $groups = get_posts($group_args, ARRAY_A);
    
    foreach ($groups as $group) {
        $all_groups[] = array(
            'id' => $group->ID,
            'name' => $group->post_title
        );
    }

    return $all_groups;
    
}

function get_groups_by_franchise($franchise_id = NULL) {
    $all_groups = array();

    $group_args = array(
        'post_type' => 'group',
        'posts_per_page' => -1,
        'post_status' => 'publish',
    );

    if (!is_null($franchise_id) && !is_array($franchise_id)) {
        $group_args['meta_key'] = 'franchisee';
        $group_args['meta_value'] = $franchise_id;
    } else if (!is_null($franchise_id) && is_array($franchise_id)) {
        $group_args['meta_query'] = array(
            'relation' => 'OR'
        );
        foreach ($franchise_id as $id) {
            $group_args['meta_query'][] = array(
                'key' => 'franchisee',
                'value' => $id
            );
        }
    }

    $groups = get_posts($group_args, ARRAY_A);

    foreach ($groups as $group) {
        $all_groups[] = array(
            'id' => $group->ID,
            'name' => $group->post_title
        );
    }

    return $all_groups;
}

function get_groups_by_edit_session($groups) {
    $html = '';
    $html.= '<select class="chosen-select" disabled multiple >';
    $html.= '<option value="">Select Group</option>';
    foreach ($groups as $group) {
        $group_name = get_the_title($group);
        $html.= '<option value="' . $group . '" selected>' . $group_name . '</option>';
    }
    $html.= '</select>';
    $html.= '<select data-placeholder="Select Group" id="select_group" style="display:none;" name="nc_group[]" class="chosen-select" multiple >';
    $html.= '<option value="">Select Group</option>';
    foreach ($groups as $group) {
        $group_name = get_the_title($group);
        $html.= '<option value="' . $group . '" selected>' . $group_name . '</option>';
    }
    $html.= '</select>';
    return $html;
}

function get_groups_by_franchise_dropdown($franchisee_id = NULL, $disabled = '', $multiple = TRUE, $function = '', $name = '', $post_groups = array()) {

    $data_id = get_the_ID();

    if (defined('DOING_AJAX') && DOING_AJAX && isset($_POST) && !empty($_POST)) {
        $franchisee_id = explode(',', $_POST['franchisee_id']);
        $data_id = $_POST['post_id'];
    } else if (is_singular('event')) {
        $franchisee_id = $franchisee_id;
    } else {
        $franchisee_id = get_post_meta($data_id, 'franchises', true);
    }
    $groups = get_groups_by_franchise($franchisee_id);

    if (!empty($post_groups)) {
        $post_groups = $post_groups;
    } else {
        $post_groups = get_post_meta($data_id, 'groups');
    }

    $multiple = ($multiple) ? 'multiple' : '';
    $name = (!empty($name)) ? $name : 'nc_groups[]';

    $html = '';
    $html.= '<select data-placeholder="Select Group" id="select_group" name="' . $name . '" class="chosen-select" ' . $multiple . ' ' . $disabled . ' onchange="' . $function . '">';
    foreach ($groups as $group) {
        if (is_array($post_groups)) {
            $selected = (!empty($post_groups) && in_array($group['id'], $post_groups)) ? 'selected' : '';
        } else {
            $selected = (!empty($post_groups) && ($group['id'] == $post_groups)) ? 'selected' : '';
        }

        $html.= '<option value="' . $group['id'] . '" ' . $selected . '>' . $group['name'] . '</option>';
    }
    $html.= '</select>';

    if (defined('DOING_AJAX') && DOING_AJAX) {
        echo $html;
        exit();
    } else {
        return $html;
    }
}

function generateAlphabet($na) {
    $sa = "";
    while ($na >= 0) {
        $sa = chr($na % 26 + 65) . $sa;
        $na = floor($na / 26) - 1;
    }
    return $sa;
}

function select_all_participant($id)
{
    return '<button type="button" class="add-all-btn btn" onclick="select_all_particpant(\''.$id.'\');" >Select All</button>';
}
