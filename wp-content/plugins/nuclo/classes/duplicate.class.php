<?php

/**
 * @package  Duplicate
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Duplicate {

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action('wp_ajax_duplicate_post', array($this, 'duplicate_post'));
    }
    
    /**
     * @since  1.0
     */
    public function duplicate_post() {
        
        $data = $_REQUEST;
        
        $return = array();
        
        if(defined('DOING_AJAX') && DOING_AJAX) { 
            
            $post_id = trim($data['id']);
            
            $post = get_post($post_id);
            
            if($post) {
                
                $post_id = $this->process($post,$post->post_parent);
                
                if(!empty($post_id) || !is_wp_error($post_id)) {
                    $return['success'] = 1;
                    $return['msg'] = 'Duplicated post';
                } else {
                    $return['error'] = 1;
                    $return['msg'] = 'Something went wrong!';
                }
                
            } else {
                $return['error'] = 1;
                $return['msg'] = 'Invalid post';
            }            
            
            echo json_encode($return);
            
            exit;
            
        }
        
    }
    
    /**
     * @since  1.0
     */
    private function process($post,$parent_id) {
        
        switch ($post->post_type) {
            case 'program':
                $post_id = $this->duplicate($post,'',$parent_id);
                
                $args = array('post_parent'=>$post->ID,'post_type'=>array('bundle','extra','measure','discussion','survey','assessment'));
                
                $the_query = new WP_Query( $args );

                if ( $the_query->have_posts() ) {
                    $child_posts = $the_query->get_posts();
                    foreach($child_posts as $child_post) {
                        $child_id = $this->duplicate($child_post,'',$post_id);
                        if($child_post->post_type == 'bundle') {
                            $this->process($child_post, $child_id);
                        }
                    }
                }
                wp_reset_postdata();
                return $post_id;
                break;
            case 'bundle':
                $post_id = $this->duplicate($post,'',$parent_id);
                
                $args = array('post_parent'=>$post->ID,'post_type'=>array('extra','measure','discussion','survey','assessment'));
                
                $the_query = new WP_Query( $args );

                if ( $the_query->have_posts() ) {
                    $child_posts = $the_query->get_posts();
                    foreach($child_posts as $child_post) {
                        $child_id = $this->duplicate($child_post,'',$post_id);
                    }
                }
                wp_reset_postdata();
                return $post_id;
                break;
            case 'extra':
            case 'event':
            case 'measure':
            case 'discussion':
                $post_id = $this->duplicate($post,'',$parent_id);
                if(!empty($post_id) || !is_wp_error($post_id)) {                    
                    return $post_id;
                }
                break;
            case 'survey':
            case 'assessment':
                $post_id = $this->duplicate($post,'',$parent_id);
                if(!empty($post_id) || !is_wp_error($post_id)) {
                    $survey_id = get_post_meta($post->ID,'survey_id',true);
                    if(!empty($survey_id)) {
                        $new_survey_id = $this->quiz($survey_id);
                        update_post_meta($post_id, 'survey_id', $new_survey_id);
                        return $post_id;
                    }
                }
                break;
        }
        
    }
    
    /**
     * @since  1.0
     */
    public function duplicate($post, $status = '', $parent_id = '') {

        // We don't want to clone revisions
        if ($post->post_type == 'revision')
            return;

        if ($post->post_type != 'attachment') {
            $prefix = get_field('duplicate_post_title_prefix','option');
            $suffix = get_field('duplicate_post_title_suffix','option');
            if (!empty($prefix))
                $prefix.= " ";
            if (!empty($suffix))
                $suffix = " " . $suffix;
            if (get_field('duplicate_post_copystatus','option') == 0)
                $status = 'draft';
        }
        
        global $current_user;

        $new_post = array(
            'menu_order' => $post->menu_order,
            'comment_status' => $post->comment_status,
            'ping_status' => $post->ping_status,
            'post_author' => $current_user->ID,
            'post_content' => $post->post_content,
            'post_excerpt' => $post->post_excerpt,
            'post_mime_type' => $post->post_mime_type,
            'post_parent' => $new_post_parent = empty($parent_id) ? $post->post_parent : $parent_id,
            'post_password' => $post->post_password,
            'post_status' => $new_post_status = (empty($status)) ? $post->post_status : $status,
            'post_title' => $prefix . $post->post_title . $suffix,
            'post_type' => $post->post_type,
        );

        /*if (get_option('duplicate_post_copydate') == 1) {
            $new_post['post_date'] = $new_post_date = $post->post_date;
            $new_post['post_date_gmt'] = get_gmt_from_date($new_post_date);
        }*/

        $new_post_id = wp_insert_post($new_post);

        // If the copy is published or scheduled, we have to set a proper slug.
        if ($new_post_status == 'publish' || $new_post_status == 'future') {
            $post_name = wp_unique_post_slug($post->post_name, $new_post_id, $new_post_status, $post->post_type, $new_post_parent);

            $new_post = array();
            $new_post['ID'] = $new_post_id;
            $new_post['post_name'] = $post_name;

            // Update the post into the database
            wp_update_post($new_post);
        }

        // If you have written a plugin which uses non-WP database tables to save
        // information about a post you can hook this action to dupe that data.
        if ($post->post_type == 'page' || (function_exists('is_post_type_hierarchical') && is_post_type_hierarchical($post->post_type)))
            $this->meta_data($new_post_id, $post);
        else
            $this->meta_data($new_post_id, $post);

        delete_post_meta($new_post_id, '_dp_original');

        add_post_meta($new_post_id, '_dp_original', $post->ID);

        return $new_post_id;
    }

    /**
     * @since  1.0
     */
    private function meta_data($new_id, $post) {
        $post_meta_keys = get_post_custom_keys($post->ID);
        if (empty($post_meta_keys))
            return;
        $meta_blacklist = explode(",", get_field('duplicate_post_blacklist','option'));
        if ($meta_blacklist == "")
            $meta_blacklist = array();
        $meta_keys = array_diff($post_meta_keys, $meta_blacklist);

        foreach ($meta_keys as $meta_key) {
            $meta_values = get_post_custom_values($meta_key, $post->ID);
            foreach ($meta_values as $meta_value) {
                $meta_value = maybe_unserialize($meta_value);
                add_post_meta($new_id, $meta_key, $meta_value);
            }
        }
    }
    
    /**
     * @since  1.0
     */
    private function quiz($id) {
        
        global $wpdb;

        // Duplicate quiz
        $rowToDuplicate = $wpdb->get_row("SELECT `id`, `name`, `settings`, `type`, `user_id` FROM `" . WPSQT_TABLE_QUIZ_SURVEYS . "` WHERE `id` = '" . $id . "'", ARRAY_A);

        $insert = $wpdb->insert(
                WPSQT_TABLE_QUIZ_SURVEYS, array(
            'name' => $rowToDuplicate['name'],
            'settings' => $rowToDuplicate['settings'],
            'type' => $rowToDuplicate['type'],
            'timestamp' => date("Y-m-d H:i:s"),
            'user_id' => $rowToDuplicate['user_id']
                )
        );

        $newSectionIds = array();

        if ($insert == 1) {
            $itemId = $wpdb->insert_id;

            // Duplicate sections
            $sectionInfos = $wpdb->get_results("SELECT `id`, `name`, `limit`, `order`, `difficulty`, `desc` FROM `" . WPSQT_TABLE_SECTIONS . "` WHERE `item_id` = '" . $rowToDuplicate['id'] . "'", ARRAY_A);

            if (isset($sectionInfos) && !empty($sectionInfos) && $sectionInfos != NULL) {
                foreach ($sectionInfos as $sectionInfo) {
                    $insert = $wpdb->insert(
                            WPSQT_TABLE_SECTIONS, array(
                        'item_id' => $itemId,
                        'name' => $sectionInfo['name'],
                        'limit' => $sectionInfo['limit'],
                        'order' => $sectionInfo['order'],
                        'difficulty' => $sectionInfo['difficulty'],
                        'timestamp' => date("Y-m-d H:i:s"),
                        'desc'  => $sectionInfo['desc']
                            )
                    );
                    $newSectionIds[$sectionInfo['id']] = $wpdb->insert_id;
                    if ($insert != 1) {
                        return false;
                    }
                }
            }

            // Duplicate questions
            $questionInfos = $wpdb->get_results("SELECT `name`, `type`, `section_id`, `difficulty`, `meta` FROM `" . WPSQT_TABLE_QUESTIONS . "` WHERE `item_id` = '" . $rowToDuplicate['id'] . "'", ARRAY_A);

            if (isset($questionInfos) && !empty($questionInfos) && $questionInfos != NULL) {
                foreach ($questionInfos as $questionInfo) {
                    $insert = $wpdb->insert(
                            WPSQT_TABLE_QUESTIONS, array(
                        'item_id' => $itemId,
                        'name' => $questionInfo['name'],
                        'type' => $questionInfo['type'],
                        'section_id' => $newSectionIds[$questionInfo['section_id']],
                        'difficulty' => $questionInfo['difficulty'],
                        'meta' => $questionInfo['meta'],
                        'timestamp' => date("Y-m-d H:i:s")
                            )
                    );
                    if ($insert != 1) {
                        return false;
                    }
                }
            }

            // Duplicate form fields
            $formInfos = $wpdb->get_results("SELECT `name`, `type`, `required`, `validation` FROM `" . WPSQT_TABLE_FORMS . "` WHERE `item_id` = '" . $rowToDuplicate['id'] . "'", ARRAY_A);

            if (isset($formInfos) && !empty($formInfos) && $formInfos != NULL) {
                foreach ($formInfos as $formInfo) {
                    $insert = $wpdb->insert(
                            WPSQT_TABLE_FORMS, array(
                        'item_id' => $itemId,
                        'name' => $formInfo['name'],
                        'type' => $formInfo['type'],
                        'required' => $formInfo['required'],
                        'validation' => $formInfo['validation'],
                        'timestamp' => date("Y-m-d H:i:s")
                            )
                    );
                    if ($insert != 1) {
                        return false;
                    }
                }
            }
            
            return $itemId;
            
        } else {
            
            return false;
        }
    }
}
new Duplicate();