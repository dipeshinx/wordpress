<?php
/**
 * @package  Assessment Report
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class AssessmentReport {
    
    /**
     * Store all quiz data
     * @since 1.0
     */
    var $results;
    
    var $id;
    
    var $resultid;
    
    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct($id=null,$resultid=null) {
        add_action( 'init', array($this, 'rewrites_init'), 1, 0);
        add_action( 'init', array($this, 'rewrite_tag'), 10, 0);
        add_action( 'init', array($this, 'update_single_result'),10,0);
        add_filter( 'query_vars', array($this, 'query_vars'));
        
        if ( is_plugin_active( 'wp-survey-and-quiz-tool/wp-survey-and-quiz-tool.php' ) ) {
            include_once( ABSPATH . 'wp-content/plugins/wp-survey-and-quiz-tool/wp-survey-and-quiz-tool.php' );
            include_once( ABSPATH . 'wp-content/plugins/wp-survey-and-quiz-tool/lib/Wpsqt/System.php' );
        }  else {
            return false;
        }        
        $this->results = Wpsqt_System::getAllItemDetails('quiz');
        $this->id = $id;
        $this->resultid = $resultid;
        
    }
    
    /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('assessment/(.+?)/([^/]*)/([^/]*)/?$', 'index.php?assessment=$matches[1]&status=$matches[2]&resultid=$matches[3]', 'top');
        add_rewrite_rule('assessment/(.+?)/([^/]*)/?$', 'index.php?assessment=$matches[1]&status=$matches[2]', 'top');
    }
    
    /**
     * @since  1.0
     */
    public function query_vars($query_vars) {
        $query_vars[] = 'status';
        $query_vars[] = 'resultid';
        return $query_vars;
    }

    /**
     * @since  1.0
     */
    public function rewrite_tag() {
        add_rewrite_tag('%status%', '([^&]+)');
        add_rewrite_tag('%resultid%', '([^&]+)');
    }
    
    /**
     * @since 1.0
     */
    public function lists() {
        if(!empty($this->results) && !empty($this->id)) :
            foreach ($this->results as $result) :
                if($result['id'] == $this->id)
                    return $result;
            endforeach;
        endif;
    }
    
    /**
     * @since 1.0
     */
    public function result_counts() {
        
        global $wpdb;
        
        $unviewed = $wpdb->get_results(
                                        $wpdb->prepare( "SELECT * 
                                                         FROM `".WPSQT_TABLE_RESULTS."` 
                                                         WHERE item_id = %d 
                                                         AND LCASE(status) = 'unviewed'
                                                         ORDER BY ID DESC" 
                                                                        , array($this->id) )	, ARRAY_A	
                                                                        );
        $accepted = $wpdb->get_results(
                                        $wpdb->prepare( "SELECT * 
                                                         FROM `".WPSQT_TABLE_RESULTS."` 
                                                         WHERE item_id = %d 
                                                         AND LCASE(status) = 'accepted'
                                                         ORDER BY ID DESC" 
                                                                        , array($this->id))	, ARRAY_A	
                                                                        );
        $rejected = $wpdb->get_results(
                                        $wpdb->prepare( "SELECT * 
                                                         FROM `".WPSQT_TABLE_RESULTS."` 
                                                         WHERE item_id = %d 
                                                         AND LCASE(status) = 'rejected'
                                                         ORDER BY ID DESC" 
                                                                        , array($this->id))	, ARRAY_A	
                                                                        );
        
        $counts = array('unviewed_count' => sizeof($unviewed), 'accepted_count' => sizeof($accepted),'rejected_count' => sizeof($rejected));
        
        return $counts;
    }
    
    /**
     * @since 1.0
     */
    public function results() {
        
        global $wpdb;
        
        $status = get_query_var('status');

        if ( !isset($status) || !isset(${$status}) ) {								
            $rawResults = $wpdb->get_results(
                                    $wpdb->prepare( "SELECT * 
                                                     FROM `".WPSQT_TABLE_RESULTS."` 
                                                     WHERE item_id = %d 
                                                     ORDER BY ID DESC" 
                                                                    , array($this->id))	, ARRAY_A	
                                                                    );
        } else {
            $rawResults = ${$status};
        }
        
        $itemsPerPage = get_option('posts_per_page');
        $currentPage = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;	
        $startNumber = ( ($currentPage - 1) * $itemsPerPage );

        $results = array_slice($rawResults , $startNumber , $itemsPerPage );
        
        return $results;
    }
    
    /**
     * @since 1.0
     */
    public function single_result() {
        
        global $wpdb;
        
        $results = array();;
		
        if(!empty($this->resultid)) : 
            $rawResult = $wpdb->get_row($wpdb->prepare("SELECT * FROM `".WPSQT_TABLE_RESULTS."` WHERE id = %d", array($this->resultid)),ARRAY_A);

            $rawResult['sections'] = unserialize($rawResult['sections']);
            $rawResult['person'] = unserialize($rawResult['person']);
            $results = $rawResult;

            $timeTaken = "";
            $seconds = $rawResult['timetaken'] % 60;
            $minutes = intval($rawResult['timetaken'] / 60);
            $hours = intval($minutes / 60);
            $minutes = $minutes % 60;
            $days = intval($hours / 24);
            $hours = $hours % 24;
            if ($days != 0){
                $timeTaken .= $days.' Days ';
            }
            if ($hours != 0){
                $timeTaken .= $hours.' Hours ';
            }
            if ($minutes != 0){
                $timeTaken .= $minutes.' Minutes ';
            }
            if ($seconds != 0){
                $timeTaken .= $seconds.' Seconds';
            }
            $timeTaken = trim($timeTaken);
            $results['timeTaken'] = $timeTaken;
        
        endif;
        
        return $results;
    }
    
    /**
     * @since 1.0
     */
    public static function result($item_id,$username) {
        
        global $wpdb;
        
        $result = array();
		
        if(!empty($item_id) && !empty($username)) : 
            $rawResult = $wpdb->get_row($wpdb->prepare("SELECT * FROM `".WPSQT_TABLE_RESULTS."` WHERE item_id = %d AND person_name = %s", array($item_id,$username)),ARRAY_A);        
            $rawResult['sections'] = unserialize($rawResult['sections']);
            $rawResult['person'] = unserialize($rawResult['person']);
            $result['start_date'] = $rawResult['sections'][0]['timestamp'];
            $result['status'] = $rawResult['pass'];
        endif;
        
        return $result;
    }
    
    /**
     * @since 1.0
     */
    public function update_single_result() {
        
        global $wpdb;
        
        $results = array();;
		
        if(!empty($this->resultid)) : 
            $rawResult = $wpdb->get_row($wpdb->prepare("SELECT * FROM `".WPSQT_TABLE_RESULTS."` WHERE id = %d", array($this->resultid)),ARRAY_A);

            $rawResult['sections'] = unserialize($rawResult['sections']);
            $rawResult['person'] = unserialize($rawResult['person']);
            $results = $rawResult;

            $timeTaken = "";
            $seconds = $rawResult['timetaken'] % 60;
            $minutes = intval($rawResult['timetaken'] / 60);
            $hours = intval($minutes / 60);
            $minutes = $minutes % 60;
            $days = intval($hours / 24);
            $hours = $hours % 24;
            if ($days != 0){
                $timeTaken .= $days.' Days ';
            }
            if ($hours != 0){
                $timeTaken .= $hours.' Hours ';
            }
            if ($minutes != 0){
                $timeTaken .= $minutes.' Minutes ';
            }
            if ($seconds != 0){
                $timeTaken .= $seconds.' Seconds';
            }
            $timeTaken = trim($timeTaken);
            $results['timeTaken'] = $timeTaken;
        
        endif;
        
        return $results;
    }
}

new AssessmentReport();