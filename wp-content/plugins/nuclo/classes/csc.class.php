<?php

/**
 * @package  CSC
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class CSC {

    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action('country_state_city', array($this, 'fields'), 10, 5);
        add_action('wp_ajax_nc_states', array($this, 'ajax_states'));
        add_action('wp_enqueue_scripts', array($this, 'csc_script'));
    }
    
    /**
     * @since 1.0
     */
    public static function fix() {
        global $wpdb;
        $csc_check = get_option('csc_fix');
        if(empty($csc_check)) {
            $metas = $wpdb->get_results("SELECT * FROM " . $wpdb->postmeta . " WHERE meta_key IN ('country','state','city')");
            foreach($metas as $meta) {
                switch ($meta->meta_key) {
                    case 'country':
                        if(!empty($meta->meta_value) && is_string($meta->meta_value)) {
                            $country_id = nc_country_id($meta->meta_value);
                            update_post_meta($meta->post_id,'country',$country_id,$meta->meta_value);
                        }
                        break;
                    case 'state':
                        if(!empty($meta->meta_value) && is_string($meta->meta_value)) {
                            $state_id = nc_state_id($meta->meta_value);
                            update_post_meta($meta->post_id,'state',$state_id,$meta->meta_value);
                        }
                        break;
                }
            }
            $user_metas = $wpdb->get_results("SELECT * FROM " . $wpdb->usermeta . " WHERE meta_key IN ('country','state','city')");
            foreach($user_metas as $meta) {
                switch ($meta->meta_key) {
                    case 'country':
                        if(!empty($meta->meta_value) && is_string($meta->meta_value)) {
                            $country_id = nc_country_id($meta->meta_value);
                            update_user_meta($meta->user_id,'country',$country_id,$meta->meta_value);
                        }
                        break;
                    case 'state':
                        if(!empty($meta->meta_value) && is_string($meta->meta_value)) {
                            $state_id = nc_state_id($meta->meta_value);
                            update_user_meta($meta->user_id,'state',$state_id,$meta->meta_value);
                        }
                        break;
                }
            }
            update_option('csc_fix', 'yes');
        }
    }

    /**
     * @since 1.0
     */
    public function fields($country, $state, $city, $message, $required=true) {
        wp_enqueue_script('cscScript-js');
        ?>
        <div class="col-md-6">
            <div class="form-group <?php echo isset($message['error']['country']) && !empty($message['error']['country']) ? 'has-feedback has-error' : ''; ?>">
                <?php if($required) { ?>
                <div class="col-sm-12">
                <?php } ?>
                    <label class="control-label">Country <?php if($required){ ?><sup class="text-danger">*</sup><?php } ?></label>
                    <?php $this->get_countries($country); ?>
                    <?php if (isset($message['error']['country']) && !empty($message['error']['country'])) { ?>
                        <span class="error-help-inline"><?php echo $message['error']['country']; ?></span>
                    <?php } ?>
                <?php if($required) { ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php if($required) { ?>
        </div>
        <div class="row">
        <?php } ?>
            <div class="col-md-6">
                <div class="form-group <?php echo isset($message['error']['state']) && !empty($message['error']['state']) ? 'has-feedback has-error' : ''; ?>"> 
                    <?php if($required) { ?>
                    <div class="col-sm-12">
                    <?php } ?>
                        <label class="control-label">State/Province <?php if($required){ ?><sup class="text-danger">*</sup><?php } ?></label>
                        <?php $this->get_states($state, $country); ?>
                        <?php if (isset($message['error']['state']) && !empty($message['error']['state'])) { ?>
                            <span class="error-help-inline"><?php echo $message['error']['state']; ?></span>
                        <?php } ?>
                    <?php if($required) { ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php if($required) { ?>
                    <div class="col-sm-12">
                    <?php } ?>
                        <label class="control-label">City <?php if($required){ ?><sup class="text-danger">*</sup><?php } ?></label>
                        <input type="text" value="<?php echo $city; ?>" class="form-control" id="city"  placeholder=""  name="city" />
                        <?php if (isset($message['error']['city']) && !empty($message['error']['city'])) { ?>
                            <span class="error-help-inline"><?php echo $message['error']['city']; ?></span>
                        <?php } ?>
                    <?php if($required) { ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        <?php
    }

    /**
     * @since 1.0
     */
    private function get_countries($my_country) {
        global $wpdb;
        $countries = $wpdb->get_results("SELECT id, name FROM " . $wpdb->prefix . "countries");
        echo '<select id="country" name="country"><option value="">-- Select Country --</option>';
        foreach ($countries as $country) {
            $selected = ($my_country == $country->id) ? 'selected' : '';
            echo '<option value=' . $country->id . ' ' . $selected . '>' . $country->name . '</option>';
        }
        echo '</select>';
    }

    /**
     * @since 1.0
     */
    private function get_states($my_state, $my_country) {
        global $wpdb;
        $states = $wpdb->get_results("SELECT id, name FROM " . $wpdb->prefix . "states WHERE country_id=" . $my_country);
        echo '<select id="state" name="state"><option value="">-- Select State --</option>';
        foreach ($states as $state) {
            $selected = ($my_state == $state->id) ? 'selected' : '';
            echo '<option value=' . $state->id . ' ' . $selected . '>' . $state->name . '</option>';
        }
        echo '</select>';
    }

    /**
     * @since 1.0
     */
    public function ajax_states() {
        global $wpdb;
        $my_country = $_POST['country'];
        $states = $wpdb->get_results("SELECT id, name FROM " . $wpdb->prefix . "states WHERE country_id=" . $my_country);
        if (null !== $states) {
            $res = array();
            foreach ($states as $state) {
                $res[$state->id] = $state->name;
            }
            $data = array('status' => 'success', 'tp' => 1, 'msg' => "States fetched successfully.", 'result' => $res);
        } else {
            $data = array('status' => 'error', 'tp' => 0, 'msg' => 'Error in States.');
        }
        echo json_encode($data);
        exit;
    }


    /**
     * @since 1.0
     */
    public function csc_script() {
        wp_register_script('cscScript-js', get_template_directory_uri() . '/assets/js/csc.js', array('jquery'), '1.0.2', true);
    }

}

new CSC();

/**
 * @since 1.0
 */
function nc_country_name($country_id=0) {
    global $wpdb;
    $country = $wpdb->get_var( "SELECT name FROM " . $wpdb->prefix . "countries WHERE id=".$country_id );
    return $country;
}

/**
 * @since 1.0
 */
function nc_country_id($country_name='') {
    global $wpdb;
    $country = '';
    if(!empty($country_name)) {
        $country = $wpdb->get_var( "SELECT id FROM " . $wpdb->prefix . "countries WHERE name LIKE '".$country_name."'" );
    }    
    return $country;
}

/**
 * @since 1.0
 */
function nc_state_name($state_id=0) {
    global $wpdb;
    $state = $wpdb->get_var( "SELECT name FROM " . $wpdb->prefix . "states WHERE id=".$state_id );
    return $state;
}

/**
 * @since 1.0
 */
function nc_state_id($state_name='') {
    global $wpdb;
    $state = '';
    if(!empty($state_name)) {
        $state = $wpdb->get_var( "SELECT id FROM " . $wpdb->prefix . "states WHERE name LIKE '".$state_name."'" );
    }
    return $state;
}

/**
 * @since 1.0
 */
function nc_city_name($city_id=0) {
    global $wpdb;
    $city = $wpdb->get_var( "SELECT name FROM " . $wpdb->prefix . "cities WHERE id=".$city_id );
    return $city;
}