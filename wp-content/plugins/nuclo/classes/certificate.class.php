<?php

/**
 * @package  Certificates
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class Certificates {


    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct() {
        add_action( 'init', array($this,'register_post_type') );
        add_action( 'wp_ajax_upload_certificate', array($this,'upload_certificate'));
        add_action( 'certificate', array($this,'save_post'));
        add_action( 'wp_ajax_delete_certificate', array($this, 'delete_certificate'));
    }
    
    /**
     * @since 1.0
     */
    public function register_post_type() {
        if( function_exists('get_field') ) :
            $labels = array(
                    'name'               => _x( get_field('certificates_general_name', 'option'), 'post type general name'            , 'nuclo' ),
                    'singular_name'      => _x( get_field('certificates_singular_name', 'option'), 'post type singular name'          , 'nuclo' ),
                    'menu_name'          => _x( get_field('certificates_general_name', 'option'), 'admin menu'                        , 'nuclo' ),
                    'name_admin_bar'     => _x( get_field('certificates_singular_name', 'option'), 'add new on admin bar'             , 'nuclo' ),
                    'add_new'            => _x( 'Add New', strtolower(get_field('certificates_singular_name', 'option'))              , 'nuclo' ),
                    'add_new_item'       => __( 'Add New '.get_field('certificates_singular_name', 'option')                          , 'nuclo' ),
                    'new_item'           => __( 'New '.get_field('certificates_singular_name', 'option')                              , 'nuclo' ),
                    'edit_item'          => __( 'Edit '.get_field('certificates_singular_name', 'option')                             , 'nuclo' ),
                    'view_item'          => __( 'View '.get_field('certificates_singular_name', 'option')                             , 'nuclo' ),
                    'all_items'          => __( 'All '.get_field('certificates_general_name', 'option')                               , 'nuclo' ),
                    'search_items'       => __( 'Search '.get_field('certificates_general_name', 'option')                            , 'nuclo' ),
                    'parent_item_colon'  => __( 'Parent '.get_field('certificates_general_name', 'option').':'                        , 'nuclo' ),
                    'not_found'          => __( 'No '. strtolower(get_field('certificates_general_name', 'option')).' found.'         , 'nuclo' ),
                    'not_found_in_trash' => __( 'No '. strtolower(get_field('certificates_general_name', 'option')).' found in Trash.', 'nuclo' )
            );

            $args = array(
                    'labels'             => $labels,
                    'public'             => true,
                    'publicly_queryable' => true,
                    'exclude_from_search'=> true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'query_var'          => true,
                    'rewrite'            => array( 'slug' => 'certificate' ),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => true,
                    'menu_position'      => null,
                    'supports'           => array( 'title','author', 'thumbnail' )
            );

            register_post_type( 'certificate', $args );
        endif;
    }
    
    /**
     * @since 1.0
     */
    public function upload_certificate() {
    
        if(!(is_array($_POST) && is_array($_FILES) && defined('DOING_AJAX') && DOING_AJAX && current_user_can( 'create_certificates')) ) return;

        $allowed =  array('jpg','jpeg','png');
        
        $upfilename = $_FILES['files']['name'];
        $extallowed = pathinfo($upfilename, PATHINFO_EXTENSION);

        if(in_array($extallowed, $allowed) && $_FILES['files']['size'] <= '5000000' ) {

            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );
            
            $post_name = explode('.', $upfilename);

            $args = array( 'post_status' => 'publish', 'post_type' => 'certificate','post_title' => $post_name[0] );
            
            $post_id = wp_insert_post($args);

            $upload_id = media_handle_upload( 'files', $post_id );

            set_post_thumbnail( $post_id, $upload_id );

            $data = array();
            
            if($upload_id && $post_id) :
                $data['url'] = get_permalink($post_id);  
                echo json_encode($data);
            endif;

        } else {
            $data['error'] = 'Invalid file!';
            echo json_encode($data);
        }
        
        exit;
        
    }    
    
    /**
     * @since 1.0
     */
    public function save_post() {
        if(isset($_POST) && !empty($_POST) && wp_verify_nonce( $_POST['nuclo'], 'nuclo' )) {
            if(!(is_array($_POST) && current_user_can( 'edit_certificates'))) return;
            
            $post_id = get_the_ID();
            $data = $_POST;

            $post_id = wp_update_post(array( 'ID' => $post_id, 'post_title' => $data['postname'], 'post_name' => sanitize_title($data['postname'])));

            if($post_id != 0) {
                update_post_meta( $post_id, 't1top' , number_format( $data['t1top'], 2, '.', '') );
                update_post_meta( $post_id, 't1left', number_format( $data['t1left'], 2, '.', '') );
                update_post_meta( $post_id, 't2top' , number_format( $data['t2top'], 2, '.', '')  );
                update_post_meta( $post_id, 't2left', number_format( $data['t2left'], 2, '.', '') );
                update_post_meta( $post_id, 't3top' , number_format( $data['t3top'], 2, '.', '')  );
                update_post_meta( $post_id, 't3left', number_format( $data['t3left'], 2, '.', '') );
                wp_redirect(get_permalink($post_id).'?msg=success');
                exit;
            }
        }
    }
    
    /**
     * @since 1.0
     */
    public function delete_certificate() {		
        if(!(is_array($_POST) && defined('DOING_AJAX') && DOING_AJAX && current_user_can( 'delete_certificates'))) { 
            $return['status'] = 'error';
            $return['data']   = 'Something went wrong';                
            echo json_encode($return);
            exit;
        }

        $data = $_POST;

        if(!empty($data['id'])) {
            $program_id = (int) $data['id'];
            wp_delete_post($program_id);
            $return['status'] = 'success';
            $return['data']   = 'Successfully deleted.';                
            echo json_encode($return);
        } else {
            $return['status'] = 'error';
            $return['data']   = 'you can not delete this.';                
            echo json_encode($return);
        }
        exit;
    }
    
}

new Certificates();