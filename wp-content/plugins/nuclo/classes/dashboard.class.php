<?php

/**
 * @package  Dashboard
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */

class Dashboard {
    
    var $months;
    
    var $others;
    
    var $results;
    
    var $challenges;
    var $event_list;
    var $group;
      
    
    public function __construct($user_id) {        
        $this->user_id = $user_id;
        $this->months = array('January' => '', 'February' => '', 'March' => '', 'April' => '', 'May' => '', 'June' => '', 'July' => '', 'August' => '', 'September' => '', 'October' => '', 'November' => '', 'December' => '');
        $this->groups = $this->get_groups();                    
    }
    
    public function get_groups(){
        
        $grp_args = array(
                    'post_type'      => 'group',
                    'post_status'    => 'publish',
                    'meta_key'       => 'franchisee',
                    'meta_value'     => get_current_user_franchisee_id(),
                    'posts_per_page' => -1
                );

        # Getting all programs
        $groups = get_posts( $grp_args );        
        
        
        $franchies_group_ids    = array();
        
        foreach($groups as $franchisee_group){
            
            $franchies_group_ids[$franchisee_group->ID]  = $franchisee_group->post_title;
            
        }
        
        return $franchies_group_ids;        
        
    }
    
    public function get_attendance_tracker_chart($group_id){
        
        $this->group    = $group_id;
        
        $event_args = array(
            'post_type' => array('event'),
            'post_author' => $this->user_id,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC',            
        );
        
        $event_query = get_posts($event_args);     
        
        foreach($event_query as $event){
            $event_list_main[]  = $event->ID;
        }
        
        $attend = 0;
        $absent = 0;
        $total  = 0;
        
        $users  = get_field('users',$group_id);
        if(isset($users) && !empty($users)){
            foreach($users as $user){                        
                foreach($event_list_main as $event){                               
                    $value  = get_user_meta($user['ID'], 'module_result_'.$event,true);                                            
                    if($value == 1){                                
                        $attend++;
                    }else{                                
                        $absent++;
                    } 
                    $total++;
                }     
            }
        }
       
        
        return json_encode(array(
            array('name'  =>  'Attended','y'    => ($attend>0)?(100*$attend)/$total:0),
            array('name'  =>  'Absent','y'    => ($absent>0)?(100*$absent)/$total:0)                        
        ));
        
    }
    
    public function get_active_user_count(){
        
        $grp_args = array(
            'post_type'      => 'group',
            'post_status'    => 'publish',  
            'posts_per_page' => -1,
            'meta_query'     => array( 
                'relation'=>'AND',
                array(  'key' => 'franchisee',
                        'value' => get_current_user_franchisee_id()),
                array(  'relation'=>'OR',
                array('key'=>'group_start_date',
                       'value' => date("Y").'0101', 
                       'compare' => '>', 'type' => 'DATE'),
                array('key'=>'group_start_date',
                       'value' => (date("Y")+1).'0101', 
                       'compare' => '<', 'type' => 'DATE')))
            );

        # Getting all programs
        $groups = get_posts( $grp_args );
        $total = 0;  
        
        foreach ($groups as $group) {
            $users  = get_field('users',$group->ID);    
            $total += count($users);
        }
        
//        $active_group_users_count   = 0;
//        
//        foreach(array_keys($this->groups) as $group){
//            $start_date     = strtotime(str_replace('/','-',get_field('group_start_date',$group)));
//            $end_date       = strtotime(str_replace('/','-',get_field('group_end_date',$group)));
//            $current_date   = time();
//            
//            if(($start_date < $current_date) && ($current_date < $end_date)){
//                $this->active_groups[$group]  = $this->groups[$group];
//            }
//        }
//        
//        foreach(array_keys($this->active_groups) as $group){
//            $users  = get_field('users',$group);    
//            $active_group_users_count += count($users);            
//        }
        
        return $total;
    }    
    
    private function get_results($submission='') {
        $this->results = array();        
        $users  = get_field('users',$this->group);  
        $submission = ($submission=='plan')?'plan':'result';
        foreach($this->months as $modules) {
            if(isset($modules) && !empty($modules)){
                foreach($modules as $name => $ids) {  
                    if(!in_array($name,array('event','feedback'))) { 
                        foreach($ids as $id) {                       
                            if($id) {
                                $type = get_field('measure_type',$id);
                                if($type == $submission) {                                                                          
                                     foreach($users as $user){                                         
                                        $post   = get_user_meta($user['ID'], 'submission_'.$submission.'_' . $id, true);
                                        $this->results[] = $post;
                                     }                                    
                                }                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    public function get_action_plan_chart_status($group_id) {           
        
        $this->group    = $group_id;
        
        $total      = 0;
        $complete   = 0;
        $patially   = 0;
        $notstarted = 0;
        
        $this->get_months();
        
        $this->get_results('plan');  
        $plans = $this->results;
        
        $this->get_results('result');  
        $results = $this->results;
        
        if(count($plans) > count($results)) {
            $total = count($plans);
            foreach($plans as $key => $value) {
                if(!empty($value) && !empty($results[$key])) {
                    $complete++;
                } else if(!empty($value) || !empty($results[$key])) {
                    $patially++;
                } else {
                    $notstarted++;
                }
            }
        } else {            
            $total = count($results);
            foreach($results as $key => $value) {
                if(!empty($value) && !empty($plans[$key])) {
                    $complete++;
                } else if(!empty($value) || !empty($plans[$key])) {
                    $patially++;
                } else {
                    $notstarted++;
                }
            }
        }
        $return = array(
                        array('name'=>'Completed','y'=> ($complete>0)?round((100*$complete)/$total):0),
                        array('name'=>'Not Started','y'=>($notstarted>0)?round((100*$notstarted)/$total):0),
                        array('name'=>'Partially Completed','y'=>($patially>0)?round((100*$patially)/$total):0)
        );        
        
        return json_encode($return);
    }
    
    private function get_modules() {
        $module_args = array(
            'post_type' => array('survey', 'extra', 'measure', 'discussion', 'assessment', 'bundle'),            
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC',
        );
        $module_query = new WP_Query($module_args);
        if ($module_query->have_posts()) {
            while ($module_query->have_posts()) {
                $module_query->the_post();

                $month_num = get_field('month_of_module');
                $menu_order = get_post_field('menu_order', get_the_ID());

                if (!empty($month_num)) {
                    $month_name = date('F', mktime(0, 0, 0, $month_num, 10));
                    if (get_post_type() == 'survey') {
                        $this->months[$month_name]['feedback'] = get_the_ID();
                    } else if (get_post_type() == 'bundle') {
                        $bundle_args = array(
                            'post_type' => array('survey', 'extra', 'measure', 'discussion', 'assessment'),
                            'post_parent' => get_the_ID(),
                            'posts_per_page' => -1,
                            'orderby' => 'menu_order',
                            'order' => 'ASC',
                        );
                        $bundle_query = new WP_Query($bundle_args);
                        $bundle_title = get_the_title();
                        if ($bundle_query->have_posts()) {
                            while ($bundle_query->have_posts()) {
                                $bundle_query->the_post();
                                $menu_order = get_post_field('menu_order', get_the_ID());
                                $this->months[$month_name][$bundle_title][$menu_order] = get_the_ID();
                            }
                        }
                        wp_reset_postdata();
                    } else {
                        $this->months[$month_name][$menu_order] = get_the_ID();
                    }
                } else {
                    $this->others[] = get_the_ID();                    
                }
            }
        }
        wp_reset_postdata();
    }
    
    private function get_months() {        
//        $this->get_events();
        $this->get_modules();
        $date = get_field('group_start_date',$this->group);
        $start = str_replace('/', '-', $date);
        $start_month = date('F',strtotime($start));

        foreach($this->months as $key => $month) {
           if($key == $start_month) {
              break;
           } else {
              $item = $this->months[$key];
              unset($this->months[$key]);
              $this->months[$key] = $item;
           }   
        }
    }
    
    public function get_feedback_chart($group_id) {
        
        global $current_user;
        
        $months = array('January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0);
        
        $bpm_obj = new BPM($current_user->ID);
        
        $return = array();         
        $users  = array();
        
        if(is_array($group_id)){
            foreach($group_id   as $group){
                $user_list  = get_field('users',$group);
                foreach($user_list  as $user){
                    $users[]    = $user;
                }                
            }            
        }else{        
            $users  = get_field('users',$group_id);
            
        }
      
        $participants = array();
        
        foreach($users as $user){  
            $participants[] = $user['ID'];
        }
        foreach($months as $month => $count) {
            $feedback_id = $bpm_obj->months[$month]['feedback'];
            $module1     = get_survey_feedback($participants,$feedback_id,0);
            $module2     = get_survey_feedback($participants,$feedback_id,1);
            $total       = round(($module1+$module2)/2,2);
            $return[]    = array('name'=>$month,'y'=>$total);
        }   
        
             
        return json_encode($return);
        
    }
    
    
}

function get_monthly_enrollments_chart($group_id) {    
    
    $curent_year = date('Y');
         
    $months = array('January - '.$curent_year => 0, 'February - '.$curent_year => 0, 'March - '.$curent_year => 0, 'April - '.$curent_year => 0, 'May - '.$curent_year => 0, 'June - '.$curent_year => 0, 'July - '.$curent_year => 0, 'August - '.$curent_year => 0, 'September - '.$curent_year => 0, 'October - '.$curent_year => 0, 'November - '.$curent_year => 0, 'December - '.$curent_year => 0);
        
    $grp_args = array(
        'post_type'      => 'group',
        'post_status'    => 'publish',
        'include'        => (is_array($group_id))?$group_id:array($group_id),
        'posts_per_page' => -1,
        'meta_key'       => 'franchisee',
        'meta_value'     => get_current_user_franchisee_id()
    );
    
    # Getting all programs
    $groups = get_posts( $grp_args );
    
    $users = array(); 
    
    $return = array(); 
    
    foreach($groups as $group){
        $users  = get_field('users',$group->ID);     
        
        $start_date = get_field('group_start_date',$group->ID);
                
        if(!empty($start_date)) 
            $new_start_date = str_replace('/', '-', $start_date);

        $end_date = get_field('group_end_date',$group->ID);
        if(!empty($end_date))
            $new_end_date = str_replace('/', '-', $end_date);

        if(!empty($new_start_date) && !empty($new_end_date)) {
            $start    = new DateTime($new_start_date);
            $start->modify('first day of this month');
            $end      = new DateTime($new_end_date);
            $end->modify('first day of next month');
            $interval = DateInterval::createFromDateString('1 month');
            $period   = new DatePeriod($start, $interval, $end);            
            foreach($users as $user) {
                $client = get_user_meta($user['ID'], 'client',true); 
                if($period) {
                    foreach ($period as $dt) {
                        if($client) { 
                            if(isset($months[$dt->format("F - Y")])) {
                                $months[$dt->format("F - Y")]++;
                            }
                        }
                    }
                }
            }
        }
    }
    

//    foreach($groups as $group){
//        
//        $users  = get_field('users',$group->ID);
//        
//        $my_users = array();  
//        
//        foreach($users as $user) {
//            
//            $client = get_user_meta($user['ID'], 'client',true);
//            
//            if(!empty($client)) {
//                
//                $my_users[] = $client;
//                
//            }
//            
//        }        
//
//        $final_users = array_diff($my_users,$all_users);        
//        
//        $all_users = array_merge($all_users, $final_users);
//        
//        $date = get_field('group_start_date',$group->ID);
//
//        $date = str_replace('/', '-', $date);
//        
//        $month  = date('F',strtotime($date));
//        
//        $months[$month] += count($final_users);
//        
//    }
    
     
    foreach($months as $month => $value) {
        $name       = date('F',strtotime($month));
        $return[]   = array('name'=>$name,'y'=>$value);
    }
   
    return json_encode($return);
    
}

function get_feedback_chart($franchisee_id) {    
    
    global $current_user;
        
    $months = array('January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0);

    $bpm_obj = new BPM($current_user->ID);

    $return = array();         

    if($franchisee_id == 'all') {
      $users = get_franchise_users('');
    } else {
      $users = get_franchise_users($franchisee_id);
    }
    

    foreach($months as $month => $count) {
        $feedback_id = $bpm_obj->months[$month]['feedback'];
        $module1     = get_survey_feedback($users,$feedback_id,0);
        $module2     = get_survey_feedback($users,$feedback_id,1);
        $total       = round(($module1+$module2)/2,2);
        $return[]    = array('name'=>$month,'y'=>$total);
    }   

    return json_encode($return);
}

function get_pending_actions($user_id){
    
    $bpm_obj = new BPM($user_id);    
    $pending_actions = array();
    $current_month = date('m');
    $count = 1;
    $months = array(
        'January'   => 1,
        'February'  => 2,
        'March'     => 3,
        'April'     => 4,
        'May'       => 5,
        'June'      => 6,
        'July'      => 7,
        'August'    => 8,
        'September' => 9,
        'October'   => 10,
        'November'  => 11,
        'December'  => 12
    );
    
    
    foreach($bpm_obj->months as $month => $modules) { 
        
        if($months[$month] > $current_month){
            if(isset($bpm_obj->others[0])) {
                $status = get_user_meta($user_id,'module_result_'.$bpm_obj->others[0],true); 
                if(!$status && !isset($pending_actions[$bpm_obj->others[0]])){
                    $pending_actions[$bpm_obj->others[0]] = '<a class="client-link" href="'.get_permalink($bpm_obj->others[0]).'">Complete <span>Pre Development</span></a>';  
                }
            }
            break;
        }
       
        if(isset($bpm_obj->others[0])) {
            $status = get_user_meta($user_id,'module_result_'.$bpm_obj->others[0],true); 
            if(!$status && !isset($pending_actions[$bpm_obj->others[0]])){
                $pending_actions[$bpm_obj->others[0]] = '<a class="client-link" href="'.get_permalink($bpm_obj->others[0]).'">Complete <span>Pre Development</span></a>';  
            }
        }
        
         
        
        if(isset($bpm_obj->others[1]) && $count == 6) {
            $status = get_user_meta($user_id,'module_result_'.$bpm_obj->others[1],true); 
            if(!$status && !isset($pending_actions[$bpm_obj->others[0]])){
                $pending_actions[$bpm_obj->others[1]] = '<a class="client-link" href="'.get_permalink($bpm_obj->others[1]).'">Complete <span>Mid Development</span></a>';  
            }
        }

        if(isset($bpm_obj->others[2]) && $count == 12) {
            $status = get_user_meta($user_id,'module_result_'.$bpm_obj->others[2],true); 
            if(!$status && !isset($pending_actions[$bpm_obj->others[2]])){
                $pending_actions[$bpm_obj->others[2]] = '<a class="client-link" href="'.get_permalink($bpm_obj->others[2]).'">Complete <span>Final Development</span></a>';  
            }
        }

        if(isset($modules) && !empty($modules)){
            foreach($modules as $name => $module) {  
                if(!in_array($name,array('event','feedback'))) {                 
                    if(isset($modules['event']) && !empty($modules['event'])) {                
                        $status = get_user_meta($user_id,'module_result_'.$modules['event'],true);                 
                        if(!$status && !isset($pending_actions[$modules['event']])){
                            $pending_actions[$modules['event']] = '<a class="client-link" href="'.get_permalink($modules['event']).'">Attend <span>Session</span> for '.$month.'</a>';  
                        }
                    }
                    if(!empty($modules['event']) && isset($modules['feedback']) && !empty($modules['feedback'])) {
                        $status = get_user_meta($user_id,'module_result_'.$modules['feedback'],true);                 
                        if(!$status && !isset($pending_actions[$modules['feedback']])){
                            $pending_actions[$modules['feedback']] = '<a class="client-link" href="'.get_permalink($modules['feedback']).'">Complete <span>Session Feedback</span> for '.$month.'</a>';  
                        }
                    }
                    
                    if(!empty($modules['event'])){
                        foreach($module as $id) { 
                            $type = get_field('measure_type',$id);  
                            $status = get_user_meta($user_id,'module_result_'.$id,true);
                            if($type=='plan' && !$status){ 
                                $pending_actions[] = '<a class="client-link" href="'.get_permalink($id).'">Complete <span>Action Plan</span> for '.$name.'</a>';                          
                            } else if($type=='result' && !$status){
                                $pending_actions[] = '<a class="client-link" href="'.get_permalink($id).'">Complete <span>Results</span> for '.$name.'</a>';                          
                            }
                        }    
                    }
                }
            }
        }
        $count++;
    }
    
    $height = (empty($pending_actions) || count($pending_actions) == 1)?'40px':'250px';
 
    $html   = "";
    
    $html  .= '<li class="dropdown notification pending-actions">';
    $html  .= '<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">';
    $html  .= '<i class="fa fa-warning"></i> <span class="label label-primary">'.count($pending_actions).'</span>';
    $html  .= '</a>';
    $html  .= '<ul class="dropdown-menu dropdown-messages no-padding " style="padding-left: 0;min-height: '.$height.';max-height: '.$height.';overflow: hidden;overflow-y: auto;">';
    
    if($pending_actions) {
        foreach($pending_actions as $action){
            $html  .= '<li class="border-bottom">';            
            $html  .= '<div class="p-xs clearfix">';
            $html  .= '<div class="media-body">';
            $html  .= stripslashes($action);
            $html  .= '</div>';
            $html  .= '</div>';            
            $html  .= '</li>';
        }
    }else{        
        $html  .= '<li>';
        $html  .= '<a href="#" class="no-padding no-margins">';
        $html  .= '<div class="p-xs clearfix">';
        $html  .= '<div class="media-body">';
        $html  .= '<strong>No Pending Action</strong>';
        $html  .= '</div>';
        $html  .= '</div>';
        $html  .= '</a>';
        $html  .= '</li>';
    }
    $html  .= '</ul>';
    $html  .= '</li>';
    
    return $html;
}

function get_best_idea_notification($user_id){
    
    global $wpdb;
    
    $query      = "SELECT meta_key , meta_value FROM {$wpdb->usermeta} WHERE meta_key LIKE '%best_idea_%' AND user_id = {$user_id}";    
    $results    = $wpdb->get_results($query);
    $event_list = get_event_list('past', true);
    
    $height = (empty($results) || count($results) == 1)?'160px':'250px';
    
    $html   = "";
    
    if(!empty($results)) {
        $html  .= '<li class="dropdown notification best-idea">';
        $html  .= '<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">';
        $html  .= '<i class="fa fa-lightbulb-o"></i> <span class="label label-primary">'.count($results).'</span>';
        $html  .= '</a>';
        $html  .= '<ul class="dropdown-menu dropdown-messages no-padding " style="padding-left: 0;min-height: '.$height.';max-height: '.$height.';overflow: hidden;overflow-y: auto;">';

        foreach($results as $result){
            $session_array  = explode('_', $result->meta_key);
            $session        = $session_array[count($session_array)-2];
            $modules        = get_event_module($session);    
            
            foreach($modules[$result->meta_value] as $id){
                $tag    = get_field('tag',$id);
                if($tag == 'Action Plan'){                   
                    $action  = '<a href="'.get_permalink($id).'" target="_blank">';
                    $action .= 'Congratulations! Your action plan has been chosen as the best idea for the module';
                    $action .= ' "'.$result->meta_value.'".';
                    $action .= 'You have earned 100 additional points towards your participation level.';
                    $action .= '</a>';
                           
                }
            }
            $html  .= '<li class="border-bottom">';            
            $html  .= '<div class="p-xs clearfix">';
            $html  .= '<div class="media-body"><strong>';
            $html  .= stripslashes($action);
            $html  .= '</strong></div>';
            $html  .= '</div>';            
            $html  .= '</li>';
        }

        $html  .= '</ul>';
        $html  .= '</li>';
    }
    
    return $html;
}