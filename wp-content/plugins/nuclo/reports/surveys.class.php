<?php
/**
 * @package  Survey Report
 * @author   Nisarg Patel <nisarg.patel@nuvedalearning.com>
 * @version  1.0
 * @access   public
 */
class SurveyReport extends AssessmentReport {
    
    /**
     * Store all quiz data
     * @since 1.0
     */
    var $results;
    
    var $id;
    
    var $resultid;
    
    /**
     * Adds the generic hooks that are required throughout
     * the theme.
     * @since 1.0
     */
    public function __construct($id=null,$resultid=null) {
        add_action( 'init', array($this, 'rewrites_init'), 1, 0);
        add_action( 'init', array($this, 'rewrite_tag'), 10, 0);
        add_filter( 'query_vars', array($this, 'query_vars'));
        
        if ( is_plugin_active( 'wp-survey-and-quiz-tool/wp-survey-and-quiz-tool.php' ) ) {
            include_once( ABSPATH . 'wp-content/plugins/wp-survey-and-quiz-tool/wp-survey-and-quiz-tool.php' );
            include_once( ABSPATH . 'wp-content/plugins/wp-survey-and-quiz-tool/lib/Wpsqt/System.php' );
        }  else {
            return false;
        }        
        $this->results = Wpsqt_System::getAllItemDetails('survey');
        $this->id = $id;
        $this->resultid = $resultid;
    }
    
    /**
     * @since  1.0
     */
    public function rewrites_init() {
        add_rewrite_rule('survey/(.+?)/([^/]*)/([^/]*)/?$', 'index.php?survey=$matches[1]&type=$matches[2]&resultid=$matches[3]', 'top');
        add_rewrite_rule('survey/(.+?)/([^/]*)/?$', 'index.php?survey=$matches[1]&type=$matches[2]', 'top');
        
    }
    
    /**
     * @since  1.0
     */
    public function query_vars($query_vars) {
        $query_vars[] = 'type';
        return $query_vars;
    }

    /**
     * @since  1.0
     */
    public function rewrite_tag() {
        add_rewrite_tag('%type%', '([^&]+)');
    }
    
    /**
     * @since 1.0
     */
    public function lists() {
        if(!empty($this->results) && !empty($this->id)) :
            foreach ($this->results as $result) :
                if($result['id'] == $this->id)
                    return $result;
            endforeach;
        endif;
    }
    
    /**
     * @since 1.0
     */
    public function results() {
        
        global $wpdb;
        
        $status = get_query_var('status');

        if ( !isset($status) || !isset(${$status}) ) {								
            $rawResults = $wpdb->get_results(
                                    $wpdb->prepare( "SELECT * 
                                                     FROM `".WPSQT_TABLE_RESULTS."` 
                                                     WHERE item_id = %d 
                                                     ORDER BY ID DESC" 
                                                                    , array($this->id))	, ARRAY_A	
                                                                    );
        } else {
            $rawResults = ${$status};
        }
        
        $itemsPerPage = get_option('posts_per_page');
        $currentPage = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;	
        $startNumber = ( ($currentPage - 1) * $itemsPerPage );

        $results = array_slice($rawResults , $startNumber , $itemsPerPage );
        
        return $results;
    }
    
    /**
     * @since 1.0
     */
    public function single_result() {        
        $result = parent::single_result();          
        return $result;        
    }
    
    /**
     * @since 1.0
     */
    public function results_total() {
        global $wpdb;
        
        $results = array();
        
        $result = $wpdb->get_row($wpdb->prepare("SELECT * FROM `".WPSQT_TABLE_SURVEY_CACHE."` WHERE item_id = %d",array($this->id)), ARRAY_A);
        
        $results['sections'] = unserialize($result['sections']);

        if (!empty($results['sections'])) {
            foreach($results['sections'] as $section) {
                foreach($section['questions'] as $questionKey => $question) {
                    if ($question['type'] == 'Free Text') {
                        $uncachedResult = $wpdb->get_results($wpdb->prepare("SELECT * FROM `".WPSQT_TABLE_RESULTS."` WHERE item_id = %d",array($this->id)), ARRAY_A);
                        $results['uncachedresults'] = $uncachedResult;
                        // Storing all the IDs for free text questions
                        $results['freetextq'][] = $questionKey;
                    }
                }
            }
        }    
        
        return $results;
    }
    
    /**
     * @since 1.0
     */
    public static function result($item_id,$username) {
        
        global $wpdb;
        
        $result = array();
		
        if(!empty($item_id) && !empty($username)) : 
            $rawResult = $wpdb->get_var("SELECT count(*) FROM `".WPSQT_TABLE_RESULTS."` WHERE item_id = $item_id AND person_name = '".$username."'");   
            if($rawResult > 0) {
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
            }
        endif;
        
        return $result;
    }
   
}

new SurveyReport();